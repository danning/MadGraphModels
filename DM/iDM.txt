Requestor: Miriam Diamond
Content: Inelastic dark matter
Paper: http://arxiv.org/abs/1508.03050
JIRA: https://its.cern.ch/jira/browse/AGENE-1106