# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.0.1 for Mac OS X x86 (64-bit) (September 27, 2016)
# Date: Mon 7 May 2018 22:29:14


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_133_1 = Coupling(name = 'R2GC_133_1',
                      value = '(3*G**3)/(16.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_134_2 = Coupling(name = 'R2GC_134_2',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_135_3 = Coupling(name = 'R2GC_135_3',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_138_4 = Coupling(name = 'R2GC_138_4',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_138_5 = Coupling(name = 'R2GC_138_5',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_139_6 = Coupling(name = 'R2GC_139_6',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_139_7 = Coupling(name = 'R2GC_139_7',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_140_8 = Coupling(name = 'R2GC_140_8',
                      value = '(complex(0,1)*G**2*gSd33*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'DMS':1,'QCD':2,'QED':1})

R2GC_140_9 = Coupling(name = 'R2GC_140_9',
                      value = '(complex(0,1)*G**2*gSu33*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'DMS':1,'QCD':2,'QED':1})

R2GC_141_10 = Coupling(name = 'R2GC_141_10',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_141_11 = Coupling(name = 'R2GC_141_11',
                       value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_142_12 = Coupling(name = 'R2GC_142_12',
                       value = '(complex(0,1)*G**2*gPd33*yb**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_142_13 = Coupling(name = 'R2GC_142_13',
                       value = '-(complex(0,1)*G**2*gPu33*yt**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_143_14 = Coupling(name = 'R2GC_143_14',
                       value = '(complex(0,1)*G**2*gSd33*yb**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_143_15 = Coupling(name = 'R2GC_143_15',
                       value = '(complex(0,1)*G**2*gSu33*yt**2)/(16.*cmath.pi**2)',
                       order = {'DMS':1,'QCD':2,'QED':2})

R2GC_144_16 = Coupling(name = 'R2GC_144_16',
                       value = '-(complex(0,1)*G**2*gPd33**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*gSd33**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'DMS':2,'QCD':2,'QED':2})

R2GC_144_17 = Coupling(name = 'R2GC_144_17',
                       value = '-(complex(0,1)*G**2*gPu33**2*yt**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*gSu33**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'DMS':2,'QCD':2,'QED':2})

R2GC_145_18 = Coupling(name = 'R2GC_145_18',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_145_19 = Coupling(name = 'R2GC_145_19',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_146_20 = Coupling(name = 'R2GC_146_20',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_146_21 = Coupling(name = 'R2GC_146_21',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_147_22 = Coupling(name = 'R2GC_147_22',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_147_23 = Coupling(name = 'R2GC_147_23',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_148_24 = Coupling(name = 'R2GC_148_24',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_148_25 = Coupling(name = 'R2GC_148_25',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_149_26 = Coupling(name = 'R2GC_149_26',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_149_27 = Coupling(name = 'R2GC_149_27',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_150_28 = Coupling(name = 'R2GC_150_28',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_150_29 = Coupling(name = 'R2GC_150_29',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_151_30 = Coupling(name = 'R2GC_151_30',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_152_31 = Coupling(name = 'R2GC_152_31',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_154_32 = Coupling(name = 'R2GC_154_32',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_156_33 = Coupling(name = 'R2GC_156_33',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_156_34 = Coupling(name = 'R2GC_156_34',
                       value = '(CKM2x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_156_35 = Coupling(name = 'R2GC_156_35',
                       value = '(CKM2x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_156_36 = Coupling(name = 'R2GC_156_36',
                       value = '(CKM1x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_156_37 = Coupling(name = 'R2GC_156_37',
                       value = '(CKM1x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_167_38 = Coupling(name = 'R2GC_167_38',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_167_39 = Coupling(name = 'R2GC_167_39',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_168_40 = Coupling(name = 'R2GC_168_40',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_168_41 = Coupling(name = 'R2GC_168_41',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_169_42 = Coupling(name = 'R2GC_169_42',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_169_43 = Coupling(name = 'R2GC_169_43',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_170_44 = Coupling(name = 'R2GC_170_44',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_171_45 = Coupling(name = 'R2GC_171_45',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_171_46 = Coupling(name = 'R2GC_171_46',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_172_47 = Coupling(name = 'R2GC_172_47',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_173_48 = Coupling(name = 'R2GC_173_48',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_174_49 = Coupling(name = 'R2GC_174_49',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_175_50 = Coupling(name = 'R2GC_175_50',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_176_51 = Coupling(name = 'R2GC_176_51',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_181_52 = Coupling(name = 'R2GC_181_52',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_198_53 = Coupling(name = 'R2GC_198_53',
                       value = '-(CKM2x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_199_54 = Coupling(name = 'R2GC_199_54',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_200_55 = Coupling(name = 'R2GC_200_55',
                       value = '-(CKM2x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_201_56 = Coupling(name = 'R2GC_201_56',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_202_57 = Coupling(name = 'R2GC_202_57',
                       value = '-(CKM1x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_203_58 = Coupling(name = 'R2GC_203_58',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_204_59 = Coupling(name = 'R2GC_204_59',
                       value = '-(CKM1x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_205_60 = Coupling(name = 'R2GC_205_60',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_209_61 = Coupling(name = 'R2GC_209_61',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_211_62 = Coupling(name = 'R2GC_211_62',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_212_63 = Coupling(name = 'R2GC_212_63',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_213_64 = Coupling(name = 'R2GC_213_64',
                       value = '-(G**2*gPd33*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

R2GC_214_65 = Coupling(name = 'R2GC_214_65',
                       value = '-(complex(0,1)*G**2*gSd33*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

R2GC_215_66 = Coupling(name = 'R2GC_215_66',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_215_67 = Coupling(name = 'R2GC_215_67',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_216_68 = Coupling(name = 'R2GC_216_68',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_217_69 = Coupling(name = 'R2GC_217_69',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_218_70 = Coupling(name = 'R2GC_218_70',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_218_71 = Coupling(name = 'R2GC_218_71',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_219_72 = Coupling(name = 'R2GC_219_72',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_219_73 = Coupling(name = 'R2GC_219_73',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_221_74 = Coupling(name = 'R2GC_221_74',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_222_75 = Coupling(name = 'R2GC_222_75',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_227_76 = Coupling(name = 'R2GC_227_76',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_228_77 = Coupling(name = 'R2GC_228_77',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_231_78 = Coupling(name = 'R2GC_231_78',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_232_79 = Coupling(name = 'R2GC_232_79',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_233_80 = Coupling(name = 'R2GC_233_80',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_234_81 = Coupling(name = 'R2GC_234_81',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_235_82 = Coupling(name = 'R2GC_235_82',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_236_83 = Coupling(name = 'R2GC_236_83',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_237_84 = Coupling(name = 'R2GC_237_84',
                       value = '-(G**2*gPu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

R2GC_238_85 = Coupling(name = 'R2GC_238_85',
                       value = '-(complex(0,1)*G**2*gSu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_157_1 = Coupling(name = 'UVGC_157_1',
                      value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_158_2 = Coupling(name = 'UVGC_158_2',
                      value = {-1:'G**3/(32.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_159_3 = Coupling(name = 'UVGC_159_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_160_4 = Coupling(name = 'UVGC_160_4',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_162_5 = Coupling(name = 'UVGC_162_5',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_167_6 = Coupling(name = 'UVGC_167_6',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_167_7 = Coupling(name = 'UVGC_167_7',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_168_8 = Coupling(name = 'UVGC_168_8',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_168_9 = Coupling(name = 'UVGC_168_9',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_170_10 = Coupling(name = 'UVGC_170_10',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_170_11 = Coupling(name = 'UVGC_170_11',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_171_12 = Coupling(name = 'UVGC_171_12',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_171_13 = Coupling(name = 'UVGC_171_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_172_14 = Coupling(name = 'UVGC_172_14',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_173_15 = Coupling(name = 'UVGC_173_15',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_174_16 = Coupling(name = 'UVGC_174_16',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_175_17 = Coupling(name = 'UVGC_175_17',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_176_18 = Coupling(name = 'UVGC_176_18',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(6.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_177_19 = Coupling(name = 'UVGC_177_19',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )'},
                       order = {'QCD':2,'QED':1})

UVGC_178_20 = Coupling(name = 'UVGC_178_20',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_178_21 = Coupling(name = 'UVGC_178_21',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_178_22 = Coupling(name = 'UVGC_178_22',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_178_23 = Coupling(name = 'UVGC_178_23',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_178_24 = Coupling(name = 'UVGC_178_24',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_178_25 = Coupling(name = 'UVGC_178_25',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(6.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_179_26 = Coupling(name = 'UVGC_179_26',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_180_27 = Coupling(name = 'UVGC_180_27',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_181_28 = Coupling(name = 'UVGC_181_28',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_182_29 = Coupling(name = 'UVGC_182_29',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_198_30 = Coupling(name = 'UVGC_198_30',
                       value = {-1:'(CKM2x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_198_31 = Coupling(name = 'UVGC_198_31',
                       value = {-1:'-(CKM2x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_199_32 = Coupling(name = 'UVGC_199_32',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_199_33 = Coupling(name = 'UVGC_199_33',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_200_34 = Coupling(name = 'UVGC_200_34',
                       value = {-1:'(CKM2x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_200_35 = Coupling(name = 'UVGC_200_35',
                       value = {-1:'-(CKM2x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_201_36 = Coupling(name = 'UVGC_201_36',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_201_37 = Coupling(name = 'UVGC_201_37',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_202_38 = Coupling(name = 'UVGC_202_38',
                       value = {-1:'(CKM1x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_202_39 = Coupling(name = 'UVGC_202_39',
                       value = {-1:'-(CKM1x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_203_40 = Coupling(name = 'UVGC_203_40',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_203_41 = Coupling(name = 'UVGC_203_41',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_204_42 = Coupling(name = 'UVGC_204_42',
                       value = {-1:'(CKM1x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_204_43 = Coupling(name = 'UVGC_204_43',
                       value = {-1:'-(CKM1x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_205_44 = Coupling(name = 'UVGC_205_44',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_205_45 = Coupling(name = 'UVGC_205_45',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_206_46 = Coupling(name = 'UVGC_206_46',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(3.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(6.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2)/(6.*cmath.pi**2) ) - (complex(0,1)*G**2)/(6.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_207_47 = Coupling(name = 'UVGC_207_47',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(3.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_208_48 = Coupling(name = 'UVGC_208_48',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(3.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(6.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/cmath.pi**2 if MB else -(complex(0,1)*G**3)/(6.*cmath.pi**2) ) + (complex(0,1)*G**3)/(6.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_209_49 = Coupling(name = 'UVGC_209_49',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2*MB)/(12.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_210_50 = Coupling(name = 'UVGC_210_50',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(3.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_211_51 = Coupling(name = 'UVGC_211_51',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (5*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_212_52 = Coupling(name = 'UVGC_212_52',
                       value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-5*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_213_53 = Coupling(name = 'UVGC_213_53',
                       value = {-1:'( -(G**2*gPd33*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*gPd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*gPd33*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-5*G**2*gPd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) + (G**2*gPd33*yb*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*gPd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*gPd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_214_54 = Coupling(name = 'UVGC_214_54',
                       value = {-1:'( -(complex(0,1)*G**2*gSd33*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*gSd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*gSd33*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-5*complex(0,1)*G**2*gSd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gSd33*yb*reglog(MB/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*gSd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*gSd33*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'DMS':1,'QCD':2,'QED':1})

UVGC_215_55 = Coupling(name = 'UVGC_215_55',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_215_56 = Coupling(name = 'UVGC_215_56',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_216_57 = Coupling(name = 'UVGC_216_57',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_216_58 = Coupling(name = 'UVGC_216_58',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_216_59 = Coupling(name = 'UVGC_216_59',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_216_60 = Coupling(name = 'UVGC_216_60',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_217_61 = Coupling(name = 'UVGC_217_61',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_217_62 = Coupling(name = 'UVGC_217_62',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_217_63 = Coupling(name = 'UVGC_217_63',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_218_64 = Coupling(name = 'UVGC_218_64',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_218_65 = Coupling(name = 'UVGC_218_65',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_218_66 = Coupling(name = 'UVGC_218_66',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_218_67 = Coupling(name = 'UVGC_218_67',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_219_68 = Coupling(name = 'UVGC_219_68',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_219_69 = Coupling(name = 'UVGC_219_69',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_220_70 = Coupling(name = 'UVGC_220_70',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_220_71 = Coupling(name = 'UVGC_220_71',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_220_72 = Coupling(name = 'UVGC_220_72',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_220_73 = Coupling(name = 'UVGC_220_73',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_221_74 = Coupling(name = 'UVGC_221_74',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_221_75 = Coupling(name = 'UVGC_221_75',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_221_76 = Coupling(name = 'UVGC_221_76',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_221_77 = Coupling(name = 'UVGC_221_77',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_221_78 = Coupling(name = 'UVGC_221_78',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_222_79 = Coupling(name = 'UVGC_222_79',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_222_80 = Coupling(name = 'UVGC_222_80',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_223_81 = Coupling(name = 'UVGC_223_81',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_223_82 = Coupling(name = 'UVGC_223_82',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_223_83 = Coupling(name = 'UVGC_223_83',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_223_84 = Coupling(name = 'UVGC_223_84',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_224_85 = Coupling(name = 'UVGC_224_85',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_225_86 = Coupling(name = 'UVGC_225_86',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_226_87 = Coupling(name = 'UVGC_226_87',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_227_88 = Coupling(name = 'UVGC_227_88',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_228_89 = Coupling(name = 'UVGC_228_89',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) )'},
                       order = {'QCD':2,'QED':1})

UVGC_228_90 = Coupling(name = 'UVGC_228_90',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_228_91 = Coupling(name = 'UVGC_228_91',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_229_92 = Coupling(name = 'UVGC_229_92',
                       value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_230_93 = Coupling(name = 'UVGC_230_93',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_231_94 = Coupling(name = 'UVGC_231_94',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -(G**2*yb)/(6.*cmath.pi**2) )',0:'( (5*G**2*yb)/(12.*cmath.pi**2) - (G**2*yb*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (G**2*yb)/(12.*cmath.pi**2) ) - (G**2*yb)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_231_95 = Coupling(name = 'UVGC_231_95',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*G**2*yb)/(24.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_231_96 = Coupling(name = 'UVGC_231_96',
                       value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_232_97 = Coupling(name = 'UVGC_232_97',
                       value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MB else (G**2*yb)/(6.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(12.*cmath.pi**2) + (G**2*yb*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(G**2*yb)/(12.*cmath.pi**2) ) + (G**2*yb)/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_232_98 = Coupling(name = 'UVGC_232_98',
                       value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MT else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb)/(24.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_232_99 = Coupling(name = 'UVGC_232_99',
                       value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_233_100 = Coupling(name = 'UVGC_233_100',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else (G**2*yt)/(12.*cmath.pi**2) )'},
                        order = {'QCD':2,'QED':1})

UVGC_233_101 = Coupling(name = 'UVGC_233_101',
                        value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_233_102 = Coupling(name = 'UVGC_233_102',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_234_103 = Coupling(name = 'UVGC_234_103',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MB else -(G**2*yt)/(12.*cmath.pi**2) )'},
                        order = {'QCD':2,'QED':1})

UVGC_234_104 = Coupling(name = 'UVGC_234_104',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_234_105 = Coupling(name = 'UVGC_234_105',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_235_106 = Coupling(name = 'UVGC_235_106',
                        value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_236_107 = Coupling(name = 'UVGC_236_107',
                        value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_237_108 = Coupling(name = 'UVGC_237_108',
                        value = {-1:'( -(G**2*gPu33*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*gPu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*gPu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*gPu33*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*gPu33*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*gPu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*gPu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'DMS':1,'QCD':2,'QED':1})

UVGC_238_109 = Coupling(name = 'UVGC_238_109',
                        value = {-1:'( -(complex(0,1)*G**2*gSu33*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*gSu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*gSu33*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*complex(0,1)*G**2*gSu33*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gSu33*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*gSu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*gSu33*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'DMS':1,'QCD':2,'QED':1})

