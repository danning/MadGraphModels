# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:24:59


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS13 = Lorentz(name = 'UUS13',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV18 = Lorentz(name = 'UUV18',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2)')

UUV19 = Lorentz(name = 'UUV19',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,3)')

SSS13 = Lorentz(name = 'SSS13',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS26 = Lorentz(name = 'FFS26',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS27 = Lorentz(name = 'FFS27',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV53 = Lorentz(name = 'FFV53',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV54 = Lorentz(name = 'FFV54',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,1)*P(-1,3)*Gamma(3,2,1)')

FFV55 = Lorentz(name = 'FFV55',
                spins = [ 2, 2, 3 ],
                structure = 'P(-1,3)*P(3,1)*Gamma(-1,2,1)')

FFV56 = Lorentz(name = 'FFV56',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV57 = Lorentz(name = 'FFV57',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS13 = Lorentz(name = 'VSS13',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS13 = Lorentz(name = 'VVS13',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV13 = Lorentz(name = 'VVV13',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS13 = Lorentz(name = 'SSSS13',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

VVSS13 = Lorentz(name = 'VVSS13',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV61 = Lorentz(name = 'VVVV61',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV62 = Lorentz(name = 'VVVV62',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV63 = Lorentz(name = 'VVVV63',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV64 = Lorentz(name = 'VVVV64',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV65 = Lorentz(name = 'VVVV65',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

