Requestor: Alvaro Lopez Solis
Content: Flavoured dark matter model connecting dark matter to  right-handed quarks via new colored mediator
Webpage: https://doi.org/10.1007/JHEP01%282021%29194
Paper: https://doi.org/10.1007/JHEP01%282021%29194
Source: Giacomo Polesello
