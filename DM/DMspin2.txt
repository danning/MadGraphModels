Requestor: Lydia Roos
Content: Spin 2 dark matter simplified model
Paper: https://arxiv.org/abs/1605.09359
JIRA: https://its.cern.ch/jira/browse/AGENE-1196
