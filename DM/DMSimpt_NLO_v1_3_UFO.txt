Requestor: Spyros Argyropoulos and Zirui Wang
Contents: General t-channel dark matter model UFO
Paper: http://arxiv.org/abs/2001.05024
Website: http://feynrules.irmp.ucl.ac.be/wiki/DMsimpt
Code: http://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/DMsimpt/dmsimpt_v1.3.1.tar.gz
JIRA: https://its.cern.ch/jira/browse/AGENE-1852
