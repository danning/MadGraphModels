Requestor: Mata Perego
Content: Wino-like Dark Matter triplet
Paper 1: https://arxiv.org/abs/1407.7058
Paper 2: https://arxiv.org/abs/hep-ph/0512090
JIRA: https://its.cern.ch/jira/browse/AGENE-1357