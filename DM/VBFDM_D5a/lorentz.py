# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:21:33


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS9 = Lorentz(name = 'UUS9',
               spins = [ -1, -1, 1 ],
               structure = '1')

UUV10 = Lorentz(name = 'UUV10',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2)')

UUV11 = Lorentz(name = 'UUV11',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,3)')

SSS9 = Lorentz(name = 'SSS9',
               spins = [ 1, 1, 1 ],
               structure = '1')

FFS18 = Lorentz(name = 'FFS18',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS19 = Lorentz(name = 'FFS19',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV37 = Lorentz(name = 'FFV37',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV38 = Lorentz(name = 'FFV38',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV39 = Lorentz(name = 'FFV39',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS9 = Lorentz(name = 'VSS9',
               spins = [ 3, 1, 1 ],
               structure = 'P(1,2) - P(1,3)')

VVS9 = Lorentz(name = 'VVS9',
               spins = [ 3, 3, 1 ],
               structure = 'Metric(1,2)')

VVV9 = Lorentz(name = 'VVV9',
               spins = [ 3, 3, 3 ],
               structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS9 = Lorentz(name = 'SSSS9',
                spins = [ 1, 1, 1, 1 ],
                structure = '1')

FFVV20 = Lorentz(name = 'FFVV20',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Identity(2,1)*Metric(3,4)')

VVSS9 = Lorentz(name = 'VVSS9',
                spins = [ 3, 3, 1, 1 ],
                structure = 'Metric(1,2)')

VVVV41 = Lorentz(name = 'VVVV41',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV42 = Lorentz(name = 'VVVV42',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV43 = Lorentz(name = 'VVVV43',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV44 = Lorentz(name = 'VVVV44',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV45 = Lorentz(name = 'VVVV45',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

