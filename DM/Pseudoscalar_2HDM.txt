Requestor: Valerio Ippolito
Content: NLO 2HDM including a dark-matter candidate with PS interactions
Paper1: http://arxiv.org/abs/1701.07427
Paper2: http://arXiv.org/abs/1404.3716
Paper3: http://arXiv.org/abs/1509.01110
Paper4: http://arXiv.org/abs/1611.04593
JIRA: https://its.cern.ch/jira/browse/AGENE-1321

Update requestor: Oleg Brandt
Changes: Better handling of b-quark, 5FS, Wt+DM, relic calculations
Source: https://github.com/LHC-DMWG/model-repository/tree/master/models/Pseudoscalar_2HDM
JIRA: https://its.cern.ch/jira/browse/AGENE-1652