Requestor: Kevin Bauer
Content: Mono-Z' (Z' + MET) models
Paper: http://arxiv.org/abs/1504.01386
JIRA: https://its.cern.ch/jira/browse/AGENE-1101
