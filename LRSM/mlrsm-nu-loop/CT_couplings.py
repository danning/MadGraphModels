# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.2.0 for Mac OS X ARM (64-bit) (November 18, 2022)
# Date: Sat 2 Mar 2024 16:50:22


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_1146_1 = Coupling(name = 'R2GC_1146_1',
                       value = '(CKML2x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x3))/(192.*cmath.pi**2) + (CKMR2x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x3))/(192.*cmath.pi**2) - (CKML2x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x3)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR2x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x3)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_2 = Coupling(name = 'R2GC_1146_2',
                       value = '(CKML3x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x3))/(192.*cmath.pi**2) + (CKMR3x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x3))/(192.*cmath.pi**2) - (CKML3x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x3)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR3x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x3)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_3 = Coupling(name = 'R2GC_1146_3',
                       value = '(CKML1x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x3))/(192.*cmath.pi**2) + (CKMR1x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x3))/(192.*cmath.pi**2) - (CKML1x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x3)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR1x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x3)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_4 = Coupling(name = 'R2GC_1146_4',
                       value = '(CKML2x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x1))/(192.*cmath.pi**2) + (CKMR2x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x1))/(192.*cmath.pi**2) - (CKML2x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x1)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR2x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x1)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_5 = Coupling(name = 'R2GC_1146_5',
                       value = '(CKML2x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x2))/(192.*cmath.pi**2) + (CKMR2x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x2))/(192.*cmath.pi**2) - (CKML2x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x2)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR2x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x2)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_6 = Coupling(name = 'R2GC_1146_6',
                       value = '(CKML3x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x1))/(192.*cmath.pi**2) + (CKMR3x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x1))/(192.*cmath.pi**2) - (CKML3x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x1)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR3x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x1)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_7 = Coupling(name = 'R2GC_1146_7',
                       value = '(CKMR1x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x1)*cmath.cos(xi)**2)/(96.*cmath.pi**2) + (CKML1x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x1)*cmath.sin(xi)**2)/(96.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_8 = Coupling(name = 'R2GC_1146_8',
                       value = '(CKML3x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x2))/(192.*cmath.pi**2) + (CKMR3x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x2))/(192.*cmath.pi**2) - (CKML3x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x2)*cmath.cos(2*xi))/(192.*cmath.pi**2) + (CKMR3x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x2)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1146_9 = Coupling(name = 'R2GC_1146_9',
                       value = '(CKMR1x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x2)*cmath.cos(xi)**2)/(96.*cmath.pi**2) + (CKML1x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x2)*cmath.sin(xi)**2)/(96.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_1147_10 = Coupling(name = 'R2GC_1147_10',
                        value = '(CKML2x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x3))/(192.*cmath.pi**2) + (CKMR2x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x3))/(192.*cmath.pi**2) + (CKML2x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x3)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR2x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x3)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_11 = Coupling(name = 'R2GC_1147_11',
                        value = '(CKML3x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x3))/(192.*cmath.pi**2) + (CKMR3x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x3))/(192.*cmath.pi**2) + (CKML3x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x3)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR3x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x3)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_12 = Coupling(name = 'R2GC_1147_12',
                        value = '(CKML1x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x3))/(192.*cmath.pi**2) + (CKMR1x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x3))/(192.*cmath.pi**2) + (CKML1x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x3)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR1x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x3)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_13 = Coupling(name = 'R2GC_1147_13',
                        value = '(CKML2x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x1))/(192.*cmath.pi**2) + (CKMR2x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x1))/(192.*cmath.pi**2) + (CKML2x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x1)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR2x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x1)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_14 = Coupling(name = 'R2GC_1147_14',
                        value = '(CKML2x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x2))/(192.*cmath.pi**2) + (CKMR2x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x2))/(192.*cmath.pi**2) + (CKML2x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x2)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR2x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x2)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_15 = Coupling(name = 'R2GC_1147_15',
                        value = '(CKML3x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x1))/(192.*cmath.pi**2) + (CKMR3x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x1))/(192.*cmath.pi**2) + (CKML3x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x1)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR3x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x1)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_16 = Coupling(name = 'R2GC_1147_16',
                        value = '(CKML1x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x1)*cmath.cos(xi)**2)/(96.*cmath.pi**2) + (CKMR1x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x1)*cmath.sin(xi)**2)/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_17 = Coupling(name = 'R2GC_1147_17',
                        value = '(CKML3x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x2))/(192.*cmath.pi**2) + (CKMR3x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x2))/(192.*cmath.pi**2) + (CKML3x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x2)*cmath.cos(2*xi))/(192.*cmath.pi**2) - (CKMR3x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x2)*cmath.cos(2*xi))/(192.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1147_18 = Coupling(name = 'R2GC_1147_18',
                        value = '(CKML1x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x2)*cmath.cos(xi)**2)/(96.*cmath.pi**2) + (CKMR1x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x2)*cmath.sin(xi)**2)/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1253_19 = Coupling(name = 'R2GC_1253_19',
                        value = '-0.125*(complex(0,1)*G**2*MT*ON1x1*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) + (G**2*MT*ON4x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (G**2*MT*ON4x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1254_20 = Coupling(name = 'R2GC_1254_20',
                        value = '-0.125*(complex(0,1)*G**2*MT*ON1x2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) + (G**2*MT*ON4x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (G**2*MT*ON4x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1255_21 = Coupling(name = 'R2GC_1255_21',
                        value = '-0.125*(complex(0,1)*G**2*MT*ON1x3*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) + (G**2*MT*ON4x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (G**2*MT*ON4x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1256_22 = Coupling(name = 'R2GC_1256_22',
                        value = '-0.125*(complex(0,1)*G**2*MT*ON1x4*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) + (G**2*MT*ON4x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*MT*ON3x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2)) - (G**2*MT*ON4x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1257_23 = Coupling(name = 'R2GC_1257_23',
                        value = '-0.0625*(CKML3x3*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 - (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1257_24 = Coupling(name = 'R2GC_1257_24',
                        value = '-0.0625*(CKML3x1*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 - (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1257_25 = Coupling(name = 'R2GC_1257_25',
                        value = '-0.0625*(CKML3x2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 - (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(16.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1258_26 = Coupling(name = 'R2GC_1258_26',
                        value = '-0.0625*(CKML3x3*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 - (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1258_27 = Coupling(name = 'R2GC_1258_27',
                        value = '-0.0625*(CKML3x1*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 - (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1258_28 = Coupling(name = 'R2GC_1258_28',
                        value = '-0.0625*(CKML3x2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 - (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(16.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1259_29 = Coupling(name = 'R2GC_1259_29',
                        value = '(CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1260_30 = Coupling(name = 'R2GC_1260_30',
                        value = '(CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1261_31 = Coupling(name = 'R2GC_1261_31',
                        value = '(CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1262_32 = Coupling(name = 'R2GC_1262_32',
                        value = '(complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1263_33 = Coupling(name = 'R2GC_1263_33',
                        value = '(complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1264_34 = Coupling(name = 'R2GC_1264_34',
                        value = '(complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1265_35 = Coupling(name = 'R2GC_1265_35',
                        value = '(complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1266_36 = Coupling(name = 'R2GC_1266_36',
                        value = '(complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1267_37 = Coupling(name = 'R2GC_1267_37',
                        value = '(complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1268_38 = Coupling(name = 'R2GC_1268_38',
                        value = '(complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1269_39 = Coupling(name = 'R2GC_1269_39',
                        value = '(complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1270_40 = Coupling(name = 'R2GC_1270_40',
                        value = '(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1271_41 = Coupling(name = 'R2GC_1271_41',
                        value = '(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1272_42 = Coupling(name = 'R2GC_1272_42',
                        value = '(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_1273_43 = Coupling(name = 'R2GC_1273_43',
                        value = '-0.3333333333333333*(CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(2*beta))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_1274_44 = Coupling(name = 'R2GC_1274_44',
                        value = '-0.3333333333333333*(CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(2*beta))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_1275_45 = Coupling(name = 'R2GC_1275_45',
                        value = '-0.3333333333333333*(CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(2*beta))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_1276_46 = Coupling(name = 'R2GC_1276_46',
                        value = '-0.3333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(2*beta))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_1277_47 = Coupling(name = 'R2GC_1277_47',
                        value = '-0.3333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(2*beta))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_1278_48 = Coupling(name = 'R2GC_1278_48',
                        value = '-0.3333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(2*beta))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_1279_49 = Coupling(name = 'R2GC_1279_49',
                        value = '-0.03125*(CKML3x3*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*sec(2*beta)**2)/cmath.pi**2 + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*sec(2*beta)**2)/(64.*cmath.pi**2) - (CKMR3x3*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(32.*cmath.pi**2) + (CKML3x3*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.cos(4*beta)**2*sec(2*beta)**2)/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1279_50 = Coupling(name = 'R2GC_1279_50',
                        value = '-0.0625*(CKMR3x1*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(32.*cmath.pi**2) + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (CKML3x1*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.tan(2*beta)**2)/(16.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.tan(2*beta)**2)/(32.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.cos(4*beta)*cmath.tan(2*beta)**2)/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1279_51 = Coupling(name = 'R2GC_1279_51',
                        value = '-0.0625*(CKMR3x2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(32.*cmath.pi**2) + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (CKML3x2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.tan(2*beta)**2)/(16.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.tan(2*beta)**2)/(32.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.cos(4*beta)*cmath.tan(2*beta)**2)/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1287_52 = Coupling(name = 'R2GC_1287_52',
                        value = '(-7*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(3072.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(9216.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2*zetaLR**2) - (11*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(2304.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2*zetaLR**4) - (5*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(9216.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(27648.*cmath.pi**2*zetaLR**4) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2*zetaLR**2) - (complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(6912.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1287_53 = Coupling(name = 'R2GC_1287_53',
                        value = '(-5*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(3456.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(1536.*cmath.pi**2) - (5*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(4608.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**4*cmath.tan(thetaw))/(1728.*cmath.pi**2*zetaLR**2) - (5*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(1728.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(1152.*cmath.pi**2) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(3456.*cmath.pi**2*zetaLR**4) - (7*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(3456.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(4608.*cmath.pi**2) - (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(13824.*cmath.pi**2*zetaLR**4) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(1728.*cmath.pi**2*zetaLR**2) - (complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**4*cmath.tan(thetaw))/(1728.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1322_54 = Coupling(name = 'R2GC_1322_54',
                        value = '(ep*complex(0,1)*G**2*Yu3x3**2)/(32.*cmath.pi**2) - (ep*complex(0,1)*G**2*Yu3x3**2*cmath.tan(thetaw)**2)/(32.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1323_55 = Coupling(name = 'R2GC_1323_55',
                        value = '-0.16666666666666666*(ep*G**2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) + (ep*G**2*Yu3x3*cmath.tan(thetaw)**2)/(6.*cmath.pi**2*zetaLR**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1421_56 = Coupling(name = 'R2GC_1421_56',
                        value = '(ep*G**2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) + (ep*G**2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1422_57 = Coupling(name = 'R2GC_1422_57',
                        value = '(ep*G**2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) + (ep*G**2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1423_58 = Coupling(name = 'R2GC_1423_58',
                        value = '(ep*G**2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) + (ep*G**2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1424_59 = Coupling(name = 'R2GC_1424_59',
                        value = '(ep*G**2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) + (ep*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(64.*cmath.pi**2) - (ep*G**2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) + (ep*G**2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2) - (ep*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(64.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':2,'QED':2})

R2GC_1456_60 = Coupling(name = 'R2GC_1456_60',
                        value = '-0.015625*(ep**2*complex(0,1)*G**2*Yu3x3**2)/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3**2*cmath.tan(thetaw)**2)/(32.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*Yu3x3**2*cmath.tan(thetaw)**4)/(64.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':2})

R2GC_1457_61 = Coupling(name = 'R2GC_1457_61',
                        value = '-0.0625*(complex(0,1)*G**2*Yu3x3**2)/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3**2)/(64.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3**2*cmath.tan(thetaw)**2)/(32.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*Yu3x3**2*cmath.tan(thetaw)**4)/(64.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':2})

R2GC_1488_62 = Coupling(name = 'R2GC_1488_62',
                        value = '(complex(0,1)*G**3*gw*cmath.cos(thetaw))/(192.*cmath.pi**2) + (ep**2*complex(0,1)*G**3*gw*sec(thetaw))/(768.*cmath.pi**2) - (complex(0,1)*G**3*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(576.*cmath.pi**2) - (ep**2*complex(0,1)*G**3*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(576.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**3*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(2304.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':3,'QED':1})

R2GC_1488_63 = Coupling(name = 'R2GC_1488_63',
                        value = '-0.005208333333333333*(complex(0,1)*G**3*gw*cmath.cos(thetaw))/cmath.pi**2 - (ep**2*complex(0,1)*G**3*gw*sec(thetaw))/(768.*cmath.pi**2) + (5*complex(0,1)*G**3*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(576.*cmath.pi**2) + (ep**2*complex(0,1)*G**3*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(288.*cmath.pi**2*zetaLR**2) - (5*ep**2*complex(0,1)*G**3*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(2304.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':3,'QED':1})

R2GC_1489_64 = Coupling(name = 'R2GC_1489_64',
                        value = '-0.08333333333333333*(complex(0,1)*G**2*gw*cmath.cos(thetaw))/cmath.pi**2 - (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(36.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(144.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(144.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':1})

R2GC_1490_65 = Coupling(name = 'R2GC_1490_65',
                        value = '-0.020833333333333332*(ep**2*complex(0,1)*G**2*gw*sec(thetaw))/cmath.pi**2 + (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(18.*cmath.pi**2) + (5*ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(144.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(72.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':1})

R2GC_1491_66 = Coupling(name = 'R2GC_1491_66',
                        value = '(complex(0,1)*G**2*gw*cmath.cos(thetaw))/(12.*cmath.pi**2) - (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(36.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(144.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(144.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':1})

R2GC_1492_67 = Coupling(name = 'R2GC_1492_67',
                        value = '(ep**2*complex(0,1)*G**2*gw*sec(thetaw))/(48.*cmath.pi**2) - (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(9.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(144.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(36.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':1})

R2GC_1528_68 = Coupling(name = 'R2GC_1528_68',
                        value = '(G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3)/(24.*cmath.pi**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*cmath.tan(thetaw)**2)/(12.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*cmath.tan(thetaw)**4)/(24.*cmath.pi**2*zetaLR**4*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_1686_69 = Coupling(name = 'R2GC_1686_69',
                        value = '-0.03125*(G**2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*G**2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) + (G**2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) - (ep**2*G**2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (ep**2*G**2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) - (ep**2*G**2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':2})

R2GC_1687_70 = Coupling(name = 'R2GC_1687_70',
                        value = '-0.03125*(G**2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*G**2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) + (G**2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) - (ep**2*G**2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (ep**2*G**2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) - (ep**2*G**2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':2})

R2GC_1688_71 = Coupling(name = 'R2GC_1688_71',
                        value = '-0.03125*(G**2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*G**2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) + (G**2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) - (ep**2*G**2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (ep**2*G**2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) - (ep**2*G**2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':2})

R2GC_1689_72 = Coupling(name = 'R2GC_1689_72',
                        value = '-0.03125*(G**2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*G**2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(256.*cmath.pi**2) + (G**2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) - (ep**2*G**2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(32.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(256.*cmath.pi**2) - (ep**2*G**2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) - (ep**2*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**2)/(128.*cmath.pi**2*zetaLR**2) + (ep**2*G**2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) - (ep**2*G**2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta)*cmath.tan(thetaw)**4)/(256.*cmath.pi**2*zetaLR**4)',
                        order = {'QCD':2,'QED':2})

R2GC_1783_73 = Coupling(name = 'R2GC_1783_73',
                        value = '(35*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (91*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (25*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (61*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**12)/(49152.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**12)/(196608.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (77*complex(0,1)*G**2*gw**2*zetaLR**4*sec(thetaw)**12)/(65536.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(442368.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (35*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(884736.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(884736.*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(147456.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(6144.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(24576.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (33*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(2*thetaw)*sec(thetaw)**12)/(16384.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (85*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (167*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (65*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (19*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(393216.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(98304.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(393216.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (165*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(4*thetaw)*sec(thetaw)**12)/(131072.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(65536.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(589824.*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(98304.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(36864.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(147456.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (55*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(6*thetaw)*sec(thetaw)**12)/(98304.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (11*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(65536.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (13*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(147456.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (13*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(589824.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (11*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(8*thetaw)*sec(thetaw)**12)/(65536.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(884736.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (11*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(36864.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(147456.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(10*thetaw)*sec(thetaw)**12)/(32768.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(1.179648e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(12*thetaw)*sec(thetaw)**12)/(393216.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':2})

R2GC_1783_74 = Coupling(name = 'R2GC_1783_74',
                        value = '(119*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(110592.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (13*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(442368.*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (47*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**12)/(884736.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**12)/(24576.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**12)/(98304.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (77*complex(0,1)*G**2*gw**2*zetaLR**4*sec(thetaw)**12)/(65536.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (17*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(442368.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (23*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(884736.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (17*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(884736.*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(147456.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(3072.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**12)/(12288.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (33*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(2*thetaw)*sec(thetaw)**12)/(16384.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (289*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (163*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (61*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (49*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(589824.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(49152.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**12)/(196608.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (165*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(4*thetaw)*sec(thetaw)**12)/(131072.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (17*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (31*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(589824.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (17*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(589824.*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(98304.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(18432.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**12)/(73728.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (55*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(6*thetaw)*sec(thetaw)**12)/(98304.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (17*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(221184.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(442368.*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) + (11*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (13*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(73728.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (13*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (11*complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(8*thetaw)*sec(thetaw)**12)/(65536.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (17*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(884736.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (47*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (17*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(294912.*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(18432.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**12)/(73728.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(10*thetaw)*sec(thetaw)**12)/(32768.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (17*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(3.538944e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (11*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**4*(zetaLR**2 - cmath.tan(thetaw)**2)) - (13*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(1.769472e6*cmath.pi**2*zetaLR**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(147456.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(12*thetaw)*sec(thetaw)**12)/(589824.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**4*cmath.cos(12*thetaw)*sec(thetaw)**12)/(393216.*cmath.pi**2*(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':2})

R2GC_1784_75 = Coupling(name = 'R2GC_1784_75',
                        value = '(9*complex(0,1)*G**3*gw*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**6)/(1024.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**6)/(512.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (9*complex(0,1)*G**3*gw*zetaLR**2*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**6)/(512.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**6)/(512.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (21*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(256.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(1024.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(512.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(256.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(2*thetaw)*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':3,'QED':1})

R2GC_1784_76 = Coupling(name = 'R2GC_1784_76',
                        value = '(-9*complex(0,1)*G**3*gw*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**6)/(1024.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**6)/(512.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (9*complex(0,1)*G**3*gw*zetaLR**2*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**6)/(512.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**6)/(512.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (21*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(256.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(1024.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(512.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(2*thetaw)**2*sec(thetaw)**6)/(256.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (3*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (3*complex(0,1)*G**3*gw*zetaLR**2*cmath.cos(2*thetaw)*cmath.cos(4*thetaw)*sec(thetaw)**6)/(1024.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':3,'QED':1})

R2GC_1785_77 = Coupling(name = 'R2GC_1785_77',
                        value = '(49*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(884736.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (707*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (47*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (179*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**11)/(98304.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (21*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**11)/(65536.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (23*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(442368.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (1079*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (137*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(12288.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (35*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(65536.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (65*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (451*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (61*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (73*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(196608.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(16384.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(98304.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (49*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(1.179648e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(1.179648e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (89*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(1.179648e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(73728.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (15*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(131072.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (17*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (13*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (31*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (13*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(294912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(196608.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(3.538944e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(3.538944e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(3.538944e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(73728.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(393216.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(589824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':2})

R2GC_1785_78 = Coupling(name = 'R2GC_1785_78',
                        value = '(7*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(221184.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (749*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (89*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (239*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**11)/(49152.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (21*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**11)/(65536.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(442368.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (1103*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (25*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (137*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(6144.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (35*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**11)/(65536.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (61*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (25*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(110592.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(110592.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (59*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(442368.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(98304.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**11)/(16384.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(294912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (25*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(1.179648e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (25*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(1.179648e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (89*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(1.179648e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(36864.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (15*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**11)/(131072.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(221184.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (19*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(1.769472e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (13*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(147456.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(8*thetaw)*sec(thetaw)**11)/(196608.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (23*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (23*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(3.538944e6*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (25*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(3.538944e6*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(3.538944e6*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(36864.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(10*thetaw)*sec(thetaw)**11)/(393216.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(884736.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(442368.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(442368.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(221184.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(12*thetaw)*sec(thetaw)**11)/(294912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':2})

R2GC_1786_79 = Coupling(name = 'R2GC_1786_79',
                        value = '(complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**5*cmath.tan(thetaw))/(4608.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (11*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(9216.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(3072.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(9216.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(1536.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(27648.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(9216.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':2})

R2GC_1786_80 = Coupling(name = 'R2GC_1786_80',
                        value = '(5*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (11*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*zetaLR**2*sec(thetaw)**5*cmath.tan(thetaw))/(2304.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(768.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(3456.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(2*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(1536.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(1536.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(4*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(768.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(13824.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(6912.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(3456.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw**2*zetaLR**2*cmath.cos(6*thetaw)*sec(thetaw)**5*cmath.tan(thetaw))/(4608.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':2})

R2GC_1863_81 = Coupling(name = 'R2GC_1863_81',
                        value = '-0.0013020833333333333*(ep**2*complex(0,1)*G**3*gw)/(cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**3*gw*zetaLR**2)/(192.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**3*gw*cmath.tan(thetaw)**2)/(576.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**2)/(2304.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**2)/(384.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**4)/(768.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**4)/(1152.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**6)/(2304.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':3,'QED':1})

R2GC_1863_82 = Coupling(name = 'R2GC_1863_82',
                        value = '(ep**2*complex(0,1)*G**3*gw)/(768.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**3*gw*zetaLR**2)/(192.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*complex(0,1)*G**3*gw*cmath.tan(thetaw)**2)/(576.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**2)/(2304.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**2)/(384.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**4)/(768.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (5*ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**4)/(1152.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (5*ep**2*complex(0,1)*G**3*gw*cmath.tan(thetaw)**6)/(2304.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':3,'QED':1})

R2GC_1864_83 = Coupling(name = 'R2GC_1864_83',
                        value = '(ep**2*complex(0,1)*G**2*gw)/(48.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(36.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(144.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(24.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(48.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(72.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(144.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1865_84 = Coupling(name = 'R2GC_1865_84',
                        value = '-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR**2)/(cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(18.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(72.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(36.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(72.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1866_85 = Coupling(name = 'R2GC_1866_85',
                        value = '-0.020833333333333332*(ep**2*complex(0,1)*G**2*gw)/(cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(36.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(144.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(24.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(48.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(72.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(144.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':1})

R2GC_1867_86 = Coupling(name = 'R2GC_1867_86',
                        value = '(complex(0,1)*G**2*gw*zetaLR**2)/(12.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(9.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(36.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(18.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(36.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',
                        order = {'QCD':2,'QED':1})

R2GC_235_87 = Coupling(name = 'R2GC_235_87',
                       value = '(3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_236_88 = Coupling(name = 'R2GC_236_88',
                       value = '-0.125*(complex(0,1)*G**2*MT**2)/cmath.pi**2',
                       order = {'QCD':2})

R2GC_237_89 = Coupling(name = 'R2GC_237_89',
                       value = '-0.03125*(CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3))/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_237_90 = Coupling(name = 'R2GC_237_90',
                       value = '-0.03125*(CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1))/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_237_91 = Coupling(name = 'R2GC_237_91',
                       value = '-0.03125*(CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2))/cmath.pi**2',
                       order = {'QCD':2,'QED':2})

R2GC_238_92 = Coupling(name = 'R2GC_238_92',
                       value = '-0.0625*(CKML3x3*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3))/cmath.pi**2 - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3))/(32.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x3)*cmath.cos(4*beta))/(32.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_238_93 = Coupling(name = 'R2GC_238_93',
                       value = '-0.0625*(CKML3x1*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1))/cmath.pi**2 - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1))/(32.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x1)*cmath.cos(4*beta))/(32.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_238_94 = Coupling(name = 'R2GC_238_94',
                       value = '-0.0625*(CKML3x2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2))/cmath.pi**2 - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2))/(32.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKML3x2)*cmath.cos(4*beta))/(32.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_246_95 = Coupling(name = 'R2GC_246_95',
                       value = '-0.005208333333333333*G**4/cmath.pi**2',
                       order = {'QCD':4})

R2GC_246_96 = Coupling(name = 'R2GC_246_96',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_247_97 = Coupling(name = 'R2GC_247_97',
                       value = '-0.005208333333333333*(complex(0,1)*G**4)/cmath.pi**2',
                       order = {'QCD':4})

R2GC_247_98 = Coupling(name = 'R2GC_247_98',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_248_99 = Coupling(name = 'R2GC_248_99',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_248_100 = Coupling(name = 'R2GC_248_100',
                        value = '-0.015625*(complex(0,1)*G**4)/cmath.pi**2',
                        order = {'QCD':4})

R2GC_249_101 = Coupling(name = 'R2GC_249_101',
                        value = '-0.020833333333333332*(complex(0,1)*G**4)/cmath.pi**2',
                        order = {'QCD':4})

R2GC_250_102 = Coupling(name = 'R2GC_250_102',
                        value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_250_103 = Coupling(name = 'R2GC_250_103',
                        value = '-0.03125*(complex(0,1)*G**4)/cmath.pi**2',
                        order = {'QCD':4})

R2GC_251_104 = Coupling(name = 'R2GC_251_104',
                        value = '-0.0625*(complex(0,1)*G**4)/cmath.pi**2',
                        order = {'QCD':4})

R2GC_252_105 = Coupling(name = 'R2GC_252_105',
                        value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_253_106 = Coupling(name = 'R2GC_253_106',
                        value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_255_107 = Coupling(name = 'R2GC_255_107',
                        value = '-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',
                        order = {'QCD':3})

R2GC_269_108 = Coupling(name = 'R2GC_269_108',
                        value = '-0.16666666666666666*(CKML2x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_270_109 = Coupling(name = 'R2GC_270_109',
                        value = '-0.16666666666666666*(CKMR2x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_271_110 = Coupling(name = 'R2GC_271_110',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML2x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_272_111 = Coupling(name = 'R2GC_272_111',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_273_112 = Coupling(name = 'R2GC_273_112',
                        value = '-0.16666666666666666*(CKML2x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_274_113 = Coupling(name = 'R2GC_274_113',
                        value = '-0.16666666666666666*(CKMR2x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_275_114 = Coupling(name = 'R2GC_275_114',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML2x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_276_115 = Coupling(name = 'R2GC_276_115',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_277_116 = Coupling(name = 'R2GC_277_116',
                        value = '-0.16666666666666666*(CKML2x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_278_117 = Coupling(name = 'R2GC_278_117',
                        value = '-0.16666666666666666*(CKMR2x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_279_118 = Coupling(name = 'R2GC_279_118',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML2x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_280_119 = Coupling(name = 'R2GC_280_119',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_281_120 = Coupling(name = 'R2GC_281_120',
                        value = '-0.16666666666666666*(CKML1x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_282_121 = Coupling(name = 'R2GC_282_121',
                        value = '-0.16666666666666666*(CKMR1x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_283_122 = Coupling(name = 'R2GC_283_122',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML1x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_284_123 = Coupling(name = 'R2GC_284_123',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_285_124 = Coupling(name = 'R2GC_285_124',
                        value = '-0.16666666666666666*(CKML1x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_286_125 = Coupling(name = 'R2GC_286_125',
                        value = '-0.16666666666666666*(CKMR1x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_287_126 = Coupling(name = 'R2GC_287_126',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML1x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_288_127 = Coupling(name = 'R2GC_288_127',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_289_128 = Coupling(name = 'R2GC_289_128',
                        value = '-0.16666666666666666*(CKML1x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_290_129 = Coupling(name = 'R2GC_290_129',
                        value = '-0.16666666666666666*(CKMR1x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_291_130 = Coupling(name = 'R2GC_291_130',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML1x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_292_131 = Coupling(name = 'R2GC_292_131',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_293_132 = Coupling(name = 'R2GC_293_132',
                        value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_293_133 = Coupling(name = 'R2GC_293_133',
                        value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_294_134 = Coupling(name = 'R2GC_294_134',
                        value = '-0.0625*(complex(0,1)*G**2)/cmath.pi**2',
                        order = {'QCD':2})

R2GC_295_135 = Coupling(name = 'R2GC_295_135',
                        value = 'G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_296_136 = Coupling(name = 'R2GC_296_136',
                        value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_296_137 = Coupling(name = 'R2GC_296_137',
                        value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_297_138 = Coupling(name = 'R2GC_297_138',
                        value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_297_139 = Coupling(name = 'R2GC_297_139',
                        value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_299_140 = Coupling(name = 'R2GC_299_140',
                        value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_300_141 = Coupling(name = 'R2GC_300_141',
                        value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_304_142 = Coupling(name = 'R2GC_304_142',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_305_143 = Coupling(name = 'R2GC_305_143',
                        value = '(CKMR3x1*ep*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_306_144 = Coupling(name = 'R2GC_306_144',
                        value = '(CKMR3x2*ep*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_307_145 = Coupling(name = 'R2GC_307_145',
                        value = '(CKMR3x3*ep*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_308_146 = Coupling(name = 'R2GC_308_146',
                        value = '(complex(0,1)*G**2*ON1x1*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_309_147 = Coupling(name = 'R2GC_309_147',
                        value = '(complex(0,1)*G**2*ON1x2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_310_148 = Coupling(name = 'R2GC_310_148',
                        value = '(complex(0,1)*G**2*ON1x3*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_311_149 = Coupling(name = 'R2GC_311_149',
                        value = '(complex(0,1)*G**2*ON1x4*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_312_150 = Coupling(name = 'R2GC_312_150',
                        value = '(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_313_151 = Coupling(name = 'R2GC_313_151',
                        value = '(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_314_152 = Coupling(name = 'R2GC_314_152',
                        value = '(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_315_153 = Coupling(name = 'R2GC_315_153',
                        value = '-0.16666666666666666*(CKML3x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_316_154 = Coupling(name = 'R2GC_316_154',
                        value = '-0.16666666666666666*(CKML3x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_317_155 = Coupling(name = 'R2GC_317_155',
                        value = '-0.16666666666666666*(CKML3x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_318_156 = Coupling(name = 'R2GC_318_156',
                        value = '-0.16666666666666666*(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_319_157 = Coupling(name = 'R2GC_319_157',
                        value = '-0.16666666666666666*(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_320_158 = Coupling(name = 'R2GC_320_158',
                        value = '-0.16666666666666666*(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_321_159 = Coupling(name = 'R2GC_321_159',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_322_160 = Coupling(name = 'R2GC_322_160',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_323_161 = Coupling(name = 'R2GC_323_161',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_324_162 = Coupling(name = 'R2GC_324_162',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_325_163 = Coupling(name = 'R2GC_325_163',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_326_164 = Coupling(name = 'R2GC_326_164',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_327_165 = Coupling(name = 'R2GC_327_165',
                        value = '(CKMR3x3*ep*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x3)*sec(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_327_166 = Coupling(name = 'R2GC_327_166',
                        value = '(CKMR3x1*ep*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x1)*sec(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_327_167 = Coupling(name = 'R2GC_327_167',
                        value = '(CKMR3x2*ep*complex(0,1)*G**2*Yu3x3**2*complexconjugate(CKMR3x2)*sec(2*beta))/(16.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_328_168 = Coupling(name = 'R2GC_328_168',
                        value = '-0.3333333333333333*(CKMR3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(12.*cmath.pi**2) + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_329_169 = Coupling(name = 'R2GC_329_169',
                        value = '-0.3333333333333333*(CKMR3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(12.*cmath.pi**2) + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_330_170 = Coupling(name = 'R2GC_330_170',
                        value = '-0.3333333333333333*(CKMR3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(12.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_331_171 = Coupling(name = 'R2GC_331_171',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_332_172 = Coupling(name = 'R2GC_332_172',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_333_173 = Coupling(name = 'R2GC_333_173',
                        value = '-0.3333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_334_174 = Coupling(name = 'R2GC_334_174',
                        value = '(9*complex(0,1)*G**3*gw*sec(thetaw)**5)/(512.*cmath.pi**2) - (9*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**5)/(2048.*cmath.pi**2) - (9*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**5)/(2048.*cmath.pi**2*zetaLR**4) + (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**5)/(1024.*cmath.pi**2*zetaLR**2) + (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**5)/(128.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**5)/(512.*cmath.pi**2) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**5)/(512.*cmath.pi**2*zetaLR**4) + (3*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(512.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(2048.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(2048.*cmath.pi**2*zetaLR**4) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(1024.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':3,'QED':1})

R2GC_334_175 = Coupling(name = 'R2GC_334_175',
                        value = '(-9*complex(0,1)*G**3*gw*sec(thetaw)**5)/(512.*cmath.pi**2) + (9*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**5)/(2048.*cmath.pi**2) + (9*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**5)/(2048.*cmath.pi**2*zetaLR**4) - (3*ep**2*complex(0,1)*G**3*gw*sec(thetaw)**5)/(1024.*cmath.pi**2*zetaLR**2) - (3*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**5)/(128.*cmath.pi**2) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**5)/(512.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(2*thetaw)*sec(thetaw)**5)/(512.*cmath.pi**2*zetaLR**4) - (3*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(512.*cmath.pi**2) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(2048.*cmath.pi**2) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(2048.*cmath.pi**2*zetaLR**4) + (3*ep**2*complex(0,1)*G**3*gw*cmath.cos(4*thetaw)*sec(thetaw)**5)/(1024.*cmath.pi**2*zetaLR**2)',
                        order = {'QCD':3,'QED':1})

R2GC_335_176 = Coupling(name = 'R2GC_335_176',
                        value = '(35*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(27648.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(147456.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(36864.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(16384.*cmath.pi**2*zetaLR**2) + (233*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(147456.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(27648.*cmath.pi**2*zetaLR**2) + (535*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(36864.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(27648.*cmath.pi**2*zetaLR**4) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2*zetaLR**2) + (35*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(73728.*cmath.pi**2) + (13*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(294912.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(294912.*cmath.pi**2*zetaLR**4) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(24576.*cmath.pi**2*zetaLR**2) + (7*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(55296.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(49152.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2*zetaLR**2) + (5*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(221184.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(294912.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(884736.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(221184.*cmath.pi**2*zetaLR**2) + (complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_335_177 = Coupling(name = 'R2GC_335_177',
                        value = '(259*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(221184.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(73728.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(147456.*cmath.pi**2*zetaLR**4) + (11*ep**2*complex(0,1)*G**2*gw**2*sec(thetaw)**10)/(147456.*cmath.pi**2*zetaLR**2) + (109*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(55296.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(73728.*cmath.pi**2) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(221184.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(2*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2*zetaLR**2) + (133*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(18432.*cmath.pi**2) + (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2*zetaLR**4) - (13*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(4*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2*zetaLR**2) + (7*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(12288.*cmath.pi**2) + (13*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(147456.*cmath.pi**2) - (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(147456.*cmath.pi**2*zetaLR**4) - (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(6*thetaw)*sec(thetaw)**10)/(36864.*cmath.pi**2*zetaLR**2) + (49*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(221184.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(24576.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2*zetaLR**4) + (19*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(8*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2*zetaLR**2) + (7*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(147456.*cmath.pi**2) + (5*ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(442368.*cmath.pi**2*zetaLR**4) + (ep**2*complex(0,1)*G**2*gw**2*cmath.cos(10*thetaw)*sec(thetaw)**10)/(55296.*cmath.pi**2*zetaLR**2) + (complex(0,1)*G**2*gw**2*cmath.cos(12*thetaw)*sec(thetaw)**10)/(110592.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_567_178 = Coupling(name = 'R2GC_567_178',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_568_179 = Coupling(name = 'R2GC_568_179',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_569_180 = Coupling(name = 'R2GC_569_180',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_570_181 = Coupling(name = 'R2GC_570_181',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_571_182 = Coupling(name = 'R2GC_571_182',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_572_183 = Coupling(name = 'R2GC_572_183',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_573_184 = Coupling(name = 'R2GC_573_184',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_574_185 = Coupling(name = 'R2GC_574_185',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_575_186 = Coupling(name = 'R2GC_575_186',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_576_187 = Coupling(name = 'R2GC_576_187',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_577_188 = Coupling(name = 'R2GC_577_188',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_578_189 = Coupling(name = 'R2GC_578_189',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_579_190 = Coupling(name = 'R2GC_579_190',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_580_191 = Coupling(name = 'R2GC_580_191',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_581_192 = Coupling(name = 'R2GC_581_192',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_582_193 = Coupling(name = 'R2GC_582_193',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_583_194 = Coupling(name = 'R2GC_583_194',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_584_195 = Coupling(name = 'R2GC_584_195',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_585_196 = Coupling(name = 'R2GC_585_196',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_586_197 = Coupling(name = 'R2GC_586_197',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_587_198 = Coupling(name = 'R2GC_587_198',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_588_199 = Coupling(name = 'R2GC_588_199',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_589_200 = Coupling(name = 'R2GC_589_200',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_590_201 = Coupling(name = 'R2GC_590_201',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_591_202 = Coupling(name = 'R2GC_591_202',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_592_203 = Coupling(name = 'R2GC_592_203',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_593_204 = Coupling(name = 'R2GC_593_204',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_594_205 = Coupling(name = 'R2GC_594_205',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_595_206 = Coupling(name = 'R2GC_595_206',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_596_207 = Coupling(name = 'R2GC_596_207',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_597_208 = Coupling(name = 'R2GC_597_208',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_598_209 = Coupling(name = 'R2GC_598_209',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_599_210 = Coupling(name = 'R2GC_599_210',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_600_211 = Coupling(name = 'R2GC_600_211',
                        value = '(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_601_212 = Coupling(name = 'R2GC_601_212',
                        value = '(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_602_213 = Coupling(name = 'R2GC_602_213',
                        value = '(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_603_214 = Coupling(name = 'R2GC_603_214',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_604_215 = Coupling(name = 'R2GC_604_215',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_605_216 = Coupling(name = 'R2GC_605_216',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_606_217 = Coupling(name = 'R2GC_606_217',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_607_218 = Coupling(name = 'R2GC_607_218',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_608_219 = Coupling(name = 'R2GC_608_219',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_609_220 = Coupling(name = 'R2GC_609_220',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_610_221 = Coupling(name = 'R2GC_610_221',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_611_222 = Coupling(name = 'R2GC_611_222',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_612_223 = Coupling(name = 'R2GC_612_223',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_613_224 = Coupling(name = 'R2GC_613_224',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_614_225 = Coupling(name = 'R2GC_614_225',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_615_226 = Coupling(name = 'R2GC_615_226',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_616_227 = Coupling(name = 'R2GC_616_227',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_617_228 = Coupling(name = 'R2GC_617_228',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_618_229 = Coupling(name = 'R2GC_618_229',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_619_230 = Coupling(name = 'R2GC_619_230',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_620_231 = Coupling(name = 'R2GC_620_231',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_621_232 = Coupling(name = 'R2GC_621_232',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_622_233 = Coupling(name = 'R2GC_622_233',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_623_234 = Coupling(name = 'R2GC_623_234',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_624_235 = Coupling(name = 'R2GC_624_235',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_625_236 = Coupling(name = 'R2GC_625_236',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_626_237 = Coupling(name = 'R2GC_626_237',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_627_238 = Coupling(name = 'R2GC_627_238',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_628_239 = Coupling(name = 'R2GC_628_239',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_629_240 = Coupling(name = 'R2GC_629_240',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_630_241 = Coupling(name = 'R2GC_630_241',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_631_242 = Coupling(name = 'R2GC_631_242',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_632_243 = Coupling(name = 'R2GC_632_243',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_633_244 = Coupling(name = 'R2GC_633_244',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_634_245 = Coupling(name = 'R2GC_634_245',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_635_246 = Coupling(name = 'R2GC_635_246',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_636_247 = Coupling(name = 'R2GC_636_247',
                        value = '(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_637_248 = Coupling(name = 'R2GC_637_248',
                        value = '(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_638_249 = Coupling(name = 'R2GC_638_249',
                        value = '(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_691_250 = Coupling(name = 'R2GC_691_250',
                        value = '(CKML3x1*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_692_251 = Coupling(name = 'R2GC_692_251',
                        value = '(CKML3x2*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_693_252 = Coupling(name = 'R2GC_693_252',
                        value = '(CKML3x3*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_694_253 = Coupling(name = 'R2GC_694_253',
                        value = '(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1))/(3.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(4*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_695_254 = Coupling(name = 'R2GC_695_254',
                        value = '(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2))/(3.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(4*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_696_255 = Coupling(name = 'R2GC_696_255',
                        value = '(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3))/(3.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(4*beta))/(12.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_759_256 = Coupling(name = 'R2GC_759_256',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_759_257 = Coupling(name = 'R2GC_759_257',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_759_258 = Coupling(name = 'R2GC_759_258',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_759_259 = Coupling(name = 'R2GC_759_259',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x1**2*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x1**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x1**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x1**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x1**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) + (G**2*ON1x1*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (G**2*ON1x1*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_759_260 = Coupling(name = 'R2GC_759_260',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_759_261 = Coupling(name = 'R2GC_759_261',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_759_262 = Coupling(name = 'R2GC_759_262',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x1**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x1**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_263 = Coupling(name = 'R2GC_760_263',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_264 = Coupling(name = 'R2GC_760_264',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_265 = Coupling(name = 'R2GC_760_265',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_266 = Coupling(name = 'R2GC_760_266',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x1*ON1x2*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON1x2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x2*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x1*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x2*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x1*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_267 = Coupling(name = 'R2GC_760_267',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_268 = Coupling(name = 'R2GC_760_268',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_760_269 = Coupling(name = 'R2GC_760_269',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_270 = Coupling(name = 'R2GC_761_270',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_271 = Coupling(name = 'R2GC_761_271',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_272 = Coupling(name = 'R2GC_761_272',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_273 = Coupling(name = 'R2GC_761_273',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x2**2*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x2**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x2**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x2**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x2**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) + (G**2*ON1x2*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (G**2*ON1x2*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_274 = Coupling(name = 'R2GC_761_274',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_275 = Coupling(name = 'R2GC_761_275',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_761_276 = Coupling(name = 'R2GC_761_276',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x2**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x2**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_277 = Coupling(name = 'R2GC_762_277',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_278 = Coupling(name = 'R2GC_762_278',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_279 = Coupling(name = 'R2GC_762_279',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_280 = Coupling(name = 'R2GC_762_280',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x1*ON1x3*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON1x3*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x3*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x1*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x3*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x1*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_281 = Coupling(name = 'R2GC_762_281',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_282 = Coupling(name = 'R2GC_762_282',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_762_283 = Coupling(name = 'R2GC_762_283',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_284 = Coupling(name = 'R2GC_763_284',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_285 = Coupling(name = 'R2GC_763_285',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_286 = Coupling(name = 'R2GC_763_286',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_287 = Coupling(name = 'R2GC_763_287',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x2*ON1x3*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON1x3*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x3*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x2*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x3*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x2*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_288 = Coupling(name = 'R2GC_763_288',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_289 = Coupling(name = 'R2GC_763_289',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_763_290 = Coupling(name = 'R2GC_763_290',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x2*ON3x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x2*ON4x3*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_291 = Coupling(name = 'R2GC_764_291',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_292 = Coupling(name = 'R2GC_764_292',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_293 = Coupling(name = 'R2GC_764_293',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_294 = Coupling(name = 'R2GC_764_294',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x3**2*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x3**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x3**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x3**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x3**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) + (G**2*ON1x3*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (G**2*ON1x3*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_295 = Coupling(name = 'R2GC_764_295',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_296 = Coupling(name = 'R2GC_764_296',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_764_297 = Coupling(name = 'R2GC_764_297',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x3**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x3**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_298 = Coupling(name = 'R2GC_765_298',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_299 = Coupling(name = 'R2GC_765_299',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_300 = Coupling(name = 'R2GC_765_300',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_301 = Coupling(name = 'R2GC_765_301',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x1*ON1x4*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON1x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x4*ON4x1*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x1*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x4*ON4x1*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x1*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_302 = Coupling(name = 'R2GC_765_302',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_303 = Coupling(name = 'R2GC_765_303',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_765_304 = Coupling(name = 'R2GC_765_304',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x1*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x1*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_305 = Coupling(name = 'R2GC_766_305',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_306 = Coupling(name = 'R2GC_766_306',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_307 = Coupling(name = 'R2GC_766_307',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_308 = Coupling(name = 'R2GC_766_308',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x2*ON1x4*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON1x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x4*ON4x2*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x2*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x4*ON4x2*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x2*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_309 = Coupling(name = 'R2GC_766_309',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_310 = Coupling(name = 'R2GC_766_310',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_766_311 = Coupling(name = 'R2GC_766_311',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x2*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x2*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_312 = Coupling(name = 'R2GC_767_312',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_313 = Coupling(name = 'R2GC_767_313',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_314 = Coupling(name = 'R2GC_767_314',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_315 = Coupling(name = 'R2GC_767_315',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x3*ON1x4*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON1x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x4*ON4x3*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) + (G**2*ON1x3*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x4*ON4x3*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2) - (G**2*ON1x3*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(64.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_316 = Coupling(name = 'R2GC_767_316',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_317 = Coupling(name = 'R2GC_767_317',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_767_318 = Coupling(name = 'R2GC_767_318',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x3*ON3x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x3*ON4x4*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_319 = Coupling(name = 'R2GC_768_319',
                        value = '-0.0625*(CKML3x3*CKMR3x3*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x3*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_320 = Coupling(name = 'R2GC_768_320',
                        value = '-0.0625*(CKML3x1*CKMR3x1*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x1*CKMR3x1*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_321 = Coupling(name = 'R2GC_768_321',
                        value = '-0.0625*(CKML3x2*CKMR3x2*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x2*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_322 = Coupling(name = 'R2GC_768_322',
                        value = '-0.03125*(complex(0,1)*G**2*ON1x4**2*Yu3x3**2*sec(2*beta)**2)/cmath.pi**2 - (complex(0,1)*G**2*ON3x4**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON4x4**2*Yu3x3**2*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON3x4**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) + (complex(0,1)*G**2*ON4x4**2*Yu3x3**2*cmath.cos(4*beta)*sec(2*beta)**2)/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) + (G**2*ON1x4*ON4x4*Yu3x3**2*cmath.exp(-(alp*complex(0,1)))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*ON3x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2) - (G**2*ON1x4*ON4x4*Yu3x3**2*cmath.exp(alp*complex(0,1))*sec(2*beta)**2*cmath.sin(4*beta))/(32.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_323 = Coupling(name = 'R2GC_768_323',
                        value = '-0.0625*(CKML3x3*CKMR3x1*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x1*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x3*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_324 = Coupling(name = 'R2GC_768_324',
                        value = '-0.0625*(CKML3x3*CKMR3x2*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x3*CKMR3x2*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x3)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x2*CKMR3x3*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x3)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_768_325 = Coupling(name = 'R2GC_768_325',
                        value = '-0.0625*(CKML3x2*CKMR3x1*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/cmath.pi**2 - (CKML3x2*CKMR3x1*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x2)*complexconjugate(CKMR3x1)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON3x4**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2) - (CKML3x1*CKMR3x2*complex(0,1)*G**2*ON4x4**2*Yu3x3**2*complexconjugate(CKML3x1)*complexconjugate(CKMR3x2)*sec(2*beta)**2)/(16.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_788_326 = Coupling(name = 'R2GC_788_326',
                        value = '-0.006944444444444444*(complex(0,1)*G**3*gw*cmath.sin(thetaw))/cmath.pi**2',
                        order = {'QCD':3,'QED':1})

R2GC_788_327 = Coupling(name = 'R2GC_788_327',
                        value = '(complex(0,1)*G**3*gw*cmath.sin(thetaw))/(72.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

R2GC_789_328 = Coupling(name = 'R2GC_789_328',
                        value = '(complex(0,1)*G**2*gw*cmath.sin(thetaw))/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_790_329 = Coupling(name = 'R2GC_790_329',
                        value = '-0.1111111111111111*(complex(0,1)*G**2*gw*cmath.sin(thetaw))/cmath.pi**2',
                        order = {'QCD':2,'QED':1})

R2GC_808_330 = Coupling(name = 'R2GC_808_330',
                        value = '(complex(0,1)*G**2*gw**2*cmath.sin(thetaw)**2)/(216.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_808_331 = Coupling(name = 'R2GC_808_331',
                        value = '(complex(0,1)*G**2*gw**2*cmath.sin(thetaw)**2)/(54.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_332 = Coupling(name = 'R2GC_955_332',
                        value = '(CKML2x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x3)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR2x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x3)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_333 = Coupling(name = 'R2GC_955_333',
                        value = '(CKML3x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x3)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR3x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x3)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_334 = Coupling(name = 'R2GC_955_334',
                        value = '(CKML1x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x3)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR1x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x3)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_335 = Coupling(name = 'R2GC_955_335',
                        value = '(CKML2x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x1)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR2x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x1)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_336 = Coupling(name = 'R2GC_955_336',
                        value = '(CKML2x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x2)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR2x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x2)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_337 = Coupling(name = 'R2GC_955_337',
                        value = '(CKML3x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x1)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR3x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x1)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_338 = Coupling(name = 'R2GC_955_338',
                        value = '(CKML1x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x1)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR1x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x1)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_339 = Coupling(name = 'R2GC_955_339',
                        value = '(CKML3x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x2)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR3x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x2)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_955_340 = Coupling(name = 'R2GC_955_340',
                        value = '(CKML1x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x2)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR1x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x2)*cmath.cos(xi)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_341 = Coupling(name = 'R2GC_956_341',
                        value = '(CKML2x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x3)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR2x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x3)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_342 = Coupling(name = 'R2GC_956_342',
                        value = '(CKML3x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x3)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR3x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x3)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_343 = Coupling(name = 'R2GC_956_343',
                        value = '(CKML1x3*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x3)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR1x3*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x3)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_344 = Coupling(name = 'R2GC_956_344',
                        value = '(CKML2x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x1)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR2x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x1)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_345 = Coupling(name = 'R2GC_956_345',
                        value = '(CKML2x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML2x2)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR2x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR2x2)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_346 = Coupling(name = 'R2GC_956_346',
                        value = '(CKML3x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x1)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR3x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x1)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_347 = Coupling(name = 'R2GC_956_347',
                        value = '(CKML1x1*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x1)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR1x1*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x1)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_348 = Coupling(name = 'R2GC_956_348',
                        value = '(CKML3x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML3x2)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR3x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR3x2)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_956_349 = Coupling(name = 'R2GC_956_349',
                        value = '(CKML1x2*complex(0,1)*G**2*gw**2*complexconjugate(CKML1x2)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2) - (CKMR1x2*complex(0,1)*G**2*gw**2*zetaLR**2*complexconjugate(CKMR1x2)*cmath.cos(xi)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_957_350 = Coupling(name = 'R2GC_957_350',
                        value = '-0.16666666666666666*(CKML2x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_958_351 = Coupling(name = 'R2GC_958_351',
                        value = '(CKMR2x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_959_352 = Coupling(name = 'R2GC_959_352',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML2x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_960_353 = Coupling(name = 'R2GC_960_353',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_961_354 = Coupling(name = 'R2GC_961_354',
                        value = '-0.16666666666666666*(CKML2x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_962_355 = Coupling(name = 'R2GC_962_355',
                        value = '(CKMR2x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_963_356 = Coupling(name = 'R2GC_963_356',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML2x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_964_357 = Coupling(name = 'R2GC_964_357',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_965_358 = Coupling(name = 'R2GC_965_358',
                        value = '-0.16666666666666666*(CKML2x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_966_359 = Coupling(name = 'R2GC_966_359',
                        value = '(CKMR2x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_967_360 = Coupling(name = 'R2GC_967_360',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML2x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_968_361 = Coupling(name = 'R2GC_968_361',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_969_362 = Coupling(name = 'R2GC_969_362',
                        value = '-0.16666666666666666*(CKML1x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_970_363 = Coupling(name = 'R2GC_970_363',
                        value = '(CKMR1x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_971_364 = Coupling(name = 'R2GC_971_364',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML1x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_972_365 = Coupling(name = 'R2GC_972_365',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_973_366 = Coupling(name = 'R2GC_973_366',
                        value = '-0.16666666666666666*(CKML1x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_974_367 = Coupling(name = 'R2GC_974_367',
                        value = '(CKMR1x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_975_368 = Coupling(name = 'R2GC_975_368',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML1x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_976_369 = Coupling(name = 'R2GC_976_369',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_977_370 = Coupling(name = 'R2GC_977_370',
                        value = '-0.16666666666666666*(CKML1x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_978_371 = Coupling(name = 'R2GC_978_371',
                        value = '(CKMR1x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_979_372 = Coupling(name = 'R2GC_979_372',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML1x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_980_373 = Coupling(name = 'R2GC_980_373',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_981_374 = Coupling(name = 'R2GC_981_374',
                        value = '-0.16666666666666666*(CKML3x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_982_375 = Coupling(name = 'R2GC_982_375',
                        value = '-0.16666666666666666*(CKML3x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_983_376 = Coupling(name = 'R2GC_983_376',
                        value = '-0.16666666666666666*(CKML3x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_984_377 = Coupling(name = 'R2GC_984_377',
                        value = '(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_985_378 = Coupling(name = 'R2GC_985_378',
                        value = '(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_986_379 = Coupling(name = 'R2GC_986_379',
                        value = '(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_987_380 = Coupling(name = 'R2GC_987_380',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_988_381 = Coupling(name = 'R2GC_988_381',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_989_382 = Coupling(name = 'R2GC_989_382',
                        value = '-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_990_383 = Coupling(name = 'R2GC_990_383',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_991_384 = Coupling(name = 'R2GC_991_384',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_992_385 = Coupling(name = 'R2GC_992_385',
                        value = '(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_1259_1 = Coupling(name = 'UVGC_1259_1',
                       value = {-1:'(CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/(96.*cmath.pi**2) - (CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1259_2 = Coupling(name = 'UVGC_1259_2',
                       value = {-1:'-0.020833333333333332*(CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/cmath.pi**2 + (CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.125*(CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/cmath.pi**2 + (3*CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(4*beta))/(16.*cmath.pi**2) + (CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1259_3 = Coupling(name = 'UVGC_1259_3',
                       value = {-1:'(CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1260_4 = Coupling(name = 'UVGC_1260_4',
                       value = {-1:'(CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/(96.*cmath.pi**2) - (CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1260_5 = Coupling(name = 'UVGC_1260_5',
                       value = {-1:'-0.020833333333333332*(CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/cmath.pi**2 + (CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.125*(CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/cmath.pi**2 + (3*CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(4*beta))/(16.*cmath.pi**2) + (CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1260_6 = Coupling(name = 'UVGC_1260_6',
                       value = {-1:'(CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1261_7 = Coupling(name = 'UVGC_1261_7',
                       value = {-1:'(CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/(96.*cmath.pi**2) - (CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1261_8 = Coupling(name = 'UVGC_1261_8',
                       value = {-1:'-0.020833333333333332*(CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/cmath.pi**2 + (CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.125*(CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sin(4*beta))/cmath.pi**2 + (3*CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(4*beta))/(16.*cmath.pi**2) + (CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1261_9 = Coupling(name = 'UVGC_1261_9',
                       value = {-1:'(CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(12.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_1262_10 = Coupling(name = 'UVGC_1262_10',
                        value = {-1:'(complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (G**2*ON4x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x1*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1263_11 = Coupling(name = 'UVGC_1263_11',
                        value = {-1:'(complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) + (G**2*ON4x1*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x1*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x1*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1264_12 = Coupling(name = 'UVGC_1264_12',
                        value = {-1:'(complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (G**2*ON4x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x2*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1265_13 = Coupling(name = 'UVGC_1265_13',
                        value = {-1:'(complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) + (G**2*ON4x2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x2*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x2*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1266_14 = Coupling(name = 'UVGC_1266_14',
                        value = {-1:'(complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (G**2*ON4x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x3*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1267_15 = Coupling(name = 'UVGC_1267_15',
                        value = {-1:'(complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) + (G**2*ON4x3*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x3*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x3*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1268_16 = Coupling(name = 'UVGC_1268_16',
                        value = {-1:'(complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (G**2*ON4x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x4*Yu3x3*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1269_17 = Coupling(name = 'UVGC_1269_17',
                        value = {-1:'(complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (G**2*ON4x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) + (G**2*ON4x4*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sqrt(2)*cmath.tan(2*beta))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON3x4*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (G**2*ON4x4*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1270_18 = Coupling(name = 'UVGC_1270_18',
                        value = {-1:'(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/(96.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1270_19 = Coupling(name = 'UVGC_1270_19',
                        value = {-1:'-0.020833333333333332*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/cmath.pi**2 + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.125*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(4*beta))/(16.*cmath.pi**2) + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1270_20 = Coupling(name = 'UVGC_1270_20',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1271_21 = Coupling(name = 'UVGC_1271_21',
                        value = {-1:'(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/(96.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1271_22 = Coupling(name = 'UVGC_1271_22',
                        value = {-1:'-0.020833333333333332*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/cmath.pi**2 + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.125*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(4*beta))/(16.*cmath.pi**2) + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1271_23 = Coupling(name = 'UVGC_1271_23',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1272_24 = Coupling(name = 'UVGC_1272_24',
                        value = {-1:'(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/(96.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1272_25 = Coupling(name = 'UVGC_1272_25',
                        value = {-1:'-0.020833333333333332*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/cmath.pi**2 + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.125*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(4*beta))/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(4*beta))/(16.*cmath.pi**2) + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1272_26 = Coupling(name = 'UVGC_1272_26',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(3.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(4*beta)*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1273_27 = Coupling(name = 'UVGC_1273_27',
                        value = {-1:'(CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(24.*cmath.pi**2) - (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1273_28 = Coupling(name = 'UVGC_1273_28',
                        value = {-1:'-0.08333333333333333*(CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.5*(CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 + (3*CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2) + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(beta)**2*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1273_29 = Coupling(name = 'UVGC_1273_29',
                        value = {-1:'-0.3333333333333333*(CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(2*beta))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_1274_30 = Coupling(name = 'UVGC_1274_30',
                        value = {-1:'(CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(24.*cmath.pi**2) - (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1274_31 = Coupling(name = 'UVGC_1274_31',
                        value = {-1:'-0.08333333333333333*(CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.5*(CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 + (3*CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2) + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(beta)**2*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1274_32 = Coupling(name = 'UVGC_1274_32',
                        value = {-1:'-0.3333333333333333*(CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(2*beta))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_1275_33 = Coupling(name = 'UVGC_1275_33',
                        value = {-1:'(CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/(24.*cmath.pi**2) - (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1275_34 = Coupling(name = 'UVGC_1275_34',
                        value = {-1:'-0.08333333333333333*(CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.5*(CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*cmath.tan(2*beta))/cmath.pi**2 + (3*CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(beta)**2*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(beta)**2*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1275_35 = Coupling(name = 'UVGC_1275_35',
                        value = {-1:'-0.3333333333333333*(CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.exp(alp*complex(0,1))*cmath.sin(2*beta))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_1276_36 = Coupling(name = 'UVGC_1276_36',
                        value = {-1:'(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1276_37 = Coupling(name = 'UVGC_1276_37',
                        value = {-1:'-0.08333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.5*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(beta)**2*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1276_38 = Coupling(name = 'UVGC_1276_38',
                        value = {-1:'-0.3333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(2*beta))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_1277_39 = Coupling(name = 'UVGC_1277_39',
                        value = {-1:'(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1277_40 = Coupling(name = 'UVGC_1277_40',
                        value = {-1:'-0.08333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.5*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(beta)**2*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1277_41 = Coupling(name = 'UVGC_1277_41',
                        value = {-1:'-0.3333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(2*beta))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_1278_42 = Coupling(name = 'UVGC_1278_42',
                        value = {-1:'(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1278_43 = Coupling(name = 'UVGC_1278_43',
                        value = {-1:'-0.08333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(12.*cmath.pi**2)',0:'-0.5*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*cmath.tan(2*beta))/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.tan(2*beta))/(4.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(beta)**2*cmath.tan(2*beta))/(2.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(beta)**2*cmath.tan(2*beta))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_1278_44 = Coupling(name = 'UVGC_1278_44',
                        value = {-1:'-0.3333333333333333*(ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(2*beta))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_1323_45 = Coupling(name = 'UVGC_1323_45',
                        value = {-1:'-0.25*(ep*G**2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) + (ep*G**2*Yu3x3*cmath.tan(thetaw)**2)/(4.*cmath.pi**2*zetaLR**2*cmath.sqrt(2))',0:'-0.3333333333333333*(ep*G**2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2)) + (ep*G**2*Yu3x3*reglog(MT/MU_R))/(2.*cmath.pi**2*cmath.sqrt(2)) + (ep*G**2*Yu3x3*cmath.tan(thetaw)**2)/(3.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep*G**2*Yu3x3*reglog(MT/MU_R)*cmath.tan(thetaw)**2)/(2.*cmath.pi**2*zetaLR**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1499_46 = Coupling(name = 'UVGC_1499_46',
                        value = {-1:'(complex(0,1)*G**2*gw*cmath.cos(thetaw))/(8.*cmath.pi**2) - (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(96.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(96.*cmath.pi**2*zetaLR**4)',0:'(complex(0,1)*G**2*gw*cmath.cos(thetaw))/(6.*cmath.pi**2) - (complex(0,1)*G**2*gw*cmath.cos(thetaw)*reglog(MT/MU_R))/(4.*cmath.pi**2) - (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(18.*cmath.pi**2) + (complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.sin(thetaw)*cmath.tan(thetaw))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(72.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*sec(thetaw)*cmath.tan(thetaw)**2)/(48.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(72.*cmath.pi**2*zetaLR**4) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*sec(thetaw)*cmath.tan(thetaw)**4)/(48.*cmath.pi**2*zetaLR**4)'},
                        order = {'QCD':2,'QED':1})

UVGC_1500_47 = Coupling(name = 'UVGC_1500_47',
                        value = {-1:'(ep**2*complex(0,1)*G**2*gw*sec(thetaw))/(32.*cmath.pi**2) - (complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(6.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(96.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(24.*cmath.pi**2*zetaLR**4)',0:'(ep**2*complex(0,1)*G**2*gw*sec(thetaw))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*sec(thetaw))/(16.*cmath.pi**2) - (2*complex(0,1)*G**2*gw*cmath.sin(thetaw)*cmath.tan(thetaw))/(9.*cmath.pi**2) + (complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.sin(thetaw)*cmath.tan(thetaw))/(3.*cmath.pi**2) - (7*ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**2)/(72.*cmath.pi**2*zetaLR**2) + (7*ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*sec(thetaw)*cmath.tan(thetaw)**2)/(48.*cmath.pi**2*zetaLR**2) + (ep**2*complex(0,1)*G**2*gw*sec(thetaw)*cmath.tan(thetaw)**4)/(18.*cmath.pi**2*zetaLR**4) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*sec(thetaw)*cmath.tan(thetaw)**4)/(12.*cmath.pi**2*zetaLR**4)'},
                        order = {'QCD':2,'QED':1})

UVGC_1528_48 = Coupling(name = 'UVGC_1528_48',
                        value = {-1:'(G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3)/(24.*cmath.pi**2*cmath.sqrt(2)) + (G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(6.*cmath.pi**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(48.*cmath.pi**2*cmath.sqrt(2)) - (G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(48.*cmath.pi**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*cmath.tan(thetaw)**2)/(12.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.tan(thetaw)**2)/(24.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2*cmath.tan(thetaw)**2)/(24.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*cmath.tan(thetaw)**4)/(24.*cmath.pi**2*zetaLR**4*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.tan(thetaw)**4)/(48.*cmath.pi**2*zetaLR**4*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2*cmath.tan(thetaw)**4)/(48.*cmath.pi**2*zetaLR**4*cmath.sqrt(2))',0:'-0.08333333333333333*(ep**2*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (G**2*Yu3x3*cmath.cos(beta)**2*cmath.sqrt(2)*sec(2*beta))/(3.*cmath.pi**2) - (G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(8.*cmath.pi**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (G**2*Yu3x3*cmath.sqrt(2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2) + (G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(cmath.pi**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(8.*cmath.pi**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.tan(thetaw)**2)/(6.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.tan(thetaw)**2)/(4.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2*cmath.tan(thetaw)**2)/(6.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2*cmath.tan(thetaw)**2)/(4.*cmath.pi**2*zetaLR**2*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.tan(thetaw)**4)/(12.*cmath.pi**2*zetaLR**4*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.tan(thetaw)**4)/(8.*cmath.pi**2*zetaLR**4*cmath.sqrt(2)) + (ep**2*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2*cmath.tan(thetaw)**4)/(12.*cmath.pi**2*zetaLR**4*cmath.sqrt(2)) - (ep**2*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2*cmath.tan(thetaw)**4)/(8.*cmath.pi**2*zetaLR**4*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1874_49 = Coupling(name = 'UVGC_1874_49',
                        value = {-1:'-0.03125*(ep**2*complex(0,1)*G**2*gw)/(cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(24.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(96.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(16.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(32.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(48.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(96.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',0:'-0.041666666666666664*(ep**2*complex(0,1)*G**2*gw)/(cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R))/(16.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(18.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(72.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(12.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**2)/(12.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**2)/(48.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**2)/(8.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(24.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(36.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**4)/(16.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**4)/(24.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(72.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**6)/(48.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))'},
                        order = {'QCD':2,'QED':1})

UVGC_1875_50 = Coupling(name = 'UVGC_1875_50',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR**2)/(8.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(6.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(24.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(12.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(24.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))',0:'(complex(0,1)*G**2*gw*zetaLR**2)/(6.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (complex(0,1)*G**2*gw*zetaLR**2*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(9.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**2)/(18.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**2)/(3.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**2)/(12.*cmath.pi**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**4)/(9.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**4)/(6.*cmath.pi**2*zetaLR**2*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) + (ep**2*complex(0,1)*G**2*gw*cmath.tan(thetaw)**6)/(18.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2)) - (ep**2*complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.tan(thetaw)**6)/(12.*cmath.pi**2*zetaLR**4*cmath.sqrt(zetaLR**2 - cmath.tan(thetaw)**2))'},
                        order = {'QCD':2,'QED':1})

UVGC_239_51 = Coupling(name = 'UVGC_239_51',
                       value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_240_52 = Coupling(name = 'UVGC_240_52',
                       value = {-1:'G**3/(32.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_241_53 = Coupling(name = 'UVGC_241_53',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2)/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_246_54 = Coupling(name = 'UVGC_246_54',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_246_55 = Coupling(name = 'UVGC_246_55',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_247_56 = Coupling(name = 'UVGC_247_56',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_247_57 = Coupling(name = 'UVGC_247_57',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_249_58 = Coupling(name = 'UVGC_249_58',
                       value = {-1:'-0.0078125*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_249_59 = Coupling(name = 'UVGC_249_59',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_250_60 = Coupling(name = 'UVGC_250_60',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_250_61 = Coupling(name = 'UVGC_250_61',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_251_62 = Coupling(name = 'UVGC_251_62',
                       value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2'},
                       order = {'QCD':4})

UVGC_252_63 = Coupling(name = 'UVGC_252_63',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_253_64 = Coupling(name = 'UVGC_253_64',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_254_65 = Coupling(name = 'UVGC_254_65',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_254_66 = Coupling(name = 'UVGC_254_66',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_254_67 = Coupling(name = 'UVGC_254_67',
                       value = {-1:'-0.0078125*(complex(0,1)*G**3)/cmath.pi**2'},
                       order = {'QCD':3})

UVGC_254_68 = Coupling(name = 'UVGC_254_68',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_255_69 = Coupling(name = 'UVGC_255_69',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_269_70 = Coupling(name = 'UVGC_269_70',
                       value = {-1:'(CKML2x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_269_71 = Coupling(name = 'UVGC_269_71',
                       value = {-1:'-0.08333333333333333*(CKML2x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_270_72 = Coupling(name = 'UVGC_270_72',
                       value = {-1:'(CKMR2x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_270_73 = Coupling(name = 'UVGC_270_73',
                       value = {-1:'-0.08333333333333333*(CKMR2x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_271_74 = Coupling(name = 'UVGC_271_74',
                       value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML2x3)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_271_75 = Coupling(name = 'UVGC_271_75',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML2x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_272_76 = Coupling(name = 'UVGC_272_76',
                       value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x3)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_272_77 = Coupling(name = 'UVGC_272_77',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_273_78 = Coupling(name = 'UVGC_273_78',
                       value = {-1:'(CKML2x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_273_79 = Coupling(name = 'UVGC_273_79',
                       value = {-1:'-0.08333333333333333*(CKML2x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_274_80 = Coupling(name = 'UVGC_274_80',
                       value = {-1:'(CKMR2x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_274_81 = Coupling(name = 'UVGC_274_81',
                       value = {-1:'-0.08333333333333333*(CKMR2x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_275_82 = Coupling(name = 'UVGC_275_82',
                       value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML2x1)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_275_83 = Coupling(name = 'UVGC_275_83',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML2x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_276_84 = Coupling(name = 'UVGC_276_84',
                       value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x1)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_276_85 = Coupling(name = 'UVGC_276_85',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_277_86 = Coupling(name = 'UVGC_277_86',
                       value = {-1:'(CKML2x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_277_87 = Coupling(name = 'UVGC_277_87',
                       value = {-1:'-0.08333333333333333*(CKML2x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_278_88 = Coupling(name = 'UVGC_278_88',
                       value = {-1:'(CKMR2x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_278_89 = Coupling(name = 'UVGC_278_89',
                       value = {-1:'-0.08333333333333333*(CKMR2x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_279_90 = Coupling(name = 'UVGC_279_90',
                       value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML2x2)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_279_91 = Coupling(name = 'UVGC_279_91',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML2x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_280_92 = Coupling(name = 'UVGC_280_92',
                       value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x2)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_280_93 = Coupling(name = 'UVGC_280_93',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_281_94 = Coupling(name = 'UVGC_281_94',
                       value = {-1:'(CKML1x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_281_95 = Coupling(name = 'UVGC_281_95',
                       value = {-1:'-0.08333333333333333*(CKML1x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_282_96 = Coupling(name = 'UVGC_282_96',
                       value = {-1:'(CKMR1x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_282_97 = Coupling(name = 'UVGC_282_97',
                       value = {-1:'-0.08333333333333333*(CKMR1x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_283_98 = Coupling(name = 'UVGC_283_98',
                       value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML1x3)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_283_99 = Coupling(name = 'UVGC_283_99',
                       value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML1x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_284_100 = Coupling(name = 'UVGC_284_100',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x3)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_284_101 = Coupling(name = 'UVGC_284_101',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_285_102 = Coupling(name = 'UVGC_285_102',
                        value = {-1:'(CKML1x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_285_103 = Coupling(name = 'UVGC_285_103',
                        value = {-1:'-0.08333333333333333*(CKML1x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_286_104 = Coupling(name = 'UVGC_286_104',
                        value = {-1:'(CKMR1x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_286_105 = Coupling(name = 'UVGC_286_105',
                        value = {-1:'-0.08333333333333333*(CKMR1x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_287_106 = Coupling(name = 'UVGC_287_106',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML1x1)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_287_107 = Coupling(name = 'UVGC_287_107',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML1x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_288_108 = Coupling(name = 'UVGC_288_108',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x1)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_288_109 = Coupling(name = 'UVGC_288_109',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_289_110 = Coupling(name = 'UVGC_289_110',
                        value = {-1:'(CKML1x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_289_111 = Coupling(name = 'UVGC_289_111',
                        value = {-1:'-0.08333333333333333*(CKML1x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_290_112 = Coupling(name = 'UVGC_290_112',
                        value = {-1:'(CKMR1x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_290_113 = Coupling(name = 'UVGC_290_113',
                        value = {-1:'-0.08333333333333333*(CKMR1x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_291_114 = Coupling(name = 'UVGC_291_114',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML1x2)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_291_115 = Coupling(name = 'UVGC_291_115',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML1x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_292_116 = Coupling(name = 'UVGC_292_116',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x2)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_292_117 = Coupling(name = 'UVGC_292_117',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_293_118 = Coupling(name = 'UVGC_293_118',
                        value = {-1:'(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**2*reglog(MT/MU_R))/cmath.pi**2'},
                        order = {'QCD':2})

UVGC_294_119 = Coupling(name = 'UVGC_294_119',
                        value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_294_120 = Coupling(name = 'UVGC_294_120',
                        value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_294_121 = Coupling(name = 'UVGC_294_121',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2)/cmath.pi**2',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_295_122 = Coupling(name = 'UVGC_295_122',
                        value = {-1:'-0.020833333333333332*G**3/cmath.pi**2'},
                        order = {'QCD':3})

UVGC_295_123 = Coupling(name = 'UVGC_295_123',
                        value = {-1:'G**3/(24.*cmath.pi**2)',0:'-0.08333333333333333*(G**3*reglog(MT/MU_R))/cmath.pi**2'},
                        order = {'QCD':3})

UVGC_296_124 = Coupling(name = 'UVGC_296_124',
                        value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_296_125 = Coupling(name = 'UVGC_296_125',
                        value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_296_126 = Coupling(name = 'UVGC_296_126',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2'},
                        order = {'QCD':4})

UVGC_297_127 = Coupling(name = 'UVGC_297_127',
                        value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_297_128 = Coupling(name = 'UVGC_297_128',
                        value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_298_129 = Coupling(name = 'UVGC_298_129',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**4)/cmath.pi**2'},
                        order = {'QCD':4})

UVGC_298_130 = Coupling(name = 'UVGC_298_130',
                        value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_298_131 = Coupling(name = 'UVGC_298_131',
                        value = {0:'-0.08333333333333333*(complex(0,1)*G**4*reglog(MT/MU_R))/cmath.pi**2'},
                        order = {'QCD':4})

UVGC_299_132 = Coupling(name = 'UVGC_299_132',
                        value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_299_133 = Coupling(name = 'UVGC_299_133',
                        value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_299_134 = Coupling(name = 'UVGC_299_134',
                        value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_299_135 = Coupling(name = 'UVGC_299_135',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**4)/cmath.pi**2',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_136 = Coupling(name = 'UVGC_300_136',
                        value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_300_137 = Coupling(name = 'UVGC_300_137',
                        value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_138 = Coupling(name = 'UVGC_301_138',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_139 = Coupling(name = 'UVGC_301_139',
                        value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_301_140 = Coupling(name = 'UVGC_301_140',
                        value = {0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_302_141 = Coupling(name = 'UVGC_302_141',
                        value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_303_142 = Coupling(name = 'UVGC_303_142',
                        value = {-1:'-0.16666666666666666*(complex(0,1)*G**3)/cmath.pi**2',0:'-0.3333333333333333*(complex(0,1)*G**3)/cmath.pi**2 + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_304_143 = Coupling(name = 'UVGC_304_143',
                        value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                        order = {'QCD':2})

UVGC_305_144 = Coupling(name = 'UVGC_305_144',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*ep*complex(0,1)*G**2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_305_145 = Coupling(name = 'UVGC_305_145',
                        value = {-1:'(CKMR3x1*ep*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(CKMR3x1*ep*complex(0,1)*G**2*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2)) - (3*CKMR3x1*ep*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_305_146 = Coupling(name = 'UVGC_305_146',
                        value = {-1:'(CKMR3x1*ep*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_306_147 = Coupling(name = 'UVGC_306_147',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*ep*complex(0,1)*G**2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_306_148 = Coupling(name = 'UVGC_306_148',
                        value = {-1:'(CKMR3x2*ep*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(CKMR3x2*ep*complex(0,1)*G**2*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2)) - (3*CKMR3x2*ep*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_306_149 = Coupling(name = 'UVGC_306_149',
                        value = {-1:'(CKMR3x2*ep*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_307_150 = Coupling(name = 'UVGC_307_150',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*ep*complex(0,1)*G**2*Yu3x3)/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_307_151 = Coupling(name = 'UVGC_307_151',
                        value = {-1:'(CKMR3x3*ep*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(CKMR3x3*ep*complex(0,1)*G**2*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2)) - (3*CKMR3x3*ep*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_307_152 = Coupling(name = 'UVGC_307_152',
                        value = {-1:'(CKMR3x3*ep*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_308_153 = Coupling(name = 'UVGC_308_153',
                        value = {-1:'(complex(0,1)*G**2*ON1x1*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON1x1*Yu3x3*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON1x1*Yu3x3*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_309_154 = Coupling(name = 'UVGC_309_154',
                        value = {-1:'(complex(0,1)*G**2*ON1x2*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON1x2*Yu3x3*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON1x2*Yu3x3*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_310_155 = Coupling(name = 'UVGC_310_155',
                        value = {-1:'(complex(0,1)*G**2*ON1x3*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON1x3*Yu3x3*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON1x3*Yu3x3*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_311_156 = Coupling(name = 'UVGC_311_156',
                        value = {-1:'(complex(0,1)*G**2*ON1x4*Yu3x3)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*ON1x4*Yu3x3*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*ON1x4*Yu3x3*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_312_157 = Coupling(name = 'UVGC_312_157',
                        value = {-1:'-0.041666666666666664*(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_312_158 = Coupling(name = 'UVGC_312_158',
                        value = {-1:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1))/(2.*cmath.pi**2*cmath.sqrt(2)) - (3*ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_312_159 = Coupling(name = 'UVGC_312_159',
                        value = {-1:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_313_160 = Coupling(name = 'UVGC_313_160',
                        value = {-1:'-0.041666666666666664*(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_313_161 = Coupling(name = 'UVGC_313_161',
                        value = {-1:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2))/(2.*cmath.pi**2*cmath.sqrt(2)) - (3*ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_313_162 = Coupling(name = 'UVGC_313_162',
                        value = {-1:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_314_163 = Coupling(name = 'UVGC_314_163',
                        value = {-1:'-0.041666666666666664*(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_314_164 = Coupling(name = 'UVGC_314_164',
                        value = {-1:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3))/(2.*cmath.pi**2*cmath.sqrt(2)) - (3*ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_314_165 = Coupling(name = 'UVGC_314_165',
                        value = {-1:'(ep*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_315_166 = Coupling(name = 'UVGC_315_166',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_315_167 = Coupling(name = 'UVGC_315_167',
                        value = {-1:'-0.08333333333333333*(CKML3x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKML3x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*gw*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_315_168 = Coupling(name = 'UVGC_315_168',
                        value = {-1:'-0.08333333333333333*(CKML3x1*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_316_169 = Coupling(name = 'UVGC_316_169',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_316_170 = Coupling(name = 'UVGC_316_170',
                        value = {-1:'-0.08333333333333333*(CKML3x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKML3x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*gw*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_316_171 = Coupling(name = 'UVGC_316_171',
                        value = {-1:'-0.08333333333333333*(CKML3x2*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_317_172 = Coupling(name = 'UVGC_317_172',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_317_173 = Coupling(name = 'UVGC_317_173',
                        value = {-1:'-0.08333333333333333*(CKML3x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKML3x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*gw*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_317_174 = Coupling(name = 'UVGC_317_174',
                        value = {-1:'-0.08333333333333333*(CKML3x3*complex(0,1)*G**2*gw*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_318_175 = Coupling(name = 'UVGC_318_175',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_318_176 = Coupling(name = 'UVGC_318_176',
                        value = {-1:'-0.08333333333333333*(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_318_177 = Coupling(name = 'UVGC_318_177',
                        value = {-1:'-0.08333333333333333*(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_319_178 = Coupling(name = 'UVGC_319_178',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_319_179 = Coupling(name = 'UVGC_319_179',
                        value = {-1:'-0.08333333333333333*(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_319_180 = Coupling(name = 'UVGC_319_180',
                        value = {-1:'-0.08333333333333333*(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_320_181 = Coupling(name = 'UVGC_320_181',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_320_182 = Coupling(name = 'UVGC_320_182',
                        value = {-1:'-0.08333333333333333*(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_320_183 = Coupling(name = 'UVGC_320_183',
                        value = {-1:'-0.08333333333333333*(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_321_184 = Coupling(name = 'UVGC_321_184',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_321_185 = Coupling(name = 'UVGC_321_185',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_321_186 = Coupling(name = 'UVGC_321_186',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_322_187 = Coupling(name = 'UVGC_322_187',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_322_188 = Coupling(name = 'UVGC_322_188',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_322_189 = Coupling(name = 'UVGC_322_189',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_323_190 = Coupling(name = 'UVGC_323_190',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_323_191 = Coupling(name = 'UVGC_323_191',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_323_192 = Coupling(name = 'UVGC_323_192',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_324_193 = Coupling(name = 'UVGC_324_193',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_324_194 = Coupling(name = 'UVGC_324_194',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_324_195 = Coupling(name = 'UVGC_324_195',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_325_196 = Coupling(name = 'UVGC_325_196',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_325_197 = Coupling(name = 'UVGC_325_197',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_325_198 = Coupling(name = 'UVGC_325_198',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_326_199 = Coupling(name = 'UVGC_326_199',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.cos(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_326_200 = Coupling(name = 'UVGC_326_200',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.cos(xi)*reglog(MT/MU_R))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_326_201 = Coupling(name = 'UVGC_326_201',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.cos(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_328_202 = Coupling(name = 'UVGC_328_202',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(24.*cmath.pi**2) - (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(96.*cmath.pi**2) - (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(96.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_328_203 = Coupling(name = 'UVGC_328_203',
                        value = {-1:'-0.08333333333333333*(CKMR3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(48.*cmath.pi**2) + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(48.*cmath.pi**2)',0:'-0.5*(CKMR3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(8.*cmath.pi**2) + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(8.*cmath.pi**2) + (3*CKMR3x1*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (3*CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2) - (3*CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_328_204 = Coupling(name = 'UVGC_328_204',
                        value = {-1:'-0.3333333333333333*(CKMR3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(12.*cmath.pi**2) + (CKMR3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_329_205 = Coupling(name = 'UVGC_329_205',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(24.*cmath.pi**2) - (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(96.*cmath.pi**2) - (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(96.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_329_206 = Coupling(name = 'UVGC_329_206',
                        value = {-1:'-0.08333333333333333*(CKMR3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(48.*cmath.pi**2) + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(48.*cmath.pi**2)',0:'-0.5*(CKMR3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(8.*cmath.pi**2) + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(8.*cmath.pi**2) + (3*CKMR3x2*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (3*CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2) - (3*CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_329_207 = Coupling(name = 'UVGC_329_207',
                        value = {-1:'-0.3333333333333333*(CKMR3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(12.*cmath.pi**2) + (CKMR3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_330_208 = Coupling(name = 'UVGC_330_208',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(24.*cmath.pi**2) - (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(96.*cmath.pi**2) - (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(96.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_330_209 = Coupling(name = 'UVGC_330_209',
                        value = {-1:'-0.08333333333333333*(CKMR3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(48.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(48.*cmath.pi**2)',0:'-0.5*(CKMR3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(8.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(8.*cmath.pi**2) + (3*CKMR3x3*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (3*CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2) - (3*CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_330_210 = Coupling(name = 'UVGC_330_210',
                        value = {-1:'-0.3333333333333333*(CKMR3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta))/cmath.pi**2 + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*sec(2*beta))/(12.*cmath.pi**2) + (CKMR3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_331_211 = Coupling(name = 'UVGC_331_211',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(96.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*sec(2*beta))/(96.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_331_212 = Coupling(name = 'UVGC_331_212',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(48.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*sec(2*beta))/(48.*cmath.pi**2)',0:'-0.5*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(8.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*sec(2*beta))/(8.*cmath.pi**2) + (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_331_213 = Coupling(name = 'UVGC_331_213',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_332_214 = Coupling(name = 'UVGC_332_214',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(96.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*sec(2*beta))/(96.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_332_215 = Coupling(name = 'UVGC_332_215',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(48.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*sec(2*beta))/(48.*cmath.pi**2)',0:'-0.5*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(8.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*sec(2*beta))/(8.*cmath.pi**2) + (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_332_216 = Coupling(name = 'UVGC_332_216',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_333_217 = Coupling(name = 'UVGC_333_217',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(96.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*sec(2*beta))/(96.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_333_218 = Coupling(name = 'UVGC_333_218',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(48.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*sec(2*beta))/(48.*cmath.pi**2)',0:'-0.5*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(8.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*sec(2*beta))/(8.*cmath.pi**2) + (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*reglog(MT/MU_R)*sec(2*beta))/(16.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_333_219 = Coupling(name = 'UVGC_333_219',
                        value = {-1:'-0.3333333333333333*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/cmath.pi**2 + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(4*beta)*sec(2*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_567_220 = Coupling(name = 'UVGC_567_220',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_567_221 = Coupling(name = 'UVGC_567_221',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_568_222 = Coupling(name = 'UVGC_568_222',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_568_223 = Coupling(name = 'UVGC_568_223',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_568_224 = Coupling(name = 'UVGC_568_224',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_569_225 = Coupling(name = 'UVGC_569_225',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_569_226 = Coupling(name = 'UVGC_569_226',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_569_227 = Coupling(name = 'UVGC_569_227',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_570_228 = Coupling(name = 'UVGC_570_228',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_570_229 = Coupling(name = 'UVGC_570_229',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_571_230 = Coupling(name = 'UVGC_571_230',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_571_231 = Coupling(name = 'UVGC_571_231',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_571_232 = Coupling(name = 'UVGC_571_232',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_572_233 = Coupling(name = 'UVGC_572_233',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_572_234 = Coupling(name = 'UVGC_572_234',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_572_235 = Coupling(name = 'UVGC_572_235',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_573_236 = Coupling(name = 'UVGC_573_236',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_573_237 = Coupling(name = 'UVGC_573_237',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_574_238 = Coupling(name = 'UVGC_574_238',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_574_239 = Coupling(name = 'UVGC_574_239',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_574_240 = Coupling(name = 'UVGC_574_240',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_575_241 = Coupling(name = 'UVGC_575_241',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_575_242 = Coupling(name = 'UVGC_575_242',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_575_243 = Coupling(name = 'UVGC_575_243',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_576_244 = Coupling(name = 'UVGC_576_244',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_576_245 = Coupling(name = 'UVGC_576_245',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_577_246 = Coupling(name = 'UVGC_577_246',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_577_247 = Coupling(name = 'UVGC_577_247',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_577_248 = Coupling(name = 'UVGC_577_248',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_578_249 = Coupling(name = 'UVGC_578_249',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_578_250 = Coupling(name = 'UVGC_578_250',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_578_251 = Coupling(name = 'UVGC_578_251',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_579_252 = Coupling(name = 'UVGC_579_252',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_579_253 = Coupling(name = 'UVGC_579_253',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_579_254 = Coupling(name = 'UVGC_579_254',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_580_255 = Coupling(name = 'UVGC_580_255',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_580_256 = Coupling(name = 'UVGC_580_256',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_581_257 = Coupling(name = 'UVGC_581_257',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_581_258 = Coupling(name = 'UVGC_581_258',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_581_259 = Coupling(name = 'UVGC_581_259',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_582_260 = Coupling(name = 'UVGC_582_260',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_582_261 = Coupling(name = 'UVGC_582_261',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_582_262 = Coupling(name = 'UVGC_582_262',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_583_263 = Coupling(name = 'UVGC_583_263',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_583_264 = Coupling(name = 'UVGC_583_264',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_584_265 = Coupling(name = 'UVGC_584_265',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_584_266 = Coupling(name = 'UVGC_584_266',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_584_267 = Coupling(name = 'UVGC_584_267',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_585_268 = Coupling(name = 'UVGC_585_268',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_585_269 = Coupling(name = 'UVGC_585_269',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_585_270 = Coupling(name = 'UVGC_585_270',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_586_271 = Coupling(name = 'UVGC_586_271',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_586_272 = Coupling(name = 'UVGC_586_272',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_587_273 = Coupling(name = 'UVGC_587_273',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_587_274 = Coupling(name = 'UVGC_587_274',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_587_275 = Coupling(name = 'UVGC_587_275',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_588_276 = Coupling(name = 'UVGC_588_276',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_588_277 = Coupling(name = 'UVGC_588_277',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_588_278 = Coupling(name = 'UVGC_588_278',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_589_279 = Coupling(name = 'UVGC_589_279',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_589_280 = Coupling(name = 'UVGC_589_280',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_590_281 = Coupling(name = 'UVGC_590_281',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_590_282 = Coupling(name = 'UVGC_590_282',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_590_283 = Coupling(name = 'UVGC_590_283',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_591_284 = Coupling(name = 'UVGC_591_284',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_591_285 = Coupling(name = 'UVGC_591_285',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_591_286 = Coupling(name = 'UVGC_591_286',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_592_287 = Coupling(name = 'UVGC_592_287',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_592_288 = Coupling(name = 'UVGC_592_288',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_592_289 = Coupling(name = 'UVGC_592_289',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_593_290 = Coupling(name = 'UVGC_593_290',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_593_291 = Coupling(name = 'UVGC_593_291',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_594_292 = Coupling(name = 'UVGC_594_292',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_594_293 = Coupling(name = 'UVGC_594_293',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_594_294 = Coupling(name = 'UVGC_594_294',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_595_295 = Coupling(name = 'UVGC_595_295',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_595_296 = Coupling(name = 'UVGC_595_296',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_595_297 = Coupling(name = 'UVGC_595_297',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_596_298 = Coupling(name = 'UVGC_596_298',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_596_299 = Coupling(name = 'UVGC_596_299',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_597_300 = Coupling(name = 'UVGC_597_300',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_597_301 = Coupling(name = 'UVGC_597_301',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_597_302 = Coupling(name = 'UVGC_597_302',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_598_303 = Coupling(name = 'UVGC_598_303',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_598_304 = Coupling(name = 'UVGC_598_304',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_598_305 = Coupling(name = 'UVGC_598_305',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_599_306 = Coupling(name = 'UVGC_599_306',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_599_307 = Coupling(name = 'UVGC_599_307',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_600_308 = Coupling(name = 'UVGC_600_308',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_600_309 = Coupling(name = 'UVGC_600_309',
                        value = {0:'(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_600_310 = Coupling(name = 'UVGC_600_310',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_601_311 = Coupling(name = 'UVGC_601_311',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_601_312 = Coupling(name = 'UVGC_601_312',
                        value = {0:'(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_601_313 = Coupling(name = 'UVGC_601_313',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_602_314 = Coupling(name = 'UVGC_602_314',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_602_315 = Coupling(name = 'UVGC_602_315',
                        value = {0:'(CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKMR3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_603_316 = Coupling(name = 'UVGC_603_316',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_603_317 = Coupling(name = 'UVGC_603_317',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_604_318 = Coupling(name = 'UVGC_604_318',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_604_319 = Coupling(name = 'UVGC_604_319',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_604_320 = Coupling(name = 'UVGC_604_320',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_605_321 = Coupling(name = 'UVGC_605_321',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_605_322 = Coupling(name = 'UVGC_605_322',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_605_323 = Coupling(name = 'UVGC_605_323',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_606_324 = Coupling(name = 'UVGC_606_324',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_606_325 = Coupling(name = 'UVGC_606_325',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_607_326 = Coupling(name = 'UVGC_607_326',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_607_327 = Coupling(name = 'UVGC_607_327',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_607_328 = Coupling(name = 'UVGC_607_328',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_608_329 = Coupling(name = 'UVGC_608_329',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_608_330 = Coupling(name = 'UVGC_608_330',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_608_331 = Coupling(name = 'UVGC_608_331',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_609_332 = Coupling(name = 'UVGC_609_332',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_609_333 = Coupling(name = 'UVGC_609_333',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_610_334 = Coupling(name = 'UVGC_610_334',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_610_335 = Coupling(name = 'UVGC_610_335',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_610_336 = Coupling(name = 'UVGC_610_336',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_611_337 = Coupling(name = 'UVGC_611_337',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_611_338 = Coupling(name = 'UVGC_611_338',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_611_339 = Coupling(name = 'UVGC_611_339',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_612_340 = Coupling(name = 'UVGC_612_340',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_612_341 = Coupling(name = 'UVGC_612_341',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_613_342 = Coupling(name = 'UVGC_613_342',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_613_343 = Coupling(name = 'UVGC_613_343',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_613_344 = Coupling(name = 'UVGC_613_344',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_614_345 = Coupling(name = 'UVGC_614_345',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_614_346 = Coupling(name = 'UVGC_614_346',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_614_347 = Coupling(name = 'UVGC_614_347',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x1)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_615_348 = Coupling(name = 'UVGC_615_348',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_615_349 = Coupling(name = 'UVGC_615_349',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_615_350 = Coupling(name = 'UVGC_615_350',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_616_351 = Coupling(name = 'UVGC_616_351',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_616_352 = Coupling(name = 'UVGC_616_352',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_617_353 = Coupling(name = 'UVGC_617_353',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_617_354 = Coupling(name = 'UVGC_617_354',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_617_355 = Coupling(name = 'UVGC_617_355',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_618_356 = Coupling(name = 'UVGC_618_356',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_618_357 = Coupling(name = 'UVGC_618_357',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_618_358 = Coupling(name = 'UVGC_618_358',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_619_359 = Coupling(name = 'UVGC_619_359',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_619_360 = Coupling(name = 'UVGC_619_360',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_620_361 = Coupling(name = 'UVGC_620_361',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_620_362 = Coupling(name = 'UVGC_620_362',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_620_363 = Coupling(name = 'UVGC_620_363',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_621_364 = Coupling(name = 'UVGC_621_364',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_621_365 = Coupling(name = 'UVGC_621_365',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_621_366 = Coupling(name = 'UVGC_621_366',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_622_367 = Coupling(name = 'UVGC_622_367',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_622_368 = Coupling(name = 'UVGC_622_368',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_623_369 = Coupling(name = 'UVGC_623_369',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_623_370 = Coupling(name = 'UVGC_623_370',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_623_371 = Coupling(name = 'UVGC_623_371',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_624_372 = Coupling(name = 'UVGC_624_372',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_624_373 = Coupling(name = 'UVGC_624_373',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_624_374 = Coupling(name = 'UVGC_624_374',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_625_375 = Coupling(name = 'UVGC_625_375',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_625_376 = Coupling(name = 'UVGC_625_376',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_626_377 = Coupling(name = 'UVGC_626_377',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_626_378 = Coupling(name = 'UVGC_626_378',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_626_379 = Coupling(name = 'UVGC_626_379',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x2)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_627_380 = Coupling(name = 'UVGC_627_380',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_627_381 = Coupling(name = 'UVGC_627_381',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_627_382 = Coupling(name = 'UVGC_627_382',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_628_383 = Coupling(name = 'UVGC_628_383',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_628_384 = Coupling(name = 'UVGC_628_384',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_628_385 = Coupling(name = 'UVGC_628_385',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_629_386 = Coupling(name = 'UVGC_629_386',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_629_387 = Coupling(name = 'UVGC_629_387',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x1*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x1*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_630_388 = Coupling(name = 'UVGC_630_388',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_630_389 = Coupling(name = 'UVGC_630_389',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_630_390 = Coupling(name = 'UVGC_630_390',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_631_391 = Coupling(name = 'UVGC_631_391',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_631_392 = Coupling(name = 'UVGC_631_392',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_631_393 = Coupling(name = 'UVGC_631_393',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_632_394 = Coupling(name = 'UVGC_632_394',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_632_395 = Coupling(name = 'UVGC_632_395',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x2*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_633_396 = Coupling(name = 'UVGC_633_396',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_633_397 = Coupling(name = 'UVGC_633_397',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_633_398 = Coupling(name = 'UVGC_633_398',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_634_399 = Coupling(name = 'UVGC_634_399',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_634_400 = Coupling(name = 'UVGC_634_400',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_634_401 = Coupling(name = 'UVGC_634_401',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_635_402 = Coupling(name = 'UVGC_635_402',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_635_403 = Coupling(name = 'UVGC_635_403',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x3*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x3*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_636_404 = Coupling(name = 'UVGC_636_404',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_636_405 = Coupling(name = 'UVGC_636_405',
                        value = {0:'(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_636_406 = Coupling(name = 'UVGC_636_406',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_637_407 = Coupling(name = 'UVGC_637_407',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_637_408 = Coupling(name = 'UVGC_637_408',
                        value = {0:'(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_637_409 = Coupling(name = 'UVGC_637_409',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_638_410 = Coupling(name = 'UVGC_638_410',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_638_411 = Coupling(name = 'UVGC_638_411',
                        value = {0:'(CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*sec(2*beta))/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(2.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*sec(2*beta)*cmath.sin(beta)**2)/(3.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*complex(0,1)*G**2*ON3x4*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2)) - (CKML3x3*G**2*ON4x4*Yu3x3*complexconjugate(CKMR3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_691_412 = Coupling(name = 'UVGC_691_412',
                        value = {-1:'-0.041666666666666664*(CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/cmath.pi**2 + (CKML3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_691_413 = Coupling(name = 'UVGC_691_413',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2) - (CKML3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(6.*cmath.pi**2)',0:'(CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(2.*cmath.pi**2) - (3*CKML3x1*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (CKML3x1*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/cmath.pi**2 + (3*CKML3x1*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(4.*cmath.pi**2) - (3*CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/cmath.pi**2 + (3*CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**4)/(2.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_691_414 = Coupling(name = 'UVGC_691_414',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2) + (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2) - (CKML3x1*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_692_415 = Coupling(name = 'UVGC_692_415',
                        value = {-1:'-0.041666666666666664*(CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/cmath.pi**2 + (CKML3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_692_416 = Coupling(name = 'UVGC_692_416',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2) - (CKML3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(6.*cmath.pi**2)',0:'(CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(2.*cmath.pi**2) - (3*CKML3x2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (CKML3x2*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/cmath.pi**2 + (3*CKML3x2*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(4.*cmath.pi**2) - (3*CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/cmath.pi**2 + (3*CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**4)/(2.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_692_417 = Coupling(name = 'UVGC_692_417',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2) + (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2) - (CKML3x2*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_693_418 = Coupling(name = 'UVGC_693_418',
                        value = {-1:'-0.041666666666666664*(CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/cmath.pi**2 + (CKML3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_693_419 = Coupling(name = 'UVGC_693_419',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2) - (CKML3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(6.*cmath.pi**2)',0:'(CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta))/(2.*cmath.pi**2) - (3*CKML3x3*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (CKML3x3*complex(0,1)*G**2*Yu3x3*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/cmath.pi**2 + (3*CKML3x3*complex(0,1)*G**2*Yu3x3*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(4.*cmath.pi**2) - (3*CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**4*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/cmath.pi**2 + (3*CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**4)/(2.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_693_420 = Coupling(name = 'UVGC_693_420',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*Yu3x3)/(3.*cmath.pi**2) + (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3)/(12.*cmath.pi**2) - (CKML3x3*ep**2*complex(0,1)*G**2*Yu3x3*cmath.cos(4*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_694_421 = Coupling(name = 'UVGC_694_421',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/cmath.pi**2 + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_694_422 = Coupling(name = 'UVGC_694_422',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(6.*cmath.pi**2)',0:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta))/(2.*cmath.pi**2) - (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/cmath.pi**2 + (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(4.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**4*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**4)/(2.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_694_423 = Coupling(name = 'UVGC_694_423',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1))/(3.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x1)*cmath.cos(4*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_695_424 = Coupling(name = 'UVGC_695_424',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/cmath.pi**2 + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_695_425 = Coupling(name = 'UVGC_695_425',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(6.*cmath.pi**2)',0:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta))/(2.*cmath.pi**2) - (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/cmath.pi**2 + (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(4.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**4*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**4)/(2.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_695_426 = Coupling(name = 'UVGC_695_426',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2))/(3.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x2)*cmath.cos(4*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_696_427 = Coupling(name = 'UVGC_696_427',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/cmath.pi**2 + (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(24.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_696_428 = Coupling(name = 'UVGC_696_428',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(12.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(12.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/(6.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/(6.*cmath.pi**2)',0:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta))/(2.*cmath.pi**2) - (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta))/(4.*cmath.pi**2) - (complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**4*sec(2*beta)*cmath.sin(beta)**2)/cmath.pi**2 + (3*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(4.*cmath.pi**2) - (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**4*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**2)/(2.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*sec(2*beta)*cmath.sin(beta)**4)/cmath.pi**2 + (3*ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(beta)**2*reglog(MT/MU_R)*sec(2*beta)*cmath.sin(beta)**4)/(2.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_696_429 = Coupling(name = 'UVGC_696_429',
                        value = {-1:'(complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3))/(3.*cmath.pi**2) + (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3))/(12.*cmath.pi**2) - (ep**2*complex(0,1)*G**2*Yu3x3*complexconjugate(CKML3x3)*cmath.cos(4*beta))/(12.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_778_430 = Coupling(name = 'UVGC_778_430',
                        value = {-1:'-0.027777777777777776*(complex(0,1)*G**2*gw*cmath.sin(thetaw))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_779_431 = Coupling(name = 'UVGC_779_431',
                        value = {-1:'(complex(0,1)*G**2*gw*cmath.sin(thetaw))/(18.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_789_432 = Coupling(name = 'UVGC_789_432',
                        value = {-1:'(complex(0,1)*G**2*gw*cmath.sin(thetaw))/(36.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_790_433 = Coupling(name = 'UVGC_790_433',
                        value = {-1:'-0.05555555555555555*(complex(0,1)*G**2*gw*cmath.sin(thetaw))/cmath.pi**2'},
                        order = {'QCD':2,'QED':1})

UVGC_795_434 = Coupling(name = 'UVGC_795_434',
                        value = {-1:'-0.1111111111111111*(complex(0,1)*G**2*gw*cmath.sin(thetaw))/cmath.pi**2',0:'(-2*complex(0,1)*G**2*gw*cmath.sin(thetaw))/(9.*cmath.pi**2) + (complex(0,1)*G**2*gw*reglog(MT/MU_R)*cmath.sin(thetaw))/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_957_435 = Coupling(name = 'UVGC_957_435',
                        value = {-1:'(CKML2x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_957_436 = Coupling(name = 'UVGC_957_436',
                        value = {-1:'-0.08333333333333333*(CKML2x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_958_437 = Coupling(name = 'UVGC_958_437',
                        value = {-1:'-0.041666666666666664*(CKMR2x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_958_438 = Coupling(name = 'UVGC_958_438',
                        value = {-1:'(CKMR2x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_959_439 = Coupling(name = 'UVGC_959_439',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML2x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_959_440 = Coupling(name = 'UVGC_959_440',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML2x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_960_441 = Coupling(name = 'UVGC_960_441',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_960_442 = Coupling(name = 'UVGC_960_442',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_961_443 = Coupling(name = 'UVGC_961_443',
                        value = {-1:'(CKML2x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_961_444 = Coupling(name = 'UVGC_961_444',
                        value = {-1:'-0.08333333333333333*(CKML2x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_962_445 = Coupling(name = 'UVGC_962_445',
                        value = {-1:'-0.041666666666666664*(CKMR2x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_962_446 = Coupling(name = 'UVGC_962_446',
                        value = {-1:'(CKMR2x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_963_447 = Coupling(name = 'UVGC_963_447',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML2x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_963_448 = Coupling(name = 'UVGC_963_448',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML2x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_964_449 = Coupling(name = 'UVGC_964_449',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_964_450 = Coupling(name = 'UVGC_964_450',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_965_451 = Coupling(name = 'UVGC_965_451',
                        value = {-1:'(CKML2x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_965_452 = Coupling(name = 'UVGC_965_452',
                        value = {-1:'-0.08333333333333333*(CKML2x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_966_453 = Coupling(name = 'UVGC_966_453',
                        value = {-1:'-0.041666666666666664*(CKMR2x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_966_454 = Coupling(name = 'UVGC_966_454',
                        value = {-1:'(CKMR2x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_967_455 = Coupling(name = 'UVGC_967_455',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML2x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_967_456 = Coupling(name = 'UVGC_967_456',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML2x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_968_457 = Coupling(name = 'UVGC_968_457',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_968_458 = Coupling(name = 'UVGC_968_458',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR2x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_969_459 = Coupling(name = 'UVGC_969_459',
                        value = {-1:'(CKML1x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_969_460 = Coupling(name = 'UVGC_969_460',
                        value = {-1:'-0.08333333333333333*(CKML1x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_970_461 = Coupling(name = 'UVGC_970_461',
                        value = {-1:'-0.041666666666666664*(CKMR1x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_970_462 = Coupling(name = 'UVGC_970_462',
                        value = {-1:'(CKMR1x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_971_463 = Coupling(name = 'UVGC_971_463',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML1x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_971_464 = Coupling(name = 'UVGC_971_464',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML1x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_972_465 = Coupling(name = 'UVGC_972_465',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_972_466 = Coupling(name = 'UVGC_972_466',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_973_467 = Coupling(name = 'UVGC_973_467',
                        value = {-1:'(CKML1x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_973_468 = Coupling(name = 'UVGC_973_468',
                        value = {-1:'-0.08333333333333333*(CKML1x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_974_469 = Coupling(name = 'UVGC_974_469',
                        value = {-1:'-0.041666666666666664*(CKMR1x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_974_470 = Coupling(name = 'UVGC_974_470',
                        value = {-1:'(CKMR1x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_975_471 = Coupling(name = 'UVGC_975_471',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML1x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_975_472 = Coupling(name = 'UVGC_975_472',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML1x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_976_473 = Coupling(name = 'UVGC_976_473',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_976_474 = Coupling(name = 'UVGC_976_474',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_977_475 = Coupling(name = 'UVGC_977_475',
                        value = {-1:'(CKML1x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_977_476 = Coupling(name = 'UVGC_977_476',
                        value = {-1:'-0.08333333333333333*(CKML1x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_978_477 = Coupling(name = 'UVGC_978_477',
                        value = {-1:'-0.041666666666666664*(CKMR1x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_978_478 = Coupling(name = 'UVGC_978_478',
                        value = {-1:'(CKMR1x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_979_479 = Coupling(name = 'UVGC_979_479',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML1x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_979_480 = Coupling(name = 'UVGC_979_480',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML1x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_980_481 = Coupling(name = 'UVGC_980_481',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_980_482 = Coupling(name = 'UVGC_980_482',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR1x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_981_483 = Coupling(name = 'UVGC_981_483',
                        value = {-1:'(CKML3x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_981_484 = Coupling(name = 'UVGC_981_484',
                        value = {-1:'-0.08333333333333333*(CKML3x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKML3x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKML3x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_981_485 = Coupling(name = 'UVGC_981_485',
                        value = {-1:'-0.08333333333333333*(CKML3x1*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_982_486 = Coupling(name = 'UVGC_982_486',
                        value = {-1:'(CKML3x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_982_487 = Coupling(name = 'UVGC_982_487',
                        value = {-1:'-0.08333333333333333*(CKML3x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKML3x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKML3x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_982_488 = Coupling(name = 'UVGC_982_488',
                        value = {-1:'-0.08333333333333333*(CKML3x2*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_983_489 = Coupling(name = 'UVGC_983_489',
                        value = {-1:'(CKML3x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_983_490 = Coupling(name = 'UVGC_983_490',
                        value = {-1:'-0.08333333333333333*(CKML3x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(CKML3x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2)) + (CKML3x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_983_491 = Coupling(name = 'UVGC_983_491',
                        value = {-1:'-0.08333333333333333*(CKML3x3*complex(0,1)*G**2*gw*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_984_492 = Coupling(name = 'UVGC_984_492',
                        value = {-1:'-0.041666666666666664*(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_984_493 = Coupling(name = 'UVGC_984_493',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_984_494 = Coupling(name = 'UVGC_984_494',
                        value = {-1:'(CKMR3x1*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_985_495 = Coupling(name = 'UVGC_985_495',
                        value = {-1:'-0.041666666666666664*(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_985_496 = Coupling(name = 'UVGC_985_496',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_985_497 = Coupling(name = 'UVGC_985_497',
                        value = {-1:'(CKMR3x2*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_986_498 = Coupling(name = 'UVGC_986_498',
                        value = {-1:'-0.041666666666666664*(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_986_499 = Coupling(name = 'UVGC_986_499',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2)) - (CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_986_500 = Coupling(name = 'UVGC_986_500',
                        value = {-1:'(CKMR3x3*complex(0,1)*G**2*gw*zetaLR*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_987_501 = Coupling(name = 'UVGC_987_501',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_987_502 = Coupling(name = 'UVGC_987_502',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_987_503 = Coupling(name = 'UVGC_987_503',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x1)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_988_504 = Coupling(name = 'UVGC_988_504',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_988_505 = Coupling(name = 'UVGC_988_505',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_988_506 = Coupling(name = 'UVGC_988_506',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x2)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_989_507 = Coupling(name = 'UVGC_989_507',
                        value = {-1:'(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(24.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_989_508 = Coupling(name = 'UVGC_989_508',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))',0:'-0.16666666666666666*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_989_509 = Coupling(name = 'UVGC_989_509',
                        value = {-1:'-0.08333333333333333*(complex(0,1)*G**2*gw*complexconjugate(CKML3x3)*cmath.exp(alp*complex(0,1))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_990_510 = Coupling(name = 'UVGC_990_510',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_990_511 = Coupling(name = 'UVGC_990_511',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_990_512 = Coupling(name = 'UVGC_990_512',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x1)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_991_513 = Coupling(name = 'UVGC_991_513',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_991_514 = Coupling(name = 'UVGC_991_514',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_991_515 = Coupling(name = 'UVGC_991_515',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x2)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_992_516 = Coupling(name = 'UVGC_992_516',
                        value = {-1:'-0.041666666666666664*(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_992_517 = Coupling(name = 'UVGC_992_517',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(6.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*reglog(MT/MU_R)*cmath.sin(xi))/(4.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_992_518 = Coupling(name = 'UVGC_992_518',
                        value = {-1:'(complex(0,1)*G**2*gw*zetaLR*complexconjugate(CKMR3x3)*cmath.exp(-(alp*complex(0,1)))*cmath.sin(xi))/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

