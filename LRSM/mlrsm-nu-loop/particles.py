# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.2.0 for Mac OS X ARM (64-bit) (November 18, 2022)
# Date: Sat 2 Mar 2024 16:50:05


from __future__ import division
from object_library import all_particles, Particle
import parameters as Param

import propagators as Prop

a = Particle(pdg_code = 22,
             name = 'a',
             antiname = 'a',
             spin = 3,
             color = 1,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'a',
             antitexname = 'a',
             charge = 0,
             GhostNumber = 0,
             YBL = 0)

Z = Particle(pdg_code = 23,
             name = 'Z',
             antiname = 'Z',
             spin = 3,
             color = 1,
             mass = Param.MZ,
             width = Param.WZ,
             texname = 'Z',
             antitexname = 'Z',
             charge = 0,
             GhostNumber = 0,
             YBL = 0)

ZR = Particle(pdg_code = 32,
              name = 'ZR',
              antiname = 'ZR',
              spin = 3,
              color = 1,
              mass = Param.MZR,
              width = Param.WZR,
              texname = 'ZR',
              antitexname = 'ZR',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

W__plus__ = Particle(pdg_code = 24,
                     name = 'W+',
                     antiname = 'W-',
                     spin = 3,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'W+',
                     antitexname = 'W-',
                     charge = 1,
                     GhostNumber = 0,
                     YBL = 0)

W__minus__ = W__plus__.anti()

WR__plus__ = Particle(pdg_code = 34,
                      name = 'WR+',
                      antiname = 'WR-',
                      spin = 3,
                      color = 1,
                      mass = Param.MWR,
                      width = Param.WWR,
                      texname = 'WR+',
                      antitexname = 'WR-',
                      charge = 1,
                      GhostNumber = 0,
                      YBL = 0)

WR__minus__ = WR__plus__.anti()

g = Particle(pdg_code = 21,
             name = 'g',
             antiname = 'g',
             spin = 3,
             color = 8,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'g',
             antitexname = 'g',
             charge = 0,
             GhostNumber = 0,
             YBL = 0)

ghA = Particle(pdg_code = 9000012,
               name = 'ghA',
               antiname = 'ghA~',
               spin = -1,
               color = 1,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghA',
               antitexname = 'ghA~',
               charge = 0,
               GhostNumber = 1,
               YBL = 0)

ghA__tilde__ = ghA.anti()

ghZ = Particle(pdg_code = 9000013,
               name = 'ghZ',
               antiname = 'ghZ~',
               spin = -1,
               color = 1,
               mass = Param.MZ,
               width = Param.WZ,
               texname = 'ghZ',
               antitexname = 'ghZ~',
               charge = 0,
               GhostNumber = 1,
               YBL = 0)

ghZ__tilde__ = ghZ.anti()

ghZR = Particle(pdg_code = 9000014,
                name = 'ghZR',
                antiname = 'ghZR~',
                spin = -1,
                color = 1,
                mass = Param.MZR,
                width = Param.WZR,
                texname = 'ghZR',
                antitexname = 'ghZR~',
                charge = 0,
                GhostNumber = 1,
                YBL = 0)

ghZR__tilde__ = ghZR.anti()

ghWp = Particle(pdg_code = 9000015,
                name = 'ghWp',
                antiname = 'ghWp~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.WW,
                texname = 'ghWp',
                antitexname = 'ghWp~',
                charge = 1,
                GhostNumber = 1,
                YBL = 0)

ghWp__tilde__ = ghWp.anti()

ghWm = Particle(pdg_code = 9000016,
                name = 'ghWm',
                antiname = 'ghWm~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.WW,
                texname = 'ghWm',
                antitexname = 'ghWm~',
                charge = -1,
                GhostNumber = 1,
                YBL = 0)

ghWm__tilde__ = ghWm.anti()

ghWRp = Particle(pdg_code = 9000006,
                 name = 'ghWRp',
                 antiname = 'ghWRp~',
                 spin = -1,
                 color = 1,
                 mass = Param.MWR,
                 width = Param.WWR,
                 texname = 'ghWRp',
                 antitexname = 'ghWRp~',
                 charge = 1,
                 GhostNumber = 1,
                 YBL = 0)

ghWRp__tilde__ = ghWRp.anti()

ghWRm = Particle(pdg_code = 9000007,
                 name = 'ghWRm',
                 antiname = 'ghWRm~',
                 spin = -1,
                 color = 1,
                 mass = Param.MWR,
                 width = Param.WWR,
                 texname = 'ghWRm',
                 antitexname = 'ghWRm~',
                 charge = -1,
                 GhostNumber = 1,
                 YBL = 0)

ghWRm__tilde__ = ghWRm.anti()

ghG = Particle(pdg_code = 82,
               name = 'ghG',
               antiname = 'ghG~',
               spin = -1,
               color = 8,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghG',
               antitexname = 'ghG~',
               charge = 0,
               GhostNumber = 1,
               YBL = 0)

ghG__tilde__ = ghG.anti()

Hp = Particle(pdg_code = 37,
              name = 'Hp',
              antiname = 'Hp~',
              spin = 1,
              color = 1,
              mass = Param.mHp,
              width = Param.WHp,
              texname = 'Hp',
              antitexname = 'Hp~',
              charge = 1,
              GhostNumber = 0,
              YBL = 0)

Hp__tilde__ = Hp.anti()

PhipL = Particle(pdg_code = 9000008,
                 name = 'PhipL',
                 antiname = 'PhipL~',
                 spin = 1,
                 color = 1,
                 mass = Param.MW,
                 width = Param.WW,
                 texname = 'PhipL',
                 antitexname = 'PhipL~',
                 goldstone = True,
                 charge = 1,
                 GhostNumber = 0,
                 YBL = 0)

PhipL__tilde__ = PhipL.anti()

PhipR = Particle(pdg_code = 9000009,
                 name = 'PhipR',
                 antiname = 'PhipR~',
                 spin = 1,
                 color = 1,
                 mass = Param.MWR,
                 width = Param.WWR,
                 texname = 'PhipR',
                 antitexname = 'PhipR~',
                 goldstone = True,
                 charge = 1,
                 GhostNumber = 0,
                 YBL = 0)

PhipR__tilde__ = PhipR.anti()

Phi0L = Particle(pdg_code = 9000010,
                 name = 'Phi0L',
                 antiname = 'Phi0L',
                 spin = 1,
                 color = 1,
                 mass = Param.MZ,
                 width = Param.WZ,
                 texname = 'Phi0L',
                 antitexname = 'Phi0L',
                 goldstone = True,
                 charge = 0,
                 GhostNumber = 0,
                 YBL = 0)

Phi0R = Particle(pdg_code = 9000011,
                 name = 'Phi0R',
                 antiname = 'Phi0R',
                 spin = 1,
                 color = 1,
                 mass = Param.MZR,
                 width = Param.WZR,
                 texname = 'Phi0R',
                 antitexname = 'Phi0R',
                 goldstone = True,
                 charge = 0,
                 GhostNumber = 0,
                 YBL = 0)

DR__plus____plus__ = Particle(pdg_code = 9000001,
                              name = 'DR++',
                              antiname = 'DR--',
                              spin = 1,
                              color = 1,
                              mass = Param.mDRpp,
                              width = Param.WDRpp,
                              texname = 'DR++',
                              antitexname = 'DR--',
                              charge = 2,
                              GhostNumber = 0,
                              YBL = 0)

DR__minus____minus__ = DR__plus____plus__.anti()

DL__plus____plus__ = Particle(pdg_code = 9000002,
                              name = 'DL++',
                              antiname = 'DL--',
                              spin = 1,
                              color = 1,
                              mass = Param.mDLpp,
                              width = Param.WDLpp,
                              texname = 'DL++',
                              antitexname = 'DL--',
                              charge = 2,
                              GhostNumber = 0,
                              YBL = 0)

DL__minus____minus__ = DL__plus____plus__.anti()

DL__plus__ = Particle(pdg_code = 9000003,
                      name = 'DL+',
                      antiname = 'DL-',
                      spin = 1,
                      color = 1,
                      mass = Param.mDLp,
                      width = Param.WDLp,
                      texname = 'DL+',
                      antitexname = 'DL-',
                      charge = 1,
                      GhostNumber = 0,
                      YBL = 0)

DL__minus__ = DL__plus__.anti()

DL0 = Particle(pdg_code = 9000004,
               name = 'DL0',
               antiname = 'DL0',
               spin = 1,
               color = 1,
               mass = Param.mDL0,
               width = Param.WDL0,
               texname = 'DL0',
               antitexname = 'DL0',
               charge = 0,
               GhostNumber = 0,
               YBL = 0)

chiL0 = Particle(pdg_code = 9000005,
                 name = 'chiL0',
                 antiname = 'chiL0',
                 spin = 1,
                 color = 1,
                 mass = Param.mChiL0,
                 width = Param.WchiL0,
                 texname = 'chiL0',
                 antitexname = 'chiL0',
                 charge = 0,
                 GhostNumber = 0,
                 YBL = 0)

h = Particle(pdg_code = 25,
             name = 'h',
             antiname = 'h',
             spin = 1,
             color = 1,
             mass = Param.mh,
             width = Param.Wh,
             texname = 'h',
             antitexname = 'h',
             charge = 0,
             GhostNumber = 0,
             YBL = 0)

DD = Particle(pdg_code = 45,
              name = 'DD',
              antiname = 'DD',
              spin = 1,
              color = 1,
              mass = Param.mDD,
              width = Param.WDD,
              texname = 'DD',
              antitexname = 'DD',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

HH = Particle(pdg_code = 35,
              name = 'HH',
              antiname = 'HH',
              spin = 1,
              color = 1,
              mass = Param.mHH,
              width = Param.WHH,
              texname = 'HH',
              antitexname = 'HH',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

AA = Particle(pdg_code = 55,
              name = 'AA',
              antiname = 'AA',
              spin = 1,
              color = 1,
              mass = Param.mAA,
              width = Param.WAA,
              texname = 'AA',
              antitexname = 'AA',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

u = Particle(pdg_code = 2,
             name = 'u',
             antiname = 'u~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'u',
             antitexname = 'u~',
             charge = 2/3,
             GhostNumber = 0,
             YBL = 0)

u__tilde__ = u.anti()

c = Particle(pdg_code = 4,
             name = 'c',
             antiname = 'c~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'c',
             antitexname = 'c~',
             charge = 2/3,
             GhostNumber = 0,
             YBL = 0)

c__tilde__ = c.anti()

t = Particle(pdg_code = 6,
             name = 't',
             antiname = 't~',
             spin = 2,
             color = 3,
             mass = Param.MT,
             width = Param.WT,
             texname = 't',
             antitexname = 't~',
             charge = 2/3,
             GhostNumber = 0,
             YBL = 0)

t__tilde__ = t.anti()

d = Particle(pdg_code = 1,
             name = 'd',
             antiname = 'd~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'd',
             antitexname = 'd~',
             charge = -1/3,
             GhostNumber = 0,
             YBL = 0)

d__tilde__ = d.anti()

s = Particle(pdg_code = 3,
             name = 's',
             antiname = 's~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 's',
             antitexname = 's~',
             charge = -1/3,
             GhostNumber = 0,
             YBL = 0)

s__tilde__ = s.anti()

b = Particle(pdg_code = 5,
             name = 'b',
             antiname = 'b~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'b',
             antitexname = 'b~',
             charge = -1/3,
             GhostNumber = 0,
             YBL = 0)

b__tilde__ = b.anti()

ve = Particle(pdg_code = 12,
              name = 've',
              antiname = 've',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 've',
              antitexname = 've',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

vm = Particle(pdg_code = 14,
              name = 'vm',
              antiname = 'vm',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'vm',
              antitexname = 'vm',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

vt = Particle(pdg_code = 16,
              name = 'vt',
              antiname = 'vt',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'vt',
              antitexname = 'vt',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

N1 = Particle(pdg_code = 60,
              name = 'N1',
              antiname = 'N1',
              spin = 2,
              color = 1,
              mass = Param.MN1,
              width = Param.WN1,
              texname = 'N1',
              antitexname = 'N1',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

N2 = Particle(pdg_code = 62,
              name = 'N2',
              antiname = 'N2',
              spin = 2,
              color = 1,
              mass = Param.MN2,
              width = Param.WN2,
              texname = 'N2',
              antitexname = 'N2',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

N3 = Particle(pdg_code = 64,
              name = 'N3',
              antiname = 'N3',
              spin = 2,
              color = 1,
              mass = Param.MN3,
              width = Param.WN3,
              texname = 'N3',
              antitexname = 'N3',
              charge = 0,
              GhostNumber = 0,
              YBL = 0)

e__minus__ = Particle(pdg_code = 11,
                      name = 'e-',
                      antiname = 'e+',
                      spin = 2,
                      color = 1,
                      mass = Param.ZERO,
                      width = Param.ZERO,
                      texname = 'e-',
                      antitexname = 'e+',
                      charge = -1,
                      GhostNumber = 0,
                      YBL = 0)

e__plus__ = e__minus__.anti()

mu__minus__ = Particle(pdg_code = 13,
                       name = 'mu-',
                       antiname = 'mu+',
                       spin = 2,
                       color = 1,
                       mass = Param.ZERO,
                       width = Param.ZERO,
                       texname = 'mu-',
                       antitexname = 'mu+',
                       charge = -1,
                       GhostNumber = 0,
                       YBL = 0)

mu__plus__ = mu__minus__.anti()

tau__minus__ = Particle(pdg_code = 15,
                        name = 'tau-',
                        antiname = 'tau+',
                        spin = 2,
                        color = 1,
                        mass = Param.MTA,
                        width = Param.ZERO,
                        texname = 'tau-',
                        antitexname = 'tau+',
                        charge = -1,
                        GhostNumber = 0,
                        YBL = 0)

tau__plus__ = tau__minus__.anti()

