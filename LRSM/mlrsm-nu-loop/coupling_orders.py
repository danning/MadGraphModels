# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.2.0 for Mac OS X ARM (64-bit) (November 18, 2022)
# Date: Sat 2 Mar 2024 16:50:22


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

