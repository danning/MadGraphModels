# This file was automatically created by FeynRules 2.3.10
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Wed 13 Apr 2016 19:04:27



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
lambda1 = Parameter(name = 'lambda1',
                    nature = 'external',
                    type = 'real',
                    value = 0.13123,
                    texname = '\\lambda _1',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 1 ])

lambda2 = Parameter(name = 'lambda2',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\lambda _2',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 2 ])

lambda3 = Parameter(name = 'lambda3',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\lambda _3',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 3 ])

lambda4 = Parameter(name = 'lambda4',
                    nature = 'external',
                    type = 'real',
                    value = 0.,
                    texname = '\\lambda _4',
                    lhablock = 'HIGGSBLOCK',
                    lhacode = [ 4 ])

rho1 = Parameter(name = 'rho1',
                 nature = 'external',
                 type = 'real',
                 value = 4.71911,
                 texname = '\\rho _1',
                 lhablock = 'HIGGSBLOCK',
                 lhacode = [ 5 ])

rho2 = Parameter(name = 'rho2',
                 nature = 'external',
                 type = 'real',
                 value = 4.71911,
                 texname = '\\rho _2',
                 lhablock = 'HIGGSBLOCK',
                 lhacode = [ 6 ])

rho3 = Parameter(name = 'rho3',
                 nature = 'external',
                 type = 'real',
                 value = 28.3147,
                 texname = '\\rho _3',
                 lhablock = 'HIGGSBLOCK',
                 lhacode = [ 7 ])

rho4 = Parameter(name = 'rho4',
                 nature = 'external',
                 type = 'real',
                 value = 4.71911,
                 texname = '\\rho _4',
                 lhablock = 'HIGGSBLOCK',
                 lhacode = [ 8 ])

alpha1 = Parameter(name = 'alpha1',
                   nature = 'external',
                   type = 'real',
                   value = 18.8765,
                   texname = '\\alpha _1',
                   lhablock = 'HIGGSBLOCK',
                   lhacode = [ 9 ])

alpha2 = Parameter(name = 'alpha2',
                   nature = 'external',
                   type = 'real',
                   value = 18.8765,
                   texname = '\\alpha _2',
                   lhablock = 'HIGGSBLOCK',
                   lhacode = [ 10 ])

alpha3 = Parameter(name = 'alpha3',
                   nature = 'external',
                   type = 'real',
                   value = 18.8765,
                   texname = '\\alpha _3',
                   lhablock = 'HIGGSBLOCK',
                   lhacode = [ 11 ])

ISMix1x1 = Parameter(name = 'ISMix1x1',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix1x1}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 1, 1 ])

ISMix1x2 = Parameter(name = 'ISMix1x2',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix1x2}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 1, 2 ])

ISMix1x3 = Parameter(name = 'ISMix1x3',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix1x3}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 1, 3 ])

ISMix2x1 = Parameter(name = 'ISMix2x1',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix2x1}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 2, 1 ])

ISMix2x2 = Parameter(name = 'ISMix2x2',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix2x2}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 2, 2 ])

ISMix2x3 = Parameter(name = 'ISMix2x3',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix2x3}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 2, 3 ])

ISMix3x1 = Parameter(name = 'ISMix3x1',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix3x1}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 3, 1 ])

ISMix3x2 = Parameter(name = 'ISMix3x2',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix3x2}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 3, 2 ])

ISMix3x3 = Parameter(name = 'ISMix3x3',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{ISMix3x3}',
                     lhablock = 'IMMYMix',
                     lhacode = [ 3, 3 ])

RSMix1x1 = Parameter(name = 'RSMix1x1',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix1x1}',
                     lhablock = 'MYMix',
                     lhacode = [ 1, 1 ])

RSMix1x2 = Parameter(name = 'RSMix1x2',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix1x2}',
                     lhablock = 'MYMix',
                     lhacode = [ 1, 2 ])

RSMix1x3 = Parameter(name = 'RSMix1x3',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix1x3}',
                     lhablock = 'MYMix',
                     lhacode = [ 1, 3 ])

RSMix2x1 = Parameter(name = 'RSMix2x1',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix2x1}',
                     lhablock = 'MYMix',
                     lhacode = [ 2, 1 ])

RSMix2x2 = Parameter(name = 'RSMix2x2',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix2x2}',
                     lhablock = 'MYMix',
                     lhacode = [ 2, 2 ])

RSMix2x3 = Parameter(name = 'RSMix2x3',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix2x3}',
                     lhablock = 'MYMix',
                     lhacode = [ 2, 3 ])

RSMix3x1 = Parameter(name = 'RSMix3x1',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix3x1}',
                     lhablock = 'MYMix',
                     lhacode = [ 3, 1 ])

RSMix3x2 = Parameter(name = 'RSMix3x2',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix3x2}',
                     lhablock = 'MYMix',
                     lhacode = [ 3, 2 ])

RSMix3x3 = Parameter(name = 'RSMix3x3',
                     nature = 'external',
                     type = 'real',
                     value = 1,
                     texname = '\\text{RSMix3x3}',
                     lhablock = 'MYMix',
                     lhacode = [ 3, 3 ])

sL12 = Parameter(name = 'sL12',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{sL}_{12}',
                 lhablock = 'PMNSBLOCK',
                 lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.94,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000117456,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.118,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

vev = Parameter(name = 'vev',
                nature = 'external',
                type = 'real',
                value = 245.36,
                texname = '\\text{vev}',
                lhablock = 'VEVS',
                lhacode = [ 1 ])

k1 = Parameter(name = 'k1',
               nature = 'external',
               type = 'real',
               value = 245.289,
               texname = '\\text{k1}',
               lhablock = 'VEVS',
               lhacode = [ 2 ])

vR = Parameter(name = 'vR',
               nature = 'external',
               type = 'real',
               value = 8680.08,
               texname = '\\text{vR}',
               lhablock = 'VEVS',
               lhacode = [ 3 ])

sk2 = Parameter(name = 'sk2',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{sk2}',
                lhablock = 'VEVS',
                lhacode = [ 5 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 79.9513,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 173.3,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.18,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MN4 = Parameter(name = 'MN4',
                nature = 'external',
                type = 'real',
                value = 100,
                texname = '\\text{MN4}',
                lhablock = 'MASS',
                lhacode = [ 9900012 ])

MN5 = Parameter(name = 'MN5',
                nature = 'external',
                type = 'real',
                value = 10000,
                texname = '\\text{MN5}',
                lhablock = 'MASS',
                lhacode = [ 9900014 ])

MN6 = Parameter(name = 'MN6',
                nature = 'external',
                type = 'real',
                value = 10000,
                texname = '\\text{MN6}',
                lhablock = 'MASS',
                lhacode = [ 9900016 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125.7,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MH01 = Parameter(name = 'MH01',
                 nature = 'external',
                 type = 'real',
                 value = 20000.,
                 texname = '\\text{MH01}',
                 lhablock = 'MASS',
                 lhacode = [ 35 ])

MH02 = Parameter(name = 'MH02',
                 nature = 'external',
                 type = 'real',
                 value = 20000.,
                 texname = '\\text{MH02}',
                 lhablock = 'MASS',
                 lhacode = [ 43 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WW2 = Parameter(name = 'WW2',
                nature = 'external',
                type = 'real',
                value = 69,
                texname = '\\text{WW2}',
                lhablock = 'DECAY',
                lhacode = [ 34 ])

WZ2 = Parameter(name = 'WZ2',
                nature = 'external',
                type = 'real',
                value = 80,
                texname = '\\text{WZ2}',
                lhablock = 'DECAY',
                lhacode = [ 32 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WN4 = Parameter(name = 'WN4',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = '\\text{WN4}',
                lhablock = 'DECAY',
                lhacode = [ 9900012 ])

WN5 = Parameter(name = 'WN5',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = '\\text{WN5}',
                lhablock = 'DECAY',
                lhacode = [ 9900014 ])

WN6 = Parameter(name = 'WN6',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = '\\text{WN6}',
                lhablock = 'DECAY',
                lhacode = [ 9900016 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00417,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WH01 = Parameter(name = 'WH01',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WH01}',
                 lhablock = 'DECAY',
                 lhacode = [ 35 ])

WH02 = Parameter(name = 'WH02',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WH02}',
                 lhablock = 'DECAY',
                 lhacode = [ 43 ])

WH03 = Parameter(name = 'WH03',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WH03}',
                 lhablock = 'DECAY',
                 lhacode = [ 44 ])

WHP1 = Parameter(name = 'WHP1',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WHP1}',
                 lhablock = 'DECAY',
                 lhacode = [ 37 ])

WHP2 = Parameter(name = 'WHP2',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WHP2}',
                 lhablock = 'DECAY',
                 lhacode = [ 38 ])

WHPPL = Parameter(name = 'WHPPL',
                  nature = 'external',
                  type = 'real',
                  value = 0.6,
                  texname = '\\text{WHPPL}',
                  lhablock = 'DECAY',
                  lhacode = [ 61 ])

WHPPR = Parameter(name = 'WHPPR',
                  nature = 'external',
                  type = 'real',
                  value = 0.6,
                  texname = '\\text{WHPPR}',
                  lhablock = 'DECAY',
                  lhacode = [ 62 ])

WA01 = Parameter(name = 'WA01',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WA01}',
                 lhablock = 'DECAY',
                 lhacode = [ 36 ])

WA02 = Parameter(name = 'WA02',
                 nature = 'external',
                 type = 'real',
                 value = 0.6,
                 texname = '\\text{WA02}',
                 lhablock = 'DECAY',
                 lhacode = [ 45 ])

CKML1x1 = Parameter(name = 'CKML1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '1',
                    texname = '\\text{CKML1x1}')

CKML1x2 = Parameter(name = 'CKML1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '0',
                    texname = '\\text{CKML1x2}')

CKML1x3 = Parameter(name = 'CKML1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = '0',
                    texname = '\\text{CKML1x3}')

CKML2x1 = Parameter(name = 'CKML2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '0',
                    texname = '\\text{CKML2x1}')

CKML2x2 = Parameter(name = 'CKML2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '1',
                    texname = '\\text{CKML2x2}')

CKML2x3 = Parameter(name = 'CKML2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = '0',
                    texname = '\\text{CKML2x3}')

CKML3x1 = Parameter(name = 'CKML3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = '0',
                    texname = '\\text{CKML3x1}')

CKML3x2 = Parameter(name = 'CKML3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = '0',
                    texname = '\\text{CKML3x2}')

CKML3x3 = Parameter(name = 'CKML3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = '1',
                    texname = '\\text{CKML3x3}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'MW/MZ',
               texname = 'c_w')

KL1x1 = Parameter(name = 'KL1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - sL12**2)',
                  texname = '\\text{KL1x1}')

KL1x2 = Parameter(name = 'KL1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'sL12',
                  texname = '\\text{KL1x2}')

KL1x3 = Parameter(name = 'KL1x3',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL1x3}')

KL2x1 = Parameter(name = 'KL2x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-sL12',
                  texname = '\\text{KL2x1}')

KL2x2 = Parameter(name = 'KL2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(1 - sL12**2)',
                  texname = '\\text{KL2x2}')

KL2x3 = Parameter(name = 'KL2x3',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL2x3}')

KL3x1 = Parameter(name = 'KL3x1',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL3x1}')

KL3x2 = Parameter(name = 'KL3x2',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL3x2}')

KL3x3 = Parameter(name = 'KL3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KL3x3}')

KL4x1 = Parameter(name = 'KL4x1',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL4x1}')

KL5x2 = Parameter(name = 'KL5x2',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL5x2}')

KL6x3 = Parameter(name = 'KL6x3',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KL6x3}')

KR1x1 = Parameter(name = 'KR1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KR1x1}')

KR2x2 = Parameter(name = 'KR2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KR2x2}')

KR3x3 = Parameter(name = 'KR3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = '\\text{KR3x3}')

KR4x1 = Parameter(name = 'KR4x1',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KR4x1}')

KR5x2 = Parameter(name = 'KR5x2',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KR5x2}')

KR6x3 = Parameter(name = 'KR6x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{KR6x3}')

MA02 = Parameter(name = 'MA02',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt((-2*rho1 + rho3)*vR**2)/cmath.sqrt(2)',
                 texname = 'M_{\\text{A02}}')

MH03 = Parameter(name = 'MH03',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt((-2*rho1 + rho3)*vR**2)/cmath.sqrt(2)',
                 texname = 'M_{\\text{H03}}')

SMix1x1 = Parameter(name = 'SMix1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix1x1 + RSMix1x1',
                    texname = '\\text{SMix1x1}')

SMix1x2 = Parameter(name = 'SMix1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix1x2 + RSMix1x2',
                    texname = '\\text{SMix1x2}')

SMix1x3 = Parameter(name = 'SMix1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix1x3 + RSMix1x3',
                    texname = '\\text{SMix1x3}')

SMix2x1 = Parameter(name = 'SMix2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix2x1 + RSMix2x1',
                    texname = '\\text{SMix2x1}')

SMix2x2 = Parameter(name = 'SMix2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix2x2 + RSMix2x2',
                    texname = '\\text{SMix2x2}')

SMix2x3 = Parameter(name = 'SMix2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix2x3 + RSMix2x3',
                    texname = '\\text{SMix2x3}')

SMix3x1 = Parameter(name = 'SMix3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix3x1 + RSMix3x1',
                    texname = '\\text{SMix3x1}')

SMix3x2 = Parameter(name = 'SMix3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix3x2 + RSMix3x2',
                    texname = '\\text{SMix3x2}')

SMix3x3 = Parameter(name = 'SMix3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'complex(0,1)*ISMix3x3 + RSMix3x3',
                    texname = '\\text{SMix3x3}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

WD1x1 = Parameter(name = 'WD1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{WD1x1}')

WD2x2 = Parameter(name = 'WD2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{WD2x2}')

WD3x3 = Parameter(name = 'WD3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{WD3x3}')

Wl1x1 = Parameter(name = 'Wl1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{Wl1x1}')

Wl2x2 = Parameter(name = 'Wl2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{Wl2x2}')

Wl3x3 = Parameter(name = 'Wl3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{Wl3x3}')

WU1x1 = Parameter(name = 'WU1x1',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{WU1x1}')

WU2x2 = Parameter(name = 'WU2x2',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{WU2x2}')

WU3x3 = Parameter(name = 'WU3x3',
                  nature = 'internal',
                  type = 'real',
                  value = '1',
                  texname = '\\text{WU3x3}')

yDO1x1 = Parameter(name = 'yDO1x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yDO1x1}')

yDO2x2 = Parameter(name = 'yDO2x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yDO2x2}')

yDO3x3 = Parameter(name = 'yDO3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'MB',
                   texname = '\\text{yDO3x3}')

yML1x1 = Parameter(name = 'yML1x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yML1x1}')

yML2x2 = Parameter(name = 'yML2x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yML2x2}')

yML3x3 = Parameter(name = 'yML3x3',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yML3x3}')

yMU1x1 = Parameter(name = 'yMU1x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yMU1x1}')

yMU2x2 = Parameter(name = 'yMU2x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yMU2x2}')

yMU3x3 = Parameter(name = 'yMU3x3',
                   nature = 'internal',
                   type = 'real',
                   value = 'MT',
                   texname = '\\text{yMU3x3}')

yNL1x1 = Parameter(name = 'yNL1x1',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yNL1x1}')

yNL2x2 = Parameter(name = 'yNL2x2',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yNL2x2}')

yNL3x3 = Parameter(name = 'yNL3x3',
                   nature = 'internal',
                   type = 'real',
                   value = '0',
                   texname = '\\text{yNL3x3}')

yNL4x4 = Parameter(name = 'yNL4x4',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN4',
                   texname = '\\text{yNL4x4}')

yNL5x5 = Parameter(name = 'yNL5x5',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN5',
                   texname = '\\text{yNL5x5}')

yNL6x6 = Parameter(name = 'yNL6x6',
                   nature = 'internal',
                   type = 'real',
                   value = 'MN6',
                   texname = '\\text{yNL6x6}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

gs = Parameter(name = 'gs',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
               texname = 'g_s')

k2 = Parameter(name = 'k2',
               nature = 'internal',
               type = 'real',
               value = 'sk2*cmath.sqrt(-k1**2 + vev**2)',
               texname = '\\text{k2}')

CKMR1x1 = Parameter(name = 'CKMR1x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML1x1*WD1x1*WU1x1',
                    texname = '\\text{CKMR1x1}')

CKMR1x2 = Parameter(name = 'CKMR1x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML1x2*WD2x2*WU1x1',
                    texname = '\\text{CKMR1x2}')

CKMR1x3 = Parameter(name = 'CKMR1x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML1x3*WD3x3*WU1x1',
                    texname = '\\text{CKMR1x3}')

CKMR2x1 = Parameter(name = 'CKMR2x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML2x1*WD1x1*WU2x2',
                    texname = '\\text{CKMR2x1}')

CKMR2x2 = Parameter(name = 'CKMR2x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML2x2*WD2x2*WU2x2',
                    texname = '\\text{CKMR2x2}')

CKMR2x3 = Parameter(name = 'CKMR2x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML2x3*WD3x3*WU2x2',
                    texname = '\\text{CKMR2x3}')

CKMR3x1 = Parameter(name = 'CKMR3x1',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML3x1*WD1x1*WU3x3',
                    texname = '\\text{CKMR3x1}')

CKMR3x2 = Parameter(name = 'CKMR3x2',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML3x2*WD2x2*WU3x3',
                    texname = '\\text{CKMR3x2}')

CKMR3x3 = Parameter(name = 'CKMR3x3',
                    nature = 'internal',
                    type = 'complex',
                    value = 'CKML3x3*WD3x3*WU3x3',
                    texname = '\\text{CKMR3x3}')

eps = Parameter(name = 'eps',
                nature = 'internal',
                type = 'real',
                value = '(2*k1*k2)/vev**2',
                texname = '\\text{eps}')

kminus = Parameter(name = 'kminus',
                   nature = 'internal',
                   type = 'real',
                   value = 'cmath.sqrt(k1**2 - k2**2)',
                   texname = '\\text{kminus}')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

sxi = Parameter(name = 'sxi',
                nature = 'internal',
                type = 'real',
                value = '-((k1*k2)/(vR**2*cmath.sqrt(2 + (2*k1**2*k2**2)/vR**4)*cmath.sqrt(1 + 1/cmath.sqrt(1 + (k1**2*k2**2)/vR**4))))',
                texname = 's_{\\theta }')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

cxi = Parameter(name = 'cxi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - sxi**2)',
                texname = 'c_{\\theta }')

MA01 = Parameter(name = 'MA01',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(-2*(2*lambda2 - lambda3)*vev**2 + (alpha3*vR**2)/(2.*cmath.sqrt(1 - eps**2)))',
                 texname = 'M_{\\text{A01}}')

MHP1 = Parameter(name = 'MHP1',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(((-2*rho1 + rho3)*vR**2)/2. + (alpha3*vev**2*cmath.sqrt(1 - eps**2))/4.)',
                 texname = 'M_{\\text{HP1}}')

MHP2 = Parameter(name = 'MHP2',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(alpha3*(vR**2/cmath.sqrt(1 - eps**2) + (vev**2*cmath.sqrt(1 - eps**2))/2.))/cmath.sqrt(2)',
                 texname = 'M_{\\text{HP2}}')

MHPPL = Parameter(name = 'MHPPL',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt((-2*rho1 + rho3)*vR**2 + alpha3*vev**2*cmath.sqrt(1 - eps**2))/cmath.sqrt(2)',
                  texname = 'M_{\\text{HPPL}}')

MHPPR = Parameter(name = 'MHPPR',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(2*rho2*vR**2 + (alpha3*vev**2*cmath.sqrt(1 - eps**2))/2.)',
                  texname = 'M_{\\text{HPPR}}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cmath.sqrt(1 - 2*sw**2)',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

MW2 = Parameter(name = 'MW2',
                nature = 'internal',
                type = 'real',
                value = '(gw*cmath.sqrt(vev**2 + vR**2 + cmath.sqrt(4*k1**2*k2**2 + vR**4)))/2.',
                texname = 'M_{\\text{W2}}')

MZ2 = Parameter(name = 'MZ2',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(gw**2*vev**2 + 2*(g1**2 + gw**2)*vR**2 + cmath.sqrt(-4*gw**2*(2*g1**2 + gw**2)*vev**2*vR**2 + (gw**2*vev**2 + 2*(g1**2 + gw**2)*vR**2)**2))/2.',
                texname = 'M_{\\text{Z2}}')

sphi = Parameter(name = 'sphi',
                 nature = 'internal',
                 type = 'real',
                 value = '-cmath.sqrt(0.5 - 0.5*cmath.sqrt(1 - (gw**4*(1 - 2*sw**2)*vev**4)/(4.*cw**4*(-MZ**2 + MZ2**2)**2)))',
                 texname = 's_{\\phi }')

cphi = Parameter(name = 'cphi',
                 nature = 'internal',
                 type = 'real',
                 value = 'cmath.sqrt(1 - sphi**2)',
                 texname = 'c_{\\phi }')

