Requestor: Robert Les
Contents: Spin 0 radion in warped extra dimension scenario which stabilizes the curvature of the warped extra dimension
Theory paper: arxiv.org/abs/hep-ph/9907447
Model paper: arXiv.org/abs/1404.0102
Source: https://github.com/CrossSectionsLHC/WED?files=1 
JIRA: https://its.cern.ch/jira/browse/AGENE-1563