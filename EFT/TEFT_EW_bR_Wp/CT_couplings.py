# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Mon 13 Jun 2016 10:16:36


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '(-7*ccc*ctG*complex(0,1)*G**2*gp**2*MT)/(18.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':2.,'QCD':2})

R2GC_101_2 = Coupling(name = 'R2GC_101_2',
                      value = '(2*ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(9.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_102_3 = Coupling(name = 'R2GC_102_3',
                      value = '(7*ccc*ctG*complex(0,1)*G**3*MT**2)/(48.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3})

R2GC_103_4 = Coupling(name = 'R2GC_103_4',
                      value = '-(ccc*ctG*complex(0,1)*G**2*gp*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1.5,'QCD':2})

R2GC_104_5 = Coupling(name = 'R2GC_104_5',
                      value = '(7*ccc*ctG*complex(0,1)*G**2*MT**3)/(6.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2})

R2GC_105_6 = Coupling(name = 'R2GC_105_6',
                      value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(8.*cmath.pi**2*Lambda**2*sw**2)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_106_7 = Coupling(name = 'R2GC_106_7',
                      value = '-(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_107_8 = Coupling(name = 'R2GC_107_8',
                      value = '(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_108_9 = Coupling(name = 'R2GC_108_9',
                      value = '(ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(24.*cmath.pi**2*Lambda**2*sw**2)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_109_10 = Coupling(name = 'R2GC_109_10',
                       value = '(11*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_110_11 = Coupling(name = 'R2GC_110_11',
                       value = '(13*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_111_12 = Coupling(name = 'R2GC_111_12',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*MT*cmath.sqrt(2))/(9.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_112_13 = Coupling(name = 'R2GC_112_13',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_113_14 = Coupling(name = 'R2GC_113_14',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_114_15 = Coupling(name = 'R2GC_114_15',
                       value = '(ccc*ctG*ee*complex(0,1)*G**3*MT)/(96.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_115_16 = Coupling(name = 'R2GC_115_16',
                       value = '(-5*ccc*ctG*ee*complex(0,1)*G**3*MT)/(96.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_116_17 = Coupling(name = 'R2GC_116_17',
                       value = '(-25*ccc*ctG*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_117_18 = Coupling(name = 'R2GC_117_18',
                       value = '(-5*ccc*ctG*ee*complex(0,1)*G**3*MT)/(24.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_118_19 = Coupling(name = 'R2GC_118_19',
                       value = '(-55*ccc*ctG*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_119_20 = Coupling(name = 'R2GC_119_20',
                       value = '(-29*ccc*ctG*ee*complex(0,1)*G**3*MT)/(96.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_120_21 = Coupling(name = 'R2GC_120_21',
                       value = '-(ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(18.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_121_22 = Coupling(name = 'R2GC_121_22',
                       value = '(-7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_122_23 = Coupling(name = 'R2GC_122_23',
                       value = '-(ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_123_24 = Coupling(name = 'R2GC_123_24',
                       value = '-(ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_124_25 = Coupling(name = 'R2GC_124_25',
                       value = '(-7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(18.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_125_26 = Coupling(name = 'R2GC_125_26',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*gp*MT*cmath.sqrt(2))/(9.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_126_27 = Coupling(name = 'R2GC_126_27',
                       value = '(ccc*cfb*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_127_28 = Coupling(name = 'R2GC_127_28',
                       value = '(ccc*cfQ3*ee*complex(0,1)*G**2*gp*MT**2)/(24.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_128_29 = Coupling(name = 'R2GC_128_29',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_129_30 = Coupling(name = 'R2GC_129_30',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_130_31 = Coupling(name = 'R2GC_130_31',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_131_32 = Coupling(name = 'R2GC_131_32',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_132_33 = Coupling(name = 'R2GC_132_33',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_134_34 = Coupling(name = 'R2GC_134_34',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_135_35 = Coupling(name = 'R2GC_135_35',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_136_36 = Coupling(name = 'R2GC_136_36',
                       value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_139_37 = Coupling(name = 'R2GC_139_37',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_147_38 = Coupling(name = 'R2GC_147_38',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_152_39 = Coupling(name = 'R2GC_152_39',
                       value = '-(ccc*cfb*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cfQ1*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cft*ee**2*complex(0,1)*G**2*MT**2)/(36.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(24.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_153_40 = Coupling(name = 'R2GC_153_40',
                       value = '(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_154_41 = Coupling(name = 'R2GC_154_41',
                       value = '(ccc*cfb*ee**2*complex(0,1)*G**2*MT**2)/(144.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee**2*complex(0,1)*G**2*MT**2)/(144.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(48.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_155_42 = Coupling(name = 'R2GC_155_42',
                       value = '-(ccc*cfb*ee*complex(0,1)*G**3*MT**2)/(96.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee*complex(0,1)*G**3*MT**2)/(48.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*complex(0,1)*G**3*MT**2)/(96.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_156_43 = Coupling(name = 'R2GC_156_43',
                       value = '(5*ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw) - (7*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_157_44 = Coupling(name = 'R2GC_157_44',
                       value = '(ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(9.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_158_45 = Coupling(name = 'R2GC_158_45',
                       value = '(ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(6.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_159_46 = Coupling(name = 'R2GC_159_46',
                       value = '(7*ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw) - (13*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_160_47 = Coupling(name = 'R2GC_160_47',
                       value = '(13*ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw) - (19*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_161_48 = Coupling(name = 'R2GC_161_48',
                       value = '(11*ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw) - (7*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(24.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_162_49 = Coupling(name = 'R2GC_162_49',
                       value = '(-29*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw) + (25*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(576.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_163_50 = Coupling(name = 'R2GC_163_50',
                       value = '(-55*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(384.*cmath.pi**2*Lambda**2*sw) + (95*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(1152.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_164_51 = Coupling(name = 'R2GC_164_51',
                       value = '(-5*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(48.*cmath.pi**2*Lambda**2*sw) + (35*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(288.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_165_52 = Coupling(name = 'R2GC_165_52',
                       value = '(-25*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(384.*cmath.pi**2*Lambda**2*sw) + (185*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(1152.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_166_53 = Coupling(name = 'R2GC_166_53',
                       value = '(ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw) + (115*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(576.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_167_54 = Coupling(name = 'R2GC_167_54',
                       value = '(-5*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw) + (115*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(576.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_168_55 = Coupling(name = 'R2GC_168_55',
                       value = '(-5*ccc*ctG*cw*ee*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*complex(0,1)*G**2*gp*MT*sw)/(36.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_169_56 = Coupling(name = 'R2GC_169_56',
                       value = '(5*ccc*ctG*cw*ee*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*complex(0,1)*G**2*gp*MT*sw)/(36.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_170_57 = Coupling(name = 'R2GC_170_57',
                       value = '-(ccc*ctG*cw*ee*complex(0,1)*G**2*gp*MT)/(24.*cmath.pi**2*Lambda**2*sw) + (7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_171_58 = Coupling(name = 'R2GC_171_58',
                       value = '(ccc*ctG*cw*ee*complex(0,1)*G**2*gp*MT)/(24.*cmath.pi**2*Lambda**2*sw) + (7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_172_59 = Coupling(name = 'R2GC_172_59',
                       value = '(-7*ccc*ctG*cw*ee*complex(0,1)*G**2*gp*MT)/(72.*cmath.pi**2*Lambda**2*sw) + (7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_173_60 = Coupling(name = 'R2GC_173_60',
                       value = '(7*ccc*ctG*cw*ee*complex(0,1)*G**2*gp*MT)/(72.*cmath.pi**2*Lambda**2*sw) + (7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT*sw)/(72.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_174_61 = Coupling(name = 'R2GC_174_61',
                       value = '-(ccc*cfb*ee*G**2*MT**2)/(24.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ1*ee*G**2*MT**2)/(12.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*G**2*MT**2)/(24.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*G**2*MT**2)/(16.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*G**2*MT**2*sw)/(16.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_175_62 = Coupling(name = 'R2GC_175_62',
                       value = '(3*ccc*cfb*ee*complex(0,1)*G**3*MT**2)/(32.*cw*cmath.pi**2*Lambda**2*sw) - (3*ccc*cfQ1*ee*complex(0,1)*G**3*MT**2)/(16.*cw*cmath.pi**2*Lambda**2*sw) + (3*ccc*cft*ee*complex(0,1)*G**3*MT**2)/(32.*cw*cmath.pi**2*Lambda**2*sw) + (3*ccc*ctG*cw*ee*complex(0,1)*G**3*MT**2)/(16.*cmath.pi**2*Lambda**2*sw) + (3*ccc*ctG*ee*complex(0,1)*G**3*MT**2*sw)/(16.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_176_63 = Coupling(name = 'R2GC_176_63',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_177_64 = Coupling(name = 'R2GC_177_64',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_177_65 = Coupling(name = 'R2GC_177_65',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_178_66 = Coupling(name = 'R2GC_178_66',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_178_67 = Coupling(name = 'R2GC_178_67',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_179_68 = Coupling(name = 'R2GC_179_68',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_179_69 = Coupling(name = 'R2GC_179_69',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_180_70 = Coupling(name = 'R2GC_180_70',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_180_71 = Coupling(name = 'R2GC_180_71',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_181_72 = Coupling(name = 'R2GC_181_72',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_181_73 = Coupling(name = 'R2GC_181_73',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_182_74 = Coupling(name = 'R2GC_182_74',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_182_75 = Coupling(name = 'R2GC_182_75',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_183_76 = Coupling(name = 'R2GC_183_76',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_183_77 = Coupling(name = 'R2GC_183_77',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_184_78 = Coupling(name = 'R2GC_184_78',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_185_79 = Coupling(name = 'R2GC_185_79',
                       value = '(complex(0,1)*G**2*gp**2)/(48.*cmath.pi**2)',
                       order = {'EFT':1.,'QCD':2})

R2GC_186_80 = Coupling(name = 'R2GC_186_80',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_187_81 = Coupling(name = 'R2GC_187_81',
                       value = '(ee*complex(0,1)*G**2*gp)/(48.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'EFT':0.5,'QCD':2,'QED':1})

R2GC_201_82 = Coupling(name = 'R2GC_201_82',
                       value = '(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw) - (5*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(54.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_202_83 = Coupling(name = 'R2GC_202_83',
                       value = '(-7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw) + (35*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(108.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_203_84 = Coupling(name = 'R2GC_203_84',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2) + (ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2) + (17*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(216.*cw**2*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_204_85 = Coupling(name = 'R2GC_204_85',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2) - (7*ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(48.*cmath.pi**2*Lambda**2*sw**2) - (119*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(432.*cw**2*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_206_86 = Coupling(name = 'R2GC_206_86',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_206_87 = Coupling(name = 'R2GC_206_87',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_207_88 = Coupling(name = 'R2GC_207_88',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_207_89 = Coupling(name = 'R2GC_207_89',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_208_90 = Coupling(name = 'R2GC_208_90',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_208_91 = Coupling(name = 'R2GC_208_91',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_209_92 = Coupling(name = 'R2GC_209_92',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_210_93 = Coupling(name = 'R2GC_210_93',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_210_94 = Coupling(name = 'R2GC_210_94',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_216_95 = Coupling(name = 'R2GC_216_95',
                       value = '-(complex(0,1)*G**2*gp)/(6.*cmath.pi**2)',
                       order = {'EFT':0.5,'QCD':2})

R2GC_217_96 = Coupling(name = 'R2GC_217_96',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_222_97 = Coupling(name = 'R2GC_222_97',
                       value = '-(ccc*ctG*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2})

R2GC_228_98 = Coupling(name = 'R2GC_228_98',
                       value = '-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_229_99 = Coupling(name = 'R2GC_229_99',
                       value = '-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_230_100 = Coupling(name = 'R2GC_230_100',
                        value = '(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw) - (17*ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':2,'QED':1})

R2GC_231_101 = Coupling(name = 'R2GC_231_101',
                        value = '(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) + (5*ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw) + (7*ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':2,'QED':1})

R2GC_233_102 = Coupling(name = 'R2GC_233_102',
                        value = 'G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_233_103 = Coupling(name = 'R2GC_233_103',
                        value = '(11*G**3)/(64.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_234_104 = Coupling(name = 'R2GC_234_104',
                        value = '-G**3/(24.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_234_105 = Coupling(name = 'R2GC_234_105',
                        value = '(-11*G**3)/(64.*cmath.pi**2)',
                        order = {'QCD':3})

R2GC_235_106 = Coupling(name = 'R2GC_235_106',
                        value = '(11*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_235_107 = Coupling(name = 'R2GC_235_107',
                        value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_236_108 = Coupling(name = 'R2GC_236_108',
                        value = '(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_236_109 = Coupling(name = 'R2GC_236_109',
                        value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_237_110 = Coupling(name = 'R2GC_237_110',
                        value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_237_111 = Coupling(name = 'R2GC_237_111',
                        value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_238_112 = Coupling(name = 'R2GC_238_112',
                        value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_238_113 = Coupling(name = 'R2GC_238_113',
                        value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_242_114 = Coupling(name = 'R2GC_242_114',
                        value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

R2GC_244_115 = Coupling(name = 'R2GC_244_115',
                        value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_245_116 = Coupling(name = 'R2GC_245_116',
                        value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_247_117 = Coupling(name = 'R2GC_247_117',
                        value = '(ccc*ctG*G**3*MT**2)/(8.*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':3})

R2GC_248_118 = Coupling(name = 'R2GC_248_118',
                        value = '-(ccc*ctG*G**3*MT**2)/(8.*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':3})

R2GC_249_119 = Coupling(name = 'R2GC_249_119',
                        value = '(ccc*ctG*complex(0,1)*G**4*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':4})

R2GC_250_120 = Coupling(name = 'R2GC_250_120',
                        value = '-(ccc*ctG*complex(0,1)*G**4*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':4})

R2GC_253_121 = Coupling(name = 'R2GC_253_121',
                        value = '(3*ccc*ctG*complex(0,1)*G**4*MT)/(64.*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':4})

R2GC_254_122 = Coupling(name = 'R2GC_254_122',
                        value = '(-3*ccc*ctG*complex(0,1)*G**4*MT)/(64.*cmath.pi**2*Lambda**2)',
                        order = {'EFT':1,'QCD':4})

R2GC_261_123 = Coupling(name = 'R2GC_261_123',
                        value = '(7*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                        order = {'EFT':1,'QCD':2,'QED':1})

R2GC_263_124 = Coupling(name = 'R2GC_263_124',
                        value = '(5*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                        order = {'EFT':1,'QCD':2,'QED':1})

R2GC_67_125 = Coupling(name = 'R2GC_67_125',
                       value = '-(ccc*ctG*complex(0,1)*G**2*MT)/(2.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2})

R2GC_68_126 = Coupling(name = 'R2GC_68_126',
                       value = '(4*ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_69_127 = Coupling(name = 'R2GC_69_127',
                       value = '(-14*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(27.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_70_128 = Coupling(name = 'R2GC_70_128',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_71_129 = Coupling(name = 'R2GC_71_129',
                       value = '(7*ccc*ctG*complex(0,1)*G**3*MT)/(16.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3})

R2GC_72_130 = Coupling(name = 'R2GC_72_130',
                       value = '(-7*ccc*ctG*ee*complex(0,1)*G**3*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_73_131 = Coupling(name = 'R2GC_73_131',
                       value = '(-65*ccc*ctG*ee*complex(0,1)*G**3*MT)/(288.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_74_132 = Coupling(name = 'R2GC_74_132',
                       value = '(-17*ccc*ctG*complex(0,1)*G**4*MT)/(192.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_75_133 = Coupling(name = 'R2GC_75_133',
                       value = '(-11*ccc*ctG*complex(0,1)*G**4*MT)/(96.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_76_134 = Coupling(name = 'R2GC_76_134',
                       value = '(17*ccc*ctG*complex(0,1)*G**4*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_77_135 = Coupling(name = 'R2GC_77_135',
                       value = '(-37*ccc*ctG*complex(0,1)*G**4*MT)/(72.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_78_136 = Coupling(name = 'R2GC_78_136',
                       value = '(ccc*ctG*complex(0,1)*G**2*gp*MT)/(9.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2})

R2GC_79_137 = Coupling(name = 'R2GC_79_137',
                       value = '(5*ccc*ctG*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2})

R2GC_80_138 = Coupling(name = 'R2GC_80_138',
                       value = '(7*ccc*ctG*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2})

R2GC_81_139 = Coupling(name = 'R2GC_81_139',
                       value = '(2*ccc*ctG*complex(0,1)*G**2*gp*MT)/(9.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2})

R2GC_82_140 = Coupling(name = 'R2GC_82_140',
                       value = '(11*ccc*ctG*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2})

R2GC_83_141 = Coupling(name = 'R2GC_83_141',
                       value = '(13*ccc*ctG*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2})

R2GC_84_142 = Coupling(name = 'R2GC_84_142',
                       value = '-(ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(18.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_85_143 = Coupling(name = 'R2GC_85_143',
                       value = '-(ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(9.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_86_144 = Coupling(name = 'R2GC_86_144',
                       value = '(-5*ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_87_145 = Coupling(name = 'R2GC_87_145',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(6.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_88_146 = Coupling(name = 'R2GC_88_146',
                       value = '(-7*ccc*ctG*ee*complex(0,1)*G**2*gp*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':2,'QED':1})

R2GC_89_147 = Coupling(name = 'R2GC_89_147',
                       value = '(ccc*ctG*complex(0,1)*G**3*gp*MT)/(96.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':3})

R2GC_90_148 = Coupling(name = 'R2GC_90_148',
                       value = '(-5*ccc*ctG*complex(0,1)*G**3*gp*MT)/(96.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':3})

R2GC_91_149 = Coupling(name = 'R2GC_91_149',
                       value = '(-25*ccc*ctG*complex(0,1)*G**3*gp*MT)/(192.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':3})

R2GC_92_150 = Coupling(name = 'R2GC_92_150',
                       value = '(-5*ccc*ctG*complex(0,1)*G**3*gp*MT)/(24.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':3})

R2GC_93_151 = Coupling(name = 'R2GC_93_151',
                       value = '(-55*ccc*ctG*complex(0,1)*G**3*gp*MT)/(192.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':3})

R2GC_94_152 = Coupling(name = 'R2GC_94_152',
                       value = '(-29*ccc*ctG*complex(0,1)*G**3*gp*MT)/(96.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1.5,'QCD':3})

R2GC_95_153 = Coupling(name = 'R2GC_95_153',
                       value = '-(ccc*ctG*complex(0,1)*G**2*gp**2*MT)/(18.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':2.,'QCD':2})

R2GC_96_154 = Coupling(name = 'R2GC_96_154',
                       value = '(-7*ccc*ctG*complex(0,1)*G**2*gp**2*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':2.,'QCD':2})

R2GC_97_155 = Coupling(name = 'R2GC_97_155',
                       value = '(2*ccc*ctG*complex(0,1)*G**2*gp**2*MT)/(9.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':2.,'QCD':2})

R2GC_98_156 = Coupling(name = 'R2GC_98_156',
                       value = '-(ccc*ctG*complex(0,1)*G**2*gp**2*MT)/(4.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':2.,'QCD':2})

R2GC_99_157 = Coupling(name = 'R2GC_99_157',
                       value = '-(ccc*ctG*complex(0,1)*G**2*gp**2*MT)/(3.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':2.,'QCD':2})

UVGC_188_1 = Coupling(name = 'UVGC_188_1',
                      value = {-1:'(-22*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(81.*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_189_2 = Coupling(name = 'UVGC_189_2',
                      value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_190_3 = Coupling(name = 'UVGC_190_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_191_4 = Coupling(name = 'UVGC_191_4',
                      value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_193_5 = Coupling(name = 'UVGC_193_5',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_198_6 = Coupling(name = 'UVGC_198_6',
                      value = {-1:'(complex(0,1)*G**2)/(6.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_201_7 = Coupling(name = 'UVGC_201_7',
                      value = {-1:'(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(27.*cmath.pi**2*Lambda**2*sw) - (5*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(81.*cw*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_202_8 = Coupling(name = 'UVGC_202_8',
                      value = {-1:'(-11*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(108.*cmath.pi**2*Lambda**2*sw) + (55*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(324.*cw*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_203_9 = Coupling(name = 'UVGC_203_9',
                      value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(54.*cmath.pi**2*Lambda**2) + (ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2) + (17*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(324.*cw**2*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_204_10 = Coupling(name = 'UVGC_204_10',
                       value = {-1:'(11*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(216.*cmath.pi**2*Lambda**2) - (11*ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(144.*cmath.pi**2*Lambda**2*sw**2) - (187*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(1296.*cw**2*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_205_11 = Coupling(name = 'UVGC_205_11',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_205_12 = Coupling(name = 'UVGC_205_12',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_206_13 = Coupling(name = 'UVGC_206_13',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_206_14 = Coupling(name = 'UVGC_206_14',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_207_15 = Coupling(name = 'UVGC_207_15',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_207_16 = Coupling(name = 'UVGC_207_16',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_209_17 = Coupling(name = 'UVGC_209_17',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_209_18 = Coupling(name = 'UVGC_209_18',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_210_19 = Coupling(name = 'UVGC_210_19',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_210_20 = Coupling(name = 'UVGC_210_20',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_211_21 = Coupling(name = 'UVGC_211_21',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_211_22 = Coupling(name = 'UVGC_211_22',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_211_23 = Coupling(name = 'UVGC_211_23',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_211_24 = Coupling(name = 'UVGC_211_24',
                       value = {-1:'(-3*complex(0,1)*G**3)/(16.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_216_25 = Coupling(name = 'UVGC_216_25',
                       value = {-1:'(complex(0,1)*G**2*gp)/(24.*cmath.pi**2)'},
                       order = {'EFT':0.5,'QCD':2})

UVGC_216_26 = Coupling(name = 'UVGC_216_26',
                       value = {-1:'-(complex(0,1)*G**2*gp)/(12.*cmath.pi**2)'},
                       order = {'EFT':0.5,'QCD':2})

UVGC_217_27 = Coupling(name = 'UVGC_217_27',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_217_28 = Coupling(name = 'UVGC_217_28',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_220_29 = Coupling(name = 'UVGC_220_29',
                       value = {-1:'-(complex(0,1)*G**2*gp)/(8.*cmath.pi**2)',0:'-(complex(0,1)*G**2*gp)/(6.*cmath.pi**2) + (complex(0,1)*G**2*gp*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                       order = {'EFT':0.5,'QCD':2})

UVGC_221_30 = Coupling(name = 'UVGC_221_30',
                       value = {-1:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2)',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_222_31 = Coupling(name = 'UVGC_222_31',
                       value = {-1:'(ccc*ctG*complex(0,1)*G**2*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_223_32 = Coupling(name = 'UVGC_223_32',
                       value = {-1:'-(ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(9.*cmath.pi**2*Lambda**2) + (2*ccc*ctG*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(3.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_224_33 = Coupling(name = 'UVGC_224_33',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**3*MT**2)/(6.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT**2*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_225_34 = Coupling(name = 'UVGC_225_34',
                       value = {-1:'(3*ccc*ctG*complex(0,1)*G**2*MT**3)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**3)/(2.*cmath.pi**2*Lambda**2) - (3*ccc*ctG*complex(0,1)*G**2*MT**3*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_226_35 = Coupling(name = 'UVGC_226_35',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_227_36 = Coupling(name = 'UVGC_227_36',
                       value = {-1:'(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_228_37 = Coupling(name = 'UVGC_228_37',
                       value = {-1:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cff*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_229_38 = Coupling(name = 'UVGC_229_38',
                       value = {-1:'-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_230_39 = Coupling(name = 'UVGC_230_39',
                       value = {-1:'(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(12.*cw*cmath.pi**2*Lambda**2)',0:'(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2) - (ccc*cfQ1*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw*reglog(MT/MU_R))/(6.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_231_40 = Coupling(name = 'UVGC_231_40',
                       value = {-1:'(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(3.*cw*cmath.pi**2*Lambda**2)',0:'(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(9.*cw*cmath.pi**2*Lambda**2) - (ccc*cft*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) - (2*ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_232_41 = Coupling(name = 'UVGC_232_41',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_233_42 = Coupling(name = 'UVGC_233_42',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_233_43 = Coupling(name = 'UVGC_233_43',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_233_44 = Coupling(name = 'UVGC_233_44',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_233_45 = Coupling(name = 'UVGC_233_45',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_234_46 = Coupling(name = 'UVGC_234_46',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_234_47 = Coupling(name = 'UVGC_234_47',
                       value = {-1:'(-21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_234_48 = Coupling(name = 'UVGC_234_48',
                       value = {-1:'-G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_234_49 = Coupling(name = 'UVGC_234_49',
                       value = {-1:'-G**3/(24.*cmath.pi**2)',0:'(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_235_50 = Coupling(name = 'UVGC_235_50',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_235_51 = Coupling(name = 'UVGC_235_51',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_235_52 = Coupling(name = 'UVGC_235_52',
                       value = {-1:'(17*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_235_53 = Coupling(name = 'UVGC_235_53',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_236_54 = Coupling(name = 'UVGC_236_54',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_236_55 = Coupling(name = 'UVGC_236_55',
                       value = {-1:'(5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_237_56 = Coupling(name = 'UVGC_237_56',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_237_57 = Coupling(name = 'UVGC_237_57',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_237_58 = Coupling(name = 'UVGC_237_58',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_237_59 = Coupling(name = 'UVGC_237_59',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_238_60 = Coupling(name = 'UVGC_238_60',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_238_61 = Coupling(name = 'UVGC_238_61',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_239_62 = Coupling(name = 'UVGC_239_62',
                       value = {0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_240_63 = Coupling(name = 'UVGC_240_63',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_241_64 = Coupling(name = 'UVGC_241_64',
                       value = {-1:'(-7*complex(0,1)*G**3)/(16.*cmath.pi**2)',0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_242_65 = Coupling(name = 'UVGC_242_65',
                       value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_243_66 = Coupling(name = 'UVGC_243_66',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',0:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_244_67 = Coupling(name = 'UVGC_244_67',
                       value = {-1:'-(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'-(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_245_68 = Coupling(name = 'UVGC_245_68',
                       value = {-1:'(ee*complex(0,1)*G**2*sw)/(6.*cw*cmath.pi**2)',0:'(2*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_246_69 = Coupling(name = 'UVGC_246_69',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**2*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_247_70 = Coupling(name = 'UVGC_247_70',
                       value = {-1:'(ccc*ctG*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*G**3*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_248_71 = Coupling(name = 'UVGC_248_71',
                       value = {-1:'-(ccc*ctG*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*G**3*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_249_72 = Coupling(name = 'UVGC_249_72',
                       value = {-1:'(ccc*ctG*complex(0,1)*G**4*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**4*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_250_73 = Coupling(name = 'UVGC_250_73',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**4*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**4*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_251_74 = Coupling(name = 'UVGC_251_74',
                       value = {-1:'-(ccc*ctB*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2) + (2*ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctB*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctB*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctB*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2) + (4*ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(9.*cmath.pi**2*Lambda**2) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_252_75 = Coupling(name = 'UVGC_252_75',
                       value = {-1:'(-43*ccc*ctG*complex(0,1)*G**3*MT)/(96.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**3*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_253_76 = Coupling(name = 'UVGC_253_76',
                       value = {-1:'(11*ccc*ctG*complex(0,1)*G**4*MT)/(16.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**4*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**4*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**4*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_254_77 = Coupling(name = 'UVGC_254_77',
                       value = {-1:'(-11*ccc*ctG*complex(0,1)*G**4*MT)/(16.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**4*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**4*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**4*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_255_78 = Coupling(name = 'UVGC_255_78',
                       value = {-1:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_256_79 = Coupling(name = 'UVGC_256_79',
                       value = {-1:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_257_80 = Coupling(name = 'UVGC_257_80',
                       value = {-1:'-(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_258_81 = Coupling(name = 'UVGC_258_81',
                       value = {-1:'(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_259_82 = Coupling(name = 'UVGC_259_82',
                       value = {-1:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_260_83 = Coupling(name = 'UVGC_260_83',
                       value = {-1:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_261_84 = Coupling(name = 'UVGC_261_84',
                       value = {-1:'-(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_262_85 = Coupling(name = 'UVGC_262_85',
                       value = {-1:'(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_264_86 = Coupling(name = 'UVGC_264_86',
                       value = {-1:'-(cbW*ccc*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw)',0:'-(cbW*ccc*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_265_87 = Coupling(name = 'UVGC_265_87',
                       value = {-1:'(cbW*ccc*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw)',0:'(cbW*ccc*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_266_88 = Coupling(name = 'UVGC_266_88',
                       value = {-1:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_267_89 = Coupling(name = 'UVGC_267_89',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_268_90 = Coupling(name = 'UVGC_268_90',
                       value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_269_91 = Coupling(name = 'UVGC_269_91',
                       value = {-1:'(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_270_92 = Coupling(name = 'UVGC_270_92',
                       value = {-1:'(ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw) - (ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(6.*cw*cmath.pi**2*Lambda**2) - (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(36.*cw*cmath.pi**2*Lambda**2)',0:'-(ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(3.*cw*cmath.pi**2*Lambda**2) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2) + (ccc*ctG*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(6.*cw*cmath.pi**2*Lambda**2) - (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(18.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_271_93 = Coupling(name = 'UVGC_271_93',
                       value = {-1:'-(ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(6.*cw*cmath.pi**2*Lambda**2) + (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(36.*cw*cmath.pi**2*Lambda**2)',0:'(ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(3.*cw*cmath.pi**2*Lambda**2) - (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) - (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(6.*cw*cmath.pi**2*Lambda**2) + (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(18.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

