# This file was automatically created by FeynRules 2.4.61
# Mathematica version: 10.4.0 for Linux x86 (64-bit) (February 26, 2016)
# Date: Tue 17 Jul 2018 21:08:48


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

QNP = CouplingOrder(name = 'QNP',
                    expansion_order = 2,
                    hierarchy = 1)

