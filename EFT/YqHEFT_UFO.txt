Requestor: Patrick Dougan, Hannes Mildner
Contents: Dim-6 operators modifying Yukawa couplings
Webpage: https://feynrules.irmp.ucl.ac.be/wiki/YqHEFT
Source: https://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/YqHEFT/YqHEFT_UFO.zip
Paper: https://arxiv.org/abs/2011.09551
JIRA: https://its.cern.ch/jira/browse/AGENE-2111
