# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Wed 14 Sep 2016 20:40:23


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '(-25*ccc*ctG*ee*complex(0,1)*G**3*MT)/(192.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':3,'QED':1})

R2GC_101_2 = Coupling(name = 'R2GC_101_2',
                      value = '(-5*ccc*ctG*ee*complex(0,1)*G**3*MT)/(24.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                      order = {'EFT':1,'QCD':3,'QED':1})

R2GC_102_3 = Coupling(name = 'R2GC_102_3',
                      value = '(-5*ccc*ctG*cw*ee*complex(0,1)*G**3*MT)/(48.*cmath.pi**2*Lambda**2*sw)',
                      order = {'EFT':1,'QCD':3,'QED':1})

R2GC_103_4 = Coupling(name = 'R2GC_103_4',
                      value = '(ccc*cfb*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_104_5 = Coupling(name = 'R2GC_104_5',
                      value = '-(ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_105_6 = Coupling(name = 'R2GC_105_6',
                      value = '(-7*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(24.*cw*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_106_7 = Coupling(name = 'R2GC_106_7',
                      value = '(35*ccc*ctG*ee*complex(0,1)*G**3*MT*sw)/(288.*cw*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3,'QED':1})

R2GC_107_8 = Coupling(name = 'R2GC_107_8',
                      value = '(-17*ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_108_9 = Coupling(name = 'R2GC_108_9',
                      value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_109_10 = Coupling(name = 'R2GC_109_10',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_110_11 = Coupling(name = 'R2GC_110_11',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_111_12 = Coupling(name = 'R2GC_111_12',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_112_13 = Coupling(name = 'R2GC_112_13',
                       value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_114_14 = Coupling(name = 'R2GC_114_14',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_115_15 = Coupling(name = 'R2GC_115_15',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_117_16 = Coupling(name = 'R2GC_117_16',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_122_17 = Coupling(name = 'R2GC_122_17',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_126_18 = Coupling(name = 'R2GC_126_18',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) - (5*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_127_19 = Coupling(name = 'R2GC_127_19',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) + (5*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_128_20 = Coupling(name = 'R2GC_128_20',
                       value = '-(ccc*cfb*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cfQ1*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cft*ee**2*complex(0,1)*G**2*MT**2)/(36.*cw**2*cmath.pi**2*Lambda**2) + (ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(24.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_129_21 = Coupling(name = 'R2GC_129_21',
                       value = '(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_130_22 = Coupling(name = 'R2GC_130_22',
                       value = '(ccc*cfb*ee**2*complex(0,1)*G**2*MT**2)/(144.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee**2*complex(0,1)*G**2*MT**2)/(144.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(48.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee**2*complex(0,1)*G**2*MT**2)/(72.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_131_23 = Coupling(name = 'R2GC_131_23',
                       value = '-(ccc*cfb*ee*complex(0,1)*G**3*MT**2)/(96.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee*complex(0,1)*G**3*MT**2)/(48.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*complex(0,1)*G**3*MT**2)/(96.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_132_24 = Coupling(name = 'R2GC_132_24',
                       value = '(-7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw) + (35*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw)/(108.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_133_25 = Coupling(name = 'R2GC_133_25',
                       value = '-(ccc*cfb*ee*G**2*MT**2)/(24.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ1*ee*G**2*MT**2)/(12.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*G**2*MT**2)/(24.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*G**2*MT**2)/(16.*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*ee*G**2*MT**2*sw)/(16.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_134_26 = Coupling(name = 'R2GC_134_26',
                       value = '(3*ccc*cfb*ee*complex(0,1)*G**3*MT**2)/(32.*cw*cmath.pi**2*Lambda**2*sw) - (3*ccc*cfQ1*ee*complex(0,1)*G**3*MT**2)/(16.*cw*cmath.pi**2*Lambda**2*sw) + (3*ccc*cft*ee*complex(0,1)*G**3*MT**2)/(32.*cw*cmath.pi**2*Lambda**2*sw) + (3*ccc*ctG*cw*ee*complex(0,1)*G**3*MT**2)/(16.*cmath.pi**2*Lambda**2*sw) + (3*ccc*ctG*ee*complex(0,1)*G**3*MT**2*sw)/(16.*cw*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3,'QED':1})

R2GC_135_27 = Coupling(name = 'R2GC_135_27',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2) - (7*ccc*ctG*cw**2*ee**2*complex(0,1)*G**2*MT)/(48.*cmath.pi**2*Lambda**2*sw**2) - (119*ccc*ctG*ee**2*complex(0,1)*G**2*MT*sw**2)/(432.*cw**2*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_136_28 = Coupling(name = 'R2GC_136_28',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_137_29 = Coupling(name = 'R2GC_137_29',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_137_30 = Coupling(name = 'R2GC_137_30',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_138_31 = Coupling(name = 'R2GC_138_31',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_138_32 = Coupling(name = 'R2GC_138_32',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_139_33 = Coupling(name = 'R2GC_139_33',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_139_34 = Coupling(name = 'R2GC_139_34',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_140_35 = Coupling(name = 'R2GC_140_35',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_140_36 = Coupling(name = 'R2GC_140_36',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_141_37 = Coupling(name = 'R2GC_141_37',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_141_38 = Coupling(name = 'R2GC_141_38',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_142_39 = Coupling(name = 'R2GC_142_39',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_142_40 = Coupling(name = 'R2GC_142_40',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_143_41 = Coupling(name = 'R2GC_143_41',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_143_42 = Coupling(name = 'R2GC_143_42',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_144_43 = Coupling(name = 'R2GC_144_43',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_145_44 = Coupling(name = 'R2GC_145_44',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_147_45 = Coupling(name = 'R2GC_147_45',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_161_46 = Coupling(name = 'R2GC_161_46',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_162_47 = Coupling(name = 'R2GC_162_47',
                       value = '(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_164_48 = Coupling(name = 'R2GC_164_48',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_164_49 = Coupling(name = 'R2GC_164_49',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_165_50 = Coupling(name = 'R2GC_165_50',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_165_51 = Coupling(name = 'R2GC_165_51',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_166_52 = Coupling(name = 'R2GC_166_52',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_166_53 = Coupling(name = 'R2GC_166_53',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_167_54 = Coupling(name = 'R2GC_167_54',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_168_55 = Coupling(name = 'R2GC_168_55',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_168_56 = Coupling(name = 'R2GC_168_56',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_169_57 = Coupling(name = 'R2GC_169_57',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_177_58 = Coupling(name = 'R2GC_177_58',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_179_59 = Coupling(name = 'R2GC_179_59',
                       value = '-(ccc*ctG*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2})

R2GC_183_60 = Coupling(name = 'R2GC_183_60',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_184_61 = Coupling(name = 'R2GC_184_61',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_185_62 = Coupling(name = 'R2GC_185_62',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_186_63 = Coupling(name = 'R2GC_186_63',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(8.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_187_64 = Coupling(name = 'R2GC_187_64',
                       value = '-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_188_65 = Coupling(name = 'R2GC_188_65',
                       value = '(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_189_66 = Coupling(name = 'R2GC_189_66',
                       value = '(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(6.*cw*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_190_67 = Coupling(name = 'R2GC_190_67',
                       value = '-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icff*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_191_68 = Coupling(name = 'R2GC_191_68',
                       value = '-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icff*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_194_69 = Coupling(name = 'R2GC_194_69',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_194_70 = Coupling(name = 'R2GC_194_70',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_195_71 = Coupling(name = 'R2GC_195_71',
                       value = '(11*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_195_72 = Coupling(name = 'R2GC_195_72',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_196_73 = Coupling(name = 'R2GC_196_73',
                       value = '(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_196_74 = Coupling(name = 'R2GC_196_74',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_197_75 = Coupling(name = 'R2GC_197_75',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_197_76 = Coupling(name = 'R2GC_197_76',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_198_77 = Coupling(name = 'R2GC_198_77',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_198_78 = Coupling(name = 'R2GC_198_78',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_202_79 = Coupling(name = 'R2GC_202_79',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_204_80 = Coupling(name = 'R2GC_204_80',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                       order = {'QCD':2,'QED':1})

R2GC_207_81 = Coupling(name = 'R2GC_207_81',
                       value = '(ccc*ctG*G**3*MT**2)/(8.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3})

R2GC_208_82 = Coupling(name = 'R2GC_208_82',
                       value = '(ccc*ctG*complex(0,1)*G**4*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_209_83 = Coupling(name = 'R2GC_209_83',
                       value = '-(ccc*ctG*complex(0,1)*G**4*MT**2)/(4.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_214_84 = Coupling(name = 'R2GC_214_84',
                       value = '(3*ccc*ctG*complex(0,1)*G**4*MT)/(64.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_215_85 = Coupling(name = 'R2GC_215_85',
                       value = '(-3*ccc*ctG*complex(0,1)*G**4*MT)/(64.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_227_86 = Coupling(name = 'R2GC_227_86',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) - (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_228_87 = Coupling(name = 'R2GC_228_87',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) + (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(24.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_229_88 = Coupling(name = 'R2GC_229_88',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) + (7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_230_89 = Coupling(name = 'R2GC_230_89',
                       value = '(7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cw*cmath.pi**2*Lambda**2*cmath.sqrt(2)) - (7*ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_240_90 = Coupling(name = 'R2GC_240_90',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*MT*cmath.sqrt(2))/(9.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_242_91 = Coupling(name = 'R2GC_242_91',
                       value = '(ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_243_92 = Coupling(name = 'R2GC_243_92',
                       value = '(-5*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_244_93 = Coupling(name = 'R2GC_244_93',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_246_94 = Coupling(name = 'R2GC_246_94',
                       value = '(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_81_95 = Coupling(name = 'R2GC_81_95',
                      value = '-(ccc*ctG*complex(0,1)*G**2*MT)/(2.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2})

R2GC_82_96 = Coupling(name = 'R2GC_82_96',
                      value = '(4*ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':1})

R2GC_83_97 = Coupling(name = 'R2GC_83_97',
                      value = '(-14*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(27.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':2,'QED':2})

R2GC_84_98 = Coupling(name = 'R2GC_84_98',
                      value = '(7*ccc*ctG*complex(0,1)*G**3*MT)/(16.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3})

R2GC_85_99 = Coupling(name = 'R2GC_85_99',
                      value = '(-65*ccc*ctG*ee*complex(0,1)*G**3*MT)/(288.*cmath.pi**2*Lambda**2)',
                      order = {'EFT':1,'QCD':3,'QED':1})

R2GC_86_100 = Coupling(name = 'R2GC_86_100',
                       value = '(-17*ccc*ctG*complex(0,1)*G**4*MT)/(192.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_87_101 = Coupling(name = 'R2GC_87_101',
                       value = '(-11*ccc*ctG*complex(0,1)*G**4*MT)/(96.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_88_102 = Coupling(name = 'R2GC_88_102',
                       value = '(11*ccc*ctG*complex(0,1)*G**4*MT)/(96.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_89_103 = Coupling(name = 'R2GC_89_103',
                       value = '(17*ccc*ctG*complex(0,1)*G**4*MT)/(36.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_90_104 = Coupling(name = 'R2GC_90_104',
                       value = '(-37*ccc*ctG*complex(0,1)*G**4*MT)/(72.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':4})

R2GC_91_105 = Coupling(name = 'R2GC_91_105',
                       value = '(2*ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(9.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_92_106 = Coupling(name = 'R2GC_92_106',
                       value = '(7*ccc*ctG*complex(0,1)*G**3*MT**2)/(48.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':3})

R2GC_93_107 = Coupling(name = 'R2GC_93_107',
                       value = '(7*ccc*ctG*complex(0,1)*G**2*MT**3)/(6.*cmath.pi**2*Lambda**2)',
                       order = {'EFT':1,'QCD':2})

R2GC_94_108 = Coupling(name = 'R2GC_94_108',
                       value = '(ccc*cfQ3*ee**2*complex(0,1)*G**2*MT**2)/(24.*cmath.pi**2*Lambda**2*sw**2)',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_95_109 = Coupling(name = 'R2GC_95_109',
                       value = '(11*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_96_110 = Coupling(name = 'R2GC_96_110',
                       value = '(13*ccc*ctG*ee*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_97_111 = Coupling(name = 'R2GC_97_111',
                       value = '(11*ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw)',
                       order = {'EFT':1,'QCD':2,'QED':1})

R2GC_98_112 = Coupling(name = 'R2GC_98_112',
                       value = '-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

R2GC_99_113 = Coupling(name = 'R2GC_99_113',
                       value = '(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_149_1 = Coupling(name = 'UVGC_149_1',
                      value = {0:'(ccc*ee*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2)'},
                      order = {'EFT':1,'QCD':2,'QED':1})

UVGC_150_2 = Coupling(name = 'UVGC_150_2',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_151_3 = Coupling(name = 'UVGC_151_3',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_153_4 = Coupling(name = 'UVGC_153_4',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_158_5 = Coupling(name = 'UVGC_158_5',
                      value = {-1:'(complex(0,1)*G**2)/(6.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_161_6 = Coupling(name = 'UVGC_161_6',
                      value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_162_7 = Coupling(name = 'UVGC_162_7',
                      value = {-1:'(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2*sw**2)'},
                      order = {'EFT':1,'QCD':2,'QED':2})

UVGC_163_8 = Coupling(name = 'UVGC_163_8',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_163_9 = Coupling(name = 'UVGC_163_9',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_164_10 = Coupling(name = 'UVGC_164_10',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_164_11 = Coupling(name = 'UVGC_164_11',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_165_12 = Coupling(name = 'UVGC_165_12',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_165_13 = Coupling(name = 'UVGC_165_13',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_167_14 = Coupling(name = 'UVGC_167_14',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_167_15 = Coupling(name = 'UVGC_167_15',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_168_16 = Coupling(name = 'UVGC_168_16',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_168_17 = Coupling(name = 'UVGC_168_17',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_169_18 = Coupling(name = 'UVGC_169_18',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_170_19 = Coupling(name = 'UVGC_170_19',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_170_20 = Coupling(name = 'UVGC_170_20',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_170_21 = Coupling(name = 'UVGC_170_21',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_170_22 = Coupling(name = 'UVGC_170_22',
                       value = {-1:'(-3*complex(0,1)*G**3)/(16.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_177_23 = Coupling(name = 'UVGC_177_23',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_177_24 = Coupling(name = 'UVGC_177_24',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_179_25 = Coupling(name = 'UVGC_179_25',
                       value = {-1:'(ccc*ctG*complex(0,1)*G**2*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_180_26 = Coupling(name = 'UVGC_180_26',
                       value = {-1:'-(ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(9.*cmath.pi**2*Lambda**2) + (2*ccc*ctG*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(3.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_181_27 = Coupling(name = 'UVGC_181_27',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**3*MT**2)/(6.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT**2*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_182_28 = Coupling(name = 'UVGC_182_28',
                       value = {-1:'(3*ccc*ctG*complex(0,1)*G**2*MT**3)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**3)/(2.*cmath.pi**2*Lambda**2) - (3*ccc*ctG*complex(0,1)*G**2*MT**3*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_183_29 = Coupling(name = 'UVGC_183_29',
                       value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(18.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ee**2*G**2*ictW*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (2*ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2) - (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_184_30 = Coupling(name = 'UVGC_184_30',
                       value = {-1:'(-7*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ee**2*G**2*ictW*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(-2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (2*ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2) - (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_185_31 = Coupling(name = 'UVGC_185_31',
                       value = {-1:'(-5*ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(36.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ee**2*G**2*ictW*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (2*ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2) + (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_186_32 = Coupling(name = 'UVGC_186_32',
                       value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(72.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2) - (ccc*ee**2*G**2*ictW*MT)/(6.*cmath.pi**2*Lambda**2*sw**2)',0:'(-2*ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) - (2*ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2) + (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_187_33 = Coupling(name = 'UVGC_187_33',
                       value = {-1:'-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_188_34 = Coupling(name = 'UVGC_188_34',
                       value = {-1:'(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw)',0:'(ccc*cft*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cft*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_189_35 = Coupling(name = 'UVGC_189_35',
                       value = {-1:'(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(4.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(4.*cmath.pi**2*Lambda**2*sw)',0:'(ccc*cfQ1*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2)/(3.*cw*cmath.pi**2*Lambda**2*sw) - (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2)/(12.*cmath.pi**2*Lambda**2*sw) - (ccc*cfQ1*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*cfQ3*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2*sw) + (ccc*ctG*cw*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_190_36 = Coupling(name = 'UVGC_190_36',
                       value = {-1:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icff*MT**2)/(8.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icff*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cff*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icff*MT**2*reglog(MT/MU_R))/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_191_37 = Coupling(name = 'UVGC_191_37',
                       value = {-1:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icff*MT**2)/(8.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*cff*ee*complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icff*MT**2)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*cff*ee*complex(0,1)*G**2*MT**2*reglog(MT/MU_R))/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icff*MT**2*reglog(MT/MU_R))/(4.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_192_38 = Coupling(name = 'UVGC_192_38',
                       value = {-1:'(ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(12.*cw*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw)/(36.*cw*cmath.pi**2*Lambda**2) - (ccc*ctG*ee*complex(0,1)*G**2*MT**2*sw*reglog(MT/MU_R))/(6.*cw*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_193_39 = Coupling(name = 'UVGC_193_39',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_194_40 = Coupling(name = 'UVGC_194_40',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_194_41 = Coupling(name = 'UVGC_194_41',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_194_42 = Coupling(name = 'UVGC_194_42',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_194_43 = Coupling(name = 'UVGC_194_43',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_195_44 = Coupling(name = 'UVGC_195_44',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_195_45 = Coupling(name = 'UVGC_195_45',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_195_46 = Coupling(name = 'UVGC_195_46',
                       value = {-1:'(17*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_195_47 = Coupling(name = 'UVGC_195_47',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_196_48 = Coupling(name = 'UVGC_196_48',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_196_49 = Coupling(name = 'UVGC_196_49',
                       value = {-1:'(5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_197_50 = Coupling(name = 'UVGC_197_50',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_197_51 = Coupling(name = 'UVGC_197_51',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_197_52 = Coupling(name = 'UVGC_197_52',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_197_53 = Coupling(name = 'UVGC_197_53',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_198_54 = Coupling(name = 'UVGC_198_54',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_198_55 = Coupling(name = 'UVGC_198_55',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_199_56 = Coupling(name = 'UVGC_199_56',
                       value = {0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_200_57 = Coupling(name = 'UVGC_200_57',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2)',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_201_58 = Coupling(name = 'UVGC_201_58',
                       value = {-1:'(-7*complex(0,1)*G**3)/(16.*cmath.pi**2)',0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_202_59 = Coupling(name = 'UVGC_202_59',
                       value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_203_60 = Coupling(name = 'UVGC_203_60',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',0:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_204_61 = Coupling(name = 'UVGC_204_61',
                       value = {-1:'-(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw)',0:'-(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw)'},
                       order = {'QCD':2,'QED':1})

UVGC_205_62 = Coupling(name = 'UVGC_205_62',
                       value = {-1:'(ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2)',0:'(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_206_63 = Coupling(name = 'UVGC_206_63',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**2*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**2*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2})

UVGC_207_64 = Coupling(name = 'UVGC_207_64',
                       value = {-1:'(ccc*ctG*G**3*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*G**3*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_208_65 = Coupling(name = 'UVGC_208_65',
                       value = {-1:'(ccc*ctG*complex(0,1)*G**4*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**4*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_209_66 = Coupling(name = 'UVGC_209_66',
                       value = {-1:'-(ccc*ctG*complex(0,1)*G**4*MT**2)/(2.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**4*MT**2*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_210_67 = Coupling(name = 'UVGC_210_67',
                       value = {-1:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2)',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_211_68 = Coupling(name = 'UVGC_211_68',
                       value = {-1:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2)',0:'(cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_212_69 = Coupling(name = 'UVGC_212_69',
                       value = {-1:'-(ccc*ctB*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2) + (2*ccc*ctG*ee*complex(0,1)*G**2*MT)/(9.*cmath.pi**2*Lambda**2) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctB*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctB*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctB*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2) + (4*ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(9.*cmath.pi**2*Lambda**2) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_213_70 = Coupling(name = 'UVGC_213_70',
                       value = {-1:'(-43*ccc*ctG*complex(0,1)*G**3*MT)/(96.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**3*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**3*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':3})

UVGC_214_71 = Coupling(name = 'UVGC_214_71',
                       value = {-1:'(11*ccc*ctG*complex(0,1)*G**4*MT)/(16.*cmath.pi**2*Lambda**2)',0:'(ccc*ctG*complex(0,1)*G**4*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**4*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) - (ccc*ctG*complex(0,1)*G**4*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_215_72 = Coupling(name = 'UVGC_215_72',
                       value = {-1:'(-11*ccc*ctG*complex(0,1)*G**4*MT)/(16.*cmath.pi**2*Lambda**2)',0:'-(ccc*ctG*complex(0,1)*G**4*MT)/(3.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**4*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) + (ccc*ctG*complex(0,1)*G**4*MT*reglog(MU_R/muprime))/(12.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':4})

UVGC_216_73 = Coupling(name = 'UVGC_216_73',
                       value = {-1:'-(ccc*ee*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2)',0:'-(ccc*ee*G**2*icbW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_217_74 = Coupling(name = 'UVGC_217_74',
                       value = {-1:'(ccc*ee*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2)',0:'(ccc*ee*G**2*icbW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_218_75 = Coupling(name = 'UVGC_218_75',
                       value = {-1:'(ccc*ee*G**2*ictB*MT)/(6.*cmath.pi**2*Lambda**2) + (ccc*ee*G**2*ictW*MT)/(6.*cmath.pi**2*Lambda**2)',0:'(ccc*ee*G**2*ictB*MT)/(3.*cmath.pi**2*Lambda**2) - (ccc*ee*G**2*ictB*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) - (ccc*ee*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2) - (ccc*ee*G**2*ictB*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2) - (ccc*ee*G**2*ictW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_219_76 = Coupling(name = 'UVGC_219_76',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_220_77 = Coupling(name = 'UVGC_220_77',
                       value = {-1:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_221_78 = Coupling(name = 'UVGC_221_78',
                       value = {-1:'-(ccc*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'-(ccc*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_222_79 = Coupling(name = 'UVGC_222_79',
                       value = {-1:'(ccc*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2)',0:'(ccc*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw**2)'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_223_80 = Coupling(name = 'UVGC_223_80',
                       value = {-1:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_224_81 = Coupling(name = 'UVGC_224_81',
                       value = {-1:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_225_82 = Coupling(name = 'UVGC_225_82',
                       value = {-1:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_226_83 = Coupling(name = 'UVGC_226_83',
                       value = {-1:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (cbW*ccc*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_227_84 = Coupling(name = 'UVGC_227_84',
                       value = {-1:'-(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_228_85 = Coupling(name = 'UVGC_228_85',
                       value = {-1:'(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_229_86 = Coupling(name = 'UVGC_229_86',
                       value = {-1:'-(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_230_87 = Coupling(name = 'UVGC_230_87',
                       value = {-1:'(ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))',0:'-(ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) - (ccc*cw*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctG*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*ctW*cw*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2)) + (ccc*cw*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw**2*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_231_88 = Coupling(name = 'UVGC_231_88',
                       value = {-1:'(cbW*ccc*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw)',0:'(cbW*ccc*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_232_89 = Coupling(name = 'UVGC_232_89',
                       value = {-1:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_233_90 = Coupling(name = 'UVGC_233_90',
                       value = {-1:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_234_91 = Coupling(name = 'UVGC_234_91',
                       value = {-1:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_235_92 = Coupling(name = 'UVGC_235_92',
                       value = {-1:'-(ccc*cw*ee*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw)',0:'-(ccc*cw*ee*G**2*icbW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw)'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_236_93 = Coupling(name = 'UVGC_236_93',
                       value = {-1:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_237_94 = Coupling(name = 'UVGC_237_94',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_238_95 = Coupling(name = 'UVGC_238_95',
                       value = {-1:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_239_96 = Coupling(name = 'UVGC_239_96',
                       value = {-1:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(cbW*ccc*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*icbW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (cbW*ccc*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*icbW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':2})

UVGC_240_97 = Coupling(name = 'UVGC_240_97',
                       value = {-1:'-(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_241_98 = Coupling(name = 'UVGC_241_98',
                       value = {-1:'(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_242_99 = Coupling(name = 'UVGC_242_99',
                       value = {-1:'(ccc*ctG*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                       order = {'EFT':1,'QCD':2,'QED':1})

UVGC_243_100 = Coupling(name = 'UVGC_243_100',
                        value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                        order = {'EFT':1,'QCD':2,'QED':2})

UVGC_244_101 = Coupling(name = 'UVGC_244_101',
                        value = {-1:'(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                        order = {'EFT':1,'QCD':2,'QED':2})

UVGC_245_102 = Coupling(name = 'UVGC_245_102',
                        value = {-1:'-(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                        order = {'EFT':1,'QCD':2,'QED':2})

UVGC_246_103 = Coupling(name = 'UVGC_246_103',
                        value = {-1:'(ccc*ctG*ee**2*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT)/(12.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))',0:'-(ccc*ctW*ee**2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee**2*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctG*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ctW*ee**2*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee**2*G**2*ictW*MT*reglog(MU_R/muprime))/(3.*cmath.pi**2*Lambda**2*sw*cmath.sqrt(2))'},
                        order = {'EFT':1,'QCD':2,'QED':2})

UVGC_247_104 = Coupling(name = 'UVGC_247_104',
                        value = {-1:'(ccc*ctG*cw*ee*complex(0,1)*G**2*MT)/(12.*cmath.pi**2*Lambda**2*sw) - (ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(6.*cw*cmath.pi**2*Lambda**2) - (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw)/(36.*cw*cmath.pi**2*Lambda**2)',0:'-(ccc*ctW*cw*ee*complex(0,1)*G**2*MT)/(3.*cmath.pi**2*Lambda**2*sw) + (ccc*ctB*ee*complex(0,1)*G**2*MT*sw)/(3.*cw*cmath.pi**2*Lambda**2) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2) + (ccc*ctG*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ctW*cw*ee*complex(0,1)*G**2*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(6.*cw*cmath.pi**2*Lambda**2) - (5*ccc*ctG*ee*complex(0,1)*G**2*MT*sw*reglog(MU_R/muprime))/(18.*cw*cmath.pi**2*Lambda**2)'},
                        order = {'EFT':1,'QCD':2,'QED':1})

UVGC_248_105 = Coupling(name = 'UVGC_248_105',
                        value = {-1:'(ccc*cw*ee*G**2*ictW*MT)/(6.*cmath.pi**2*Lambda**2*sw) - (ccc*ee*G**2*ictB*MT*sw)/(6.*cw*cmath.pi**2*Lambda**2)',0:'(ccc*cw*ee*G**2*ictW*MT)/(3.*cmath.pi**2*Lambda**2*sw) - (ccc*ee*G**2*ictB*MT*sw)/(3.*cw*cmath.pi**2*Lambda**2) - (ccc*cw*ee*G**2*ictW*MT*reglog(MT/MU_R))/(2.*cmath.pi**2*Lambda**2*sw) + (ccc*ee*G**2*ictB*MT*sw*reglog(MT/MU_R))/(2.*cw*cmath.pi**2*Lambda**2) - (ccc*cw*ee*G**2*ictW*MT*reglog(MU_R/muprime))/(6.*cmath.pi**2*Lambda**2*sw) + (ccc*ee*G**2*ictB*MT*sw*reglog(MU_R/muprime))/(6.*cw*cmath.pi**2*Lambda**2)'},
                        order = {'EFT':1,'QCD':2,'QED':1})

