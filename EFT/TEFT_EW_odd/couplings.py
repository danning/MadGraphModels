# This file was automatically created by FeynRules 2.4.38
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Wed 14 Sep 2016 20:40:23


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '-((cbW*ccc*ee*complex(0,1)*MT)/Lambda**2)',
                 order = {'EFT':1,'QED':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '(cbW*ccc*ee*complex(0,1)*MT)/Lambda**2',
                 order = {'EFT':1,'QED':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '(ccc*ctG*complex(0,1)*G*MT)/Lambda**2',
                 order = {'EFT':1,'QCD':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-((ccc*ctG*G**2*MT)/Lambda**2)',
                 order = {'EFT':1,'QCD':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '(ccc*ctG*G**2*MT)/Lambda**2',
                 order = {'EFT':1,'QCD':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '-((ccc*ee*icbW*MT)/Lambda**2)',
                 order = {'EFT':1,'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(ccc*ee*icbW*MT)/Lambda**2',
                 order = {'EFT':1,'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '(ccc*ctB*ee*complex(0,1)*MT)/Lambda**2 + (ccc*ctW*ee*complex(0,1)*MT)/Lambda**2',
                 order = {'EFT':1,'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-((ccc*ee*ictB*MT)/Lambda**2) - (ccc*ee*ictW*MT)/Lambda**2',
                 order = {'EFT':1,'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '-((cbW*ccc*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2)) - (ccc*cw*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '(cbW*ccc*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2) - (ccc*cw*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '-((cbW*ccc*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2)) + (ccc*cw*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '(cbW*ccc*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2) + (ccc*cw*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '-((ccc*ctW*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2)) - (ccc*cw*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '(ccc*ctW*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2) - (ccc*cw*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '-((ccc*ctW*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2)) + (ccc*cw*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '(ccc*ctW*cw*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw**2) + (ccc*cw*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '(cbW*ccc*ee*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) - (ccc*ee*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-((cbW*ccc*ee*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw)) + (ccc*ee*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(cbW*ccc*ee*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) + (ccc*ee*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-((cbW*ccc*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw)) - (ccc*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '(cbW*ccc*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) - (ccc*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-((cbW*ccc*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw)) + (ccc*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '(cbW*ccc*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) + (ccc*ee**2*icbW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = '-((ccc*ctW*ee*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw)) - (ccc*ee*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(ccc*ctW*ee*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) - (ccc*ee*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(ccc*ctW*ee*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) + (ccc*ee*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-((ccc*ctW*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw)) - (ccc*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '(ccc*ctW*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) - (ccc*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-((ccc*ctW*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw)) + (ccc*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ccc*ctW*ee**2*complex(0,1)*MT*cmath.sqrt(2))/(Lambda**2*sw) + (ccc*ee**2*ictW*MT*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '-((ccc*cfQ1*ee*complex(0,1)*MT**2)/(cw*Lambda**2*sw)) - (ccc*cfQ3*ee*complex(0,1)*MT**2)/(cw*Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '-((ccc*cfQ1*ee*complex(0,1)*MT**2)/(cw*Lambda**2*sw)) + (ccc*cfQ3*ee*complex(0,1)*MT**2)/(cw*Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '(ccc*cff*ee*complex(0,1)*MT**2)/(Lambda**2*sw*cmath.sqrt(2)) - (ccc*ee*icff*MT**2)/(Lambda**2*sw*cmath.sqrt(2))',
                 order = {'EFT':1,'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(ccc*cff*ee*complex(0,1)*MT**2)/(Lambda**2*sw*cmath.sqrt(2)) + (ccc*ee*icff*MT**2)/(Lambda**2*sw*cmath.sqrt(2))',
                 order = {'EFT':1,'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-((cbW*ccc*ee**2*complex(0,1)*MT)/(Lambda**2*sw**2))',
                 order = {'EFT':1,'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '(cbW*ccc*ee**2*complex(0,1)*MT)/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '-((ccc*ctW*ee**2*complex(0,1)*MT)/(Lambda**2*sw**2))',
                 order = {'EFT':1,'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '(ccc*ctW*ee**2*complex(0,1)*MT)/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-((ccc*ee**2*icbW*MT)/(Lambda**2*sw**2))',
                 order = {'EFT':1,'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '(ccc*ee**2*icbW*MT)/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-((ccc*ee**2*ictW*MT)/(Lambda**2*sw**2))',
                 order = {'EFT':1,'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '(ccc*ee**2*ictW*MT)/(Lambda**2*sw**2)',
                 order = {'EFT':1,'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '-G',
                order = {'QCD':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '(cbW*ccc*cw*ee*complex(0,1)*MT)/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-((ccc*cw*ee*icbW*MT)/(Lambda**2*sw))',
                 order = {'EFT':1,'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(ccc*cfQ3*ee*complex(0,1)*MT**2*cmath.sqrt(2))/(Lambda**2*sw)',
                 order = {'EFT':1,'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-((ccc*cfb*ee*complex(0,1)*MT**2)/(cw*Lambda**2*sw))',
                 order = {'EFT':1,'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '-((ccc*cft*ee*complex(0,1)*MT**2)/(cw*Lambda**2*sw))',
                 order = {'EFT':1,'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-(ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '(ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '(ccc*ctW*cw*ee*complex(0,1)*MT)/(Lambda**2*sw) - (ccc*ctB*ee*complex(0,1)*MT*sw)/(cw*Lambda**2)',
                 order = {'EFT':1,'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-((ccc*cw*ee*ictW*MT)/(Lambda**2*sw)) + (ccc*ee*ictB*MT*sw)/(cw*Lambda**2)',
                 order = {'EFT':1,'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '-6*complex(0,1)*lam*vev',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = '-6*complex(0,1)*lam',
                order = {'QED':2})

