# This file was automatically created by FeynRules 2.3.36
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Wed 18 May 2022 16:55:01


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_100_2 = Coupling(name = 'R2GC_100_2',
                      value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_101_3 = Coupling(name = 'R2GC_101_3',
                      value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_101_4 = Coupling(name = 'R2GC_101_4',
                      value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_102_5 = Coupling(name = 'R2GC_102_5',
                      value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_102_6 = Coupling(name = 'R2GC_102_6',
                      value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_103_7 = Coupling(name = 'R2GC_103_7',
                      value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_104_8 = Coupling(name = 'R2GC_104_8',
                      value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_107_9 = Coupling(name = 'R2GC_107_9',
                      value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                      order = {'QCD':2,'QED':2})

R2GC_123_10 = Coupling(name = 'R2GC_123_10',
                       value = '-G**3/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_124_11 = Coupling(name = 'R2GC_124_11',
                       value = 'G**3/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_125_12 = Coupling(name = 'R2GC_125_12',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_125_13 = Coupling(name = 'R2GC_125_13',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_126_14 = Coupling(name = 'R2GC_126_14',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_126_15 = Coupling(name = 'R2GC_126_15',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_127_16 = Coupling(name = 'R2GC_127_16',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_127_17 = Coupling(name = 'R2GC_127_17',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_128_18 = Coupling(name = 'R2GC_128_18',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_129_19 = Coupling(name = 'R2GC_129_19',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_129_20 = Coupling(name = 'R2GC_129_20',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_130_21 = Coupling(name = 'R2GC_130_21',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_131_22 = Coupling(name = 'R2GC_131_22',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_132_23 = Coupling(name = 'R2GC_132_23',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_135_24 = Coupling(name = 'R2GC_135_24',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_137_25 = Coupling(name = 'R2GC_137_25',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_140_26 = Coupling(name = 'R2GC_140_26',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_140_27 = Coupling(name = 'R2GC_140_27',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_141_28 = Coupling(name = 'R2GC_141_28',
                       value = '(5*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_142_29 = Coupling(name = 'R2GC_142_29',
                       value = '(3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_147_30 = Coupling(name = 'R2GC_147_30',
                       value = '-G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_147_31 = Coupling(name = 'R2GC_147_31',
                       value = '(-5*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_148_32 = Coupling(name = 'R2GC_148_32',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_149_33 = Coupling(name = 'R2GC_149_33',
                       value = '(-3*G**3)/(16.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_150_34 = Coupling(name = 'R2GC_150_34',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_150_35 = Coupling(name = 'R2GC_150_35',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_151_36 = Coupling(name = 'R2GC_151_36',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_151_37 = Coupling(name = 'R2GC_151_37',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_153_38 = Coupling(name = 'R2GC_153_38',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_154_39 = Coupling(name = 'R2GC_154_39',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_156_40 = Coupling(name = 'R2GC_156_40',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_159_41 = Coupling(name = 'R2GC_159_41',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_162_42 = Coupling(name = 'R2GC_162_42',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_163_43 = Coupling(name = 'R2GC_163_43',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_164_44 = Coupling(name = 'R2GC_164_44',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_165_45 = Coupling(name = 'R2GC_165_45',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_166_46 = Coupling(name = 'R2GC_166_46',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_68_47 = Coupling(name = 'R2GC_68_47',
                      value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_69_48 = Coupling(name = 'R2GC_69_48',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_70_49 = Coupling(name = 'R2GC_70_49',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_71_50 = Coupling(name = 'R2GC_71_50',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_73_51 = Coupling(name = 'R2GC_73_51',
                      value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_74_52 = Coupling(name = 'R2GC_74_52',
                      value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw)',
                      order = {'QCD':2,'QED':1})

R2GC_75_53 = Coupling(name = 'R2GC_75_53',
                      value = '(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_76_54 = Coupling(name = 'R2GC_76_54',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_96_55 = Coupling(name = 'R2GC_96_55',
                      value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_97_56 = Coupling(name = 'R2GC_97_56',
                      value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_97_57 = Coupling(name = 'R2GC_97_57',
                      value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_98_58 = Coupling(name = 'R2GC_98_58',
                      value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_98_59 = Coupling(name = 'R2GC_98_59',
                      value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                      order = {'QCD':3,'QED':1})

R2GC_99_60 = Coupling(name = 'R2GC_99_60',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_99_61 = Coupling(name = 'R2GC_99_61',
                      value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

UVGC_109_1 = Coupling(name = 'UVGC_109_1',
                      value = {-1:'(-3*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_110_2 = Coupling(name = 'UVGC_110_2',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_111_3 = Coupling(name = 'UVGC_111_3',
                      value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_113_4 = Coupling(name = 'UVGC_113_4',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_114_5 = Coupling(name = 'UVGC_114_5',
                      value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_122_6 = Coupling(name = 'UVGC_122_6',
                      value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_122_7 = Coupling(name = 'UVGC_122_7',
                      value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_123_8 = Coupling(name = 'UVGC_123_8',
                      value = {-1:'(-11*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_123_9 = Coupling(name = 'UVGC_123_9',
                      value = {-1:'G**3/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_124_10 = Coupling(name = 'UVGC_124_10',
                       value = {-1:'(11*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_124_11 = Coupling(name = 'UVGC_124_11',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_125_12 = Coupling(name = 'UVGC_125_12',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_125_13 = Coupling(name = 'UVGC_125_13',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_126_14 = Coupling(name = 'UVGC_126_14',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_126_15 = Coupling(name = 'UVGC_126_15',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_16 = Coupling(name = 'UVGC_128_16',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_128_17 = Coupling(name = 'UVGC_128_17',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_129_18 = Coupling(name = 'UVGC_129_18',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_129_19 = Coupling(name = 'UVGC_129_19',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_130_20 = Coupling(name = 'UVGC_130_20',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_131_21 = Coupling(name = 'UVGC_131_21',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_137_22 = Coupling(name = 'UVGC_137_22',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_137_23 = Coupling(name = 'UVGC_137_23',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_139_24 = Coupling(name = 'UVGC_139_24',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_140_25 = Coupling(name = 'UVGC_140_25',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_140_26 = Coupling(name = 'UVGC_140_26',
                       value = {-1:'(39*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_140_27 = Coupling(name = 'UVGC_140_27',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(8.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_141_28 = Coupling(name = 'UVGC_141_28',
                       value = {-1:'(31*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_141_29 = Coupling(name = 'UVGC_141_29',
                       value = {-1:'(3*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_142_30 = Coupling(name = 'UVGC_142_30',
                       value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_142_31 = Coupling(name = 'UVGC_142_31',
                       value = {-1:'G**3/(32.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_143_32 = Coupling(name = 'UVGC_143_32',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_143_33 = Coupling(name = 'UVGC_143_33',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_143_34 = Coupling(name = 'UVGC_143_34',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_143_35 = Coupling(name = 'UVGC_143_35',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )',0:'( (complex(0,1)*G**3*reglog(MT/MU_R))/(24.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_144_36 = Coupling(name = 'UVGC_144_36',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_147_37 = Coupling(name = 'UVGC_147_37',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_147_38 = Coupling(name = 'UVGC_147_38',
                       value = {-1:'(-31*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_147_39 = Coupling(name = 'UVGC_147_39',
                       value = {-1:'( 0 if MT else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MT/MU_R))/(8.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_148_40 = Coupling(name = 'UVGC_148_40',
                       value = {-1:'(-45*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_148_41 = Coupling(name = 'UVGC_148_41',
                       value = {-1:'-G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_149_42 = Coupling(name = 'UVGC_149_42',
                       value = {-1:'(-53*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_149_43 = Coupling(name = 'UVGC_149_43',
                       value = {-1:'-G**3/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_150_44 = Coupling(name = 'UVGC_150_44',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_150_45 = Coupling(name = 'UVGC_150_45',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_150_46 = Coupling(name = 'UVGC_150_46',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_151_47 = Coupling(name = 'UVGC_151_47',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_48 = Coupling(name = 'UVGC_151_48',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_49 = Coupling(name = 'UVGC_152_49',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_50 = Coupling(name = 'UVGC_152_50',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_51 = Coupling(name = 'UVGC_152_51',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_153_52 = Coupling(name = 'UVGC_153_52',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_53 = Coupling(name = 'UVGC_153_53',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_54 = Coupling(name = 'UVGC_153_54',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_153_55 = Coupling(name = 'UVGC_153_55',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_154_56 = Coupling(name = 'UVGC_154_56',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_154_57 = Coupling(name = 'UVGC_154_57',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_155_58 = Coupling(name = 'UVGC_155_58',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_155_59 = Coupling(name = 'UVGC_155_59',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_155_60 = Coupling(name = 'UVGC_155_60',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(6.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_156_61 = Coupling(name = 'UVGC_156_61',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_157_62 = Coupling(name = 'UVGC_157_62',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_158_63 = Coupling(name = 'UVGC_158_63',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_159_64 = Coupling(name = 'UVGC_159_64',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_160_65 = Coupling(name = 'UVGC_160_65',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_161_66 = Coupling(name = 'UVGC_161_66',
                       value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_162_67 = Coupling(name = 'UVGC_162_67',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_163_68 = Coupling(name = 'UVGC_163_68',
                       value = {-1:'-(G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_163_69 = Coupling(name = 'UVGC_163_69',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (13*G**2*yt)/(24.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_163_70 = Coupling(name = 'UVGC_163_70',
                       value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_164_71 = Coupling(name = 'UVGC_164_71',
                       value = {-1:'(G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_164_72 = Coupling(name = 'UVGC_164_72',
                       value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MT else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yt)/(24.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_164_73 = Coupling(name = 'UVGC_164_73',
                       value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_165_74 = Coupling(name = 'UVGC_165_74',
                       value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_166_75 = Coupling(name = 'UVGC_166_75',
                       value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

