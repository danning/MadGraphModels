Requestor: Karolos Potamianos
Content: Electroweak couplings renormalized in the Gmu scheme
Source: http://madgraph.phys.ucl.ac.be/Downloads/models/loop_qcd_qed_sm_Gmu.tgz
JIRA: https://its.cern.ch/jira/browse/AGENE-1672
