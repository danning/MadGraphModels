# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 12.0.0 for Mac OS X x86 (64-bit) (April 7, 2019)
# Date: Thu 19 Aug 2021 11:22:56


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

M0 = CouplingOrder(name = 'M0',
                   expansion_order = 99,
                   hierarchy = 1)

M1 = CouplingOrder(name = 'M1',
                   expansion_order = 99,
                   hierarchy = 1)

M2 = CouplingOrder(name = 'M2',
                   expansion_order = 99,
                   hierarchy = 1)

M3 = CouplingOrder(name = 'M3',
                   expansion_order = 99,
                   hierarchy = 1)

M4 = CouplingOrder(name = 'M4',
                   expansion_order = 99,
                   hierarchy = 1)

M5 = CouplingOrder(name = 'M5',
                   expansion_order = 99,
                   hierarchy = 1)

M6 = CouplingOrder(name = 'M6',
                   expansion_order = 99,
                   hierarchy = 1)

M7 = CouplingOrder(name = 'M7',
                   expansion_order = 99,
                   hierarchy = 1)

S0 = CouplingOrder(name = 'S0',
                   expansion_order = 99,
                   hierarchy = 1)

S1 = CouplingOrder(name = 'S1',
                   expansion_order = 99,
                   hierarchy = 1)

S2 = CouplingOrder(name = 'S2',
                   expansion_order = 99,
                   hierarchy = 1)

T0 = CouplingOrder(name = 'T0',
                   expansion_order = 99,
                   hierarchy = 1)

T1 = CouplingOrder(name = 'T1',
                   expansion_order = 99,
                   hierarchy = 1)

T2 = CouplingOrder(name = 'T2',
                   expansion_order = 99,
                   hierarchy = 1)

T3 = CouplingOrder(name = 'T3',
                   expansion_order = 99,
                   hierarchy = 1)

T4 = CouplingOrder(name = 'T4',
                   expansion_order = 99,
                   hierarchy = 1)

T5 = CouplingOrder(name = 'T5',
                   expansion_order = 99,
                   hierarchy = 1)

T6 = CouplingOrder(name = 'T6',
                   expansion_order = 99,
                   hierarchy = 1)

T7 = CouplingOrder(name = 'T7',
                   expansion_order = 99,
                   hierarchy = 1)

T8 = CouplingOrder(name = 'T8',
                   expansion_order = 99,
                   hierarchy = 1)

T9 = CouplingOrder(name = 'T9',
                   expansion_order = 99,
                   hierarchy = 1)

