Requestor: Marcus De Beurs
Content: NLO EFT model for top 4fermion operator
Paper: https://arxiv.org/abs/1601.06163
Source: Cen Zhang (private communication)
JIRA: https://its.cern.ch/jira/browse/AGENE-1642