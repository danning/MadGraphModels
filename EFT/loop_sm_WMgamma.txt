Requestor: Carlo A. Gottardo
Content: W decay to pion+photon, rho+photon. The mesons and photon are scalar in the ME, Pythia recognises them as the SM states.
Paper: https://arxiv.org/abs/1410.7475
Source: https://gitlab.cern.ch/cgottard/rarewdecays
JIRA: https://its.cern.ch/jira/browse/AGENE-1797