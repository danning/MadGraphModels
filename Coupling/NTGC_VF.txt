Requestor: Artur Semushin
Content: SM + parameterization of ZZV and ZgammaV interactions (4 and 12 coefficients)
Paper: hep-ph/9910395 (f_i^V, i=4,5; h_j^V, j=1,2,3,4), 2206.11676 (h_5^V), 2308.16887 (h_6^V)
JIRA: https://its.cern.ch/jira/browse/AGENE-2245