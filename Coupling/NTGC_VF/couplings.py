# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Thu 28 Sep 2023 13:33:30


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-G',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*G',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*G**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-6*complex(0,1)*lam',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '(ee*h2a)/MZ**4',
                 order = {'H2A':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '(ee*h2Z)/MZ**4',
                 order = {'H2Z':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '(ee*h4a)/MZ**4',
                 order = {'H4A':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-((ee*h4Z)/MZ**4)',
                 order = {'H4Z':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '(ee*h5a)/MZ**4',
                 order = {'H5A':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '(ee*h5Z)/MZ**4',
                 order = {'H5Z':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-((ee*h6a)/MZ**4)',
                 order = {'H6A':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-((ee*h6Z)/MZ**4)',
                 order = {'H6Z':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '(ee*f4a)/MZ**2',
                 order = {'F4A':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(ee*f4Z)/MZ**2',
                 order = {'F4Z':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '(ee*f5a)/(2.*MZ**2)',
                 order = {'F5A':1})

GC_21 = Coupling(name = 'GC_21',
                 value = '(ee*f5Z)/(2.*MZ**2)',
                 order = {'F5Z':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-((ee*h1a)/MZ**2)',
                 order = {'H1A':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '-((ee*h1Z)/MZ**2)',
                 order = {'H1Z':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '-((ee*h2Z)/MZ**2)',
                 order = {'H2Z':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '-((ee*h3a)/MZ**2)',
                 order = {'H3A':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-((ee*h3Z)/MZ**2)',
                 order = {'H3Z':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '(ee*h4Z)/(2.*MZ**2)',
                 order = {'H4Z':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '-6*complex(0,1)*lam*vev',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

