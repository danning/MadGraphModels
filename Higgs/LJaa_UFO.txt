Requestor: Bernhard Meirose
Content: Higgs-induced lepton-jets, replacing the usrmodv4_lj* models
JIRA: https://its.cern.ch/jira/browse/AGENE-1105