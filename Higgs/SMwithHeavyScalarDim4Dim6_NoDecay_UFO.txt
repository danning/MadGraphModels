Requestor: Yue Xu
Contents: effective interactions of the lightest Higgs boson and a heavier neutral Higgs boson in a multi-Higgs system in the framework of effective Lagrangian
Source: Yongcheng Wu (private communiciation, to be posted to FeynRules)
Code: https://gitlab.cern.ch/yuxu/generic-heavy-higgs.git
Paper: https://arxiv.org/abs/1905.05421
Public link: https://feynrules.irmp.ucl.ac.be/wiki/HeavyScalarED
JIRA: https://its.cern.ch/jira/browse/AGENE-1824