Requestor: Mihail Chizhov
Content: Signatures of lower scale gauge coupling unification in the Standard Model due to extended Higgs sector
Paper: https://arxiv.org/abs/1509.07610
Presentation: https://indico.cern.ch/event/854218/contributions/3592251/attachments/1921226/3178220/Chizhov_2019_10_07.pdf
Source: Mihail Chizhov
JIRA: https://its.cern.ch/jira/browse/AGENE-1786