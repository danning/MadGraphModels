# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Tue 23 Aug 2016 20:14:26


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(AH*complex(0,1))',
                order = {'HIW':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '-ee**2/(2.*cw)',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '(cHB*ee**2*complex(0,1))/(2.*cw*NPl**2) + (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(cHW*ee**2*complex(0,1))/(4.*cw*NPl**2) + (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '-(cHB*ee**2)/(2.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(cHB*ee**2)/(2.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '-(cHW*ee**2)/(4.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(cHW*ee**2)/(4.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '-(cHB*ee**2)/(2.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '(cHB*ee**2)/(2.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '-(cHW*ee**2)/(4.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(cHW*ee**2)/(4.*cw*NPl**2) + (cHW*cw*ee**2)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '(ee**2*complex(0,1))/(2.*cw)',
                 order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '(cHW*ee**3)/(4.*cw*NPl**2) + (cWW*ee**3)/(2.*cw*NPl**2) - (cHB*cw*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cHB*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2) - (cHB*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cHB*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2) + (cHB*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '-(cHW*ee**3)/(4.*cw*NPl**2) - (cWW*ee**3)/(4.*cw*NPl**2) + (cHB*cw*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(cHW*ee**3)/(2.*NPl**2*sw**2) - (cWW*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '-(cHW*ee**3)/(4.*NPl**2*sw**2) - (cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = 'ee**2/(2.*cw)',
                 order = {'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '-(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (3*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = '(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (3*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (3*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = '(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (3*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (cWW*ee**3*complex(0,1))/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (cWW*ee**3*complex(0,1))/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (5*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (5*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-G',
                 order = {'QCD':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '(cHW*ee**3)/(4.*NPl**2*sw**2) + (cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '(cHW*ee**3)/(2.*NPl**2*sw**2) + (cWW*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_132 = Coupling(name = 'GC_132',
                  value = '(cHB*ee**3)/(2.*cw*NPl**2) - (cHW*cw*ee**3)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_133 = Coupling(name = 'GC_133',
                  value = '-(cHW*ee**3)/(4.*cw*NPl**2) - (cHW*cw*ee**3)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = '(cWW*ee**3)/(4.*cw*NPl**2) - (cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(cHW*cw*ee**3)/(2.*NPl**2*sw**2) - (cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '(cHW*ee**3)/(4.*cw*NPl**2) - (cHW*cw*ee**3)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '-(cHW*ee**3)/(4.*cw*NPl**2) - (cWW*ee**3)/(2.*cw*NPl**2) - (cHW*cw*ee**3)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = '(cWW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = '(cHB*ee**3*complex(0,1))/(2.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(cWW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_145 = Coupling(name = 'GC_145',
                  value = '-(cHB*ee**3*complex(0,1))/(2.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_147 = Coupling(name = 'GC_147',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_148 = Coupling(name = 'GC_148',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = '(cHW*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = 'G',
                 order = {'QCD':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(cHB*ee**3*complex(0,1))/(2.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_151 = Coupling(name = 'GC_151',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) - (3*cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(cHB*ee**3*complex(0,1))/(2.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_157 = Coupling(name = 'GC_157',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2) + (3*cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(cHB*ee**3)/(2.*cw*NPl**2) + (cHW*cw*ee**3)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_159 = Coupling(name = 'GC_159',
                  value = '(cHW*ee**3)/(4.*cw*NPl**2) + (cHW*cw*ee**3)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(complex(0,1)*G**2)',
                 order = {'QCD':2})

GC_160 = Coupling(name = 'GC_160',
                  value = '(cHW*ee**3)/(4.*cw*NPl**2) + (cWW*ee**3)/(4.*cw*NPl**2) + (cHW*cw*ee**3)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(cHW*ee**3)/(4.*cw*NPl**2) + (3*cHW*cw*ee**3)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(cHW*cw**2*ee**4*complex(0,1))/(2.*NPl**2*sw**4) - (cw**2*cWW*ee**4*complex(0,1))/(NPl**2*sw**4) - (cHW*ee**4*complex(0,1))/(2.*NPl**2*sw**2) - (cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_163 = Coupling(name = 'GC_163',
                  value = '-(cHW*cw**2*ee**4*complex(0,1))/(2.*NPl**2*sw**4) - (cw**2*cWW*ee**4*complex(0,1))/(NPl**2*sw**4) + (cHW*ee**4*complex(0,1))/(2.*NPl**2*sw**2) + (cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_164 = Coupling(name = 'GC_164',
                  value = '(cHW*cw**2*ee**4*complex(0,1))/(NPl**2*sw**4) + (2*cw**2*cWW*ee**4*complex(0,1))/(NPl**2*sw**4) - (cHW*ee**4*complex(0,1))/(NPl**2*sw**2) - (cWW*ee**4*complex(0,1))/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_165 = Coupling(name = 'GC_165',
                  value = '(cHW*cw**2*ee**4*complex(0,1))/(NPl**2*sw**4) + (2*cw**2*cWW*ee**4*complex(0,1))/(NPl**2*sw**4) + (cHW*ee**4*complex(0,1))/(NPl**2*sw**2) + (cWW*ee**4*complex(0,1))/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_166 = Coupling(name = 'GC_166',
                  value = '-((cHW*ee**4*complex(0,1))/(NPl**2*sw**2)) - (3*cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_167 = Coupling(name = 'GC_167',
                  value = '(2*cHW*ee**4*complex(0,1))/(NPl**2*sw**2) + (3*cWW*ee**4*complex(0,1))/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_168 = Coupling(name = 'GC_168',
                  value = '-(cHW*ee**4)/(2.*cw*NPl**2*sw**2) - (cWW*ee**4)/(2.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_169 = Coupling(name = 'GC_169',
                  value = '-(cHW*ee**4)/(4.*cw*NPl**2*sw**2) - (cWW*ee**4)/(4.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_17 = Coupling(name = 'GC_17',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_170 = Coupling(name = 'GC_170',
                  value = '(cHW*ee**4*complex(0,1))/(4.*cw*NPl**2*sw**2) + (cWW*ee**4*complex(0,1))/(4.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_171 = Coupling(name = 'GC_171',
                  value = '-(cHW*ee**4*complex(0,1))/(2.*cw*NPl**2*sw**2) - (cWW*ee**4*complex(0,1))/(2.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_172 = Coupling(name = 'GC_172',
                  value = '(cHW*ee**4)/(4.*cw*NPl**2*sw**2) + (cWW*ee**4)/(4.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_173 = Coupling(name = 'GC_173',
                  value = '(cHW*ee**4)/(2.*cw*NPl**2*sw**2) + (cWW*ee**4)/(2.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_174 = Coupling(name = 'GC_174',
                  value = '-(cHW*ee**4)/(2.*cw*NPl**2) - (cWW*ee**4)/(2.*cw*NPl**2) - (cHW*cw*ee**4)/(2.*NPl**2*sw**2) - (cw*cWW*ee**4)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_175 = Coupling(name = 'GC_175',
                  value = '-(cHW*ee**4)/(4.*cw*NPl**2) - (cWW*ee**4)/(4.*cw*NPl**2) - (cHW*cw*ee**4)/(4.*NPl**2*sw**2) - (cw*cWW*ee**4)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_176 = Coupling(name = 'GC_176',
                  value = '-(cHW*ee**4*complex(0,1))/(4.*cw*NPl**2) - (cWW*ee**4*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**4*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**4*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_177 = Coupling(name = 'GC_177',
                  value = '(cHW*ee**4*complex(0,1))/(2.*cw*NPl**2) + (cWW*ee**4*complex(0,1))/(2.*cw*NPl**2) + (cHW*cw*ee**4*complex(0,1))/(2.*NPl**2*sw**2) + (cw*cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_178 = Coupling(name = 'GC_178',
                  value = '(cHW*ee**4)/(4.*cw*NPl**2) + (cWW*ee**4)/(4.*cw*NPl**2) + (cHW*cw*ee**4)/(4.*NPl**2*sw**2) + (cw*cWW*ee**4)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_179 = Coupling(name = 'GC_179',
                  value = '(cHW*ee**4)/(2.*cw*NPl**2) + (cWW*ee**4)/(2.*cw*NPl**2) + (cHW*cw*ee**4)/(2.*NPl**2*sw**2) + (cw*cWW*ee**4)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(complex(0,1)*GH)',
                 order = {'HIG':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-(cHW*ee**2)/(2.*NPl**2*sw) - (cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-(cHB*ee**2)/(2.*NPl**2*sw) - (cHW*ee**2)/(4.*NPl**2*sw) - (cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '(cHB*ee**2)/(2.*NPl**2*sw) - (cHW*ee**2)/(4.*NPl**2*sw) - (cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-(cHW*ee**2)/(2.*NPl**2*sw) - (cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-(cHB*ee**2)/(2.*NPl**2*sw) - (cHW*ee**2)/(4.*NPl**2*sw) - (cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '(cHB*ee**2)/(2.*NPl**2*sw) - (cHW*ee**2)/(4.*NPl**2*sw) - (cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '(cHB*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cHW*ee**2*complex(0,1))/(4.*NPl**2*sw) - (cWW*ee**2*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '(cHB*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cHW*ee**2*complex(0,1))/(4.*NPl**2*sw) + (cWW*ee**2*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cWW*ee**2*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-(cHB*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cHW*ee**2*complex(0,1))/(4.*NPl**2*sw) - (cWW*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'complex(0,1)*GH',
                 order = {'HIG':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cWW*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '-(cHB*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cHW*ee**2*complex(0,1))/(4.*NPl**2*sw) + (cWW*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '-(cHB*ee**2)/(2.*NPl**2*sw) + (cHW*ee**2)/(4.*NPl**2*sw) + (cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '(cHB*ee**2)/(2.*NPl**2*sw) + (cHW*ee**2)/(4.*NPl**2*sw) + (cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '(cHW*ee**2)/(2.*NPl**2*sw) + (cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(cHB*ee**2)/(2.*NPl**2*sw) + (cHW*ee**2)/(4.*NPl**2*sw) + (cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '(cHB*ee**2)/(2.*NPl**2*sw) + (cHW*ee**2)/(4.*NPl**2*sw) + (cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '(cHW*ee**2)/(2.*NPl**2*sw) + (cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-(cHW*cw**2*ee**3)/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**3)/(2.*NPl**2*sw**3) - (cHW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_199 = Coupling(name = 'GC_199',
                  value = '(cHW*cw**2*ee**3)/(2.*NPl**2*sw**3) + (cw**2*cWW*ee**3)/(2.*NPl**2*sw**3) - (cHW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_2 = Coupling(name = 'GC_2',
                value = 'AH*complex(0,1)',
                order = {'HIW':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(G*GH)',
                 order = {'HIG':1,'QCD':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '-(cHW*cw**2*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cw**2*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_201 = Coupling(name = 'GC_201',
                  value = '(cHW*cw**2*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(cHW*cw**2*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cw**2*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = '(cHW*cw**2*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_204 = Coupling(name = 'GC_204',
                  value = '-(cHW*cw**2*ee**3*complex(0,1))/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_205 = Coupling(name = 'GC_205',
                  value = '(cHW*cw**2*ee**3*complex(0,1))/(2.*NPl**2*sw**3) + (cw**2*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = '-(cHW*cw**2*ee**3*complex(0,1))/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_207 = Coupling(name = 'GC_207',
                  value = '(cHW*cw**2*ee**3*complex(0,1))/(2.*NPl**2*sw**3) + (cw**2*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_208 = Coupling(name = 'GC_208',
                  value = '-(cHW*cw**2*ee**3)/(4.*NPl**2*sw**3) - (cw**2*cWW*ee**3)/(4.*NPl**2*sw**3) + (cHW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_209 = Coupling(name = 'GC_209',
                  value = '(cHW*cw**2*ee**3)/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**3)/(4.*NPl**2*sw**3) + (cHW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = 'G*GH',
                 order = {'HIG':1,'QCD':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHB*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_211 = Coupling(name = 'GC_211',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHB*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_212 = Coupling(name = 'GC_212',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHB*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_213 = Coupling(name = 'GC_213',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHB*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_214 = Coupling(name = 'GC_214',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_215 = Coupling(name = 'GC_215',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_216 = Coupling(name = 'GC_216',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_217 = Coupling(name = 'GC_217',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_218 = Coupling(name = 'GC_218',
                  value = '-(cHB*ee**3)/(2.*NPl**2*sw) - (cHW*ee**3)/(4.*NPl**2*sw) - (cWW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_219 = Coupling(name = 'GC_219',
                  value = '-(cHW*ee**3)/(2.*NPl**2*sw) - (cWW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(complex(0,1)*G**2*GH)',
                 order = {'HIG':1,'QCD':2})

GC_220 = Coupling(name = 'GC_220',
                  value = '-(cHB*ee**3)/(2.*NPl**2*sw) - (cHW*ee**3)/(4.*NPl**2*sw) - (cWW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = '-(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw) - (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_222 = Coupling(name = 'GC_222',
                  value = '-(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_223 = Coupling(name = 'GC_223',
                  value = '(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw) + (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_224 = Coupling(name = 'GC_224',
                  value = '(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_225 = Coupling(name = 'GC_225',
                  value = '-(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw) - (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_226 = Coupling(name = 'GC_226',
                  value = '-((cHW*ee**3*complex(0,1))/(NPl**2*sw)) - (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_227 = Coupling(name = 'GC_227',
                  value = '(cHB*ee**3*complex(0,1))/(2.*NPl**2*sw) + (cHW*ee**3*complex(0,1))/(4.*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_228 = Coupling(name = 'GC_228',
                  value = '(cHW*ee**3*complex(0,1))/(NPl**2*sw) + (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_229 = Coupling(name = 'GC_229',
                  value = '(cHB*ee**3)/(2.*NPl**2*sw) + (cHW*ee**3)/(4.*NPl**2*sw) + (cWW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = 'complex(0,1)*G**2*GH',
                 order = {'HIG':1,'QCD':2})

GC_230 = Coupling(name = 'GC_230',
                  value = '(cHB*ee**3)/(2.*NPl**2*sw) + (cHW*ee**3)/(4.*NPl**2*sw) + (cWW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_231 = Coupling(name = 'GC_231',
                  value = '(cHW*ee**3)/(NPl**2*sw) + (cWW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_232 = Coupling(name = 'GC_232',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_233 = Coupling(name = 'GC_233',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_234 = Coupling(name = 'GC_234',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_235 = Coupling(name = 'GC_235',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_236 = Coupling(name = 'GC_236',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_237 = Coupling(name = 'GC_237',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_238 = Coupling(name = 'GC_238',
                  value = '-(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_239 = Coupling(name = 'GC_239',
                  value = '(cHW*cw*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '-(complex(0,1)*gZAH)',
                 order = {'HIW':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '-(cHW*cw**2*ee**4)/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**4)/(2.*NPl**2*sw**3) - (cHW*ee**4)/(2.*NPl**2*sw) - (cWW*ee**4)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_241 = Coupling(name = 'GC_241',
                  value = '-(cHW*cw**2*ee**4)/(4.*NPl**2*sw**3) - (cw**2*cWW*ee**4)/(4.*NPl**2*sw**3) - (cHW*ee**4)/(4.*NPl**2*sw) - (cWW*ee**4)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_242 = Coupling(name = 'GC_242',
                  value = '(cHW*cw**2*ee**4*complex(0,1))/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**4*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**4*complex(0,1))/(4.*NPl**2*sw) + (cWW*ee**4*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_243 = Coupling(name = 'GC_243',
                  value = '-(cHW*cw**2*ee**4*complex(0,1))/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**3) - (cHW*ee**4*complex(0,1))/(2.*NPl**2*sw) - (cWW*ee**4*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_244 = Coupling(name = 'GC_244',
                  value = '(cHW*cw**2*ee**4)/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**4)/(4.*NPl**2*sw**3) + (cHW*ee**4)/(4.*NPl**2*sw) + (cWW*ee**4)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_245 = Coupling(name = 'GC_245',
                  value = '(cHW*cw**2*ee**4)/(2.*NPl**2*sw**3) + (cw**2*cWW*ee**4)/(2.*NPl**2*sw**3) + (cHW*ee**4)/(2.*NPl**2*sw) + (cWW*ee**4)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_246 = Coupling(name = 'GC_246',
                  value = '-(cHW*cw*ee**4*complex(0,1))/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**4*complex(0,1))/(4.*NPl**2*sw**3) - (cHW*ee**4*complex(0,1))/(4.*cw*NPl**2*sw) - (cWW*ee**4*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_247 = Coupling(name = 'GC_247',
                  value = '(-3*cHW*cw*ee**4*complex(0,1))/(4.*NPl**2*sw**3) - (5*cw*cWW*ee**4*complex(0,1))/(4.*NPl**2*sw**3) + (cHW*ee**4*complex(0,1))/(4.*cw*NPl**2*sw) + (cWW*ee**4*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_248 = Coupling(name = 'GC_248',
                  value = '(3*cHW*cw*ee**4*complex(0,1))/(2.*NPl**2*sw**3) + (5*cw*cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**3) - (cHW*ee**4*complex(0,1))/(2.*cw*NPl**2*sw) - (cWW*ee**4*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_249 = Coupling(name = 'GC_249',
                  value = '(cHW*cw*ee**4*complex(0,1))/(2.*NPl**2*sw**3) + (3*cw*cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**3) + (cHW*ee**4*complex(0,1))/(2.*cw*NPl**2*sw) + (cWW*ee**4*complex(0,1))/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':3})

GC_25 = Coupling(name = 'GC_25',
                 value = 'complex(0,1)*gZAH',
                 order = {'HIW':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '(ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_251 = Coupling(name = 'GC_251',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_252 = Coupling(name = 'GC_252',
                  value = '(2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_253 = Coupling(name = 'GC_253',
                  value = '(cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_254 = Coupling(name = 'GC_254',
                  value = '(-2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_255 = Coupling(name = 'GC_255',
                  value = '-(cHW*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '-(cHW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(cHW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '-(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = 'I1a33',
                 order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(cHW*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '-(cHW*cw*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '-(cHW*cw*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '(cHW*cw*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '(cHW*cw*ee**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '-(cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '-(cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '-(cWW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '(cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-I2a33',
                 order = {'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '-(cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-(cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '(cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = '(cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_276 = Coupling(name = 'GC_276',
                  value = '-(cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_277 = Coupling(name = 'GC_277',
                  value = '(cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_278 = Coupling(name = 'GC_278',
                  value = '-(cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_279 = Coupling(name = 'GC_279',
                  value = '(cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = 'I3a33',
                 order = {'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '-(cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_281 = Coupling(name = 'GC_281',
                  value = '(cw*cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_282 = Coupling(name = 'GC_282',
                  value = '(cw*cWW*ee**3)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_283 = Coupling(name = 'GC_283',
                  value = '-(cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_284 = Coupling(name = 'GC_284',
                  value = '(cWW*ee**4*complex(0,1))/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':3})

GC_285 = Coupling(name = 'GC_285',
                  value = '-ee/(2.*sw)',
                  order = {'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '-(ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '(ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = 'ee/(2.*sw)',
                  order = {'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '-I4a33',
                 order = {'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '-ee**2/(2.*sw)',
                  order = {'QED':2})

GC_293 = Coupling(name = 'GC_293',
                  value = '-(ee**2*complex(0,1))/(2.*sw)',
                  order = {'QED':2})

GC_294 = Coupling(name = 'GC_294',
                  value = 'ee**2/(2.*sw)',
                  order = {'QED':2})

GC_295 = Coupling(name = 'GC_295',
                  value = '(cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_296 = Coupling(name = 'GC_296',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_297 = Coupling(name = 'GC_297',
                  value = '-(cHW*ee)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '-(cHW*ee*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_299 = Coupling(name = 'GC_299',
                  value = '(cHW*ee*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-2*complex(0,1)*lam',
                 order = {'QED':2})

GC_300 = Coupling(name = 'GC_300',
                  value = '(cHW*ee)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '-(cWW*ee)/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '-(cWW*ee*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(cWW*ee*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '(cWW*ee)/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '-(cB*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '-(cB*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '(cB*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '(cB*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '-(cHW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '-4*complex(0,1)*lam',
                 order = {'QED':2})

GC_310 = Coupling(name = 'GC_310',
                  value = '-(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(cHW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '-(cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '-(cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '-(cWW*ee**2*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '(cWW*ee**2*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '(cWW*ee**2*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '(cWW*ee**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '(cWW*ee**2)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_320 = Coupling(name = 'GC_320',
                  value = '-(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_321 = Coupling(name = 'GC_321',
                  value = '(cHW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_322 = Coupling(name = 'GC_322',
                  value = '-(cWW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = '-(cWW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_324 = Coupling(name = 'GC_324',
                  value = '-(cWW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_325 = Coupling(name = 'GC_325',
                  value = '(cWW*ee**3*complex(0,1))/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_326 = Coupling(name = 'GC_326',
                  value = '-(cWW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_327 = Coupling(name = 'GC_327',
                  value = '(cWW*ee**3*complex(0,1))/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_328 = Coupling(name = 'GC_328',
                  value = '(cWW*ee**3)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_329 = Coupling(name = 'GC_329',
                  value = '(cWW*ee**3)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '(cHB*ee)/NPl**2 - (cHW*ee)/(2.*NPl**2)',
                 order = {'NP':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '-(cWW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_331 = Coupling(name = 'GC_331',
                  value = '(cWW*ee**3*complex(0,1))/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_332 = Coupling(name = 'GC_332',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '-((cBB*ee**2*complex(0,1)*sw)/(cw*NPl**2))',
                  order = {'NP':1,'QED':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '(cBB*ee**2*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '-((cBB*ee**2*complex(0,1)*sw**2)/(cw**2*NPl**2))',
                  order = {'NP':1,'QED':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(cBB*ee**2*complex(0,1)*sw**2)/(cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '-(cw*ee)/(2.*sw) - (ee*sw)/(2.*cw)',
                  order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-((cHB*ee*complex(0,1))/NPl**2) - (cHW*ee*complex(0,1))/(2.*NPl**2)',
                 order = {'NP':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '(cw*ee)/(2.*sw) + (ee*sw)/(2.*cw)',
                  order = {'QED':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '(cw*ee**2*complex(0,1))/sw - (ee**2*complex(0,1)*sw)/cw',
                  order = {'QED':2})

GC_347 = Coupling(name = 'GC_347',
                  value = '-(cw*cWW*ee)/(4.*NPl**2*sw) - (cB*ee*sw)/(2.*cw*NPl**2)',
                  order = {'NP':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(cw*cWW*ee*complex(0,1))/(4.*NPl**2*sw) - (cB*ee*complex(0,1)*sw)/(2.*cw*NPl**2)',
                  order = {'NP':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '-(cw*cWW*ee*complex(0,1))/(4.*NPl**2*sw) + (cB*ee*complex(0,1)*sw)/(2.*cw*NPl**2)',
                  order = {'NP':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(cHB*ee*complex(0,1))/NPl**2 + (cHW*ee*complex(0,1))/(2.*NPl**2)',
                 order = {'NP':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '(cw*cWW*ee)/(4.*NPl**2*sw) + (cB*ee*sw)/(2.*cw*NPl**2)',
                  order = {'NP':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '-(cHW*cw*ee)/(2.*NPl**2*sw) - (cHB*ee*sw)/(cw*NPl**2)',
                  order = {'NP':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '(cHW*cw*ee*complex(0,1))/(2.*NPl**2*sw) - (cHB*ee*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '-(cHW*cw*ee*complex(0,1))/(2.*NPl**2*sw) + (cHB*ee*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '(cHW*cw*ee)/(2.*NPl**2*sw) + (cHB*ee*sw)/(cw*NPl**2)',
                  order = {'NP':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(cw*cWW*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cB*ee**2*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '-(cw*cWW*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cB*ee**2*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(cHW*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cHB*ee**2*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '-(cHW*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cHB*ee**2*complex(0,1)*sw)/(cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(cHB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw) - (cHB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) - (cHW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '-((cHB*ee)/NPl**2) + (cHW*ee)/(2.*NPl**2)',
                 order = {'NP':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(cHB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw) + (cHB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) - (cHW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '-(cHB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw) - (cHB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) + (cHW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '-(cHB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw) + (cHB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) + (cHW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(cB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw) - (cB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) - (cWW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '(cB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw) + (cB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) - (cWW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '-(cB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) + (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw) - (cB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) + (cWW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '-(cB*cw*ee**2*complex(0,1))/(2.*NPl**2*sw) - (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw) + (cB*ee**2*complex(0,1)*sw)/(2.*cw*NPl**2) + (cWW*ee**2*complex(0,1)*sw)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_368 = Coupling(name = 'GC_368',
                  value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_369 = Coupling(name = 'GC_369',
                  value = '-(cB*ee**2*complex(0,1))/(2.*NPl**2) - (cWW*ee**2*complex(0,1))/(4.*NPl**2) - (cw**2*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(cB*ee)/(2.*NPl**2) - (cWW*ee)/(4.*NPl**2)',
                 order = {'NP':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '(cB*ee**2*complex(0,1))/(2.*NPl**2) + (cWW*ee**2*complex(0,1))/(4.*NPl**2) - (cw**2*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '-(cB*ee**2*complex(0,1))/(2.*NPl**2) - (cWW*ee**2*complex(0,1))/(4.*NPl**2) + (cw**2*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '(cB*ee**2*complex(0,1))/(2.*NPl**2) + (cWW*ee**2*complex(0,1))/(4.*NPl**2) + (cw**2*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '-(cHB*ee**2*complex(0,1))/(2.*NPl**2) - (cHW*ee**2*complex(0,1))/(4.*NPl**2) - (cHW*cw**2*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cHB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '(cHB*ee**2*complex(0,1))/(2.*NPl**2) + (cHW*ee**2*complex(0,1))/(4.*NPl**2) - (cHW*cw**2*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cHB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '-(cHB*ee**2*complex(0,1))/(2.*NPl**2) - (cHW*ee**2*complex(0,1))/(4.*NPl**2) + (cHW*cw**2*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cHB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_376 = Coupling(name = 'GC_376',
                  value = '(cHB*ee**2*complex(0,1))/(2.*NPl**2) + (cHW*ee**2*complex(0,1))/(4.*NPl**2) + (cHW*cw**2*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cHB*ee**2*complex(0,1)*sw**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '-(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '-2*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '-(cB*ee*complex(0,1))/(2.*NPl**2) - (cWW*ee*complex(0,1))/(4.*NPl**2)',
                 order = {'NP':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '-((cBB*ee**2*complex(0,1)*vev)/NPl**2)',
                  order = {'NP':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '(cBB*ee**2*complex(0,1)*vev)/NPl**2',
                  order = {'NP':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '-(cB*ee**2*vev)/(2.*cw*NPl**2)',
                  order = {'NP':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(cB*ee**2*vev)/(2.*cw*NPl**2)',
                  order = {'NP':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '-(cWW*ee**2*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '(cWW*ee**2*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '-(cWW*ee**3*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '-(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '-(ee**2*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(cB*ee*complex(0,1))/(2.*NPl**2) + (cWW*ee*complex(0,1))/(4.*NPl**2)',
                 order = {'NP':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '-(cHW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '(cHW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '-(cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '-(cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '(cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '(cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '-(cw*cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '(cw*cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_4 = Coupling(name = 'GC_4',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(cB*ee)/(2.*NPl**2) + (cWW*ee)/(4.*NPl**2)',
                 order = {'NP':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '-(cHW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_401 = Coupling(name = 'GC_401',
                  value = '(cHW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '-(cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '(cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '-(cWW*ee**3*complex(0,1)*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '(cWW*ee**3*complex(0,1)*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '(cw*cWW*ee**3*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '-(cWW*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_408 = Coupling(name = 'GC_408',
                  value = '(cWW*ee**4*complex(0,1)*vev)/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_409 = Coupling(name = 'GC_409',
                  value = '-(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-((cHB*ee**2*complex(0,1))/NPl**2) - (cHW*ee**2*complex(0,1))/(2.*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_410 = Coupling(name = 'GC_410',
                  value = '(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_411 = Coupling(name = 'GC_411',
                  value = '-(cB*ee**2*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_412 = Coupling(name = 'GC_412',
                  value = '(cB*ee**2*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_413 = Coupling(name = 'GC_413',
                  value = '-(cWW*ee**2*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_414 = Coupling(name = 'GC_414',
                  value = '-(cWW*ee**2*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_415 = Coupling(name = 'GC_415',
                  value = '(cWW*ee**2*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_416 = Coupling(name = 'GC_416',
                  value = '(cWW*ee**2*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_417 = Coupling(name = 'GC_417',
                  value = '-(cHW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_418 = Coupling(name = 'GC_418',
                  value = '(cHW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_419 = Coupling(name = 'GC_419',
                  value = '-(cWW*ee**3*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(cHB*ee**2*complex(0,1))/NPl**2 + (cHW*ee**2*complex(0,1))/(2.*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_420 = Coupling(name = 'GC_420',
                  value = '-(cWW*ee**3*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_421 = Coupling(name = 'GC_421',
                  value = '(cWW*ee**3*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_422 = Coupling(name = 'GC_422',
                  value = '(cWW*ee**3*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_423 = Coupling(name = 'GC_423',
                  value = '-(cWW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_424 = Coupling(name = 'GC_424',
                  value = '(cWW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_425 = Coupling(name = 'GC_425',
                  value = '-((cBB*ee**2*complex(0,1)*sw*vev)/(cw*NPl**2))',
                  order = {'NP':1})

GC_426 = Coupling(name = 'GC_426',
                  value = '(cBB*ee**2*complex(0,1)*sw*vev)/(cw*NPl**2)',
                  order = {'NP':1})

GC_427 = Coupling(name = 'GC_427',
                  value = '-((cBB*ee**2*complex(0,1)*sw**2*vev)/(cw**2*NPl**2))',
                  order = {'NP':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '(cBB*ee**2*complex(0,1)*sw**2*vev)/(cw**2*NPl**2)',
                  order = {'NP':1})

GC_429 = Coupling(name = 'GC_429',
                  value = '-(cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-((cB*ee**2*complex(0,1))/NPl**2) - (cWW*ee**2*complex(0,1))/(2.*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_430 = Coupling(name = 'GC_430',
                  value = '(cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_431 = Coupling(name = 'GC_431',
                  value = '-(cw**2*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '(cw**2*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '-(cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw)',
                  order = {'NP':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '(cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw)',
                  order = {'NP':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '-(cWW*ee**4*vev**3)/(16.*NPl**2*sw**4)',
                  order = {'NP':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '(cWW*ee**4*complex(0,1)*vev**3)/(16.*NPl**2*sw**4)',
                  order = {'NP':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '(cWW*ee**4*vev**3)/(16.*NPl**2*sw**4)',
                  order = {'NP':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '(cHW*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**4) + (cWW*ee**4*complex(0,1)*vev)/(NPl**2*sw**4)',
                  order = {'NP':1,'QED':2})

GC_439 = Coupling(name = 'GC_439',
                  value = '-((cHW*ee**4*complex(0,1)*vev)/(NPl**2*sw**4)) - (2*cWW*ee**4*complex(0,1)*vev)/(NPl**2*sw**4)',
                  order = {'NP':1,'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = '(cB*ee**2*complex(0,1))/NPl**2 + (cWW*ee**2*complex(0,1))/(2.*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_440 = Coupling(name = 'GC_440',
                  value = '-(cHW*ee**3*vev)/(4.*NPl**2*sw**3) - (cWW*ee**3*vev)/(4.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '(cHW*ee**3*vev)/(2.*NPl**2*sw**3) + (cWW*ee**3*vev)/(2.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '-(cHW*cw*ee**3*vev)/(2.*NPl**2*sw**3) - (cw*cWW*ee**3*vev)/(2.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '(cHW*cw*ee**3*vev)/(4.*NPl**2*sw**3) + (cw*cWW*ee**3*vev)/(4.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':1})

GC_444 = Coupling(name = 'GC_444',
                  value = '-(cHW*ee**4*vev)/(2.*NPl**2*sw**3) - (cWW*ee**4*vev)/(2.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':2})

GC_445 = Coupling(name = 'GC_445',
                  value = '-(cHW*ee**4*vev)/(4.*NPl**2*sw**3) - (cWW*ee**4*vev)/(4.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':2})

GC_446 = Coupling(name = 'GC_446',
                  value = '(cHW*ee**4*vev)/(4.*NPl**2*sw**3) + (cWW*ee**4*vev)/(4.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':2})

GC_447 = Coupling(name = 'GC_447',
                  value = '(cHW*ee**4*vev)/(2.*NPl**2*sw**3) + (cWW*ee**4*vev)/(2.*NPl**2*sw**3)',
                  order = {'NP':1,'QED':2})

GC_448 = Coupling(name = 'GC_448',
                  value = '-(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-((cBB*ee**2*complex(0,1))/NPl**2)',
                 order = {'NP':1,'QED':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '-(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '-(cHW*ee**2*vev)/(4.*NPl**2*sw**2) - (cWW*ee**2*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '-(cHW*ee**2*vev)/(4.*NPl**2*sw**2) - (cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '(cHW*ee**2*vev)/(4.*NPl**2*sw**2) + (cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '(cHW*ee**2*vev)/(4.*NPl**2*sw**2) + (cWW*ee**2*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '(cHB*ee**2*vev)/(2.*cw*NPl**2) - (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '-(cHW*ee**2*vev)/(4.*cw*NPl**2) - (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_458 = Coupling(name = 'GC_458',
                  value = '(cHB*ee**2*vev)/(2.*cw*NPl**2) - (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_459 = Coupling(name = 'GC_459',
                  value = '-(cHW*ee**2*vev)/(4.*cw*NPl**2) - (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '(cBB*ee**2*complex(0,1))/NPl**2',
                 order = {'NP':1,'QED':1})

GC_460 = Coupling(name = 'GC_460',
                  value = '-(cHB*ee**2*vev)/(2.*cw*NPl**2) + (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_461 = Coupling(name = 'GC_461',
                  value = '(cHW*ee**2*vev)/(4.*cw*NPl**2) + (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_462 = Coupling(name = 'GC_462',
                  value = '-(cHB*ee**2*vev)/(2.*cw*NPl**2) + (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_463 = Coupling(name = 'GC_463',
                  value = '(cHW*ee**2*vev)/(4.*cw*NPl**2) + (cHW*cw*ee**2*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_464 = Coupling(name = 'GC_464',
                  value = '(cHW*ee**3*vev)/(4.*cw*NPl**2) + (cWW*ee**3*vev)/(2.*cw*NPl**2) - (cHB*cw*ee**3*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_465 = Coupling(name = 'GC_465',
                  value = '-(cHW*ee**3*vev)/(4.*cw*NPl**2) - (cWW*ee**3*vev)/(4.*cw*NPl**2) + (cHB*cw*ee**3*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_466 = Coupling(name = 'GC_466',
                  value = '-(cHW*ee**3*vev)/(2.*NPl**2*sw**2) - (cWW*ee**3*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_467 = Coupling(name = 'GC_467',
                  value = '-(cHB*ee**3*complex(0,1)*vev)/(2.*NPl**2*sw**2) - (cHW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2) - (3*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_468 = Coupling(name = 'GC_468',
                  value = '(cHB*ee**3*complex(0,1)*vev)/(2.*NPl**2*sw**2) + (cHW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2) + (3*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_469 = Coupling(name = 'GC_469',
                  value = '(cHW*ee**3*vev)/(4.*NPl**2*sw**2) + (cWW*ee**3*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-(cB*ee**2)/(2.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_470 = Coupling(name = 'GC_470',
                  value = '(cHB*ee**3*vev)/(2.*cw*NPl**2) - (cHW*cw*ee**3*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_471 = Coupling(name = 'GC_471',
                  value = '-(cHW*ee**3*vev)/(4.*cw*NPl**2) - (cHW*cw*ee**3*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_472 = Coupling(name = 'GC_472',
                  value = '(cWW*ee**3*vev)/(4.*cw*NPl**2) - (cw*cWW*ee**3*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_473 = Coupling(name = 'GC_473',
                  value = '-(cHW*ee**3*vev)/(4.*cw*NPl**2) - (cWW*ee**3*vev)/(2.*cw*NPl**2) - (cHW*cw*ee**3*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**3*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_474 = Coupling(name = 'GC_474',
                  value = '-(cHB*ee**3*vev)/(2.*cw*NPl**2) + (cHW*cw*ee**3*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '(cHW*ee**3*vev)/(4.*cw*NPl**2) + (cHW*cw*ee**3*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_476 = Coupling(name = 'GC_476',
                  value = '(cHW*ee**3*vev)/(4.*cw*NPl**2) + (cWW*ee**3*vev)/(4.*cw*NPl**2) + (cHW*cw*ee**3*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**3*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_477 = Coupling(name = 'GC_477',
                  value = '-(cHW*cw**2*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**4) - (cw**2*cWW*ee**4*complex(0,1)*vev)/(NPl**2*sw**4) - (cHW*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**2) - (cWW*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_478 = Coupling(name = 'GC_478',
                  value = '(cHW*cw**2*ee**4*complex(0,1)*vev)/(NPl**2*sw**4) + (2*cw**2*cWW*ee**4*complex(0,1)*vev)/(NPl**2*sw**4) + (cHW*ee**4*complex(0,1)*vev)/(NPl**2*sw**2) + (cWW*ee**4*complex(0,1)*vev)/(NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_479 = Coupling(name = 'GC_479',
                  value = '-(cHW*ee**4*vev)/(2.*cw*NPl**2*sw**2) - (cWW*ee**4*vev)/(2.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(cB*ee**2*complex(0,1))/(2.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_480 = Coupling(name = 'GC_480',
                  value = '-(cHW*ee**4*vev)/(4.*cw*NPl**2*sw**2) - (cWW*ee**4*vev)/(4.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_481 = Coupling(name = 'GC_481',
                  value = '(cHW*ee**4*vev)/(4.*cw*NPl**2*sw**2) + (cWW*ee**4*vev)/(4.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_482 = Coupling(name = 'GC_482',
                  value = '(cHW*ee**4*vev)/(2.*cw*NPl**2*sw**2) + (cWW*ee**4*vev)/(2.*cw*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_483 = Coupling(name = 'GC_483',
                  value = '-(cHW*ee**4*vev)/(2.*cw*NPl**2) - (cWW*ee**4*vev)/(2.*cw*NPl**2) - (cHW*cw*ee**4*vev)/(2.*NPl**2*sw**2) - (cw*cWW*ee**4*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_484 = Coupling(name = 'GC_484',
                  value = '-(cHW*ee**4*vev)/(4.*cw*NPl**2) - (cWW*ee**4*vev)/(4.*cw*NPl**2) - (cHW*cw*ee**4*vev)/(4.*NPl**2*sw**2) - (cw*cWW*ee**4*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_485 = Coupling(name = 'GC_485',
                  value = '(cHW*ee**4*vev)/(4.*cw*NPl**2) + (cWW*ee**4*vev)/(4.*cw*NPl**2) + (cHW*cw*ee**4*vev)/(4.*NPl**2*sw**2) + (cw*cWW*ee**4*vev)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_486 = Coupling(name = 'GC_486',
                  value = '(cHW*ee**4*vev)/(2.*cw*NPl**2) + (cWW*ee**4*vev)/(2.*cw*NPl**2) + (cHW*cw*ee**4*vev)/(2.*NPl**2*sw**2) + (cw*cWW*ee**4*vev)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':2})

GC_487 = Coupling(name = 'GC_487',
                  value = '-(cHB*ee**2*vev)/(2.*NPl**2*sw) - (cHW*ee**2*vev)/(4.*NPl**2*sw) - (cWW*ee**2*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_488 = Coupling(name = 'GC_488',
                  value = '-(cHB*ee**2*vev)/(2.*NPl**2*sw) - (cHW*ee**2*vev)/(4.*NPl**2*sw) - (cWW*ee**2*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_489 = Coupling(name = 'GC_489',
                  value = '(cHB*ee**2*vev)/(2.*NPl**2*sw) + (cHW*ee**2*vev)/(4.*NPl**2*sw) + (cWW*ee**2*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '(cB*ee**2*complex(0,1))/(2.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_490 = Coupling(name = 'GC_490',
                  value = '(cHB*ee**2*vev)/(2.*NPl**2*sw) + (cHW*ee**2*vev)/(4.*NPl**2*sw) + (cWW*ee**2*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1})

GC_491 = Coupling(name = 'GC_491',
                  value = '-(cHW*cw**2*ee**3*vev)/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**3*vev)/(2.*NPl**2*sw**3) - (cHW*ee**3*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_492 = Coupling(name = 'GC_492',
                  value = '(cHW*cw**2*ee**3*vev)/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**3*vev)/(4.*NPl**2*sw**3) + (cHW*ee**3*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_493 = Coupling(name = 'GC_493',
                  value = '(cHW*cw*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (cHB*ee**3*complex(0,1)*vev)/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_494 = Coupling(name = 'GC_494',
                  value = '-(cHW*cw*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) + (cHB*ee**3*complex(0,1)*vev)/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_495 = Coupling(name = 'GC_495',
                  value = '-(cHB*ee**3*vev)/(2.*NPl**2*sw) - (cHW*ee**3*vev)/(4.*NPl**2*sw) - (cWW*ee**3*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_496 = Coupling(name = 'GC_496',
                  value = '-(cHB*ee**3*vev)/(2.*NPl**2*sw) - (cHW*ee**3*vev)/(4.*NPl**2*sw) - (cWW*ee**3*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_497 = Coupling(name = 'GC_497',
                  value = '(cHB*ee**3*vev)/(2.*NPl**2*sw) + (cHW*ee**3*vev)/(4.*NPl**2*sw) + (cWW*ee**3*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_498 = Coupling(name = 'GC_498',
                  value = '(cHB*ee**3*vev)/(2.*NPl**2*sw) + (cHW*ee**3*vev)/(4.*NPl**2*sw) + (cWW*ee**3*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_499 = Coupling(name = 'GC_499',
                  value = '-(cHW*cw*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cB*ee**2)/(2.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_500 = Coupling(name = 'GC_500',
                  value = '(cHW*cw*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_501 = Coupling(name = 'GC_501',
                  value = '-(cHW*cw*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1)*vev)/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_502 = Coupling(name = 'GC_502',
                  value = '(cHW*cw*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) + (3*cw*cWW*ee**3*complex(0,1)*vev)/(4.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1)*vev)/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev)/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_503 = Coupling(name = 'GC_503',
                  value = '-(cHW*cw**2*ee**4*vev)/(2.*NPl**2*sw**3) - (cw**2*cWW*ee**4*vev)/(2.*NPl**2*sw**3) - (cHW*ee**4*vev)/(2.*NPl**2*sw) - (cWW*ee**4*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_504 = Coupling(name = 'GC_504',
                  value = '-(cHW*cw**2*ee**4*vev)/(4.*NPl**2*sw**3) - (cw**2*cWW*ee**4*vev)/(4.*NPl**2*sw**3) - (cHW*ee**4*vev)/(4.*NPl**2*sw) - (cWW*ee**4*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_505 = Coupling(name = 'GC_505',
                  value = '(cHW*cw**2*ee**4*vev)/(4.*NPl**2*sw**3) + (cw**2*cWW*ee**4*vev)/(4.*NPl**2*sw**3) + (cHW*ee**4*vev)/(4.*NPl**2*sw) + (cWW*ee**4*vev)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_506 = Coupling(name = 'GC_506',
                  value = '(cHW*cw**2*ee**4*vev)/(2.*NPl**2*sw**3) + (cw**2*cWW*ee**4*vev)/(2.*NPl**2*sw**3) + (cHW*ee**4*vev)/(2.*NPl**2*sw) + (cWW*ee**4*vev)/(2.*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_507 = Coupling(name = 'GC_507',
                  value = '-(cHW*cw*ee**4*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (3*cw*cWW*ee**4*complex(0,1)*vev)/(4.*NPl**2*sw**3) - (cHW*ee**4*complex(0,1)*vev)/(4.*cw*NPl**2*sw) - (cWW*ee**4*complex(0,1)*vev)/(4.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_508 = Coupling(name = 'GC_508',
                  value = '(cHW*cw*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**3) + (3*cw*cWW*ee**4*complex(0,1)*vev)/(2.*NPl**2*sw**3) + (cHW*ee**4*complex(0,1)*vev)/(2.*cw*NPl**2*sw) + (cWW*ee**4*complex(0,1)*vev)/(2.*cw*NPl**2*sw)',
                  order = {'NP':1,'QED':2})

GC_509 = Coupling(name = 'GC_509',
                  value = '(cHB*cw*ee**2*complex(0,1)*vev)/(2.*NPl**2*sw) - (cHW*cw*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw) + (cHB*ee**2*complex(0,1)*sw*vev)/(2.*cw*NPl**2) - (cHW*ee**2*complex(0,1)*sw*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(cWW*ee**2)/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_510 = Coupling(name = 'GC_510',
                  value = '-(cHB*cw*ee**2*complex(0,1)*vev)/(2.*NPl**2*sw) + (cHW*cw*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw) - (cHB*ee**2*complex(0,1)*sw*vev)/(2.*cw*NPl**2) + (cHW*ee**2*complex(0,1)*sw*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1})

GC_511 = Coupling(name = 'GC_511',
                  value = '(cB*cw*ee**2*complex(0,1)*vev)/(2.*NPl**2*sw) - (cw*cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw) + (cB*ee**2*complex(0,1)*sw*vev)/(2.*cw*NPl**2) - (cWW*ee**2*complex(0,1)*sw*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1})

GC_512 = Coupling(name = 'GC_512',
                  value = '-(cB*cw*ee**2*complex(0,1)*vev)/(2.*NPl**2*sw) + (cw*cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw) - (cB*ee**2*complex(0,1)*sw*vev)/(2.*cw*NPl**2) + (cWW*ee**2*complex(0,1)*sw*vev)/(4.*cw*NPl**2)',
                  order = {'NP':1})

GC_513 = Coupling(name = 'GC_513',
                  value = '-(ee**2*complex(0,1)*vev)/2. - (cw**2*ee**2*complex(0,1)*vev)/(4.*sw**2) - (ee**2*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'QED':1})

GC_514 = Coupling(name = 'GC_514',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '-(cB*ee**2*complex(0,1)*vev)/(2.*NPl**2) - (cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2) - (cw**2*cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2) - (cB*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_516 = Coupling(name = 'GC_516',
                  value = '(cB*ee**2*complex(0,1)*vev)/(2.*NPl**2) + (cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2) + (cw**2*cWW*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2) + (cB*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_517 = Coupling(name = 'GC_517',
                  value = '-(cHB*ee**2*complex(0,1)*vev)/(2.*NPl**2) - (cHW*ee**2*complex(0,1)*vev)/(4.*NPl**2) - (cHW*cw**2*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2) - (cHB*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_518 = Coupling(name = 'GC_518',
                  value = '(cHB*ee**2*complex(0,1)*vev)/(2.*NPl**2) + (cHW*ee**2*complex(0,1)*vev)/(4.*NPl**2) + (cHW*cw**2*ee**2*complex(0,1)*vev)/(4.*NPl**2*sw**2) + (cHB*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_519 = Coupling(name = 'GC_519',
                  value = '(cHW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**4) + (cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**4)',
                  order = {'NP':1,'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '-(cWW*ee**2*complex(0,1))/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_520 = Coupling(name = 'GC_520',
                  value = '-(cHW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**4) - (cWW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**4)',
                  order = {'NP':1,'QED':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '(cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_522 = Coupling(name = 'GC_522',
                  value = '(cHB*ee**3*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cHW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2) + (cWW*ee**3*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_523 = Coupling(name = 'GC_523',
                  value = '-(cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_524 = Coupling(name = 'GC_524',
                  value = '-(cHB*ee**3*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) - (cHW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2) - (cWW*ee**3*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**2)',
                  order = {'NP':1})

GC_525 = Coupling(name = 'GC_525',
                  value = '-(cHW*cw**2*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**4) - (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**4) - (cHW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) - (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_526 = Coupling(name = 'GC_526',
                  value = '-(cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_527 = Coupling(name = 'GC_527',
                  value = '(cWW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**2) - (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '(cHW*cw**2*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**4) + (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**4) + (cHW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**2) + (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**2)',
                  order = {'NP':1,'QED':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '-(cHW*cw*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(cWW*ee**2*complex(0,1))/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '-(cHW*cw*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cHW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_531 = Coupling(name = 'GC_531',
                  value = '(cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_532 = Coupling(name = 'GC_532',
                  value = '-(cHW*cw*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) + (cHB*ee**3*complex(0,1)*vev**2)/(4.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_533 = Coupling(name = 'GC_533',
                  value = '(cHW*cw*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_534 = Coupling(name = 'GC_534',
                  value = '-(cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_535 = Coupling(name = 'GC_535',
                  value = '(cHW*cw*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cHB*ee**3*complex(0,1)*vev**2)/(4.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_536 = Coupling(name = 'GC_536',
                  value = '(cHW*cw*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) + (cHW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1})

GC_537 = Coupling(name = 'GC_537',
                  value = '-(cHW*cw*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cw*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**3) + (cw**3*cWW*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw**3) - (cHW*ee**4*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cw*cWW*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_538 = Coupling(name = 'GC_538',
                  value = '(cHW*cw*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**3) + (cw*cWW*ee**4*complex(0,1)*vev**2)/(2.*NPl**2*sw**3) - (cw**3*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**3) + (cHW*ee**4*complex(0,1)*vev**2)/(4.*cw*NPl**2*sw) + (cw*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw)',
                  order = {'NP':1,'QED':1})

GC_539 = Coupling(name = 'GC_539',
                  value = '-(cBB*ee**4*vev**2)/(8.*NPl**2*sw) + (cBB*ee**4*vev**2)/(8.*cw**2*NPl**2*sw) + (cWW*ee**4*vev**2)/(16.*NPl**2*sw) - (cBB*ee**4*sw*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(cWW*ee**2)/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':1})

GC_540 = Coupling(name = 'GC_540',
                  value = '-(cBB*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw) + (cBB*ee**4*complex(0,1)*vev**2)/(8.*cw**2*NPl**2*sw) + (cWW*ee**4*complex(0,1)*vev**2)/(16.*NPl**2*sw) - (cBB*ee**4*complex(0,1)*sw*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_541 = Coupling(name = 'GC_541',
                  value = '(cBB*ee**4*vev**2)/(8.*NPl**2*sw) - (cBB*ee**4*vev**2)/(8.*cw**2*NPl**2*sw) - (cWW*ee**4*vev**2)/(16.*NPl**2*sw) + (cBB*ee**4*sw*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_542 = Coupling(name = 'GC_542',
                  value = '-(cBB*ee**3*vev**2)/(8.*NPl**2) + (cBB*ee**3*vev**2)/(8.*cw**2*NPl**2) + (cWW*ee**3*vev**2)/(16.*NPl**2) - (cWW*ee**3*vev**2)/(16.*NPl**2*sw**2) + (cw**2*cWW*ee**3*vev**2)/(16.*NPl**2*sw**2) - (cBB*ee**3*sw**2*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_543 = Coupling(name = 'GC_543',
                  value = '(cBB*ee**3*complex(0,1)*vev**2)/(24.*NPl**2) - (cBB*ee**3*complex(0,1)*vev**2)/(24.*cw**2*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(48.*NPl**2) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(24.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_544 = Coupling(name = 'GC_544',
                  value = '(cBB*ee**3*complex(0,1)*vev**2)/(24.*NPl**2) - (cBB*ee**3*complex(0,1)*vev**2)/(24.*cw**2*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(48.*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(24.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_545 = Coupling(name = 'GC_545',
                  value = '-(cBB*ee**3*complex(0,1)*vev**2)/(12.*NPl**2) + (cBB*ee**3*complex(0,1)*vev**2)/(12.*cw**2*NPl**2) + (cWW*ee**3*complex(0,1)*vev**2)/(24.*NPl**2) - (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(12.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_546 = Coupling(name = 'GC_546',
                  value = '-(cBB*ee**3*complex(0,1)*vev**2)/(8.*NPl**2) + (cBB*ee**3*complex(0,1)*vev**2)/(8.*cw**2*NPl**2) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_547 = Coupling(name = 'GC_547',
                  value = '-(cBB*ee**3*complex(0,1)*vev**2)/(8.*NPl**2) + (cBB*ee**3*complex(0,1)*vev**2)/(8.*cw**2*NPl**2) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_548 = Coupling(name = 'GC_548',
                  value = '(cBB*ee**3*complex(0,1)*vev**2)/(8.*NPl**2) - (cBB*ee**3*complex(0,1)*vev**2)/(8.*cw**2*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_549 = Coupling(name = 'GC_549',
                  value = '(cBB*ee**3*complex(0,1)*vev**2)/(6.*NPl**2) - (cBB*ee**3*complex(0,1)*vev**2)/(6.*cw**2*NPl**2) - (cWW*ee**3*complex(0,1)*vev**2)/(12.*NPl**2) + (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(6.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '-(cWW*ee**3)/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':2})

GC_550 = Coupling(name = 'GC_550',
                  value = '-(cBB*ee**3*complex(0,1)*vev**2)/(4.*NPl**2) + (cBB*ee**3*complex(0,1)*vev**2)/(4.*cw**2*NPl**2) + (cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2) - (cBB*ee**3*complex(0,1)*sw**2*vev**2)/(4.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_551 = Coupling(name = 'GC_551',
                  value = '(cBB*ee**3*vev**2)/(8.*NPl**2) - (cBB*ee**3*vev**2)/(8.*cw**2*NPl**2) - (cWW*ee**3*vev**2)/(16.*NPl**2) + (cWW*ee**3*vev**2)/(16.*NPl**2*sw**2) - (cw**2*cWW*ee**3*vev**2)/(16.*NPl**2*sw**2) + (cBB*ee**3*sw**2*vev**2)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '-(cB*ee**4*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**4*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**4*vev**2)/(8.*cw*NPl**2) - (cB*ee**4*vev**2)/(8.*cw*NPl**2*sw**2) - (cw*cWW*ee**4*vev**2)/(16.*NPl**2*sw**2) - (cBB*ee**4*sw**2*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_553 = Coupling(name = 'GC_553',
                  value = '(cB*ee**4*complex(0,1)*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**4*complex(0,1)*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**4*complex(0,1)*vev**2)/(8.*cw*NPl**2) + (cB*ee**4*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw**2) + (cw*cWW*ee**4*complex(0,1)*vev**2)/(16.*NPl**2*sw**2) + (cBB*ee**4*complex(0,1)*sw**2*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_554 = Coupling(name = 'GC_554',
                  value = '(cB*ee**4*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**4*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**4*vev**2)/(8.*cw*NPl**2) + (cB*ee**4*vev**2)/(8.*cw*NPl**2*sw**2) + (cw*cWW*ee**4*vev**2)/(16.*NPl**2*sw**2) + (cBB*ee**4*sw**2*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_555 = Coupling(name = 'GC_555',
                  value = '(cBB*ee**4*complex(0,1)*vev**2)/(2.*NPl**2) - (cBB*ee**4*complex(0,1)*vev**2)/(2.*cw**2*NPl**2) - (cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2) - (cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cBB*ee**4*complex(0,1)*sw**2*vev**2)/(2.*cw**2*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_556 = Coupling(name = 'GC_556',
                  value = '-(cB*ee**3*vev**2)/(8.*cw*NPl**2*sw) - (cWW*ee**3*vev**2)/(16.*cw*NPl**2*sw) - (cB*ee**3*sw*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**3*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**3*sw*vev**2)/(8.*cw*NPl**2) - (cBB*ee**3*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_557 = Coupling(name = 'GC_557',
                  value = '-(cB*ee**3*complex(0,1)*vev**2)/(24.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(24.*NPl**2*sw) - (cB*ee**3*complex(0,1)*sw*vev**2)/(24.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(24.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(24.*cw*NPl**2) - (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(24.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_558 = Coupling(name = 'GC_558',
                  value = '-(cB*ee**3*complex(0,1)*vev**2)/(24.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(12.*NPl**2*sw) - (cB*ee**3*complex(0,1)*sw*vev**2)/(24.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(24.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(24.*cw*NPl**2) - (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(24.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_559 = Coupling(name = 'GC_559',
                  value = '(cB*ee**3*complex(0,1)*vev**2)/(12.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(24.*NPl**2*sw) + (cB*ee**3*complex(0,1)*sw*vev**2)/(12.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(12.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(12.*cw*NPl**2) + (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(12.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(cWW*ee**3*complex(0,1))/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':2})

GC_560 = Coupling(name = 'GC_560',
                  value = '-(cB*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw) - (cB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw*NPl**2) - (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_561 = Coupling(name = 'GC_561',
                  value = '(cB*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cWW*ee**3*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) + (cB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw*NPl**2) + (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_562 = Coupling(name = 'GC_562',
                  value = '(cB*ee**3*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cWW*ee**3*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw) + (cB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(8.*cw*NPl**2) + (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_563 = Coupling(name = 'GC_563',
                  value = '-(cB*ee**3*complex(0,1)*vev**2)/(6.*cw*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*vev**2)/(12.*NPl**2*sw) - (cB*ee**3*complex(0,1)*sw*vev**2)/(6.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(6.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(6.*cw*NPl**2) - (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(6.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_564 = Coupling(name = 'GC_564',
                  value = '(cB*ee**3*complex(0,1)*vev**2)/(4.*cw*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*vev**2)/(8.*NPl**2*sw) + (cB*ee**3*complex(0,1)*sw*vev**2)/(4.*cw**3*NPl**2) - (cBB*ee**3*complex(0,1)*sw*vev**2)/(4.*cw**3*NPl**2) + (cBB*ee**3*complex(0,1)*sw*vev**2)/(4.*cw*NPl**2) + (cBB*ee**3*complex(0,1)*sw**3*vev**2)/(4.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_565 = Coupling(name = 'GC_565',
                  value = '(cB*ee**3*vev**2)/(8.*cw*NPl**2*sw) + (cWW*ee**3*vev**2)/(16.*cw*NPl**2*sw) + (cB*ee**3*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**3*sw*vev**2)/(8.*cw**3*NPl**2) + (cBB*ee**3*sw*vev**2)/(8.*cw*NPl**2) + (cBB*ee**3*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '-(cw*cWW*ee**4*complex(0,1)*vev**2)/(16.*NPl**2*sw**3) + (cw**3*cWW*ee**4*complex(0,1)*vev**2)/(16.*NPl**2*sw**3) + (cBB*ee**4*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) - (cBB*cw*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw) - (cWW*ee**4*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) + (cw*cWW*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw) + (cBB*ee**4*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**4*complex(0,1)*sw*vev**2)/(4.*cw*NPl**2) + (cWW*ee**4*complex(0,1)*sw*vev**2)/(16.*cw*NPl**2) - (cBB*ee**4*complex(0,1)*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '-(cw*cWW*ee**4*complex(0,1)*vev**2)/(16.*NPl**2*sw**3) + (cw**3*cWW*ee**4*complex(0,1)*vev**2)/(16.*NPl**2*sw**3) - (cB*ee**4*complex(0,1)*vev**2)/(4.*cw*NPl**2*sw) - (cBB*ee**4*complex(0,1)*vev**2)/(8.*cw*NPl**2*sw) + (cBB*cw*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw) + (3*cWW*ee**4*complex(0,1)*vev**2)/(16.*cw*NPl**2*sw) - (3*cw*cWW*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw) - (cB*ee**4*complex(0,1)*sw*vev**2)/(4.*cw**3*NPl**2) + (3*cBB*ee**4*complex(0,1)*sw*vev**2)/(8.*cw**3*NPl**2) - (cBB*ee**4*complex(0,1)*sw*vev**2)/(4.*cw*NPl**2) + (cWW*ee**4*complex(0,1)*sw*vev**2)/(16.*cw*NPl**2) - (3*cBB*ee**4*complex(0,1)*sw**3*vev**2)/(8.*cw**3*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_568 = Coupling(name = 'GC_568',
                  value = '-(cBB*ee**4*complex(0,1)*vev**2)/(4.*NPl**2) + (cBB*ee**4*complex(0,1)*vev**2)/(4.*cw**2*NPl**2) + (cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2) - (cWW*ee**4*complex(0,1)*vev**2)/(8.*cw**2*NPl**2) - (cB*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cWW*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw**2) - (cw**2*cWW*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cB*ee**4*complex(0,1)*sw**2*vev**2)/(4.*cw**4*NPl**2) - (cBB*ee**4*complex(0,1)*sw**2*vev**2)/(4.*cw**4*NPl**2) + (cBB*ee**4*complex(0,1)*sw**4*vev**2)/(4.*cw**4*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '(cBB*ee**4*complex(0,1)*vev**2)/(4.*NPl**2) + (cB*ee**4*complex(0,1)*vev**2)/(2.*cw**2*NPl**2) - (cBB*ee**4*complex(0,1)*vev**2)/(4.*cw**2*NPl**2) + (cWW*ee**4*complex(0,1)*vev**2)/(8.*cw**2*NPl**2) + (cB*ee**4*complex(0,1)*vev**2)/(4.*NPl**2*sw**2) + (cWW*ee**4*complex(0,1)*vev**2)/(8.*NPl**2*sw**2) + (cB*ee**4*complex(0,1)*sw**2*vev**2)/(4.*cw**4*NPl**2) - (cBB*ee**4*complex(0,1)*sw**2*vev**2)/(4.*cw**4*NPl**2) + (cBB*ee**4*complex(0,1)*sw**2*vev**2)/(2.*cw**2*NPl**2) + (cBB*ee**4*complex(0,1)*sw**4*vev**2)/(4.*cw**4*NPl**2)',
                  order = {'NP':1,'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(cWW*ee**3*complex(0,1))/(4.*cw*NPl**2)',
                 order = {'NP':1,'QED':2})

GC_570 = Coupling(name = 'GC_570',
                  value = '-(cBB*ee**4*vev**3)/(16.*cw**3*NPl**2) - (cw*cWW*ee**4*vev**3)/(16.*NPl**2*sw**4) - (cWW*ee**4*vev**3)/(32.*cw*NPl**2*sw**2)',
                  order = {'NP':1})

GC_571 = Coupling(name = 'GC_571',
                  value = '-(cBB*ee**4*vev**3)/(16.*cw**3*NPl**2) + (cw*cWW*ee**4*vev**3)/(16.*NPl**2*sw**4) - (cWW*ee**4*vev**3)/(32.*cw*NPl**2*sw**2)',
                  order = {'NP':1})

GC_572 = Coupling(name = 'GC_572',
                  value = '(cBB*ee**4*vev**3)/(16.*cw**3*NPl**2) - (cw*cWW*ee**4*vev**3)/(16.*NPl**2*sw**4) + (cWW*ee**4*vev**3)/(32.*cw*NPl**2*sw**2)',
                  order = {'NP':1})

GC_573 = Coupling(name = 'GC_573',
                  value = '(cBB*ee**4*vev**3)/(16.*cw**3*NPl**2) + (cw*cWW*ee**4*vev**3)/(16.*NPl**2*sw**4) + (cWW*ee**4*vev**3)/(32.*cw*NPl**2*sw**2)',
                  order = {'NP':1})

GC_574 = Coupling(name = 'GC_574',
                  value = '(-3*cWW*ee**4*vev**3)/(32.*NPl**2*sw**3) - (cBB*ee**4*vev**3)/(16.*cw**2*NPl**2*sw)',
                  order = {'NP':1})

GC_575 = Coupling(name = 'GC_575',
                  value = '(cWW*ee**4*vev**3)/(32.*NPl**2*sw**3) - (cBB*ee**4*vev**3)/(16.*cw**2*NPl**2*sw)',
                  order = {'NP':1})

GC_576 = Coupling(name = 'GC_576',
                  value = '-(cWW*ee**4*vev**3)/(32.*NPl**2*sw**3) + (cBB*ee**4*vev**3)/(16.*cw**2*NPl**2*sw)',
                  order = {'NP':1})

GC_577 = Coupling(name = 'GC_577',
                  value = '(3*cWW*ee**4*vev**3)/(32.*NPl**2*sw**3) + (cBB*ee**4*vev**3)/(16.*cw**2*NPl**2*sw)',
                  order = {'NP':1})

GC_578 = Coupling(name = 'GC_578',
                  value = '(cw*cWW*ee**4*complex(0,1)*vev**3)/(32.*NPl**2*sw**3) - (cBB*ee**4*complex(0,1)*vev**3)/(16.*cw*NPl**2*sw) + (cWW*ee**4*complex(0,1)*vev**3)/(32.*cw*NPl**2*sw) - (cBB*ee**4*complex(0,1)*sw*vev**3)/(16.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_579 = Coupling(name = 'GC_579',
                  value = '-(cBB*ee**4*vev**3)/(8.*NPl**2*sw) + (cBB*ee**4*vev**3)/(8.*cw**2*NPl**2*sw) + (cWW*ee**4*vev**3)/(16.*NPl**2*sw) - (cBB*ee**4*sw*vev**3)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(cHW*ee**4*complex(0,1))/(2.*NPl**2*sw**4) + (cWW*ee**4*complex(0,1))/(NPl**2*sw**4)',
                 order = {'NP':1,'QED':3})

GC_580 = Coupling(name = 'GC_580',
                  value = '(cBB*ee**4*vev**3)/(8.*NPl**2*sw) - (cBB*ee**4*vev**3)/(8.*cw**2*NPl**2*sw) - (cWW*ee**4*vev**3)/(16.*NPl**2*sw) + (cBB*ee**4*sw*vev**3)/(8.*cw**2*NPl**2)',
                  order = {'NP':1})

GC_581 = Coupling(name = 'GC_581',
                  value = '(cBB*ee**4*complex(0,1)*vev**3)/(8.*cw**2*NPl**2) + (cw**2*cWW*ee**4*complex(0,1)*vev**3)/(16.*NPl**2*sw**4) + (cWW*ee**4*complex(0,1)*vev**3)/(16.*NPl**2*sw**2) + (cBB*ee**4*complex(0,1)*sw**2*vev**3)/(8.*cw**4*NPl**2)',
                  order = {'NP':1})

GC_582 = Coupling(name = 'GC_582',
                  value = '-(cB*ee**4*vev**3)/(8.*cw**3*NPl**2) + (cBB*ee**4*vev**3)/(8.*cw**3*NPl**2) - (cBB*ee**4*vev**3)/(8.*cw*NPl**2) - (cB*ee**4*vev**3)/(8.*cw*NPl**2*sw**2) - (cw*cWW*ee**4*vev**3)/(16.*NPl**2*sw**2) - (cBB*ee**4*sw**2*vev**3)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '(cB*ee**4*vev**3)/(8.*cw**3*NPl**2) - (cBB*ee**4*vev**3)/(8.*cw**3*NPl**2) + (cBB*ee**4*vev**3)/(8.*cw*NPl**2) + (cB*ee**4*vev**3)/(8.*cw*NPl**2*sw**2) + (cw*cWW*ee**4*vev**3)/(16.*NPl**2*sw**2) + (cBB*ee**4*sw**2*vev**3)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_584 = Coupling(name = 'GC_584',
                  value = '-(cw*cWW*ee**4*complex(0,1)*vev**3)/(16.*NPl**2*sw**3) + (cw**3*cWW*ee**4*complex(0,1)*vev**3)/(16.*NPl**2*sw**3) + (cBB*ee**4*complex(0,1)*vev**3)/(8.*cw*NPl**2*sw) - (cBB*cw*ee**4*complex(0,1)*vev**3)/(8.*NPl**2*sw) - (cWW*ee**4*complex(0,1)*vev**3)/(16.*cw*NPl**2*sw) + (cw*cWW*ee**4*complex(0,1)*vev**3)/(8.*NPl**2*sw) + (cBB*ee**4*complex(0,1)*sw*vev**3)/(8.*cw**3*NPl**2) - (cBB*ee**4*complex(0,1)*sw*vev**3)/(4.*cw*NPl**2) + (cWW*ee**4*complex(0,1)*sw*vev**3)/(16.*cw*NPl**2) - (cBB*ee**4*complex(0,1)*sw**3*vev**3)/(8.*cw**3*NPl**2)',
                  order = {'NP':1})

GC_585 = Coupling(name = 'GC_585',
                  value = '(cBB*ee**4*complex(0,1)*vev**3)/(4.*NPl**2) + (cB*ee**4*complex(0,1)*vev**3)/(2.*cw**2*NPl**2) - (cBB*ee**4*complex(0,1)*vev**3)/(4.*cw**2*NPl**2) + (cWW*ee**4*complex(0,1)*vev**3)/(8.*cw**2*NPl**2) + (cB*ee**4*complex(0,1)*vev**3)/(4.*NPl**2*sw**2) + (cWW*ee**4*complex(0,1)*vev**3)/(8.*NPl**2*sw**2) + (cB*ee**4*complex(0,1)*sw**2*vev**3)/(4.*cw**4*NPl**2) - (cBB*ee**4*complex(0,1)*sw**2*vev**3)/(4.*cw**4*NPl**2) + (cBB*ee**4*complex(0,1)*sw**2*vev**3)/(2.*cw**2*NPl**2) + (cBB*ee**4*complex(0,1)*sw**4*vev**3)/(4.*cw**4*NPl**2)',
                  order = {'NP':1})

GC_586 = Coupling(name = 'GC_586',
                  value = '-(yb/cmath.sqrt(2))',
                  order = {'QED':1})

GC_587 = Coupling(name = 'GC_587',
                  value = '-((complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_588 = Coupling(name = 'GC_588',
                  value = 'yb/cmath.sqrt(2)',
                  order = {'QED':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '-(yt/cmath.sqrt(2))',
                  order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '-((cHW*ee**4*complex(0,1))/(NPl**2*sw**4)) - (2*cWW*ee**4*complex(0,1))/(NPl**2*sw**4)',
                 order = {'NP':1,'QED':3})

GC_590 = Coupling(name = 'GC_590',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_591 = Coupling(name = 'GC_591',
                  value = 'yt/cmath.sqrt(2)',
                  order = {'QED':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '-ytau',
                  order = {'QED':1})

GC_593 = Coupling(name = 'GC_593',
                  value = 'ytau',
                  order = {'QED':1})

GC_594 = Coupling(name = 'GC_594',
                  value = '-(ytau/cmath.sqrt(2))',
                  order = {'QED':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_596 = Coupling(name = 'GC_596',
                  value = 'ytau/cmath.sqrt(2)',
                  order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(cHW*ee**3)/(2.*NPl**2*sw**3) - (cWW*ee**3)/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '-(cHW*ee**3)/(4.*NPl**2*sw**3) - (cWW*ee**3)/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '-(cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) - (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = '(cHW*ee**3*complex(0,1))/(4.*NPl**2*sw**3) + (cWW*ee**3*complex(0,1))/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = '-(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**3) - (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = '(cHW*ee**3*complex(0,1))/(2.*NPl**2*sw**3) + (cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '(cHW*ee**3)/(4.*NPl**2*sw**3) + (cWW*ee**3)/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '(cHW*ee**3)/(2.*NPl**2*sw**3) + (cWW*ee**3)/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '-(cHW*cw*ee**3)/(2.*NPl**2*sw**3) - (cw*cWW*ee**3)/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '-(cHW*cw*ee**3)/(4.*NPl**2*sw**3) - (cw*cWW*ee**3)/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(cHW*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**3) - (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '(cHW*cw*ee**3*complex(0,1))/(2.*NPl**2*sw**3) + (cw*cWW*ee**3*complex(0,1))/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '(cHW*cw*ee**3)/(4.*NPl**2*sw**3) + (cw*cWW*ee**3)/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '(cHW*cw*ee**3)/(2.*NPl**2*sw**3) + (cw*cWW*ee**3)/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '-(cHW*ee**4)/(2.*NPl**2*sw**3) - (cWW*ee**4)/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(cHW*ee**4)/(4.*NPl**2*sw**3) - (cWW*ee**4)/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(cHW*ee**4*complex(0,1))/(4.*NPl**2*sw**3) - (cWW*ee**4*complex(0,1))/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_77 = Coupling(name = 'GC_77',
                 value = '(cHW*ee**4*complex(0,1))/(2.*NPl**2*sw**3) + (cWW*ee**4*complex(0,1))/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_78 = Coupling(name = 'GC_78',
                 value = '(cHW*ee**4)/(4.*NPl**2*sw**3) + (cWW*ee**4)/(4.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cHW*ee**4)/(2.*NPl**2*sw**3) + (cWW*ee**4)/(2.*NPl**2*sw**3)',
                 order = {'NP':1,'QED':3})

GC_8 = Coupling(name = 'GC_8',
                value = '-2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(cHW*ee**2)/(4.*NPl**2*sw**2) - (cWW*ee**2)/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-(cHW*ee**2)/(4.*NPl**2*sw**2) - (cWW*ee**2)/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '-(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw**2) - (cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(cHW*ee**2*complex(0,1))/(2.*NPl**2*sw**2) + (cWW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '(cHW*ee**2)/(4.*NPl**2*sw**2) + (cWW*ee**2)/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(cHW*ee**2)/(4.*NPl**2*sw**2) + (cWW*ee**2)/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '-(cHB*ee**2)/(2.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '(cHB*ee**2)/(2.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '-(cHW*ee**2)/(4.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '(cHW*ee**2)/(4.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '-(cHB*ee**2)/(2.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '(cHB*ee**2)/(2.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(cHW*ee**2)/(4.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(cHW*ee**2)/(4.*cw*NPl**2) - (cHW*cw*ee**2)/(4.*NPl**2*sw**2) - (cw*cWW*ee**2)/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '-(cHB*ee**2*complex(0,1))/(2.*cw*NPl**2) - (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-(cHW*ee**2*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-(cHB*ee**2*complex(0,1))/(2.*cw*NPl**2) + (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '-(cHW*ee**2*complex(0,1))/(4.*cw*NPl**2) + (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) + (cw*cWW*ee**2*complex(0,1))/(4.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(cHB*ee**2*complex(0,1))/(2.*cw*NPl**2) - (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(cHW*ee**2*complex(0,1))/(4.*cw*NPl**2) - (cHW*cw*ee**2*complex(0,1))/(4.*NPl**2*sw**2) - (cw*cWW*ee**2*complex(0,1))/(2.*NPl**2*sw**2)',
                 order = {'NP':1,'QED':1})

