# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Tue 23 Aug 2016 20:14:26



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
NPl = Parameter(name = 'NPl',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\Lambda',
                lhablock = 'NEWCOUP',
                lhacode = [ 0 ])

cWW = Parameter(name = 'cWW',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'C_W',
                lhablock = 'NEWCOUP',
                lhacode = [ 1 ])

cHW = Parameter(name = 'cHW',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'C_{\\text{HW}}',
                lhablock = 'NEWCOUP',
                lhacode = [ 2 ])

cB = Parameter(name = 'cB',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = 'C_B',
               lhablock = 'NEWCOUP',
               lhacode = [ 3 ])

cHB = Parameter(name = 'cHB',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'C_{\\text{HB}}',
                lhablock = 'NEWCOUP',
                lhacode = [ 4 ])

cBB = Parameter(name = 'cBB',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 'C_{\\text{BB}}',
                lhablock = 'NEWCOUP',
                lhacode = [ 5 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1/Gf)/2**0.25',
                texname = '\\text{vev}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*(1 + (aEW*cWW*cmath.pi*vev**2)/(2.*NPl**2))*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

GH = Parameter(name = 'GH',
               nature = 'internal',
               type = 'real',
               value = '-G**2/(12.*cmath.pi**2*vev)',
               texname = 'G_H')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

cw2 = Parameter(name = 'cw2',
                nature = 'internal',
                type = 'real',
                value = '(MZ**2*(8*NPl**2 - 4*cB*ee**2*vev**2) - ee**2*vev**2*(8*NPl**2 + cWW*ee**2*vev**2) + 4*MZ*(2*NPl**2 + cB*ee**2*vev**2)*cmath.sqrt((MZ - ee*vev)*(MZ + ee*vev)))/(16.*MZ*NPl**2*cmath.sqrt((MZ - ee*vev)*(MZ + ee*vev)))',
                texname = '\\text{cw2}')

gZAH = Parameter(name = 'gZAH',
                 nature = 'internal',
                 type = 'real',
                 value = '((-13 + 94*cw2)*cmath.sqrt(aEW*Gf*MZ**2))/(18.*2**0.75*cmath.pi**1.5*vev)',
                 texname = '\\text{gZAH}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

AH = Parameter(name = 'AH',
               nature = 'internal',
               type = 'real',
               value = '(47*ee**2)/(72.*cmath.pi**2*vev)',
               texname = 'A_H')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(cw2)',
               texname = 'c_w')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - cw**2',
                texname = '\\text{sw2}')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/(cw*(1 + (cBB*ee**2*vev**2)/(4.*cw**2*NPl**2)))',
               texname = 'g_1')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = '(ee*vev)/(2.*sw)',
               texname = 'M_W')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/(sw*(1 + (cWW*ee**2*vev**2)/(8.*NPl**2*sw**2)))',
               texname = 'g_w')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1a33}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2a33}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3a33}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4a33}')

