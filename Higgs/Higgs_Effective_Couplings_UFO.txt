Requestor: Ana Rosario Cueto Gomez
Content: Higgs effective couplings including c Yukawa couplings and mass
Source: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Models/HiggsEffective#no1
JIRA: https://its.cern.ch/jira/browse/AGENE-1653