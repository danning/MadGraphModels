# This file was automatically created by FeynRules 2.3.0
# Mathematica version: 9.0 for Linux x86 (64-bit) (February 7, 2013)
# Date: Mon 4 May 2015 16:42:38


from object_library import all_orders, CouplingOrder


NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

