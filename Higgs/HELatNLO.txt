Requestor: Zirui Wang
Content: subset of bosonic, dimension-6 operators in the Standard Model effective field theory, relevant for electroweak Higgs production. The model can be used to compute, e.g., associated production or vector boson fusion processes at NLO accuracy in QCD merged with parton shower using software such as MadGraph5_aMC@NLO.
Paper: https://arxiv.org/abs/1609.04833
Website: http://feynrules.irmp.ucl.ac.be/wiki/HELatNLO
JIRA: https://its.cern.ch/jira/browse/AGENE-1415