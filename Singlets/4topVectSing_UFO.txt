Requestor: Romain Madar
Content: Vector singlet 4-top production
Paper: http://arxiv.org/abs/1010.6304
Run 1 Search: https://cds.cern.ch/record/1633156/
JIRA: https://its.cern.ch/jira/browse/AGENE-990