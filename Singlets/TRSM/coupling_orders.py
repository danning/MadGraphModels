# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 25 Sep 2019 11:45:41


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

HIG = CouplingOrder(name = 'HIG',
                    expansion_order = 99,
                    hierarchy = 1)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 1)

