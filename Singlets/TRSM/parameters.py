# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 25 Sep 2019 11:45:41



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

vs = Parameter(name = 'vs',
               nature = 'external',
               type = 'real',
               value = 630,
               texname = 'v_S',
               lhablock = 'FRBlock',
               lhacode = [ 1 ])

vx = Parameter(name = 'vx',
               nature = 'external',
               type = 'real',
               value = 700,
               texname = 'v_X',
               lhablock = 'FRBlock',
               lhacode = [ 2 ])

thetahs = Parameter(name = 'thetahs',
                    nature = 'external',
                    type = 'real',
                    value = 1.435,
                    texname = '\\theta _{\\text{hs}}',
                    lhablock = 'FRBlock',
                    lhacode = [ 3 ])

thetahx = Parameter(name = 'thetahx',
                    nature = 'external',
                    type = 'real',
                    value = -0.908,
                    texname = '\\theta _{\\text{hx}}',
                    lhablock = 'FRBlock',
                    lhacode = [ 4 ])

thetasx = Parameter(name = 'thetasx',
                    nature = 'external',
                    type = 'real',
                    value = -1.456,
                    texname = '\\theta _{\\text{sx}}',
                    lhablock = 'FRBlock',
                    lhacode = [ 5 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

Mh1 = Parameter(name = 'Mh1',
                nature = 'external',
                type = 'real',
                value = 50,
                texname = '\\text{Mh1}',
                lhablock = 'MASS',
                lhacode = [ 25 ])

Mh2 = Parameter(name = 'Mh2',
                nature = 'external',
                type = 'real',
                value = 60,
                texname = '\\text{Mh2}',
                lhablock = 'MASS',
                lhacode = [ 35 ])

Mh3 = Parameter(name = 'Mh3',
                nature = 'external',
                type = 'real',
                value = 125.09,
                texname = '\\text{Mh3}',
                lhablock = 'MASS',
                lhacode = [ 36 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

Wh1 = Parameter(name = 'Wh1',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{Wh1}',
                lhablock = 'DECAY',
                lhacode = [ 25 ])

Wh2 = Parameter(name = 'Wh2',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{Wh2}',
                lhablock = 'DECAY',
                lhacode = [ 35 ])

Wh3 = Parameter(name = 'Wh3',
                nature = 'external',
                type = 'real',
                value = 0.0041,
                texname = '\\text{Wh3}',
                lhablock = 'DECAY',
                lhacode = [ 36 ])

RR1x1 = Parameter(name = 'RR1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(thetahs)*cmath.cos(thetahx)',
                  texname = '\\text{RR1x1}')

RR1x2 = Parameter(name = 'RR1x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(thetahx)*cmath.sin(thetahs))',
                  texname = '\\text{RR1x2}')

RR1x3 = Parameter(name = 'RR1x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-cmath.sin(thetahx)',
                  texname = '\\text{RR1x3}')

RR2x1 = Parameter(name = 'RR2x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(thetasx)*cmath.sin(thetahs) - cmath.cos(thetahs)*cmath.sin(thetahx)*cmath.sin(thetasx)',
                  texname = '\\text{RR2x1}')

RR2x2 = Parameter(name = 'RR2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(thetahs)*cmath.cos(thetasx) + cmath.sin(thetahs)*cmath.sin(thetahx)*cmath.sin(thetasx)',
                  texname = '\\text{RR2x2}')

RR2x3 = Parameter(name = 'RR2x3',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(thetahx)*cmath.sin(thetasx))',
                  texname = '\\text{RR2x3}')

RR3x1 = Parameter(name = 'RR3x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(thetahs)*cmath.cos(thetasx)*cmath.sin(thetahx) + cmath.sin(thetahs)*cmath.sin(thetasx)',
                  texname = '\\text{RR3x1}')

RR3x2 = Parameter(name = 'RR3x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(thetasx)*cmath.sin(thetahs)*cmath.sin(thetahx)) + cmath.cos(thetahs)*cmath.sin(thetasx)',
                  texname = '\\text{RR3x2}')

RR3x3 = Parameter(name = 'RR3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(thetahx)*cmath.cos(thetasx)',
                  texname = '\\text{RR3x3}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

LS = Parameter(name = 'LS',
               nature = 'internal',
               type = 'real',
               value = '(Mh1**2*RR1x2**2 + Mh2**2*RR2x2**2 + Mh3**2*RR3x2**2)/(2.*vs**2)',
               texname = '\\lambda _S')

LSX = Parameter(name = 'LSX',
                nature = 'internal',
                type = 'real',
                value = '(Mh1**2*RR1x2*RR1x3 + Mh2**2*RR2x2*RR2x3 + Mh3**2*RR3x2*RR3x3)/(vs*vx)',
                texname = '\\lambda _{\\text{SX}}')

LX = Parameter(name = 'LX',
               nature = 'internal',
               type = 'real',
               value = '(Mh1**2*RR1x3**2 + Mh2**2*RR2x3**2 + Mh3**2*RR3x3**2)/(2.*vx**2)',
               texname = '\\lambda _X')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

Ah1 = Parameter(name = 'Ah1',
                nature = 'internal',
                type = 'real',
                value = '(47*ee**2*(1 - (2*Mh1**4)/(987.*MT**4) - (14*Mh1**2)/(705.*MT**2) + (213*Mh1**12)/(2.634632e7*MW**12) + (5*Mh1**10)/(119756.*MW**10) + (41*Mh1**8)/(180950.*MW**8) + (87*Mh1**6)/(65800.*MW**6) + (57*Mh1**4)/(6580.*MW**4) + (33*Mh1**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
                texname = 'A_{\\text{h1}}')

Ah2 = Parameter(name = 'Ah2',
                nature = 'internal',
                type = 'real',
                value = '(47*ee**2*(1 - (2*Mh2**4)/(987.*MT**4) - (14*Mh2**2)/(705.*MT**2) + (213*Mh2**12)/(2.634632e7*MW**12) + (5*Mh2**10)/(119756.*MW**10) + (41*Mh2**8)/(180950.*MW**8) + (87*Mh2**6)/(65800.*MW**6) + (57*Mh2**4)/(6580.*MW**4) + (33*Mh2**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
                texname = 'A_{\\text{h2}}')

Ah3 = Parameter(name = 'Ah3',
                nature = 'internal',
                type = 'real',
                value = '(47*ee**2*(1 - (2*Mh3**4)/(987.*MT**4) - (14*Mh3**2)/(705.*MT**2) + (213*Mh3**12)/(2.634632e7*MW**12) + (5*Mh3**10)/(119756.*MW**10) + (41*Mh3**8)/(180950.*MW**8) + (87*Mh3**6)/(65800.*MW**6) + (57*Mh3**4)/(6580.*MW**4) + (33*Mh3**2)/(470.*MW**2)))/(72.*cmath.pi**2*vev)',
                texname = 'A_{\\text{h3}}')

Gh1 = Parameter(name = 'Gh1',
                nature = 'internal',
                type = 'real',
                value = '-(G**2*(1 + (13*Mh1**6)/(16800.*MT**6) + Mh1**4/(168.*MT**4) + (7*Mh1**2)/(120.*MT**2)))/(12.*cmath.pi**2*vev)',
                texname = 'G_{\\text{h1}}')

Gh2 = Parameter(name = 'Gh2',
                nature = 'internal',
                type = 'real',
                value = '-(G**2*(1 + (13*Mh2**6)/(16800.*MT**6) + Mh2**4/(168.*MT**4) + (7*Mh2**2)/(120.*MT**2)))/(12.*cmath.pi**2*vev)',
                texname = 'G_{\\text{h2}}')

Gh3 = Parameter(name = 'Gh3',
                nature = 'internal',
                type = 'real',
                value = '-(G**2*(1 + (13*Mh3**6)/(16800.*MT**6) + Mh3**4/(168.*MT**4) + (7*Mh3**2)/(120.*MT**2)))/(12.*cmath.pi**2*vev)',
                texname = 'G_{\\text{h3}}')

LH = Parameter(name = 'LH',
               nature = 'internal',
               type = 'real',
               value = '(Mh1**2*RR1x1**2 + Mh2**2*RR2x1**2 + Mh3**2*RR3x1**2)/(2.*vev**2)',
               texname = '\\lambda _{\\Phi }')

LHS = Parameter(name = 'LHS',
                nature = 'internal',
                type = 'real',
                value = '(Mh1**2*RR1x1*RR1x2 + Mh2**2*RR2x1*RR2x2 + Mh3**2*RR3x1*RR3x2)/(vev*vs)',
                texname = '\\lambda _{\\text{$\\Phi $S}}')

LHX = Parameter(name = 'LHX',
                nature = 'internal',
                type = 'real',
                value = '(Mh1**2*RR1x1*RR1x3 + Mh2**2*RR2x1*RR2x3 + Mh3**2*RR3x1*RR3x3)/(vev*vx)',
                texname = '\\lambda _{\\text{$\\Phi $X}}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muHsq = Parameter(name = 'muHsq',
                  nature = 'internal',
                  type = 'real',
                  value = '(-2*LH*vev**2 - LHS*vs**2 - LHX*vx**2)/2.',
                  texname = '\\mu _H{}^2')

muSsq = Parameter(name = 'muSsq',
                  nature = 'internal',
                  type = 'real',
                  value = '(-(LHS*vev**2) - 2*LS*vs**2 - LSX*vx**2)/2.',
                  texname = '\\mu _S{}^2')

muXsq = Parameter(name = 'muXsq',
                  nature = 'internal',
                  type = 'real',
                  value = '(-(LHX*vev**2) - LSX*vs**2 - 2*LX*vx**2)/2.',
                  texname = '\\mu _X{}^2')

