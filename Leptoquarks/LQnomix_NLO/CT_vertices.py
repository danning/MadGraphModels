# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Wed 21 Sep 2016 09:46:12


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV4, L.VVV6, L.VVV7, L.VVV8 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_263_86,(0,0,1):C.R2GC_263_87,(0,1,0):C.R2GC_266_88,(0,1,1):C.R2GC_266_89,(0,2,0):C.R2GC_266_88,(0,2,1):C.R2GC_266_89,(0,3,0):C.R2GC_263_86,(0,3,1):C.R2GC_263_87,(0,4,0):C.R2GC_263_86,(0,4,1):C.R2GC_263_87,(0,5,0):C.R2GC_266_88,(0,5,1):C.R2GC_266_89})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,0,0):C.R2GC_131_29,(2,0,1):C.R2GC_131_30,(0,0,0):C.R2GC_131_29,(0,0,1):C.R2GC_131_30,(4,0,0):C.R2GC_129_25,(4,0,1):C.R2GC_129_26,(3,0,0):C.R2GC_129_25,(3,0,1):C.R2GC_129_26,(8,0,0):C.R2GC_130_27,(8,0,1):C.R2GC_130_28,(7,0,0):C.R2GC_136_36,(7,0,1):C.R2GC_272_94,(6,0,0):C.R2GC_135_34,(6,0,1):C.R2GC_273_95,(5,0,0):C.R2GC_129_25,(5,0,1):C.R2GC_129_26,(1,0,0):C.R2GC_129_25,(1,0,1):C.R2GC_129_26,(11,0,0):C.R2GC_133_32,(11,0,1):C.R2GC_133_33,(10,0,0):C.R2GC_133_32,(10,0,1):C.R2GC_133_33,(9,0,1):C.R2GC_132_31,(2,1,0):C.R2GC_131_29,(2,1,1):C.R2GC_131_30,(0,1,0):C.R2GC_131_29,(0,1,1):C.R2GC_131_30,(6,1,0):C.R2GC_270_91,(6,1,1):C.R2GC_270_92,(4,1,0):C.R2GC_129_25,(4,1,1):C.R2GC_129_26,(3,1,0):C.R2GC_129_25,(3,1,1):C.R2GC_129_26,(8,1,0):C.R2GC_130_27,(8,1,1):C.R2GC_274_96,(7,1,0):C.R2GC_136_36,(7,1,1):C.R2GC_136_37,(5,1,0):C.R2GC_129_25,(5,1,1):C.R2GC_129_26,(1,1,0):C.R2GC_129_25,(1,1,1):C.R2GC_129_26,(11,1,0):C.R2GC_133_32,(11,1,1):C.R2GC_133_33,(10,1,0):C.R2GC_133_32,(10,1,1):C.R2GC_133_33,(9,1,1):C.R2GC_132_31,(2,2,0):C.R2GC_131_29,(2,2,1):C.R2GC_131_30,(0,2,0):C.R2GC_131_29,(0,2,1):C.R2GC_131_30,(4,2,0):C.R2GC_129_25,(4,2,1):C.R2GC_129_26,(3,2,0):C.R2GC_129_25,(3,2,1):C.R2GC_129_26,(8,2,0):C.R2GC_130_27,(8,2,1):C.R2GC_271_93,(6,2,0):C.R2GC_135_34,(6,2,1):C.R2GC_135_35,(7,2,0):C.R2GC_269_90,(7,2,1):C.R2GC_131_30,(5,2,0):C.R2GC_129_25,(5,2,1):C.R2GC_129_26,(1,2,0):C.R2GC_129_25,(1,2,1):C.R2GC_129_26,(11,2,0):C.R2GC_133_32,(11,2,1):C.R2GC_133_33,(10,2,0):C.R2GC_133_32,(10,2,1):C.R2GC_133_33,(9,2,1):C.R2GC_132_31})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.ta__minus__, P.t, P.lqsb__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.g, P.lqsb, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_236_67,(0,1,0):C.R2GC_237_68})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.vt__tilde__, P.b__tilde__, P.lqsb ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.lqsb] ] ],
               couplings = {(0,0,0):C.R2GC_191_50})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.vm__tilde__, P.c, P.lqsc__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.c, P.g, P.lqsc] ] ],
               couplings = {(0,0,0):C.R2GC_192_51})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.mu__plus__, P.s, P.lqsc__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.g, P.lqsc, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_232_64,(0,1,0):C.R2GC_233_65})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.ve__tilde__, P.d__tilde__, P.lqsd ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
               couplings = {(0,0,0):C.R2GC_198_55})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.mu__minus__, P.c, P.lqss__tilde__ ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.c, P.g, P.lqss] ] ],
               couplings = {(0,0,0):C.R2GC_193_52,(0,1,0):C.R2GC_195_53})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.vm__tilde__, P.s__tilde__, P.lqss ],
               color = [ 'Identity(2,3)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.g, P.lqss, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_234_66})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.ta__plus__, P.b, P.lqst__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_247_72,(0,1,0):C.R2GC_248_73})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.vt__tilde__, P.t, P.lqst__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqst, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_280_97})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.e__plus__, P.d, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_200_56,(0,1,0):C.R2GC_201_57})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.ve__tilde__, P.u, P.lqsu__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_243_71})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.u, P.e__minus__, P.lqsd__tilde__ ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_239_69,(0,1,0):C.R2GC_241_70})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_286_100,(0,1,0):C.R2GC_287_101})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_254_76})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_253_75})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_289_103})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_290_104})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_288_102,(0,1,0):C.R2GC_285_99})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.ta__plus__, P.t__tilde__, P.lqsb ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsb, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_237_68,(0,1,0):C.R2GC_236_67})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.s__tilde__, P.mu__minus__, P.lqsc ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsc, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_233_65,(0,1,0):C.R2GC_232_64})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.u__tilde__, P.e__plus__, P.lqsd ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsd, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_241_70,(0,1,0):C.R2GC_239_69})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.mu__plus__, P.c__tilde__, P.lqss ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqss] ] ],
                couplings = {(0,0,0):C.R2GC_195_53,(0,1,0):C.R2GC_193_52})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.b__tilde__, P.ta__minus__, P.lqst ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_248_73,(0,1,0):C.R2GC_247_72})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.d__tilde__, P.e__minus__, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_201_57,(0,1,0):C.R2GC_200_56})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.vt, P.b, P.lqsb__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.lqsb] ] ],
                couplings = {(0,0,0):C.R2GC_191_50})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.c__tilde__, P.vm, P.lqsc ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.c, P.g, P.lqsc] ] ],
                couplings = {(0,0,0):C.R2GC_192_51})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.ve, P.d, P.lqsd__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_198_55})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.vm, P.s, P.lqss__tilde__ ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqss, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_234_66})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.t__tilde__, P.vt, P.lqst ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqst, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_280_97})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.u__tilde__, P.ve, P.lqsu ],
                color = [ 'Identity(1,3)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.lqsu, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_243_71})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.g, P.lqsu__tilde__, P.lqsu ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.g, P.lqsd__tilde__, P.lqsd ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.g, P.lqsc__tilde__, P.lqsc ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsc] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsc__tilde__, P.lqsc ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.g, P.lqss__tilde__, P.lqss ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqss] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.g, P.g, P.lqss__tilde__, P.lqss ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqss] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.g, P.lqst__tilde__, P.lqst ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.g, P.g, P.lqst__tilde__, P.lqst ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqst] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.g, P.lqsb__tilde__, P.lqsb ],
                color = [ 'T(1,3,2)' ],
                lorentz = [ L.VSS1, L.VSS2 ],
                loop_particles = [ [ [P.g, P.lqsb] ] ],
                couplings = {(0,0,0):C.R2GC_150_47,(0,1,0):C.R2GC_149_46})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.g, P.g, P.lqsb__tilde__, P.lqsb ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb] ] ],
                couplings = {(2,0,0):C.R2GC_151_48,(2,0,1):C.R2GC_151_49,(1,0,0):C.R2GC_151_48,(1,0,1):C.R2GC_151_49,(0,0,0):C.R2GC_133_33,(0,0,1):C.R2GC_147_44})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_140_41})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_140_41})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_140_41})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_197_54})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_197_54})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_197_54})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_112_21,(0,1,0):C.R2GC_102_2})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_112_21,(0,1,0):C.R2GC_102_2})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_112_21,(0,1,0):C.R2GC_102_2})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_137_38})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_137_38})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_137_38})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_138_39})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_197_54})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_197_54})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_197_54})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_113_22,(0,1,0):C.R2GC_100_1})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_113_22,(0,1,0):C.R2GC_100_1})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_113_22,(0,1,0):C.R2GC_100_1})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_250_74,(0,2,0):C.R2GC_250_74,(0,1,0):C.R2GC_139_40,(0,3,0):C.R2GC_139_40})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_281_98,(0,2,0):C.R2GC_281_98,(0,1,0):C.R2GC_139_40,(0,3,0):C.R2GC_139_40})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_139_40})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsu] ] ],
                couplings = {(0,0,0):C.R2GC_260_82,(0,1,0):C.R2GC_148_45})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsd] ] ],
                couplings = {(0,0,0):C.R2GC_257_79,(0,1,0):C.R2GC_148_45})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsc] ] ],
                couplings = {(0,0,0):C.R2GC_256_78,(0,1,0):C.R2GC_148_45})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.lqss__tilde__, P.lqss ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqss] ] ],
                couplings = {(0,0,0):C.R2GC_258_80,(0,1,0):C.R2GC_148_45})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.lqst__tilde__, P.lqst ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqst] ] ],
                couplings = {(0,0,0):C.R2GC_259_81,(0,1,0):C.R2GC_148_45})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.lqsb__tilde__, P.lqsb ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.SS1, L.SS2 ],
                loop_particles = [ [ [P.g, P.lqsb] ] ],
                couplings = {(0,0,0):C.R2GC_255_77,(0,1,0):C.R2GC_148_45})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.R2GC_262_85,(0,1,0):C.R2GC_103_3,(0,1,3):C.R2GC_103_4,(0,2,1):C.R2GC_261_83,(0,2,2):C.R2GC_261_84})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_104_5,(0,0,1):C.R2GC_104_6})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_116_24,(0,1,0):C.R2GC_116_24,(0,2,0):C.R2GC_116_24})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_108_13,(0,0,1):C.R2GC_108_14,(0,1,0):C.R2GC_108_13,(0,1,1):C.R2GC_108_14,(0,2,0):C.R2GC_108_13,(0,2,1):C.R2GC_108_14})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_111_19,(0,0,1):C.R2GC_111_20,(0,1,0):C.R2GC_111_19,(0,1,1):C.R2GC_111_20,(0,2,0):C.R2GC_111_19,(0,2,1):C.R2GC_111_20})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_106_9,(0,0,1):C.R2GC_106_10,(0,1,0):C.R2GC_106_9,(0,1,1):C.R2GC_106_10,(0,2,0):C.R2GC_106_9,(0,2,1):C.R2GC_106_10})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_110_17,(1,0,1):C.R2GC_110_18,(0,1,0):C.R2GC_109_15,(0,1,1):C.R2GC_109_16,(0,2,0):C.R2GC_109_15,(0,2,1):C.R2GC_109_16,(0,3,0):C.R2GC_109_15,(0,3,1):C.R2GC_109_16})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_107_11,(0,0,1):C.R2GC_107_12,(0,1,0):C.R2GC_107_11,(0,1,1):C.R2GC_107_12,(0,2,0):C.R2GC_107_11,(0,2,1):C.R2GC_107_12})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_105_7,(0,0,1):C.R2GC_105_8})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.g, P.g, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_105_7,(0,0,1):C.R2GC_105_8})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_115_23})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.lqsu__tilde__, P.lqsu__tilde__, P.lqsu, P.lqsu ],
                color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqsu] ], [ [P.g, P.lqsd, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd__tilde__, P.lqsd, P.lqsd ],
                color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsu] ], [ [P.g, P.lqsc, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc, P.lqsd__tilde__, P.lqsd ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsd] ], [ [P.g, P.lqsc, P.lqsd] ] ],
                couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.lqsc__tilde__, P.lqsc__tilde__, P.lqsc, P.lqsc ],
                color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc] ] ],
                couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.lqss__tilde__, P.lqss, P.lqsu__tilde__, P.lqsu ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqsu] ], [ [P.g, P.lqss, P.lqsu] ] ],
                couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.lqsd__tilde__, P.lqsd, P.lqss__tilde__, P.lqss ],
                color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.SSSS1 ],
                loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqss] ], [ [P.g, P.lqsd, P.lqss] ] ],
                couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqss] ], [ [P.g, P.lqsc, P.lqss] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.lqss__tilde__, P.lqss__tilde__, P.lqss, P.lqss ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss] ] ],
                 couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.lqst__tilde__, P.lqst, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst], [P.g, P.lqsu] ], [ [P.g, P.lqst, P.lqsu] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqst] ], [ [P.g, P.lqsd, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqst] ], [ [P.g, P.lqsc, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_105 = CTVertex(name = 'V_105',
                 type = 'R2',
                 particles = [ P.lqss__tilde__, P.lqss, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqst] ], [ [P.g, P.lqss, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_106 = CTVertex(name = 'V_106',
                 type = 'R2',
                 particles = [ P.lqst__tilde__, P.lqst__tilde__, P.lqst, P.lqst ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_107 = CTVertex(name = 'V_107',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsu] ], [ [P.g, P.lqsb, P.lqsu] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_108 = CTVertex(name = 'V_108',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsd] ], [ [P.g, P.lqsb, P.lqsd] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_109 = CTVertex(name = 'V_109',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsc] ], [ [P.g, P.lqsb, P.lqsc] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_110 = CTVertex(name = 'V_110',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqss] ], [ [P.g, P.lqsb, P.lqss] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_111 = CTVertex(name = 'V_111',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqst] ], [ [P.g, P.lqsb, P.lqst] ] ],
                 couplings = {(1,0,0):C.R2GC_203_61,(1,0,1):C.R2GC_203_62,(1,0,2):C.R2GC_203_63,(0,0,0):C.R2GC_202_58,(0,0,1):C.R2GC_202_59,(0,0,2):C.R2GC_202_60})

V_112 = CTVertex(name = 'V_112',
                 type = 'R2',
                 particles = [ P.lqsb__tilde__, P.lqsb__tilde__, P.lqsb, P.lqsb ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(1,0,0):C.R2GC_146_42,(1,0,1):C.R2GC_146_43,(0,0,0):C.R2GC_146_42,(0,0,1):C.R2GC_146_43})

V_113 = CTVertex(name = 'V_113',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6, L.VVV7, L.VVV8 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsb], [P.lqsc], [P.lqsd], [P.lqss], [P.lqst], [P.lqsu] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_263_177,(0,0,1):C.UVGC_263_178,(0,0,2):C.UVGC_263_179,(0,0,3):C.UVGC_134_16,(0,0,4):C.UVGC_263_180,(0,0,6):C.UVGC_263_181,(0,0,7):C.UVGC_263_182,(0,0,8):C.UVGC_263_183,(0,0,9):C.UVGC_263_184,(0,0,10):C.UVGC_263_185,(0,0,11):C.UVGC_263_186,(0,1,0):C.UVGC_266_197,(0,1,1):C.UVGC_266_198,(0,1,2):C.UVGC_267_207,(0,1,3):C.UVGC_267_208,(0,1,4):C.UVGC_267_209,(0,1,6):C.UVGC_267_210,(0,1,7):C.UVGC_267_211,(0,1,8):C.UVGC_267_212,(0,1,9):C.UVGC_267_213,(0,1,10):C.UVGC_267_214,(0,1,11):C.UVGC_266_206,(0,3,0):C.UVGC_266_197,(0,3,1):C.UVGC_266_198,(0,3,2):C.UVGC_266_199,(0,3,3):C.UVGC_128_6,(0,3,4):C.UVGC_266_200,(0,3,6):C.UVGC_266_201,(0,3,7):C.UVGC_266_202,(0,3,8):C.UVGC_266_203,(0,3,9):C.UVGC_266_204,(0,3,10):C.UVGC_266_205,(0,3,11):C.UVGC_266_206,(0,5,0):C.UVGC_263_177,(0,5,1):C.UVGC_263_178,(0,5,2):C.UVGC_265_189,(0,5,3):C.UVGC_265_190,(0,5,4):C.UVGC_265_191,(0,5,6):C.UVGC_265_192,(0,5,7):C.UVGC_265_193,(0,5,8):C.UVGC_265_194,(0,5,9):C.UVGC_265_195,(0,5,10):C.UVGC_265_196,(0,5,11):C.UVGC_263_186,(0,6,0):C.UVGC_263_177,(0,6,1):C.UVGC_263_178,(0,6,2):C.UVGC_264_187,(0,6,3):C.UVGC_264_188,(0,6,4):C.UVGC_263_180,(0,6,6):C.UVGC_263_181,(0,6,7):C.UVGC_263_182,(0,6,8):C.UVGC_263_183,(0,6,9):C.UVGC_263_184,(0,6,10):C.UVGC_263_185,(0,6,11):C.UVGC_263_186,(0,7,0):C.UVGC_266_197,(0,7,1):C.UVGC_266_198,(0,7,2):C.UVGC_268_215,(0,7,3):C.UVGC_268_216,(0,7,4):C.UVGC_267_209,(0,7,6):C.UVGC_267_210,(0,7,7):C.UVGC_267_211,(0,7,8):C.UVGC_267_212,(0,7,9):C.UVGC_267_213,(0,7,10):C.UVGC_267_214,(0,7,11):C.UVGC_266_206,(0,2,2):C.UVGC_128_5,(0,2,3):C.UVGC_128_6,(0,4,2):C.UVGC_134_15,(0,4,3):C.UVGC_134_16,(0,4,5):C.UVGC_134_17})

V_114 = CTVertex(name = 'V_114',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV4 ],
                 loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.lqsb], [P.lqsc], [P.lqsd], [P.lqss], [P.lqst], [P.lqsu], [P.s], [P.t], [P.u] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsb], [P.lqsc], [P.lqsd], [P.lqss], [P.lqst], [P.lqsu] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,4):C.UVGC_130_10,(2,0,5):C.UVGC_130_9,(0,0,4):C.UVGC_130_10,(0,0,5):C.UVGC_130_9,(4,0,4):C.UVGC_129_7,(4,0,5):C.UVGC_129_8,(3,0,4):C.UVGC_129_7,(3,0,5):C.UVGC_129_8,(8,0,4):C.UVGC_130_9,(8,0,5):C.UVGC_130_10,(7,0,0):C.UVGC_272_246,(7,0,3):C.UVGC_151_57,(7,0,4):C.UVGC_272_247,(7,0,5):C.UVGC_272_248,(7,0,6):C.UVGC_272_249,(7,0,8):C.UVGC_272_250,(7,0,9):C.UVGC_272_251,(7,0,10):C.UVGC_272_252,(7,0,11):C.UVGC_272_253,(7,0,12):C.UVGC_272_254,(7,0,13):C.UVGC_272_255,(6,0,0):C.UVGC_272_246,(6,0,3):C.UVGC_151_57,(6,0,4):C.UVGC_273_256,(6,0,5):C.UVGC_273_257,(6,0,6):C.UVGC_272_249,(6,0,8):C.UVGC_272_250,(6,0,9):C.UVGC_272_251,(6,0,10):C.UVGC_272_252,(6,0,11):C.UVGC_272_253,(6,0,12):C.UVGC_272_254,(6,0,13):C.UVGC_272_255,(5,0,4):C.UVGC_129_7,(5,0,5):C.UVGC_129_8,(1,0,4):C.UVGC_129_7,(1,0,5):C.UVGC_129_8,(11,0,4):C.UVGC_133_13,(11,0,5):C.UVGC_133_14,(10,0,4):C.UVGC_133_13,(10,0,5):C.UVGC_133_14,(9,0,4):C.UVGC_132_11,(9,0,5):C.UVGC_132_12,(2,1,4):C.UVGC_130_10,(2,1,5):C.UVGC_130_9,(0,1,4):C.UVGC_130_10,(0,1,5):C.UVGC_130_9,(6,1,0):C.UVGC_269_217,(6,1,4):C.UVGC_270_227,(6,1,5):C.UVGC_270_228,(6,1,6):C.UVGC_270_229,(6,1,8):C.UVGC_270_230,(6,1,9):C.UVGC_270_231,(6,1,10):C.UVGC_270_232,(6,1,11):C.UVGC_270_233,(6,1,12):C.UVGC_270_234,(6,1,13):C.UVGC_269_226,(4,1,4):C.UVGC_129_7,(4,1,5):C.UVGC_129_8,(3,1,4):C.UVGC_129_7,(3,1,5):C.UVGC_129_8,(8,1,0):C.UVGC_274_258,(8,1,3):C.UVGC_274_259,(8,1,4):C.UVGC_274_260,(8,1,5):C.UVGC_274_261,(8,1,6):C.UVGC_274_262,(8,1,8):C.UVGC_274_263,(8,1,9):C.UVGC_274_264,(8,1,10):C.UVGC_274_265,(8,1,11):C.UVGC_274_266,(8,1,12):C.UVGC_274_267,(8,1,13):C.UVGC_274_268,(7,1,1):C.UVGC_135_18,(7,1,4):C.UVGC_136_21,(7,1,5):C.UVGC_136_22,(5,1,4):C.UVGC_129_7,(5,1,5):C.UVGC_129_8,(1,1,4):C.UVGC_129_7,(1,1,5):C.UVGC_129_8,(11,1,4):C.UVGC_133_13,(11,1,5):C.UVGC_133_14,(10,1,4):C.UVGC_133_13,(10,1,5):C.UVGC_133_14,(9,1,4):C.UVGC_132_11,(9,1,5):C.UVGC_132_12,(2,2,4):C.UVGC_130_10,(2,2,5):C.UVGC_130_9,(0,2,4):C.UVGC_130_10,(0,2,5):C.UVGC_130_9,(4,2,4):C.UVGC_129_7,(4,2,5):C.UVGC_129_8,(3,2,4):C.UVGC_129_7,(3,2,5):C.UVGC_129_8,(8,2,0):C.UVGC_271_235,(8,2,3):C.UVGC_271_236,(8,2,4):C.UVGC_271_237,(8,2,5):C.UVGC_271_238,(8,2,6):C.UVGC_271_239,(8,2,8):C.UVGC_271_240,(8,2,9):C.UVGC_271_241,(8,2,10):C.UVGC_271_242,(8,2,11):C.UVGC_271_243,(8,2,12):C.UVGC_271_244,(8,2,13):C.UVGC_271_245,(6,2,2):C.UVGC_135_18,(6,2,4):C.UVGC_135_19,(6,2,5):C.UVGC_132_11,(6,2,7):C.UVGC_135_20,(7,2,0):C.UVGC_269_217,(7,2,4):C.UVGC_269_218,(7,2,5):C.UVGC_269_219,(7,2,6):C.UVGC_269_220,(7,2,8):C.UVGC_269_221,(7,2,9):C.UVGC_269_222,(7,2,10):C.UVGC_269_223,(7,2,11):C.UVGC_269_224,(7,2,12):C.UVGC_269_225,(7,2,13):C.UVGC_269_226,(5,2,4):C.UVGC_129_7,(5,2,5):C.UVGC_129_8,(1,2,4):C.UVGC_129_7,(1,2,5):C.UVGC_129_8,(11,2,4):C.UVGC_133_13,(11,2,5):C.UVGC_133_14,(10,2,4):C.UVGC_133_13,(10,2,5):C.UVGC_133_14,(9,2,4):C.UVGC_132_11,(9,2,5):C.UVGC_132_12})

V_115 = CTVertex(name = 'V_115',
                 type = 'UV',
                 particles = [ P.ta__minus__, P.t, P.lqsb__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsb] ], [ [P.g, P.lqsb, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_236_125,(0,0,2):C.UVGC_278_272,(0,0,1):C.UVGC_236_126,(0,1,0):C.UVGC_237_127,(0,1,2):C.UVGC_279_273,(0,1,1):C.UVGC_237_128})

V_116 = CTVertex(name = 'V_116',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.b__tilde__, P.lqsb ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.b, P.g, P.lqsb] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(0,0,1):C.UVGC_191_88,(0,0,0):C.UVGC_191_89})

V_117 = CTVertex(name = 'V_117',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.c, P.lqsc__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsc] ], [ [P.g, P.lqsc] ] ],
                 couplings = {(0,0,0):C.UVGC_192_90,(0,0,2):C.UVGC_192_91,(0,0,1):C.UVGC_192_92})

V_118 = CTVertex(name = 'V_118',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.s, P.lqsc__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsc] ], [ [P.g, P.lqsc, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_232_116,(0,0,2):C.UVGC_232_117,(0,0,1):C.UVGC_232_118,(0,1,0):C.UVGC_233_119,(0,1,2):C.UVGC_233_120,(0,1,1):C.UVGC_233_121})

V_119 = CTVertex(name = 'V_119',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.d__tilde__, P.lqsd ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,1):C.UVGC_198_101,(0,0,0):C.UVGC_198_102})

V_120 = CTVertex(name = 'V_120',
                 type = 'UV',
                 particles = [ P.mu__minus__, P.c, P.lqss__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqss] ], [ [P.g, P.lqss] ] ],
                 couplings = {(0,0,0):C.UVGC_194_95,(0,0,2):C.UVGC_193_93,(0,0,1):C.UVGC_193_94,(0,1,0):C.UVGC_196_98,(0,1,2):C.UVGC_195_96,(0,1,1):C.UVGC_195_97})

V_121 = CTVertex(name = 'V_121',
                 type = 'UV',
                 particles = [ P.vm__tilde__, P.s__tilde__, P.lqss ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqss] ], [ [P.g, P.lqss, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_234_122,(0,0,1):C.UVGC_234_123})

V_122 = CTVertex(name = 'V_122',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.b, P.lqst__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqst] ], [ [P.g, P.lqst] ] ],
                 couplings = {(0,0,0):C.UVGC_247_141,(0,0,2):C.UVGC_247_142,(0,0,1):C.UVGC_247_143,(0,1,0):C.UVGC_248_144,(0,1,2):C.UVGC_248_145,(0,1,1):C.UVGC_248_146})

V_123 = CTVertex(name = 'V_123',
                 type = 'UV',
                 particles = [ P.vt__tilde__, P.t, P.lqst__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqst] ], [ [P.g, P.lqst, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_280_274,(0,0,2):C.UVGC_280_275,(0,0,1):C.UVGC_280_276})

V_124 = CTVertex(name = 'V_124',
                 type = 'UV',
                 particles = [ P.e__plus__, P.d, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_200_104,(0,0,2):C.UVGC_200_105,(0,0,1):C.UVGC_200_106,(0,1,0):C.UVGC_201_107,(0,1,2):C.UVGC_201_108,(0,1,1):C.UVGC_201_109})

V_125 = CTVertex(name = 'V_125',
                 type = 'UV',
                 particles = [ P.ve__tilde__, P.u, P.lqsu__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_243_135,(0,0,2):C.UVGC_243_136,(0,0,1):C.UVGC_243_137})

V_126 = CTVertex(name = 'V_126',
                 type = 'UV',
                 particles = [ P.u, P.e__minus__, P.lqsd__tilde__ ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_239_129,(0,0,1):C.UVGC_239_130,(0,1,0):C.UVGC_241_132,(0,1,1):C.UVGC_241_133})

V_127 = CTVertex(name = 'V_127',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_286_285,(0,0,2):C.UVGC_286_286,(0,0,1):C.UVGC_286_287,(0,1,0):C.UVGC_287_288,(0,1,2):C.UVGC_287_289,(0,1,1):C.UVGC_287_290})

V_128 = CTVertex(name = 'V_128',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_254_152})

V_129 = CTVertex(name = 'V_129',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_253_151})

V_130 = CTVertex(name = 'V_130',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.G0 ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_289_294})

V_131 = CTVertex(name = 'V_131',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_290_295})

V_132 = CTVertex(name = 'V_132',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_288_291,(0,0,2):C.UVGC_288_292,(0,0,1):C.UVGC_288_293,(0,1,0):C.UVGC_285_282,(0,1,2):C.UVGC_285_283,(0,1,1):C.UVGC_285_284})

V_133 = CTVertex(name = 'V_133',
                 type = 'UV',
                 particles = [ P.ta__plus__, P.t__tilde__, P.lqsb ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsb] ], [ [P.g, P.lqsb, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_237_127,(0,0,1):C.UVGC_237_128,(0,1,0):C.UVGC_236_125,(0,1,1):C.UVGC_236_126})

V_134 = CTVertex(name = 'V_134',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.mu__minus__, P.lqsc ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsc] ], [ [P.g, P.lqsc, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_233_119,(0,0,2):C.UVGC_233_120,(0,0,1):C.UVGC_233_121,(0,1,0):C.UVGC_232_116,(0,1,2):C.UVGC_232_117,(0,1,1):C.UVGC_232_118})

V_135 = CTVertex(name = 'V_135',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.e__plus__, P.lqsd ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsd] ], [ [P.g, P.lqsd, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_241_132,(0,0,2):C.UVGC_242_134,(0,0,1):C.UVGC_241_133,(0,1,0):C.UVGC_239_129,(0,1,2):C.UVGC_240_131,(0,1,1):C.UVGC_239_130})

V_136 = CTVertex(name = 'V_136',
                 type = 'UV',
                 particles = [ P.mu__plus__, P.c__tilde__, P.lqss ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g, P.lqss] ], [ [P.g, P.lqss] ] ],
                 couplings = {(0,0,1):C.UVGC_195_96,(0,0,0):C.UVGC_195_97,(0,1,1):C.UVGC_193_93,(0,1,0):C.UVGC_193_94})

V_137 = CTVertex(name = 'V_137',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.ta__minus__, P.lqst ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqst] ], [ [P.g, P.lqst] ] ],
                 couplings = {(0,0,0):C.UVGC_248_144,(0,0,2):C.UVGC_248_145,(0,0,1):C.UVGC_248_146,(0,1,0):C.UVGC_247_141,(0,1,2):C.UVGC_247_142,(0,1,1):C.UVGC_247_143})

V_138 = CTVertex(name = 'V_138',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.e__minus__, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS3, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsu] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_201_107,(0,0,2):C.UVGC_201_108,(0,0,1):C.UVGC_201_109,(0,1,0):C.UVGC_200_104,(0,1,2):C.UVGC_200_105,(0,1,1):C.UVGC_200_106})

V_139 = CTVertex(name = 'V_139',
                 type = 'UV',
                 particles = [ P.vt, P.b, P.lqsb__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.lqsb] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(0,0,0):C.UVGC_249_147,(0,0,2):C.UVGC_191_88,(0,0,1):C.UVGC_191_89})

V_140 = CTVertex(name = 'V_140',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.vm, P.lqsc ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.lqsc] ], [ [P.g, P.lqsc] ] ],
                 couplings = {(0,0,0):C.UVGC_192_90,(0,0,2):C.UVGC_192_91,(0,0,1):C.UVGC_192_92})

V_141 = CTVertex(name = 'V_141',
                 type = 'UV',
                 particles = [ P.ve, P.d, P.lqsd__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.lqsd] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_199_103,(0,0,2):C.UVGC_198_101,(0,0,1):C.UVGC_198_102})

V_142 = CTVertex(name = 'V_142',
                 type = 'UV',
                 particles = [ P.vm, P.s, P.lqss__tilde__ ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqss] ], [ [P.g, P.lqss, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_234_122,(0,0,2):C.UVGC_235_124,(0,0,1):C.UVGC_234_123})

V_143 = CTVertex(name = 'V_143',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.vt, P.lqst ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqst] ], [ [P.g, P.lqst, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_280_274,(0,0,2):C.UVGC_280_275,(0,0,1):C.UVGC_280_276})

V_144 = CTVertex(name = 'V_144',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.ve, P.lqsu ],
                 color = [ 'Identity(1,3)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.lqsu] ], [ [P.g, P.lqsu, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_243_135,(0,0,2):C.UVGC_243_136,(0,0,1):C.UVGC_243_137})

V_145 = CTVertex(name = 'V_145',
                 type = 'UV',
                 particles = [ P.g, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsu] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_180_86,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_179_85})

V_146 = CTVertex(name = 'V_146',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsu] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_181_87,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_181_87,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_147 = CTVertex(name = 'V_147',
                 type = 'UV',
                 particles = [ P.g, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsd] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_162_74,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_161_73})

V_148 = CTVertex(name = 'V_148',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsd] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_163_75,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_163_75,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_149 = CTVertex(name = 'V_149',
                 type = 'UV',
                 particles = [ P.g, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsc] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_156_70,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_155_69})

V_150 = CTVertex(name = 'V_150',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsc] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_157_71,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_157_71,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_151 = CTVertex(name = 'V_151',
                 type = 'UV',
                 particles = [ P.g, P.lqss__tilde__, P.lqss ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqss] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_168_78,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_167_77})

V_152 = CTVertex(name = 'V_152',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqss] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_169_79,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_169_79,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_153 = CTVertex(name = 'V_153',
                 type = 'UV',
                 particles = [ P.g, P.lqst__tilde__, P.lqst ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqst] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_174_82,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_173_81})

V_154 = CTVertex(name = 'V_154',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqst] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_175_83,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_175_83,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_155 = CTVertex(name = 'V_155',
                 type = 'UV',
                 particles = [ P.g, P.lqsb__tilde__, P.lqsb ],
                 color = [ 'T(1,3,2)' ],
                 lorentz = [ L.VSS1, L.VSS2 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsb] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_150_44,(0,0,1):C.UVGC_150_45,(0,0,2):C.UVGC_150_46,(0,0,3):C.UVGC_150_47,(0,0,5):C.UVGC_150_48,(0,0,6):C.UVGC_150_49,(0,0,7):C.UVGC_150_50,(0,0,8):C.UVGC_150_51,(0,0,9):C.UVGC_150_52,(0,0,10):C.UVGC_150_53,(0,0,11):C.UVGC_150_54,(0,0,4):C.UVGC_150_55,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_149_43})

V_156 = CTVertex(name = 'V_156',
                 type = 'UV',
                 particles = [ P.g, P.g, P.lqsb__tilde__, P.lqsb ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'T(1,-1,3)*T(2,4,-1)', 'T(1,4,-1)*T(2,-1,3)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.lqsb] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(2,0,0):C.UVGC_151_56,(2,0,1):C.UVGC_151_57,(2,0,2):C.UVGC_151_58,(2,0,3):C.UVGC_151_59,(2,0,5):C.UVGC_151_60,(2,0,6):C.UVGC_151_61,(2,0,7):C.UVGC_151_62,(2,0,8):C.UVGC_151_63,(2,0,9):C.UVGC_151_64,(2,0,10):C.UVGC_151_65,(2,0,11):C.UVGC_151_66,(2,0,4):C.UVGC_151_67,(1,0,0):C.UVGC_151_56,(1,0,1):C.UVGC_151_57,(1,0,2):C.UVGC_151_58,(1,0,3):C.UVGC_151_59,(1,0,5):C.UVGC_151_60,(1,0,6):C.UVGC_151_61,(1,0,7):C.UVGC_151_62,(1,0,8):C.UVGC_151_63,(1,0,9):C.UVGC_151_64,(1,0,10):C.UVGC_151_65,(1,0,11):C.UVGC_151_66,(1,0,4):C.UVGC_151_67,(0,0,2):C.UVGC_147_40,(0,0,4):C.UVGC_147_41})

V_157 = CTVertex(name = 'V_157',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_140_26,(0,1,0):C.UVGC_119_2,(0,2,0):C.UVGC_119_2})

V_158 = CTVertex(name = 'V_158',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_140_26,(0,1,0):C.UVGC_119_2,(0,2,0):C.UVGC_119_2})

V_159 = CTVertex(name = 'V_159',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_140_26,(0,1,0):C.UVGC_276_270,(0,2,0):C.UVGC_276_270})

V_160 = CTVertex(name = 'V_160',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_141_27,(0,0,1):C.UVGC_141_28,(0,0,2):C.UVGC_141_29,(0,0,3):C.UVGC_141_30,(0,0,5):C.UVGC_141_31,(0,0,6):C.UVGC_141_32,(0,0,7):C.UVGC_141_33,(0,0,8):C.UVGC_141_34,(0,0,9):C.UVGC_141_35,(0,0,10):C.UVGC_141_36,(0,0,11):C.UVGC_141_37,(0,0,4):C.UVGC_138_24,(0,1,4):C.UVGC_122_4,(0,2,4):C.UVGC_122_4})

V_161 = CTVertex(name = 'V_161',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,2):C.UVGC_138_24,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,3):C.UVGC_141_29,(0,1,4):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,2):C.UVGC_122_4,(0,2,0):C.UVGC_141_27,(0,2,1):C.UVGC_141_28,(0,2,3):C.UVGC_141_29,(0,2,4):C.UVGC_141_30,(0,2,5):C.UVGC_141_31,(0,2,6):C.UVGC_141_32,(0,2,7):C.UVGC_141_33,(0,2,8):C.UVGC_141_34,(0,2,9):C.UVGC_141_35,(0,2,10):C.UVGC_141_36,(0,2,11):C.UVGC_141_37,(0,2,2):C.UVGC_122_4})

V_162 = CTVertex(name = 'V_162',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_138_24,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_277_271,(0,2,0):C.UVGC_141_27,(0,2,1):C.UVGC_141_28,(0,2,2):C.UVGC_141_29,(0,2,3):C.UVGC_141_30,(0,2,5):C.UVGC_141_31,(0,2,6):C.UVGC_141_32,(0,2,7):C.UVGC_141_33,(0,2,8):C.UVGC_141_34,(0,2,9):C.UVGC_141_35,(0,2,10):C.UVGC_141_36,(0,2,11):C.UVGC_141_37,(0,2,4):C.UVGC_277_271})

V_163 = CTVertex(name = 'V_163',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_197_99,(0,0,1):C.UVGC_197_100})

V_164 = CTVertex(name = 'V_164',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_197_99,(0,0,1):C.UVGC_197_100})

V_165 = CTVertex(name = 'V_165',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_282_278,(0,0,2):C.UVGC_282_279,(0,0,1):C.UVGC_197_100})

V_166 = CTVertex(name = 'V_166',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_283_280,(0,1,0):C.UVGC_284_281})

V_167 = CTVertex(name = 'V_167',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_137_23,(0,1,0):C.UVGC_121_3,(0,2,0):C.UVGC_121_3})

V_168 = CTVertex(name = 'V_168',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_137_23,(0,1,0):C.UVGC_121_3,(0,2,0):C.UVGC_121_3})

V_169 = CTVertex(name = 'V_169',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_137_23,(0,1,0):C.UVGC_245_139,(0,2,0):C.UVGC_245_139})

V_170 = CTVertex(name = 'V_170',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_141_27,(0,0,1):C.UVGC_141_28,(0,0,3):C.UVGC_141_29,(0,0,4):C.UVGC_141_30,(0,0,5):C.UVGC_141_31,(0,0,6):C.UVGC_141_32,(0,0,7):C.UVGC_141_33,(0,0,8):C.UVGC_141_34,(0,0,9):C.UVGC_141_35,(0,0,10):C.UVGC_141_36,(0,0,11):C.UVGC_141_37,(0,0,2):C.UVGC_138_24,(0,1,2):C.UVGC_122_4,(0,2,2):C.UVGC_122_4})

V_171 = CTVertex(name = 'V_171',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,4):C.UVGC_138_24,(0,1,0):C.UVGC_141_27,(0,1,1):C.UVGC_141_28,(0,1,2):C.UVGC_141_29,(0,1,3):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,4):C.UVGC_122_4,(0,2,0):C.UVGC_141_27,(0,2,1):C.UVGC_141_28,(0,2,2):C.UVGC_141_29,(0,2,3):C.UVGC_141_30,(0,2,5):C.UVGC_141_31,(0,2,6):C.UVGC_141_32,(0,2,7):C.UVGC_141_33,(0,2,8):C.UVGC_141_34,(0,2,9):C.UVGC_141_35,(0,2,10):C.UVGC_141_36,(0,2,11):C.UVGC_141_37,(0,2,4):C.UVGC_122_4})

V_172 = CTVertex(name = 'V_172',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_138_24,(0,1,0):C.UVGC_141_27,(0,1,2):C.UVGC_141_28,(0,1,3):C.UVGC_141_29,(0,1,4):C.UVGC_141_30,(0,1,5):C.UVGC_141_31,(0,1,6):C.UVGC_141_32,(0,1,7):C.UVGC_141_33,(0,1,8):C.UVGC_141_34,(0,1,9):C.UVGC_141_35,(0,1,10):C.UVGC_141_36,(0,1,11):C.UVGC_141_37,(0,1,1):C.UVGC_246_140,(0,2,0):C.UVGC_141_27,(0,2,2):C.UVGC_141_28,(0,2,3):C.UVGC_141_29,(0,2,4):C.UVGC_141_30,(0,2,5):C.UVGC_141_31,(0,2,6):C.UVGC_141_32,(0,2,7):C.UVGC_141_33,(0,2,8):C.UVGC_141_34,(0,2,9):C.UVGC_141_35,(0,2,10):C.UVGC_141_36,(0,2,11):C.UVGC_141_37,(0,2,1):C.UVGC_246_140})

V_173 = CTVertex(name = 'V_173',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_197_99,(0,0,1):C.UVGC_197_100})

V_174 = CTVertex(name = 'V_174',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_197_99,(0,0,1):C.UVGC_197_100})

V_175 = CTVertex(name = 'V_175',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_282_278,(0,0,2):C.UVGC_282_279,(0,0,1):C.UVGC_197_100})

V_176 = CTVertex(name = 'V_176',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_251_149,(0,1,0):C.UVGC_252_150})

V_177 = CTVertex(name = 'V_177',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_250_148,(0,2,0):C.UVGC_250_148,(0,1,0):C.UVGC_244_138,(0,3,0):C.UVGC_244_138})

V_178 = CTVertex(name = 'V_178',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_139_25,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_179 = CTVertex(name = 'V_179',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_139_25,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_180 = CTVertex(name = 'V_180',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_139_25,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_181 = CTVertex(name = 'V_181',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_281_277,(0,2,0):C.UVGC_281_277,(0,1,0):C.UVGC_275_269,(0,3,0):C.UVGC_275_269})

V_182 = CTVertex(name = 'V_182',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_139_25,(0,1,0):C.UVGC_118_1,(0,2,0):C.UVGC_118_1})

V_183 = CTVertex(name = 'V_183',
                 type = 'UV',
                 particles = [ P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsu] ] ],
                 couplings = {(0,0,0):C.UVGC_260_158,(0,1,0):C.UVGC_178_84})

V_184 = CTVertex(name = 'V_184',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsd] ] ],
                 couplings = {(0,0,0):C.UVGC_257_155,(0,1,0):C.UVGC_160_72})

V_185 = CTVertex(name = 'V_185',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsc] ] ],
                 couplings = {(0,0,0):C.UVGC_256_154,(0,1,0):C.UVGC_154_68})

V_186 = CTVertex(name = 'V_186',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqss] ] ],
                 couplings = {(0,0,0):C.UVGC_258_156,(0,1,0):C.UVGC_166_76})

V_187 = CTVertex(name = 'V_187',
                 type = 'UV',
                 particles = [ P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqst] ] ],
                 couplings = {(0,0,0):C.UVGC_259_157,(0,1,0):C.UVGC_172_80})

V_188 = CTVertex(name = 'V_188',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.SS1, L.SS2 ],
                 loop_particles = [ [ [P.g, P.lqsb] ] ],
                 couplings = {(0,0,0):C.UVGC_255_153,(0,1,0):C.UVGC_148_42})

V_189 = CTVertex(name = 'V_189',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.lqsb] ], [ [P.lqsc] ], [ [P.lqsd] ], [ [P.lqss] ], [ [P.lqst] ], [ [P.lqsu] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_262_167,(0,0,1):C.UVGC_262_168,(0,0,2):C.UVGC_262_169,(0,0,3):C.UVGC_262_170,(0,0,4):C.UVGC_262_171,(0,0,5):C.UVGC_262_172,(0,0,6):C.UVGC_262_173,(0,0,7):C.UVGC_262_174,(0,0,8):C.UVGC_262_175,(0,0,9):C.UVGC_262_176,(0,1,0):C.UVGC_261_159,(0,1,3):C.UVGC_261_160,(0,1,4):C.UVGC_261_161,(0,1,5):C.UVGC_261_162,(0,1,6):C.UVGC_261_163,(0,1,7):C.UVGC_261_164,(0,1,8):C.UVGC_261_165,(0,1,9):C.UVGC_261_166})

V_190 = CTVertex(name = 'V_190',
                 type = 'UV',
                 particles = [ P.lqsu__tilde__, P.lqsu__tilde__, P.lqsu, P.lqsu ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_191 = CTVertex(name = 'V_191',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqsu] ], [ [P.g, P.lqsd, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_192 = CTVertex(name = 'V_192',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd__tilde__, P.lqsd, P.lqsd ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_193 = CTVertex(name = 'V_193',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsu] ], [ [P.g, P.lqsc, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_194 = CTVertex(name = 'V_194',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqsd] ], [ [P.g, P.lqsc, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_195 = CTVertex(name = 'V_195',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc__tilde__, P.lqsc, P.lqsc ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_196 = CTVertex(name = 'V_196',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqsu] ], [ [P.g, P.lqss, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_197 = CTVertex(name = 'V_197',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqss] ], [ [P.g, P.lqsd, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_198 = CTVertex(name = 'V_198',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqss] ], [ [P.g, P.lqsc, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_199 = CTVertex(name = 'V_199',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss__tilde__, P.lqss, P.lqss ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_200 = CTVertex(name = 'V_200',
                 type = 'UV',
                 particles = [ P.lqst__tilde__, P.lqst, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst], [P.g, P.lqsu] ], [ [P.g, P.lqst, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_201 = CTVertex(name = 'V_201',
                 type = 'UV',
                 particles = [ P.lqsd__tilde__, P.lqsd, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsd], [P.g, P.lqst] ], [ [P.g, P.lqsd, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_202 = CTVertex(name = 'V_202',
                 type = 'UV',
                 particles = [ P.lqsc__tilde__, P.lqsc, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsc], [P.g, P.lqst] ], [ [P.g, P.lqsc, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_203 = CTVertex(name = 'V_203',
                 type = 'UV',
                 particles = [ P.lqss__tilde__, P.lqss, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqss], [P.g, P.lqst] ], [ [P.g, P.lqss, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_204 = CTVertex(name = 'V_204',
                 type = 'UV',
                 particles = [ P.lqst__tilde__, P.lqst__tilde__, P.lqst, P.lqst ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

V_205 = CTVertex(name = 'V_205',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsu__tilde__, P.lqsu ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsu] ], [ [P.g, P.lqsb, P.lqsu] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_206 = CTVertex(name = 'V_206',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsd__tilde__, P.lqsd ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsd] ], [ [P.g, P.lqsb, P.lqsd] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_207 = CTVertex(name = 'V_207',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqsc__tilde__, P.lqsc ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqsc] ], [ [P.g, P.lqsb, P.lqsc] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_208 = CTVertex(name = 'V_208',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqss__tilde__, P.lqss ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqss] ], [ [P.g, P.lqsb, P.lqss] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_209 = CTVertex(name = 'V_209',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb, P.lqst__tilde__, P.lqst ],
                 color = [ 'Identity(1,2)*Identity(3,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb], [P.g, P.lqst] ], [ [P.g, P.lqsb, P.lqst] ] ],
                 couplings = {(1,0,0):C.UVGC_203_113,(1,0,1):C.UVGC_203_114,(1,0,2):C.UVGC_203_115,(0,0,0):C.UVGC_202_110,(0,0,1):C.UVGC_202_111,(0,0,2):C.UVGC_202_112})

V_210 = CTVertex(name = 'V_210',
                 type = 'UV',
                 particles = [ P.lqsb__tilde__, P.lqsb__tilde__, P.lqsb, P.lqsb ],
                 color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.SSSS1 ],
                 loop_particles = [ [ [P.g] ], [ [P.g, P.lqsb] ] ],
                 couplings = {(1,0,0):C.UVGC_146_38,(1,0,1):C.UVGC_146_39,(0,0,0):C.UVGC_146_38,(0,0,1):C.UVGC_146_39})

