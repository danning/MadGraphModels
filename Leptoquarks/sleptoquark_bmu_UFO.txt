Requestor: Markus Morgenstern
Contents: S3 Leptoquark with coupling to bmu
Source: Martin Schmaltz et al (private communication);  Paper pending
Paper: https://arxiv.org/abs/1706.05033 (general model)
JIRA: https://its.cern.ch/jira/browse/AGENE-1577
