# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 3 Sep 2016 20:49:32


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QLD = CouplingOrder(name = 'QLD',
                    expansion_order = 99,
                    hierarchy = 2)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 3)

