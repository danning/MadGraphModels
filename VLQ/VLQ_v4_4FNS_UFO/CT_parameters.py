# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 22 Aug 2019 12:22:54


from object_library import all_CTparameters, CTParameter

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



invFREps = CTParameter(name = 'invFREps',
                       type = 'complex',
                       value = {-1:'1'},
                       texname = 'invFREps')

FRCTdeltaZxuuLxuG = CTParameter(name = 'FRCTdeltaZxuuLxuG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxuuLxuG')

FRCTdeltaZxuuRxuG = CTParameter(name = 'FRCTdeltaZxuuRxuG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxuuRxuG')

FRCTdeltaZxccLxcG = CTParameter(name = 'FRCTdeltaZxccLxcG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxccLxcG')

FRCTdeltaZxccRxcG = CTParameter(name = 'FRCTdeltaZxccRxcG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxccRxcG')

FRCTdeltaxMTxtG = CTParameter(name = 'FRCTdeltaxMTxtG',
                              type = 'complex',
                              value = {0:'-(G**2*MT)/(3.*cmath.pi**2) + (G**2*MT*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxMTxtG')

FRCTdeltaZxttLxtG = CTParameter(name = 'FRCTdeltaZxttLxtG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxttLxtG')

FRCTdeltaZxttRxtG = CTParameter(name = 'FRCTdeltaZxttRxtG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxttRxtG')

FRCTdeltaZxddLxdG = CTParameter(name = 'FRCTdeltaZxddLxdG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxddLxdG')

FRCTdeltaZxddRxdG = CTParameter(name = 'FRCTdeltaZxddRxdG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxddRxdG')

FRCTdeltaZxssLxsG = CTParameter(name = 'FRCTdeltaZxssLxsG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxssLxsG')

FRCTdeltaZxssRxsG = CTParameter(name = 'FRCTdeltaZxssRxsG',
                                type = 'complex',
                                value = {-1:'G**2/(12.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxssRxsG')

FRCTdeltaxMBxbG = CTParameter(name = 'FRCTdeltaxMBxbG',
                              type = 'complex',
                              value = {0:'-(G**2*MB)/(3.*cmath.pi**2) + (G**2*MB*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxMBxbG')

FRCTdeltaZxbbLxbG = CTParameter(name = 'FRCTdeltaZxbbLxbG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxbbLxbG')

FRCTdeltaZxbbRxbG = CTParameter(name = 'FRCTdeltaZxbbRxbG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxbbRxbG')

FRCTdeltaxMXxxG = CTParameter(name = 'FRCTdeltaxMXxxG',
                              type = 'complex',
                              value = {0:'-(G**2*MX)/(3.*cmath.pi**2) + (G**2*MX*reglog(MX**2/MU_R**2))/(4.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxMXxxG')

FRCTdeltaZxxxLxxG = CTParameter(name = 'FRCTdeltaZxxxLxxG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MX**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxxxLxxG')

FRCTdeltaZxxxRxxG = CTParameter(name = 'FRCTdeltaZxxxRxxG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MX**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxxxRxxG')

FRCTdeltaxMTPxtpG = CTParameter(name = 'FRCTdeltaxMTPxtpG',
                                type = 'complex',
                                value = {0:'-(G**2*MTP)/(3.*cmath.pi**2) + (G**2*MTP*reglog(MTP**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxMTPxtpG')

FRCTdeltaZxtptpLxtpG = CTParameter(name = 'FRCTdeltaZxtptpLxtpG',
                                   type = 'complex',
                                   value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MTP**2/MU_R**2))/(4.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaZxtptpLxtpG')

FRCTdeltaZxtptpRxtpG = CTParameter(name = 'FRCTdeltaZxtptpRxtpG',
                                   type = 'complex',
                                   value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MTP**2/MU_R**2))/(4.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaZxtptpRxtpG')

FRCTdeltaxMBPxbpG = CTParameter(name = 'FRCTdeltaxMBPxbpG',
                                type = 'complex',
                                value = {0:'-(G**2*MBP)/(3.*cmath.pi**2) + (G**2*MBP*reglog(MBP**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaxMBPxbpG')

FRCTdeltaZxbpbpLxbpG = CTParameter(name = 'FRCTdeltaZxbpbpLxbpG',
                                   type = 'complex',
                                   value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MBP**2/MU_R**2))/(4.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaZxbpbpLxbpG')

FRCTdeltaZxbpbpRxbpG = CTParameter(name = 'FRCTdeltaZxbpbpRxbpG',
                                   type = 'complex',
                                   value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MBP**2/MU_R**2))/(4.*cmath.pi**2)'},
                                   texname = 'FRCTdeltaZxbpbpRxbpG')

FRCTdeltaxMYxyG = CTParameter(name = 'FRCTdeltaxMYxyG',
                              type = 'complex',
                              value = {0:'-(G**2*MY)/(3.*cmath.pi**2) + (G**2*MY*reglog(MY**2/MU_R**2))/(4.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxMYxyG')

FRCTdeltaZxyyLxyG = CTParameter(name = 'FRCTdeltaZxyyLxyG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MY**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxyyLxyG')

FRCTdeltaZxyyRxyG = CTParameter(name = 'FRCTdeltaZxyyRxyG',
                                type = 'complex',
                                value = {-1:'-G**2/(6.*cmath.pi**2)',0:'-G**2/(3.*cmath.pi**2) + (G**2*reglog(MY**2/MU_R**2))/(4.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxyyRxyG')

FRCTdeltaZxGGxc = CTParameter(name = 'FRCTdeltaZxGGxc',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxc')

FRCTdeltaZxGGxd = CTParameter(name = 'FRCTdeltaZxGGxd',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxd')

FRCTdeltaZxGGxG = CTParameter(name = 'FRCTdeltaZxGGxG',
                              type = 'complex',
                              value = {-1:'(-19*G**2)/(64.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxG')

FRCTdeltaZxGGxghG = CTParameter(name = 'FRCTdeltaZxGGxghG',
                                type = 'complex',
                                value = {-1:'-G**2/(64.*cmath.pi**2)'},
                                texname = 'FRCTdeltaZxGGxghG')

FRCTdeltaZxGGxs = CTParameter(name = 'FRCTdeltaZxGGxs',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxs')

FRCTdeltaZxGGxu = CTParameter(name = 'FRCTdeltaZxGGxu',
                              type = 'complex',
                              value = {-1:'G**2/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxu')

FRCTdeltaZxGGxb = CTParameter(name = 'FRCTdeltaZxGGxb',
                              type = 'complex',
                              value = {0:'(G**2*reglog(MB**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxb')

FRCTdeltaZxGGxbp = CTParameter(name = 'FRCTdeltaZxGGxbp',
                               type = 'complex',
                               value = {0:'(G**2*reglog(MBP**2/MU_R**2))/(24.*cmath.pi**2)'},
                               texname = 'FRCTdeltaZxGGxbp')

FRCTdeltaZxGGxt = CTParameter(name = 'FRCTdeltaZxGGxt',
                              type = 'complex',
                              value = {0:'(G**2*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxt')

FRCTdeltaZxGGxtp = CTParameter(name = 'FRCTdeltaZxGGxtp',
                               type = 'complex',
                               value = {0:'(G**2*reglog(MTP**2/MU_R**2))/(24.*cmath.pi**2)'},
                               texname = 'FRCTdeltaZxGGxtp')

FRCTdeltaZxGGxx = CTParameter(name = 'FRCTdeltaZxGGxx',
                              type = 'complex',
                              value = {0:'(G**2*reglog(MX**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxx')

FRCTdeltaZxGGxy = CTParameter(name = 'FRCTdeltaZxGGxy',
                              type = 'complex',
                              value = {0:'(G**2*reglog(MY**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaZxGGxy')

FRCTdeltaxaSxb = CTParameter(name = 'FRCTdeltaxaSxb',
                             type = 'complex',
                             value = {0:'-(aS*G**2*reglog(MB**2/MU_R**2))/(24.*cmath.pi**2)'},
                             texname = 'FRCTdeltaxaSxb')

FRCTdeltaxaSxbp = CTParameter(name = 'FRCTdeltaxaSxbp',
                              type = 'complex',
                              value = {0:'-(aS*G**2*reglog(MBP**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxaSxbp')

FRCTdeltaxaSxt = CTParameter(name = 'FRCTdeltaxaSxt',
                             type = 'complex',
                             value = {0:'-(aS*G**2*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                             texname = 'FRCTdeltaxaSxt')

FRCTdeltaxaSxtp = CTParameter(name = 'FRCTdeltaxaSxtp',
                              type = 'complex',
                              value = {0:'-(aS*G**2*reglog(MTP**2/MU_R**2))/(24.*cmath.pi**2)'},
                              texname = 'FRCTdeltaxaSxtp')

FRCTdeltaxaSxx = CTParameter(name = 'FRCTdeltaxaSxx',
                             type = 'complex',
                             value = {0:'-(aS*G**2*reglog(MX**2/MU_R**2))/(24.*cmath.pi**2)'},
                             texname = 'FRCTdeltaxaSxx')

FRCTdeltaxaSxy = CTParameter(name = 'FRCTdeltaxaSxy',
                             type = 'complex',
                             value = {0:'-(aS*G**2*reglog(MY**2/MU_R**2))/(24.*cmath.pi**2)'},
                             texname = 'FRCTdeltaxaSxy')

