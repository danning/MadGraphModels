# This file was automatically created by FeynRules 2.4.68
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Thu 22 Aug 2019 12:22:54


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV2 ],
               loop_particles = [ [ [P.b], [P.bp], [P.c], [P.d], [P.s], [P.t], [P.tp], [P.u], [P.x], [P.y] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_191_64,(0,0,1):C.R2GC_191_65})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [P.b], [P.bp], [P.c], [P.d], [P.s], [P.t], [P.tp], [P.u], [P.x], [P.y] ], [ [P.g] ] ],
               couplings = {(2,1,0):C.R2GC_184_57,(2,1,1):C.R2GC_184_58,(0,1,0):C.R2GC_184_57,(0,1,1):C.R2GC_184_58,(4,1,0):C.R2GC_182_53,(4,1,1):C.R2GC_182_54,(3,1,0):C.R2GC_182_53,(3,1,1):C.R2GC_182_54,(8,1,0):C.R2GC_183_55,(8,1,1):C.R2GC_183_56,(7,1,0):C.R2GC_193_67,(7,1,1):C.R2GC_189_63,(6,1,0):C.R2GC_192_66,(6,1,1):C.R2GC_194_68,(5,1,0):C.R2GC_182_53,(5,1,1):C.R2GC_182_54,(1,1,0):C.R2GC_182_53,(1,1,1):C.R2GC_182_54,(11,0,0):C.R2GC_186_60,(11,0,1):C.R2GC_186_61,(10,0,0):C.R2GC_186_60,(10,0,1):C.R2GC_186_61,(9,0,1):C.R2GC_185_59,(0,2,0):C.R2GC_184_57,(0,2,1):C.R2GC_184_58,(2,2,0):C.R2GC_184_57,(2,2,1):C.R2GC_184_58,(5,2,0):C.R2GC_182_53,(5,2,1):C.R2GC_182_54,(1,2,0):C.R2GC_182_53,(1,2,1):C.R2GC_182_54,(7,2,0):C.R2GC_193_67,(7,2,1):C.R2GC_183_56,(6,2,0):C.R2GC_196_69,(6,2,1):C.R2GC_196_70,(4,2,0):C.R2GC_182_53,(4,2,1):C.R2GC_182_54,(3,2,0):C.R2GC_182_53,(3,2,1):C.R2GC_182_54,(8,2,0):C.R2GC_183_55,(8,2,1):C.R2GC_189_63,(0,3,0):C.R2GC_184_57,(0,3,1):C.R2GC_184_58,(2,3,0):C.R2GC_184_57,(2,3,1):C.R2GC_184_58,(5,3,0):C.R2GC_182_53,(5,3,1):C.R2GC_182_54,(1,3,0):C.R2GC_182_53,(1,3,1):C.R2GC_182_54,(7,3,0):C.R2GC_197_71,(7,3,1):C.R2GC_188_62,(4,3,0):C.R2GC_182_53,(4,3,1):C.R2GC_182_54,(3,3,0):C.R2GC_182_53,(3,3,1):C.R2GC_182_54,(8,3,0):C.R2GC_183_55,(8,3,1):C.R2GC_188_62,(6,3,0):C.R2GC_192_66})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.bp__tilde__, P.bp, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.bp, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_199_73})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.tp__tilde__, P.tp, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.tp] ] ],
               couplings = {(0,0,0):C.R2GC_210_80})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.x__tilde__, P.x, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.x] ] ],
               couplings = {(0,0,0):C.R2GC_241_87})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.y__tilde__, P.y, P.a ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.g, P.y] ] ],
               couplings = {(0,0,0):C.R2GC_245_89})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.bp__tilde__, P.d, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS3 ],
               loop_particles = [ [ [P.bp, P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_266_109,(0,1,0):C.R2GC_265_108})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.bp__tilde__, P.s, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS3 ],
               loop_particles = [ [ [P.bp, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_274_117,(0,1,0):C.R2GC_273_116})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.bp__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS3 ],
               loop_particles = [ [ [P.b, P.bp, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_249_92,(0,1,0):C.R2GC_248_91})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.tp__tilde__, P.u, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.g, P.tp, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_294_135,(0,1,0):C.R2GC_293_134})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.tp__tilde__, P.c, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.c, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_260_103,(0,1,0):C.R2GC_259_102})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.tp__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.g, P.t, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_285_127,(0,1,0):C.R2GC_284_126})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_204_78})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_230_83})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.d__tilde__, P.bp, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.bp, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_265_108,(0,1,0):C.R2GC_266_109})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.s__tilde__, P.bp, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.bp, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_273_116,(0,1,0):C.R2GC_274_117})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.b__tilde__, P.bp, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.b, P.bp, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_248_91,(0,1,0):C.R2GC_249_92})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.u__tilde__, P.tp, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.g, P.tp, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_293_134,(0,1,0):C.R2GC_294_135})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.c__tilde__, P.tp, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.c, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_259_102,(0,1,0):C.R2GC_260_103})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.t__tilde__, P.tp, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS3 ],
                loop_particles = [ [ [P.g, P.t, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_284_126,(0,1,0):C.R2GC_285_127})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.tp__tilde__, P.tp, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.bp__tilde__, P.bp, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.bp, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.x__tilde__, P.x, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.y__tilde__, P.y, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.tp__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_269_112,(0,1,0):C.R2GC_270_113})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.tp__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_277_120,(0,1,0):C.R2GC_278_121})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.tp__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_252_95,(0,1,0):C.R2GC_253_96})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.x__tilde__, P.u, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_297_138,(0,1,0):C.R2GC_298_139})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.x__tilde__, P.c, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_263_106,(0,1,0):C.R2GC_264_107})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.x__tilde__, P.t, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_288_130,(0,1,0):C.R2GC_289_131})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.bp__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_290_132,(0,1,0):C.R2GC_291_133})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.bp__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_256_99,(0,1,0):C.R2GC_257_100})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.bp__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_282_124,(0,1,0):C.R2GC_283_125})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.y__tilde__, P.d, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_271_114,(0,1,0):C.R2GC_272_115})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.y__tilde__, P.s, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_279_122,(0,1,0):C.R2GC_280_123})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.y__tilde__, P.b, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_254_97,(0,1,0):C.R2GC_255_98})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.bp__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_267_110,(0,1,0):C.R2GC_268_111})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.bp__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_275_118,(0,1,0):C.R2GC_276_119})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.bp__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.bp, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_250_93,(0,1,0):C.R2GC_251_94})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.tp__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.tp, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_295_136,(0,1,0):C.R2GC_296_137})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.tp__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_261_104,(0,1,0):C.R2GC_262_105})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.tp__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_286_128,(0,1,0):C.R2GC_287_129})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.d__tilde__, P.tp, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_269_112,(0,1,0):C.R2GC_270_113})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.s__tilde__, P.tp, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_277_120,(0,1,0):C.R2GC_278_121})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.b__tilde__, P.tp, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_252_95,(0,1,0):C.R2GC_253_96})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.u__tilde__, P.x, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_297_138,(0,1,0):C.R2GC_298_139})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.c__tilde__, P.x, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_263_106,(0,1,0):C.R2GC_264_107})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.t__tilde__, P.x, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_288_130,(0,1,0):C.R2GC_289_131})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.u__tilde__, P.bp, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_290_132,(0,1,0):C.R2GC_291_133})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.c__tilde__, P.bp, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_256_99,(0,1,0):C.R2GC_257_100})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.t__tilde__, P.bp, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_282_124,(0,1,0):C.R2GC_283_125})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.d__tilde__, P.y, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_271_114,(0,1,0):C.R2GC_272_115})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.s__tilde__, P.y, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_279_122,(0,1,0):C.R2GC_280_123})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.b__tilde__, P.y, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_254_97,(0,1,0):C.R2GC_255_98})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.d__tilde__, P.bp, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_267_110,(0,1,0):C.R2GC_268_111})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.s__tilde__, P.bp, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.bp, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_275_118,(0,1,0):C.R2GC_276_119})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.b__tilde__, P.bp, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.bp, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_250_93,(0,1,0):C.R2GC_251_94})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.u__tilde__, P.tp, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.tp, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_295_136,(0,1,0):C.R2GC_296_137})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.c__tilde__, P.tp, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_261_104,(0,1,0):C.R2GC_262_105})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.t__tilde__, P.tp, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.t, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_286_128,(0,1,0):C.R2GC_287_129})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_210_80})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_210_80})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_210_80})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_199_73})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_199_73})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_199_73})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_200_74})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_258_101})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_258_101})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_258_101})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_258_101})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_258_101})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_258_101})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_238_85,(0,1,0):C.R2GC_239_86})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV7 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_212_81,(0,1,0):C.R2GC_203_77})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV7 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_212_81,(0,1,0):C.R2GC_203_77})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_202_76,(0,1,0):C.R2GC_203_77})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_202_76,(0,1,0):C.R2GC_203_77})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_202_76,(0,1,0):C.R2GC_203_77})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_198_72})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_198_72})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_227_82,(0,1,0):C.R2GC_198_72})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_198_72})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_198_72})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_201_75,(0,1,0):C.R2GC_198_72})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.x__tilde__, P.x ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.x] ] ],
                couplings = {(0,0,0):C.R2GC_243_88,(0,1,0):C.R2GC_198_72})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.tp__tilde__, P.tp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.tp] ] ],
                couplings = {(0,0,0):C.R2GC_234_84,(0,1,0):C.R2GC_198_72})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.bp__tilde__, P.bp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF5 ],
                loop_particles = [ [ [P.bp, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_208_79,(0,1,0):C.R2GC_198_72})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.y__tilde__, P.y ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF3, L.FF5 ],
                loop_particles = [ [ [P.g, P.y] ] ],
                couplings = {(0,0,0):C.R2GC_247_90,(0,1,0):C.R2GC_198_72})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.b], [P.bp], [P.c], [P.d], [P.s], [P.t], [P.tp], [P.u], [P.x], [P.y] ], [ [P.g] ], [ [P.t] ], [ [P.tp] ], [ [P.x] ], [ [P.y] ] ],
                couplings = {(0,2,3):C.R2GC_98_140,(0,0,0):C.R2GC_109_22,(0,0,1):C.R2GC_109_23,(0,0,4):C.R2GC_109_24,(0,0,5):C.R2GC_109_25,(0,0,6):C.R2GC_109_26,(0,0,7):C.R2GC_109_27,(0,1,2):C.R2GC_106_13})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.g, P.g, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVV1 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_101_3,(0,0,1):C.R2GC_101_4})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_99_141,(0,0,1):C.R2GC_99_142})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [P.bp, P.c] ], [ [P.bp, P.t] ], [ [P.bp, P.u] ], [ [P.b, P.tp] ], [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ], [ [P.b, P.y] ], [ [P.c, P.x] ], [ [P.d, P.tp] ], [ [P.d, P.y] ], [ [P.s, P.tp] ], [ [P.s, P.y] ], [ [P.t, P.x] ], [ [P.u, P.x] ] ],
                couplings = {(0,0,3):C.R2GC_110_28,(0,0,0):C.R2GC_110_29,(0,0,1):C.R2GC_110_30,(0,0,2):C.R2GC_110_31,(0,0,7):C.R2GC_110_32,(0,0,9):C.R2GC_110_33,(0,0,5):C.R2GC_114_47,(0,0,6):C.R2GC_114_48,(0,0,8):C.R2GC_114_49,(0,0,10):C.R2GC_114_50,(0,0,11):C.R2GC_114_51,(0,0,12):C.R2GC_114_52,(0,0,4):C.R2GC_111_34})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV10 ],
                loop_particles = [ [ [P.b, P.bp] ], [ [P.b], [P.d], [P.s] ], [ [P.bp, P.d] ], [ [P.bp, P.s] ], [ [P.c, P.tp] ], [ [P.c], [P.t], [P.u] ], [ [P.tp, P.u] ], [ [P.t, P.tp] ] ],
                couplings = {(0,0,1):C.R2GC_105_11,(0,0,5):C.R2GC_105_12,(0,0,0):C.R2GC_113_41,(0,0,2):C.R2GC_113_42,(0,0,3):C.R2GC_113_43,(0,0,4):C.R2GC_113_44,(0,0,7):C.R2GC_113_45,(0,0,6):C.R2GC_113_46})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.Z ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_102_5,(0,0,1):C.R2GC_102_6})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.a, P.a, P.g, P.g ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.bp], [P.d], [P.s] ], [ [P.c], [P.t], [P.tp], [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,0):C.R2GC_107_14,(0,0,1):C.R2GC_107_15,(0,0,2):C.R2GC_107_16,(0,0,3):C.R2GC_107_17})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.Z ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(1,0,0):C.R2GC_104_9,(1,0,1):C.R2GC_104_10,(0,1,0):C.R2GC_103_7,(0,1,1):C.R2GC_103_8})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.g ],
                 color = [ 'd(2,3,4)' ],
                 lorentz = [ L.VVVV10 ],
                 loop_particles = [ [ [P.b], [P.bp], [P.d], [P.s] ], [ [P.c], [P.t], [P.tp], [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,0):C.R2GC_108_18,(0,0,1):C.R2GC_108_19,(0,0,2):C.R2GC_108_20,(0,0,3):C.R2GC_108_21})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.g, P.g, P.H, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.bp] ], [ [P.bp, P.d] ], [ [P.bp, P.s] ], [ [P.c, P.tp] ], [ [P.t] ], [ [P.tp, P.u] ], [ [P.t, P.tp] ] ],
                 couplings = {(0,0,0):C.R2GC_100_1,(0,0,5):C.R2GC_100_2,(0,0,1):C.R2GC_112_35,(0,0,2):C.R2GC_112_36,(0,0,3):C.R2GC_112_37,(0,0,4):C.R2GC_112_38,(0,0,7):C.R2GC_112_39,(0,0,6):C.R2GC_112_40})

V_105 = CTVertex(name = 'V_105',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV2 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_191_124,(0,0,1):C.UVGC_191_125,(0,0,2):C.UVGC_191_126,(0,0,3):C.UVGC_191_127,(0,0,4):C.UVGC_191_128,(0,0,5):C.UVGC_191_129,(0,0,6):C.UVGC_191_130,(0,0,7):C.UVGC_191_131,(0,0,8):C.UVGC_191_132,(0,0,9):C.UVGC_191_133,(0,0,10):C.UVGC_191_134,(0,0,11):C.UVGC_191_135})

V_106 = CTVertex(name = 'V_106',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.b], [P.bp], [P.c], [P.d], [P.s], [P.t], [P.tp], [P.u], [P.x], [P.y] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(2,1,5):C.UVGC_183_71,(2,1,6):C.UVGC_183_70,(0,1,5):C.UVGC_183_71,(0,1,6):C.UVGC_183_70,(4,1,5):C.UVGC_182_68,(4,1,6):C.UVGC_182_69,(3,1,5):C.UVGC_182_68,(3,1,6):C.UVGC_182_69,(8,1,5):C.UVGC_183_70,(8,1,6):C.UVGC_183_71,(7,1,0):C.UVGC_194_138,(7,1,1):C.UVGC_194_139,(7,1,3):C.UVGC_194_140,(7,1,4):C.UVGC_194_141,(7,1,5):C.UVGC_189_104,(7,1,6):C.UVGC_195_150,(7,1,7):C.UVGC_194_144,(7,1,8):C.UVGC_194_145,(7,1,9):C.UVGC_194_146,(7,1,10):C.UVGC_194_147,(7,1,11):C.UVGC_194_148,(7,1,12):C.UVGC_194_149,(6,1,0):C.UVGC_194_138,(6,1,1):C.UVGC_194_139,(6,1,3):C.UVGC_194_140,(6,1,4):C.UVGC_194_141,(6,1,5):C.UVGC_194_142,(6,1,6):C.UVGC_194_143,(6,1,7):C.UVGC_194_144,(6,1,8):C.UVGC_194_145,(6,1,9):C.UVGC_194_146,(6,1,10):C.UVGC_194_147,(6,1,11):C.UVGC_194_148,(6,1,12):C.UVGC_194_149,(5,1,5):C.UVGC_182_68,(5,1,6):C.UVGC_182_69,(1,1,5):C.UVGC_182_68,(1,1,6):C.UVGC_182_69,(11,0,5):C.UVGC_186_74,(11,0,6):C.UVGC_186_75,(10,0,5):C.UVGC_186_74,(10,0,6):C.UVGC_186_75,(9,0,5):C.UVGC_185_72,(9,0,6):C.UVGC_185_73,(0,2,5):C.UVGC_183_71,(0,2,6):C.UVGC_183_70,(2,2,5):C.UVGC_183_71,(2,2,6):C.UVGC_183_70,(5,2,5):C.UVGC_182_68,(5,2,6):C.UVGC_182_69,(1,2,5):C.UVGC_182_68,(1,2,6):C.UVGC_182_69,(7,2,2):C.UVGC_192_136,(7,2,5):C.UVGC_183_70,(7,2,6):C.UVGC_193_137,(6,2,0):C.UVGC_196_151,(6,2,1):C.UVGC_196_152,(6,2,3):C.UVGC_196_153,(6,2,4):C.UVGC_196_154,(6,2,5):C.UVGC_196_155,(6,2,6):C.UVGC_196_156,(6,2,7):C.UVGC_196_157,(6,2,8):C.UVGC_196_158,(6,2,9):C.UVGC_196_159,(6,2,10):C.UVGC_196_160,(6,2,11):C.UVGC_196_161,(6,2,12):C.UVGC_196_162,(4,2,5):C.UVGC_182_68,(4,2,6):C.UVGC_182_69,(3,2,5):C.UVGC_182_68,(3,2,6):C.UVGC_182_69,(8,2,0):C.UVGC_189_100,(8,2,1):C.UVGC_189_101,(8,2,3):C.UVGC_189_102,(8,2,4):C.UVGC_189_103,(8,2,5):C.UVGC_189_104,(8,2,6):C.UVGC_189_105,(8,2,7):C.UVGC_189_106,(8,2,8):C.UVGC_189_107,(8,2,9):C.UVGC_189_108,(8,2,10):C.UVGC_189_109,(8,2,11):C.UVGC_189_110,(8,2,12):C.UVGC_189_111,(0,3,5):C.UVGC_183_71,(0,3,6):C.UVGC_183_70,(2,3,5):C.UVGC_183_71,(2,3,6):C.UVGC_183_70,(5,3,5):C.UVGC_182_68,(5,3,6):C.UVGC_182_69,(1,3,5):C.UVGC_182_68,(1,3,6):C.UVGC_182_69,(7,3,0):C.UVGC_196_151,(7,3,1):C.UVGC_196_152,(7,3,3):C.UVGC_196_153,(7,3,4):C.UVGC_196_154,(7,3,5):C.UVGC_188_92,(7,3,6):C.UVGC_197_163,(7,3,7):C.UVGC_196_157,(7,3,8):C.UVGC_196_158,(7,3,9):C.UVGC_196_159,(7,3,10):C.UVGC_196_160,(7,3,11):C.UVGC_196_161,(7,3,12):C.UVGC_196_162,(4,3,5):C.UVGC_182_68,(4,3,6):C.UVGC_182_69,(3,3,5):C.UVGC_182_68,(3,3,6):C.UVGC_182_69,(8,3,0):C.UVGC_188_88,(8,3,1):C.UVGC_188_89,(8,3,3):C.UVGC_188_90,(8,3,4):C.UVGC_188_91,(8,3,5):C.UVGC_188_92,(8,3,6):C.UVGC_188_93,(8,3,7):C.UVGC_188_94,(8,3,8):C.UVGC_188_95,(8,3,9):C.UVGC_188_96,(8,3,10):C.UVGC_188_97,(8,3,11):C.UVGC_188_98,(8,3,12):C.UVGC_188_99,(6,3,2):C.UVGC_192_136,(6,3,6):C.UVGC_185_72})

V_107 = CTVertex(name = 'V_107',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.bp, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_199_165,(0,1,0):C.UVGC_125_11,(0,2,0):C.UVGC_127_13})

V_108 = CTVertex(name = 'V_108',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.tp, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_210_172,(0,1,0):C.UVGC_159_45,(0,2,0):C.UVGC_161_47})

V_109 = CTVertex(name = 'V_109',
                 type = 'UV',
                 particles = [ P.x__tilde__, P.x, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_241_182,(0,1,0):C.UVGC_171_57,(0,2,0):C.UVGC_173_59})

V_110 = CTVertex(name = 'V_110',
                 type = 'UV',
                 particles = [ P.y__tilde__, P.y, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_245_184,(0,1,0):C.UVGC_177_63,(0,2,0):C.UVGC_179_65})

V_111 = CTVertex(name = 'V_111',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.d, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.bp, P.d, P.g] ], [ [P.bp, P.g] ], [ [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_266_240,(0,0,2):C.UVGC_266_241,(0,0,0):C.UVGC_266_242,(0,1,1):C.UVGC_265_237,(0,1,2):C.UVGC_265_238,(0,1,0):C.UVGC_265_239})

V_112 = CTVertex(name = 'V_112',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.s, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_274_264,(0,0,2):C.UVGC_274_265,(0,0,1):C.UVGC_274_266,(0,1,0):C.UVGC_273_261,(0,1,2):C.UVGC_273_262,(0,1,1):C.UVGC_273_263})

V_113 = CTVertex(name = 'V_113',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.b, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.b, P.bp, P.g] ], [ [P.b, P.g] ], [ [P.bp, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_249_189,(0,0,2):C.UVGC_249_190,(0,0,0):C.UVGC_249_191,(0,1,1):C.UVGC_248_186,(0,1,2):C.UVGC_248_187,(0,1,0):C.UVGC_248_188})

V_114 = CTVertex(name = 'V_114',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.u, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.g, P.tp] ], [ [P.g, P.tp, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_294_322,(0,0,2):C.UVGC_294_323,(0,0,1):C.UVGC_294_324,(0,1,0):C.UVGC_293_319,(0,1,2):C.UVGC_293_320,(0,1,1):C.UVGC_293_321})

V_115 = CTVertex(name = 'V_115',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.c, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_260_222,(0,0,2):C.UVGC_260_223,(0,0,1):C.UVGC_260_224,(0,1,0):C.UVGC_259_219,(0,1,2):C.UVGC_259_220,(0,1,1):C.UVGC_259_221})

V_116 = CTVertex(name = 'V_116',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.t, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.g, P.t] ], [ [P.g, P.tp] ], [ [P.g, P.t, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_285_296,(0,0,1):C.UVGC_285_297,(0,0,2):C.UVGC_285_298,(0,1,0):C.UVGC_284_293,(0,1,1):C.UVGC_284_294,(0,1,2):C.UVGC_284_295})

V_117 = CTVertex(name = 'V_117',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_204_170})

V_118 = CTVertex(name = 'V_118',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_230_178})

V_119 = CTVertex(name = 'V_119',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.bp, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.bp, P.d, P.g] ], [ [P.bp, P.g] ], [ [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_265_237,(0,0,2):C.UVGC_265_238,(0,0,0):C.UVGC_265_239,(0,1,1):C.UVGC_266_240,(0,1,2):C.UVGC_266_241,(0,1,0):C.UVGC_266_242})

V_120 = CTVertex(name = 'V_120',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.bp, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_273_261,(0,0,2):C.UVGC_273_262,(0,0,1):C.UVGC_273_263,(0,1,0):C.UVGC_274_264,(0,1,2):C.UVGC_274_265,(0,1,1):C.UVGC_274_266})

V_121 = CTVertex(name = 'V_121',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.bp, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.b, P.bp, P.g] ], [ [P.b, P.g] ], [ [P.bp, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_248_186,(0,0,2):C.UVGC_248_187,(0,0,0):C.UVGC_248_188,(0,1,1):C.UVGC_249_189,(0,1,2):C.UVGC_249_190,(0,1,0):C.UVGC_249_191})

V_122 = CTVertex(name = 'V_122',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.tp, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.g, P.tp] ], [ [P.g, P.tp, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_293_319,(0,0,2):C.UVGC_293_320,(0,0,1):C.UVGC_293_321,(0,1,0):C.UVGC_294_322,(0,1,2):C.UVGC_294_323,(0,1,1):C.UVGC_294_324})

V_123 = CTVertex(name = 'V_123',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.tp, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_259_219,(0,0,2):C.UVGC_259_220,(0,0,1):C.UVGC_259_221,(0,1,0):C.UVGC_260_222,(0,1,2):C.UVGC_260_223,(0,1,1):C.UVGC_260_224})

V_124 = CTVertex(name = 'V_124',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.tp, P.H ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS3 ],
                 loop_particles = [ [ [P.g, P.t] ], [ [P.g, P.tp] ], [ [P.g, P.t, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_284_293,(0,0,1):C.UVGC_284_294,(0,0,2):C.UVGC_284_295,(0,1,0):C.UVGC_285_296,(0,1,1):C.UVGC_285_297,(0,1,2):C.UVGC_285_298})

V_125 = CTVertex(name = 'V_125',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.tp, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.tp] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,6):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,4):C.UVGC_187_80,(0,3,5):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,6):C.UVGC_162_48,(0,2,6):C.UVGC_163_49})

V_126 = CTVertex(name = 'V_126',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.bp, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.bp, P.g] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,2):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,3):C.UVGC_187_78,(0,3,4):C.UVGC_187_79,(0,3,5):C.UVGC_187_80,(0,3,6):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,2):C.UVGC_128_14,(0,2,2):C.UVGC_129_15})

V_127 = CTVertex(name = 'V_127',
                 type = 'UV',
                 particles = [ P.x__tilde__, P.x, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.x] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,6):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,4):C.UVGC_187_80,(0,3,5):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,6):C.UVGC_174_60,(0,2,6):C.UVGC_175_61})

V_128 = CTVertex(name = 'V_128',
                 type = 'UV',
                 particles = [ P.y__tilde__, P.y, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.y] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,6):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,4):C.UVGC_187_80,(0,3,5):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,6):C.UVGC_180_66,(0,2,6):C.UVGC_181_67})

V_129 = CTVertex(name = 'V_129',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_269_249,(0,0,2):C.UVGC_269_250,(0,0,1):C.UVGC_269_251,(0,1,0):C.UVGC_270_252,(0,1,2):C.UVGC_270_253,(0,1,1):C.UVGC_270_254})

V_130 = CTVertex(name = 'V_130',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_277_273,(0,0,2):C.UVGC_277_274,(0,0,1):C.UVGC_277_275,(0,1,0):C.UVGC_278_276,(0,1,2):C.UVGC_278_277,(0,1,1):C.UVGC_278_278})

V_131 = CTVertex(name = 'V_131',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_252_198,(0,0,2):C.UVGC_252_199,(0,0,1):C.UVGC_252_200,(0,1,0):C.UVGC_253_201,(0,1,2):C.UVGC_253_202,(0,1,1):C.UVGC_253_203})

V_132 = CTVertex(name = 'V_132',
                 type = 'UV',
                 particles = [ P.x__tilde__, P.u, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ], [ [P.g, P.u, P.x] ], [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_297_331,(0,0,2):C.UVGC_297_332,(0,0,1):C.UVGC_297_333,(0,1,0):C.UVGC_298_334,(0,1,2):C.UVGC_298_335,(0,1,1):C.UVGC_298_336})

V_133 = CTVertex(name = 'V_133',
                 type = 'UV',
                 particles = [ P.x__tilde__, P.c, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.x] ], [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_263_231,(0,0,2):C.UVGC_263_232,(0,0,1):C.UVGC_263_233,(0,1,0):C.UVGC_264_234,(0,1,2):C.UVGC_264_235,(0,1,1):C.UVGC_264_236})

V_134 = CTVertex(name = 'V_134',
                 type = 'UV',
                 particles = [ P.x__tilde__, P.t, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ], [ [P.g, P.t, P.x] ], [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_288_305,(0,0,2):C.UVGC_288_306,(0,0,1):C.UVGC_288_307,(0,1,0):C.UVGC_289_308,(0,1,2):C.UVGC_289_309,(0,1,1):C.UVGC_289_310})

V_135 = CTVertex(name = 'V_135',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_290_311,(0,0,2):C.UVGC_290_312,(0,0,1):C.UVGC_290_313,(0,1,0):C.UVGC_291_314,(0,1,2):C.UVGC_291_315,(0,1,1):C.UVGC_291_316})

V_136 = CTVertex(name = 'V_136',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.c, P.g] ], [ [P.bp, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_256_210,(0,0,2):C.UVGC_256_211,(0,0,0):C.UVGC_256_212,(0,1,1):C.UVGC_257_213,(0,1,2):C.UVGC_257_214,(0,1,0):C.UVGC_257_215})

V_137 = CTVertex(name = 'V_137',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_282_287,(0,0,2):C.UVGC_282_288,(0,0,1):C.UVGC_282_289,(0,1,0):C.UVGC_283_290,(0,1,2):C.UVGC_283_291,(0,1,1):C.UVGC_283_292})

V_138 = CTVertex(name = 'V_138',
                 type = 'UV',
                 particles = [ P.y__tilde__, P.d, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.y] ], [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_271_255,(0,0,2):C.UVGC_271_256,(0,0,1):C.UVGC_271_257,(0,1,0):C.UVGC_272_258,(0,1,2):C.UVGC_272_259,(0,1,1):C.UVGC_272_260})

V_139 = CTVertex(name = 'V_139',
                 type = 'UV',
                 particles = [ P.y__tilde__, P.s, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.y] ], [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_279_279,(0,0,2):C.UVGC_279_280,(0,0,1):C.UVGC_279_281,(0,1,0):C.UVGC_280_282,(0,1,2):C.UVGC_280_283,(0,1,1):C.UVGC_280_284})

V_140 = CTVertex(name = 'V_140',
                 type = 'UV',
                 particles = [ P.y__tilde__, P.b, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.y] ], [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_254_204,(0,0,2):C.UVGC_254_205,(0,0,1):C.UVGC_254_206,(0,1,0):C.UVGC_255_207,(0,1,2):C.UVGC_255_208,(0,1,1):C.UVGC_255_209})

V_141 = CTVertex(name = 'V_141',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.d, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.d, P.g] ], [ [P.bp, P.g] ], [ [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_267_243,(0,0,2):C.UVGC_267_244,(0,0,0):C.UVGC_267_245,(0,1,1):C.UVGC_268_246,(0,1,2):C.UVGC_268_247,(0,1,0):C.UVGC_268_248})

V_142 = CTVertex(name = 'V_142',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.s, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_275_267,(0,0,2):C.UVGC_275_268,(0,0,1):C.UVGC_275_269,(0,1,0):C.UVGC_276_270,(0,1,2):C.UVGC_276_271,(0,1,1):C.UVGC_276_272})

V_143 = CTVertex(name = 'V_143',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.bp, P.g] ], [ [P.b, P.g] ], [ [P.bp, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_250_192,(0,0,2):C.UVGC_250_193,(0,0,0):C.UVGC_250_194,(0,1,1):C.UVGC_251_195,(0,1,2):C.UVGC_251_196,(0,1,0):C.UVGC_251_197})

V_144 = CTVertex(name = 'V_144',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.u, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.tp] ], [ [P.g, P.tp, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_295_325,(0,0,2):C.UVGC_295_326,(0,0,1):C.UVGC_295_327,(0,1,0):C.UVGC_296_328,(0,1,2):C.UVGC_296_329,(0,1,1):C.UVGC_296_330})

V_145 = CTVertex(name = 'V_145',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.c, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_261_225,(0,0,2):C.UVGC_261_226,(0,0,1):C.UVGC_261_227,(0,1,0):C.UVGC_262_228,(0,1,2):C.UVGC_262_229,(0,1,1):C.UVGC_262_230})

V_146 = CTVertex(name = 'V_146',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ], [ [P.g, P.tp] ], [ [P.g, P.t, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_286_299,(0,0,1):C.UVGC_286_300,(0,0,2):C.UVGC_286_301,(0,1,0):C.UVGC_287_302,(0,1,1):C.UVGC_287_303,(0,1,2):C.UVGC_287_304})

V_147 = CTVertex(name = 'V_147',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.tp, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_269_249,(0,0,2):C.UVGC_269_250,(0,0,1):C.UVGC_269_251,(0,1,0):C.UVGC_270_252,(0,1,2):C.UVGC_270_253,(0,1,1):C.UVGC_270_254})

V_148 = CTVertex(name = 'V_148',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.tp, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_277_273,(0,0,2):C.UVGC_277_274,(0,0,1):C.UVGC_277_275,(0,1,0):C.UVGC_278_276,(0,1,2):C.UVGC_278_277,(0,1,1):C.UVGC_278_278})

V_149 = CTVertex(name = 'V_149',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.tp, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_252_198,(0,0,2):C.UVGC_252_199,(0,0,1):C.UVGC_252_200,(0,1,0):C.UVGC_253_201,(0,1,2):C.UVGC_253_202,(0,1,1):C.UVGC_253_203})

V_150 = CTVertex(name = 'V_150',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.x, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ], [ [P.g, P.u, P.x] ], [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_297_331,(0,0,2):C.UVGC_297_332,(0,0,1):C.UVGC_297_333,(0,1,0):C.UVGC_298_334,(0,1,2):C.UVGC_298_335,(0,1,1):C.UVGC_298_336})

V_151 = CTVertex(name = 'V_151',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.x, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.x] ], [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_263_231,(0,0,2):C.UVGC_263_232,(0,0,1):C.UVGC_263_233,(0,1,0):C.UVGC_264_234,(0,1,2):C.UVGC_264_235,(0,1,1):C.UVGC_264_236})

V_152 = CTVertex(name = 'V_152',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.x, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ], [ [P.g, P.t, P.x] ], [ [P.g, P.x] ] ],
                 couplings = {(0,0,0):C.UVGC_288_305,(0,0,2):C.UVGC_288_306,(0,0,1):C.UVGC_288_307,(0,1,0):C.UVGC_289_308,(0,1,2):C.UVGC_289_309,(0,1,1):C.UVGC_289_310})

V_153 = CTVertex(name = 'V_153',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.bp, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_290_311,(0,0,2):C.UVGC_290_312,(0,0,1):C.UVGC_290_313,(0,1,0):C.UVGC_291_314,(0,1,2):C.UVGC_291_315,(0,1,1):C.UVGC_291_316})

V_154 = CTVertex(name = 'V_154',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.bp, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.c, P.g] ], [ [P.bp, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_256_210,(0,0,2):C.UVGC_256_211,(0,0,0):C.UVGC_256_212,(0,1,1):C.UVGC_257_213,(0,1,2):C.UVGC_257_214,(0,1,0):C.UVGC_257_215})

V_155 = CTVertex(name = 'V_155',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.bp, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_282_287,(0,0,2):C.UVGC_282_288,(0,0,1):C.UVGC_282_289,(0,1,0):C.UVGC_283_290,(0,1,2):C.UVGC_283_291,(0,1,1):C.UVGC_283_292})

V_156 = CTVertex(name = 'V_156',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.y, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.y] ], [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_271_255,(0,0,2):C.UVGC_271_256,(0,0,1):C.UVGC_271_257,(0,1,0):C.UVGC_272_258,(0,1,2):C.UVGC_272_259,(0,1,1):C.UVGC_272_260})

V_157 = CTVertex(name = 'V_157',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.y, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.y] ], [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_279_279,(0,0,2):C.UVGC_279_280,(0,0,1):C.UVGC_279_281,(0,1,0):C.UVGC_280_282,(0,1,2):C.UVGC_280_283,(0,1,1):C.UVGC_280_284})

V_158 = CTVertex(name = 'V_158',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.y, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.y] ], [ [P.g, P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_254_204,(0,0,2):C.UVGC_254_205,(0,0,1):C.UVGC_254_206,(0,1,0):C.UVGC_255_207,(0,1,2):C.UVGC_255_208,(0,1,1):C.UVGC_255_209})

V_159 = CTVertex(name = 'V_159',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.bp, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.d, P.g] ], [ [P.bp, P.g] ], [ [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_267_243,(0,0,2):C.UVGC_267_244,(0,0,0):C.UVGC_267_245,(0,1,1):C.UVGC_268_246,(0,1,2):C.UVGC_268_247,(0,1,0):C.UVGC_268_248})

V_160 = CTVertex(name = 'V_160',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.bp, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.bp, P.g] ], [ [P.bp, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_275_267,(0,0,2):C.UVGC_275_268,(0,0,1):C.UVGC_275_269,(0,1,0):C.UVGC_276_270,(0,1,2):C.UVGC_276_271,(0,1,1):C.UVGC_276_272})

V_161 = CTVertex(name = 'V_161',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.bp, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.bp, P.g] ], [ [P.b, P.g] ], [ [P.bp, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_250_192,(0,0,2):C.UVGC_250_193,(0,0,0):C.UVGC_250_194,(0,1,1):C.UVGC_251_195,(0,1,2):C.UVGC_251_196,(0,1,0):C.UVGC_251_197})

V_162 = CTVertex(name = 'V_162',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.tp, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.tp] ], [ [P.g, P.tp, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_295_325,(0,0,2):C.UVGC_295_326,(0,0,1):C.UVGC_295_327,(0,1,0):C.UVGC_296_328,(0,1,2):C.UVGC_296_329,(0,1,1):C.UVGC_296_330})

V_163 = CTVertex(name = 'V_163',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.tp, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.tp] ], [ [P.g, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_261_225,(0,0,2):C.UVGC_261_226,(0,0,1):C.UVGC_261_227,(0,1,0):C.UVGC_262_228,(0,1,2):C.UVGC_262_229,(0,1,1):C.UVGC_262_230})

V_164 = CTVertex(name = 'V_164',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.tp, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ], [ [P.g, P.tp] ], [ [P.g, P.t, P.tp] ] ],
                 couplings = {(0,0,0):C.UVGC_286_299,(0,0,1):C.UVGC_286_300,(0,0,2):C.UVGC_286_301,(0,1,0):C.UVGC_287_302,(0,1,1):C.UVGC_287_303,(0,1,2):C.UVGC_287_304})

V_165 = CTVertex(name = 'V_165',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_210_172,(0,1,0):C.UVGC_165_51,(0,2,0):C.UVGC_167_53})

V_166 = CTVertex(name = 'V_166',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_210_172,(0,1,0):C.UVGC_131_17,(0,2,0):C.UVGC_133_19})

V_167 = CTVertex(name = 'V_167',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_210_172,(0,1,0):C.UVGC_152_38,(0,2,0):C.UVGC_154_40})

V_168 = CTVertex(name = 'V_168',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_199_165,(0,1,0):C.UVGC_138_24,(0,2,0):C.UVGC_140_26})

V_169 = CTVertex(name = 'V_169',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_199_165,(0,1,0):C.UVGC_145_31,(0,2,0):C.UVGC_147_33})

V_170 = CTVertex(name = 'V_170',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_199_165,(0,1,0):C.UVGC_118_4,(0,2,0):C.UVGC_120_6})

V_171 = CTVertex(name = 'V_171',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,6):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,4):C.UVGC_187_80,(0,3,5):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,6):C.UVGC_168_54,(0,2,6):C.UVGC_169_55})

V_172 = CTVertex(name = 'V_172',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.c, P.g] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,3):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,4):C.UVGC_187_79,(0,3,5):C.UVGC_187_80,(0,3,6):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,3):C.UVGC_134_20,(0,2,3):C.UVGC_135_21})

V_173 = CTVertex(name = 'V_173',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,6):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,4):C.UVGC_187_80,(0,3,5):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,6):C.UVGC_155_41,(0,2,6):C.UVGC_156_42})

V_174 = CTVertex(name = 'V_174',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,4):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,5):C.UVGC_187_80,(0,3,6):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,4):C.UVGC_141_27,(0,2,4):C.UVGC_142_28})

V_175 = CTVertex(name = 'V_175',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,6):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,2):C.UVGC_187_78,(0,3,3):C.UVGC_187_79,(0,3,4):C.UVGC_187_80,(0,3,5):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,6):C.UVGC_148_34,(0,2,6):C.UVGC_149_35})

V_176 = CTVertex(name = 'V_176',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV2, L.FFV3, L.FFV5 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.b, P.g] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,2):C.UVGC_200_166,(0,3,0):C.UVGC_187_76,(0,3,1):C.UVGC_187_77,(0,3,3):C.UVGC_187_78,(0,3,4):C.UVGC_187_79,(0,3,5):C.UVGC_187_80,(0,3,6):C.UVGC_187_81,(0,3,7):C.UVGC_187_82,(0,3,8):C.UVGC_187_83,(0,3,9):C.UVGC_187_84,(0,3,10):C.UVGC_187_85,(0,3,11):C.UVGC_187_86,(0,3,12):C.UVGC_187_87,(0,1,2):C.UVGC_121_7,(0,2,2):C.UVGC_122_8})

V_177 = CTVertex(name = 'V_177',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_292_317,(0,0,2):C.UVGC_292_318,(0,0,1):C.UVGC_258_218})

V_178 = CTVertex(name = 'V_178',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_258_216,(0,0,2):C.UVGC_258_217,(0,0,1):C.UVGC_258_218})

V_179 = CTVertex(name = 'V_179',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_281_285,(0,0,2):C.UVGC_281_286,(0,0,1):C.UVGC_258_218})

V_180 = CTVertex(name = 'V_180',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_292_317,(0,0,2):C.UVGC_292_318,(0,0,1):C.UVGC_258_218})

V_181 = CTVertex(name = 'V_181',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.c, P.g] ], [ [P.c, P.g, P.s] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_258_216,(0,0,2):C.UVGC_258_217,(0,0,1):C.UVGC_258_218})

V_182 = CTVertex(name = 'V_182',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_281_285,(0,0,2):C.UVGC_281_286,(0,0,1):C.UVGC_258_218})

V_183 = CTVertex(name = 'V_183',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_238_180,(0,1,0):C.UVGC_239_181})

V_184 = CTVertex(name = 'V_184',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV7 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_212_173,(0,2,0):C.UVGC_203_169,(0,1,0):C.UVGC_136_22})

V_185 = CTVertex(name = 'V_185',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV7 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_228_177,(0,2,0):C.UVGC_203_169,(0,1,0):C.UVGC_157_43})

V_186 = CTVertex(name = 'V_186',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_217_174,(0,2,0):C.UVGC_203_169,(0,1,0):C.UVGC_143_29})

V_187 = CTVertex(name = 'V_187',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_222_175,(0,2,0):C.UVGC_203_169,(0,1,0):C.UVGC_150_36})

V_188 = CTVertex(name = 'V_188',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV2, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_202_168,(0,2,0):C.UVGC_203_169,(0,1,0):C.UVGC_123_9})

V_189 = CTVertex(name = 'V_189',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_198_164,(0,1,0):C.UVGC_164_50,(0,2,0):C.UVGC_166_52})

V_190 = CTVertex(name = 'V_190',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_198_164,(0,1,0):C.UVGC_130_16,(0,2,0):C.UVGC_132_18})

V_191 = CTVertex(name = 'V_191',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_227_176,(0,3,0):C.UVGC_198_164,(0,0,0):C.UVGC_151_37,(0,2,0):C.UVGC_153_39})

V_192 = CTVertex(name = 'V_192',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_198_164,(0,1,0):C.UVGC_137_23,(0,2,0):C.UVGC_139_25})

V_193 = CTVertex(name = 'V_193',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF2, L.FF4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_198_164,(0,1,0):C.UVGC_144_30,(0,2,0):C.UVGC_146_32})

V_194 = CTVertex(name = 'V_194',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,1,0):C.UVGC_201_167,(0,3,0):C.UVGC_198_164,(0,0,0):C.UVGC_117_3,(0,2,0):C.UVGC_119_5})

V_195 = CTVertex(name = 'V_195',
                 type = 'UV',
                 particles = [ P.x__tilde__, P.x ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.x] ] ],
                 couplings = {(0,1,0):C.UVGC_243_183,(0,3,0):C.UVGC_198_164,(0,2,0):C.UVGC_172_58,(0,0,0):C.UVGC_170_56})

V_196 = CTVertex(name = 'V_196',
                 type = 'UV',
                 particles = [ P.tp__tilde__, P.tp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.tp] ] ],
                 couplings = {(0,1,0):C.UVGC_234_179,(0,3,0):C.UVGC_198_164,(0,0,0):C.UVGC_158_44,(0,2,0):C.UVGC_160_46})

V_197 = CTVertex(name = 'V_197',
                 type = 'UV',
                 particles = [ P.bp__tilde__, P.bp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.bp, P.g] ] ],
                 couplings = {(0,1,0):C.UVGC_208_171,(0,3,0):C.UVGC_198_164,(0,0,0):C.UVGC_124_10,(0,2,0):C.UVGC_126_12})

V_198 = CTVertex(name = 'V_198',
                 type = 'UV',
                 particles = [ P.y__tilde__, P.y ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.y] ] ],
                 couplings = {(0,1,0):C.UVGC_247_185,(0,3,0):C.UVGC_198_164,(0,0,0):C.UVGC_176_62,(0,2,0):C.UVGC_178_64})

V_199 = CTVertex(name = 'V_199',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV4, L.VV5, L.VV6 ],
                 loop_particles = [ [ [P.b] ], [ [P.bp] ], [ [P.c] ], [ [P.d] ], [ [P.g] ], [ [P.ghG] ], [ [P.s] ], [ [P.t] ], [ [P.tp] ], [ [P.u] ], [ [P.x] ], [ [P.y] ] ],
                 couplings = {(0,0,0):C.UVGC_190_112,(0,0,1):C.UVGC_190_113,(0,0,2):C.UVGC_190_114,(0,0,3):C.UVGC_190_115,(0,0,4):C.UVGC_190_116,(0,0,5):C.UVGC_190_117,(0,0,6):C.UVGC_190_118,(0,0,7):C.UVGC_190_119,(0,0,8):C.UVGC_190_120,(0,0,9):C.UVGC_190_121,(0,0,10):C.UVGC_190_122,(0,0,11):C.UVGC_190_123,(0,1,4):C.UVGC_115_1,(0,2,5):C.UVGC_116_2})

