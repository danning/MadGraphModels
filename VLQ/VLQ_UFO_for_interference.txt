Requestor: Ferdinand Schenck
Content: Vector-like quarks general top partner model, modified to handle interference
Link: http://feynrules.irmp.ucl.ac.be/wiki/VLQ (unmodified)
Merge Request: https://gitlab.cern.ch/atlas-generators-team/MadGraphModels/merge_requests/17
