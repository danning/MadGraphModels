Requestor: Dennis Sperlich
Content: Vector-like quarks general top partner model
Link: http://feynrules.irmp.ucl.ac.be/wiki/VLQ
JIRA: https://its.cern.ch/jira/browse/AGENE-929