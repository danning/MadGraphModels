# This file was automatically created by FeynRules 2.3.31
# Mathematica version: 9.0 for Linux x86 (64-bit) (February 7, 2013)
# Date: Tue 11 Sep 2018 16:19:44


from object_library import all_orders, CouplingOrder


BHL = CouplingOrder(name = 'BHL',
                    expansion_order = 99,
                    hierarchy = 2)

BHR = CouplingOrder(name = 'BHR',
                    expansion_order = 99,
                    hierarchy = 2)

BWL = CouplingOrder(name = 'BWL',
                    expansion_order = 99,
                    hierarchy = 2)

BWR = CouplingOrder(name = 'BWR',
                    expansion_order = 99,
                    hierarchy = 2)

BZL = CouplingOrder(name = 'BZL',
                    expansion_order = 99,
                    hierarchy = 2)

BZR = CouplingOrder(name = 'BZR',
                    expansion_order = 99,
                    hierarchy = 2)

EBB = CouplingOrder(name = 'EBB',
                    expansion_order = 99,
                    hierarchy = 2)

EBL = CouplingOrder(name = 'EBL',
                    expansion_order = 99,
                    hierarchy = 2)

EBR = CouplingOrder(name = 'EBR',
                    expansion_order = 99,
                    hierarchy = 2)

EDP = CouplingOrder(name = 'EDP',
                    expansion_order = 99,
                    hierarchy = 2)

EDS = CouplingOrder(name = 'EDS',
                    expansion_order = 99,
                    hierarchy = 2)

EGG = CouplingOrder(name = 'EGG',
                    expansion_order = 99,
                    hierarchy = 2)

ELP = CouplingOrder(name = 'ELP',
                    expansion_order = 99,
                    hierarchy = 2)

ELS = CouplingOrder(name = 'ELS',
                    expansion_order = 99,
                    hierarchy = 2)

ENP = CouplingOrder(name = 'ENP',
                    expansion_order = 99,
                    hierarchy = 2)

ENS = CouplingOrder(name = 'ENS',
                    expansion_order = 99,
                    hierarchy = 2)

ETL = CouplingOrder(name = 'ETL',
                    expansion_order = 99,
                    hierarchy = 2)

ETR = CouplingOrder(name = 'ETR',
                    expansion_order = 99,
                    hierarchy = 2)

EUP = CouplingOrder(name = 'EUP',
                    expansion_order = 99,
                    hierarchy = 2)

EUS = CouplingOrder(name = 'EUS',
                    expansion_order = 99,
                    hierarchy = 2)

EWW = CouplingOrder(name = 'EWW',
                    expansion_order = 99,
                    hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

THL = CouplingOrder(name = 'THL',
                    expansion_order = 99,
                    hierarchy = 2)

THR = CouplingOrder(name = 'THR',
                    expansion_order = 99,
                    hierarchy = 2)

TWL = CouplingOrder(name = 'TWL',
                    expansion_order = 99,
                    hierarchy = 2)

TWR = CouplingOrder(name = 'TWR',
                    expansion_order = 99,
                    hierarchy = 2)

TZL = CouplingOrder(name = 'TZL',
                    expansion_order = 99,
                    hierarchy = 2)

TZR = CouplingOrder(name = 'TZR',
                    expansion_order = 99,
                    hierarchy = 2)

XWL = CouplingOrder(name = 'XWL',
                    expansion_order = 99,
                    hierarchy = 2)

XWR = CouplingOrder(name = 'XWR',
                    expansion_order = 99,
                    hierarchy = 2)

YWL = CouplingOrder(name = 'YWL',
                    expansion_order = 99,
                    hierarchy = 2)

YWR = CouplingOrder(name = 'YWR',
                    expansion_order = 99,
                    hierarchy = 2)

