# This file was automatically created by FeynRules 2.3.31
# Mathematica version: 9.0 for Linux x86 (64-bit) (February 7, 2013)
# Date: Tue 11 Sep 2018 16:19:44



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
KBLw = Parameter(name = 'KBLw',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KBLw}',
                 lhablock = 'KBL',
                 lhacode = [ 1 ])

KBLz = Parameter(name = 'KBLz',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KBLz}',
                 lhablock = 'KBL',
                 lhacode = [ 2 ])

KBLh = Parameter(name = 'KBLh',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KBLh}',
                 lhablock = 'KBL',
                 lhacode = [ 3 ])

KBRw = Parameter(name = 'KBRw',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KBRw}',
                 lhablock = 'KBR',
                 lhacode = [ 1 ])

KBRz = Parameter(name = 'KBRz',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KBRz}',
                 lhablock = 'KBR',
                 lhacode = [ 2 ])

KBRh = Parameter(name = 'KBRh',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KBRh}',
                 lhablock = 'KBR',
                 lhacode = [ 3 ])

KetadS = Parameter(name = 'KetadS',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetadS}',
                   lhablock = 'KetaDownQ',
                   lhacode = [ 1 ])

KetasS = Parameter(name = 'KetasS',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetasS}',
                   lhablock = 'KetaDownQ',
                   lhacode = [ 2 ])

KetabS = Parameter(name = 'KetabS',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetabS}',
                   lhablock = 'KetaDownQ',
                   lhacode = [ 3 ])

KetadP = Parameter(name = 'KetadP',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetadP}',
                   lhablock = 'KetaDownQ',
                   lhacode = [ 4 ])

KetasP = Parameter(name = 'KetasP',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetasP}',
                   lhablock = 'KetaDownQ',
                   lhacode = [ 5 ])

KetabP = Parameter(name = 'KetabP',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetabP}',
                   lhablock = 'KetaDownQ',
                   lhacode = [ 6 ])

KetaelS = Parameter(name = 'KetaelS',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetaelS}',
                    lhablock = 'KetaLepton',
                    lhacode = [ 1 ])

KetamuS = Parameter(name = 'KetamuS',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetamuS}',
                    lhablock = 'KetaLepton',
                    lhacode = [ 2 ])

KetataS = Parameter(name = 'KetataS',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetataS}',
                    lhablock = 'KetaLepton',
                    lhacode = [ 3 ])

KetaelP = Parameter(name = 'KetaelP',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetaelP}',
                    lhablock = 'KetaLepton',
                    lhacode = [ 4 ])

KetamuP = Parameter(name = 'KetamuP',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetamuP}',
                    lhablock = 'KetaLepton',
                    lhacode = [ 5 ])

KetataP = Parameter(name = 'KetataP',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetataP}',
                    lhablock = 'KetaLepton',
                    lhacode = [ 6 ])

KetaneS = Parameter(name = 'KetaneS',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetaneS}',
                    lhablock = 'KetaNeutrino',
                    lhacode = [ 1 ])

KetanmS = Parameter(name = 'KetanmS',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetanmS}',
                    lhablock = 'KetaNeutrino',
                    lhacode = [ 2 ])

KetantS = Parameter(name = 'KetantS',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetantS}',
                    lhablock = 'KetaNeutrino',
                    lhacode = [ 3 ])

KetaneP = Parameter(name = 'KetaneP',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetaneP}',
                    lhablock = 'KetaNeutrino',
                    lhacode = [ 4 ])

KetanmP = Parameter(name = 'KetanmP',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetanmP}',
                    lhablock = 'KetaNeutrino',
                    lhacode = [ 5 ])

KetantP = Parameter(name = 'KetantP',
                    nature = 'external',
                    type = 'real',
                    value = 0,
                    texname = '\\text{KetantP}',
                    lhablock = 'KetaNeutrino',
                    lhacode = [ 6 ])

KetauS = Parameter(name = 'KetauS',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetauS}',
                   lhablock = 'KetaUpQ',
                   lhacode = [ 1 ])

KetacS = Parameter(name = 'KetacS',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetacS}',
                   lhablock = 'KetaUpQ',
                   lhacode = [ 2 ])

KetatS = Parameter(name = 'KetatS',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetatS}',
                   lhablock = 'KetaUpQ',
                   lhacode = [ 3 ])

KetauP = Parameter(name = 'KetauP',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetauP}',
                   lhablock = 'KetaUpQ',
                   lhacode = [ 4 ])

KetacP = Parameter(name = 'KetacP',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetacP}',
                   lhablock = 'KetaUpQ',
                   lhacode = [ 5 ])

KetatP = Parameter(name = 'KetatP',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{KetatP}',
                   lhablock = 'KetaUpQ',
                   lhacode = [ 6 ])

KetaVLTL = Parameter(name = 'KetaVLTL',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{KetaVLTL}',
                     lhablock = 'KetaVLQ',
                     lhacode = [ 1 ])

KetaVLTR = Parameter(name = 'KetaVLTR',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{KetaVLTR}',
                     lhablock = 'KetaVLQ',
                     lhacode = [ 2 ])

KetaVLBL = Parameter(name = 'KetaVLBL',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{KetaVLBL}',
                     lhablock = 'KetaVLQ',
                     lhacode = [ 3 ])

KetaVLBR = Parameter(name = 'KetaVLBR',
                     nature = 'external',
                     type = 'real',
                     value = 0.,
                     texname = '\\text{KetaVLBR}',
                     lhablock = 'KetaVLQ',
                     lhacode = [ 4 ])

KetaGG = Parameter(name = 'KetaGG',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{KetaGG}',
                   lhablock = 'KetaVV',
                   lhacode = [ 1 ])

KetaWW = Parameter(name = 'KetaWW',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{KetaWW}',
                   lhablock = 'KetaVV',
                   lhacode = [ 2 ])

KetaBB = Parameter(name = 'KetaBB',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{KetaBB}',
                   lhablock = 'KetaVV',
                   lhacode = [ 3 ])

KTLw = Parameter(name = 'KTLw',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KTLw}',
                 lhablock = 'KTL',
                 lhacode = [ 1 ])

KTLz = Parameter(name = 'KTLz',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KTLz}',
                 lhablock = 'KTL',
                 lhacode = [ 2 ])

KTLh = Parameter(name = 'KTLh',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KTLh}',
                 lhablock = 'KTL',
                 lhacode = [ 3 ])

KTRw = Parameter(name = 'KTRw',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KTRw}',
                 lhablock = 'KTR',
                 lhacode = [ 1 ])

KTRz = Parameter(name = 'KTRz',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KTRz}',
                 lhablock = 'KTR',
                 lhacode = [ 2 ])

KTRh = Parameter(name = 'KTRh',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{KTRh}',
                 lhablock = 'KTR',
                 lhacode = [ 3 ])

KXL = Parameter(name = 'KXL',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{KXL}',
                lhablock = 'KXL',
                lhacode = [ 1 ])

KXR = Parameter(name = 'KXR',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{KXR}',
                lhablock = 'KXR',
                lhacode = [ 1 ])

KYL = Parameter(name = 'KYL',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{KYL}',
                lhablock = 'KYL',
                lhacode = [ 1 ])

KYR = Parameter(name = 'KYR',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{KYR}',
                lhablock = 'KYR',
                lhacode = [ 1 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

MX = Parameter(name = 'MX',
               nature = 'external',
               type = 'real',
               value = 600,
               texname = '\\text{MX}',
               lhablock = 'MASS',
               lhacode = [ 6000005 ])

MTP = Parameter(name = 'MTP',
                nature = 'external',
                type = 'real',
                value = 600,
                texname = '\\text{MTP}',
                lhablock = 'MASS',
                lhacode = [ 6000006 ])

MBP = Parameter(name = 'MBP',
                nature = 'external',
                type = 'real',
                value = 600,
                texname = '\\text{MBP}',
                lhablock = 'MASS',
                lhacode = [ 6000007 ])

MY = Parameter(name = 'MY',
               nature = 'external',
               type = 'real',
               value = 600,
               texname = '\\text{MY}',
               lhablock = 'MASS',
               lhacode = [ 6000008 ])

Meta = Parameter(name = 'Meta',
                 nature = 'external',
                 type = 'real',
                 value = 200,
                 texname = '\\text{Meta}',
                 lhablock = 'MASS',
                 lhacode = [ 6000025 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WX = Parameter(name = 'WX',
               nature = 'external',
               type = 'real',
               value = 5.,
               texname = '\\text{WX}',
               lhablock = 'DECAY',
               lhacode = [ 6000005 ])

WTP = Parameter(name = 'WTP',
                nature = 'external',
                type = 'real',
                value = 5.,
                texname = '\\text{WTP}',
                lhablock = 'DECAY',
                lhacode = [ 6000006 ])

WBP = Parameter(name = 'WBP',
                nature = 'external',
                type = 'real',
                value = 5.,
                texname = '\\text{WBP}',
                lhablock = 'DECAY',
                lhacode = [ 6000007 ])

WY = Parameter(name = 'WY',
               nature = 'external',
               type = 'real',
               value = 5.,
               texname = '\\text{WY}',
               lhablock = 'DECAY',
               lhacode = [ 6000008 ])

Weta = Parameter(name = 'Weta',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = '\\text{Weta}',
                 lhablock = 'DECAY',
                 lhacode = [ 6000025 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

KetaDP1x1 = Parameter(name = 'KetaDP1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetadP',
                      texname = '\\text{KetaDP1x1}')

KetaDP2x2 = Parameter(name = 'KetaDP2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetasP',
                      texname = '\\text{KetaDP2x2}')

KetaDP3x3 = Parameter(name = 'KetaDP3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetabP',
                      texname = '\\text{KetaDP3x3}')

KetaDS1x1 = Parameter(name = 'KetaDS1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetadS',
                      texname = '\\text{KetaDS1x1}')

KetaDS2x2 = Parameter(name = 'KetaDS2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetasS',
                      texname = '\\text{KetaDS2x2}')

KetaDS3x3 = Parameter(name = 'KetaDS3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetabS',
                      texname = '\\text{KetaDS3x3}')

KetaLP1x1 = Parameter(name = 'KetaLP1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetaelP',
                      texname = '\\text{KetaLP1x1}')

KetaLP2x2 = Parameter(name = 'KetaLP2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetamuP',
                      texname = '\\text{KetaLP2x2}')

KetaLP3x3 = Parameter(name = 'KetaLP3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetataP',
                      texname = '\\text{KetaLP3x3}')

KetaLS1x1 = Parameter(name = 'KetaLS1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetaelS',
                      texname = '\\text{KetaLS1x1}')

KetaLS2x2 = Parameter(name = 'KetaLS2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetamuS',
                      texname = '\\text{KetaLS2x2}')

KetaLS3x3 = Parameter(name = 'KetaLS3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetataS',
                      texname = '\\text{KetaLS3x3}')

KetaNP1x1 = Parameter(name = 'KetaNP1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetaneP',
                      texname = '\\text{KetaNP1x1}')

KetaNP2x2 = Parameter(name = 'KetaNP2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetanmP',
                      texname = '\\text{KetaNP2x2}')

KetaNP3x3 = Parameter(name = 'KetaNP3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetantP',
                      texname = '\\text{KetaNP3x3}')

KetaNS1x1 = Parameter(name = 'KetaNS1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetaneS',
                      texname = '\\text{KetaNS1x1}')

KetaNS2x2 = Parameter(name = 'KetaNS2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetanmS',
                      texname = '\\text{KetaNS2x2}')

KetaNS3x3 = Parameter(name = 'KetaNS3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetantS',
                      texname = '\\text{KetaNS3x3}')

KetaUP1x1 = Parameter(name = 'KetaUP1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetauP',
                      texname = '\\text{KetaUP1x1}')

KetaUP2x2 = Parameter(name = 'KetaUP2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetacP',
                      texname = '\\text{KetaUP2x2}')

KetaUP3x3 = Parameter(name = 'KetaUP3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetatP',
                      texname = '\\text{KetaUP3x3}')

KetaUS1x1 = Parameter(name = 'KetaUS1x1',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetauS',
                      texname = '\\text{KetaUS1x1}')

KetaUS2x2 = Parameter(name = 'KetaUS2x2',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetacS',
                      texname = '\\text{KetaUS2x2}')

KetaUS3x3 = Parameter(name = 'KetaUS3x3',
                      nature = 'internal',
                      type = 'real',
                      value = 'KetatS',
                      texname = '\\text{KetaUS3x3}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

