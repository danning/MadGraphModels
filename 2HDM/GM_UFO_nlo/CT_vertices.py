# This file was automatically created by FeynRules 2.1.88
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Tue 3 Feb 2015 18:16:33


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.G, P.G, P.G ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV3 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.G] ] ],
               couplings = {(0,0,0):C.R2GC_415_67,(0,0,1):C.R2GC_415_68})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.G, P.G, P.G, P.G ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5, L.VVVV9 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.G] ] ],
               couplings = {(2,0,0):C.R2GC_383_46,(2,0,1):C.R2GC_383_47,(0,0,0):C.R2GC_383_46,(0,0,1):C.R2GC_383_47,(4,0,0):C.R2GC_381_42,(4,0,1):C.R2GC_381_43,(3,0,0):C.R2GC_381_42,(3,0,1):C.R2GC_381_43,(8,0,0):C.R2GC_382_44,(8,0,1):C.R2GC_382_45,(7,0,0):C.R2GC_388_53,(7,0,1):C.R2GC_419_73,(5,0,0):C.R2GC_381_42,(5,0,1):C.R2GC_381_43,(1,0,0):C.R2GC_381_42,(1,0,1):C.R2GC_381_43,(6,0,0):C.R2GC_387_51,(6,0,1):C.R2GC_420_74,(11,3,0):C.R2GC_385_49,(11,3,1):C.R2GC_385_50,(10,3,0):C.R2GC_385_49,(10,3,1):C.R2GC_385_50,(9,3,1):C.R2GC_384_48,(2,1,0):C.R2GC_383_46,(2,1,1):C.R2GC_383_47,(0,1,0):C.R2GC_383_46,(0,1,1):C.R2GC_383_47,(6,1,0):C.R2GC_416_69,(6,1,1):C.R2GC_416_70,(4,1,0):C.R2GC_381_42,(4,1,1):C.R2GC_381_43,(3,1,0):C.R2GC_381_42,(3,1,1):C.R2GC_381_43,(8,1,0):C.R2GC_382_44,(8,1,1):C.R2GC_421_75,(7,1,0):C.R2GC_388_53,(7,1,1):C.R2GC_388_54,(5,1,0):C.R2GC_381_42,(5,1,1):C.R2GC_381_43,(1,1,0):C.R2GC_381_42,(1,1,1):C.R2GC_381_43,(2,2,0):C.R2GC_383_46,(2,2,1):C.R2GC_383_47,(0,2,0):C.R2GC_383_46,(0,2,1):C.R2GC_383_47,(4,2,0):C.R2GC_381_42,(4,2,1):C.R2GC_381_43,(3,2,0):C.R2GC_381_42,(3,2,1):C.R2GC_381_43,(8,2,0):C.R2GC_382_44,(8,2,1):C.R2GC_418_72,(6,2,0):C.R2GC_387_51,(6,2,1):C.R2GC_387_52,(7,2,0):C.R2GC_417_71,(7,2,1):C.R2GC_383_47,(5,2,0):C.R2GC_381_42,(5,2,1):C.R2GC_381_43,(1,2,0):C.R2GC_381_42,(1,2,1):C.R2GC_381_43})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.G ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.b, P.G] ] ],
               couplings = {(0,0,0):C.R2GC_336_1})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.d__tilde__, P.d, P.G ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.d, P.G] ] ],
               couplings = {(0,0,0):C.R2GC_336_1})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.s__tilde__, P.s, P.G ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.G, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_336_1})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.c__tilde__, P.c, P.G ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.c, P.G] ] ],
               couplings = {(0,0,0):C.R2GC_336_1})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.t__tilde__, P.t, P.G ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.G, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_336_1})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.u__tilde__, P.u, P.G ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               loop_particles = [ [ [P.G, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_336_1})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.b, P.G, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_429_77,(0,1,0):C.R2GC_432_80})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.H3p ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_430_78,(0,1,0):C.R2GC_435_83})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_410_61})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_409_60})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_411_62})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.H3z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_412_63})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_432_80,(0,1,0):C.R2GC_429_77})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.H3p__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_435_83,(0,1,0):C.R2GC_430_78})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_433_81})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_431_79})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_434_82})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H3z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_436_84})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_389_55})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_389_55})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.G, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_389_55})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_362_37,(0,1,0):C.R2GC_339_3})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_362_37,(0,1,0):C.R2GC_339_3})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_362_37,(0,1,0):C.R2GC_339_3})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_392_57})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_392_57})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.G, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_392_57})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_361_36,(0,1,0):C.R2GC_337_2})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_361_36,(0,1,0):C.R2GC_337_2})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_361_36,(0,1,0):C.R2GC_337_2})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.G, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_401_58})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_401_58})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.G, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_401_58})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_401_58})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.G, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_401_58})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.G, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_401_58})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.G, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_391_56})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_391_56})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_425_76,(0,2,0):C.R2GC_425_76,(0,1,0):C.R2GC_391_56,(0,3,0):C.R2GC_391_56})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_391_56})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.G, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_391_56})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.R2GC_406_59,(0,2,0):C.R2GC_406_59,(0,1,0):C.R2GC_391_56,(0,3,0):C.R2GC_391_56})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.G, P.G ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.G] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.R2GC_414_66,(0,1,0):C.R2GC_344_4,(0,1,3):C.R2GC_344_5,(0,2,1):C.R2GC_413_64,(0,2,2):C.R2GC_413_65})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.G, P.G, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVV1, L.VVV2 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_355_26,(0,0,1):C.R2GC_355_27,(0,1,0):C.R2GC_355_27,(0,1,1):C.R2GC_355_26})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.G, P.G, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_345_6,(0,0,1):C.R2GC_345_7})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.G, P.G, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_346_8,(0,0,1):C.R2GC_346_9})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.G, P.G, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b, P.t], [P.c, P.s], [P.d, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_367_41,(0,1,0):C.R2GC_367_41,(0,2,0):C.R2GC_367_41})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.A, P.G, P.G, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_357_28,(0,0,1):C.R2GC_357_29,(0,1,0):C.R2GC_357_28,(0,1,1):C.R2GC_357_29,(0,2,0):C.R2GC_357_28,(0,2,1):C.R2GC_357_29})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.G, P.G, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_360_34,(0,0,1):C.R2GC_360_35,(0,1,0):C.R2GC_360_34,(0,1,1):C.R2GC_360_35,(0,2,0):C.R2GC_360_34,(0,2,1):C.R2GC_360_35})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.A, P.A, P.G, P.G ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_353_22,(0,0,1):C.R2GC_353_23,(0,1,0):C.R2GC_353_22,(0,1,1):C.R2GC_353_23,(0,2,0):C.R2GC_353_22,(0,2,1):C.R2GC_353_23})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.G, P.G, P.G, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_359_32,(1,0,1):C.R2GC_359_33,(0,1,0):C.R2GC_358_30,(0,1,1):C.R2GC_358_31,(0,2,0):C.R2GC_358_30,(0,2,1):C.R2GC_358_31,(0,3,0):C.R2GC_358_30,(0,3,1):C.R2GC_358_31})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.A, P.G, P.G, P.G ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_354_24,(0,0,1):C.R2GC_354_25,(0,1,0):C.R2GC_354_24,(0,1,1):C.R2GC_354_25,(0,2,0):C.R2GC_354_24,(0,2,1):C.R2GC_354_25})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.G, P.G, P.h, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_347_10,(0,0,1):C.R2GC_347_11})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.G, P.G, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_348_12,(0,0,1):C.R2GC_348_13})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.G, P.G, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_364_38})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.G, P.G, P.h, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_349_14,(0,0,1):C.R2GC_349_15})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.G, P.G, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_350_16,(0,0,1):C.R2GC_350_17})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.G, P.G, P.G__minus__, P.H3p ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_365_39})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.G, P.G, P.G__plus__, P.H3p__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_365_39})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.G, P.G, P.H3p__tilde__, P.H3p ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_366_40})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.G, P.G, P.G0, P.H3z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_351_18,(0,0,1):C.R2GC_351_19})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.G, P.G, P.H3z, P.H3z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_352_20,(0,0,1):C.R2GC_352_21})

V_65 = CTVertex(name = 'V_65',
                type = 'UV',
                particles = [ P.G, P.G, P.G ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV3 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.G] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_415_43,(0,0,1):C.UVGC_415_44,(0,0,2):C.UVGC_415_45,(0,0,3):C.UVGC_415_46,(0,0,4):C.UVGC_415_47})

V_66 = CTVertex(name = 'V_66',
                type = 'UV',
                particles = [ P.G, P.G, P.G, P.G ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5, L.VVVV9 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.G] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(2,0,3):C.UVGC_382_8,(2,0,4):C.UVGC_382_7,(0,0,3):C.UVGC_382_8,(0,0,4):C.UVGC_382_7,(4,0,3):C.UVGC_381_5,(4,0,4):C.UVGC_381_6,(3,0,3):C.UVGC_381_5,(3,0,4):C.UVGC_381_6,(8,0,3):C.UVGC_382_7,(8,0,4):C.UVGC_382_8,(7,0,0):C.UVGC_419_59,(7,0,2):C.UVGC_419_60,(7,0,3):C.UVGC_419_61,(7,0,4):C.UVGC_419_62,(7,0,5):C.UVGC_419_63,(5,0,3):C.UVGC_381_5,(5,0,4):C.UVGC_381_6,(1,0,3):C.UVGC_381_5,(1,0,4):C.UVGC_381_6,(6,0,0):C.UVGC_419_59,(6,0,2):C.UVGC_419_60,(6,0,3):C.UVGC_420_64,(6,0,4):C.UVGC_420_65,(6,0,5):C.UVGC_419_63,(11,3,3):C.UVGC_385_11,(11,3,4):C.UVGC_385_12,(10,3,3):C.UVGC_385_11,(10,3,4):C.UVGC_385_12,(9,3,3):C.UVGC_384_9,(9,3,4):C.UVGC_384_10,(2,1,3):C.UVGC_382_8,(2,1,4):C.UVGC_382_7,(0,1,3):C.UVGC_382_8,(0,1,4):C.UVGC_382_7,(6,1,0):C.UVGC_416_48,(6,1,3):C.UVGC_416_49,(6,1,4):C.UVGC_416_50,(6,1,5):C.UVGC_416_51,(4,1,3):C.UVGC_381_5,(4,1,4):C.UVGC_381_6,(3,1,3):C.UVGC_381_5,(3,1,4):C.UVGC_381_6,(8,1,0):C.UVGC_421_66,(8,1,2):C.UVGC_421_67,(8,1,3):C.UVGC_421_68,(8,1,4):C.UVGC_421_69,(8,1,5):C.UVGC_421_70,(7,1,1):C.UVGC_387_18,(7,1,3):C.UVGC_388_20,(7,1,4):C.UVGC_388_21,(5,1,3):C.UVGC_381_5,(5,1,4):C.UVGC_381_6,(1,1,3):C.UVGC_381_5,(1,1,4):C.UVGC_381_6,(2,2,3):C.UVGC_382_8,(2,2,4):C.UVGC_382_7,(0,2,3):C.UVGC_382_8,(0,2,4):C.UVGC_382_7,(4,2,3):C.UVGC_381_5,(4,2,4):C.UVGC_381_6,(3,2,3):C.UVGC_381_5,(3,2,4):C.UVGC_381_6,(8,2,0):C.UVGC_418_54,(8,2,2):C.UVGC_418_55,(8,2,3):C.UVGC_418_56,(8,2,4):C.UVGC_418_57,(8,2,5):C.UVGC_418_58,(6,2,1):C.UVGC_387_18,(6,2,3):C.UVGC_387_19,(6,2,4):C.UVGC_384_9,(7,2,0):C.UVGC_416_48,(7,2,3):C.UVGC_417_52,(7,2,4):C.UVGC_417_53,(7,2,5):C.UVGC_416_51,(5,2,3):C.UVGC_381_5,(5,2,4):C.UVGC_381_6,(1,2,3):C.UVGC_381_5,(1,2,4):C.UVGC_381_6})

V_67 = CTVertex(name = 'V_67',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.G ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV5 ],
                loop_particles = [ [ [P.b] ], [ [P.b, P.G] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.G] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,1):C.UVGC_371_3,(0,1,0):C.UVGC_386_13,(0,1,2):C.UVGC_386_14,(0,1,3):C.UVGC_386_15,(0,1,4):C.UVGC_386_16,(0,1,5):C.UVGC_386_17,(0,1,1):C.UVGC_405_29})

V_68 = CTVertex(name = 'V_68',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.G ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.G] ], [ [P.G] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.UVGC_371_3,(0,1,0):C.UVGC_386_13,(0,1,1):C.UVGC_386_14,(0,1,3):C.UVGC_386_15,(0,1,4):C.UVGC_386_16,(0,1,5):C.UVGC_386_17})

V_69 = CTVertex(name = 'V_69',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.G ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.G] ], [ [P.ghG] ], [ [P.G, P.s] ], [ [P.t] ] ],
                couplings = {(0,0,4):C.UVGC_371_3,(0,1,0):C.UVGC_386_13,(0,1,1):C.UVGC_386_14,(0,1,2):C.UVGC_386_15,(0,1,3):C.UVGC_386_16,(0,1,5):C.UVGC_386_17})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.G ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.G] ], [ [P.G] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.UVGC_371_3,(0,1,0):C.UVGC_386_13,(0,1,1):C.UVGC_386_14,(0,1,3):C.UVGC_386_15,(0,1,4):C.UVGC_386_16,(0,1,5):C.UVGC_386_17})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.G ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.G] ], [ [P.ghG] ], [ [P.G, P.t] ], [ [P.t] ] ],
                couplings = {(0,0,4):C.UVGC_371_3,(0,1,0):C.UVGC_386_13,(0,1,1):C.UVGC_386_14,(0,1,2):C.UVGC_386_15,(0,1,3):C.UVGC_386_16,(0,1,5):C.UVGC_386_17,(0,1,4):C.UVGC_424_73})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.G ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV4, L.FFV5 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.G] ], [ [P.ghG] ], [ [P.G, P.u] ], [ [P.t] ] ],
                couplings = {(0,0,4):C.UVGC_371_3,(0,1,0):C.UVGC_386_13,(0,1,1):C.UVGC_386_14,(0,1,2):C.UVGC_386_15,(0,1,3):C.UVGC_386_16,(0,1,5):C.UVGC_386_17})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G] ], [ [P.b, P.G, P.t] ], [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_429_79,(0,0,2):C.UVGC_429_80,(0,0,1):C.UVGC_429_81,(0,1,0):C.UVGC_432_86,(0,1,2):C.UVGC_432_87,(0,1,1):C.UVGC_432_88})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.H3p ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G] ], [ [P.b, P.G, P.t] ], [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_430_82,(0,0,2):C.UVGC_430_83,(0,0,1):C.UVGC_430_84,(0,1,0):C.UVGC_435_91,(0,1,2):C.UVGC_435_92,(0,1,1):C.UVGC_435_93})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_410_34})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_409_33})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_411_35})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.H3z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_412_36})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G] ], [ [P.b, P.G, P.t] ], [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_432_86,(0,0,2):C.UVGC_432_87,(0,0,1):C.UVGC_432_88,(0,1,0):C.UVGC_429_79,(0,1,2):C.UVGC_429_80,(0,1,1):C.UVGC_429_81})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.H3p__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.G] ], [ [P.b, P.G, P.t] ], [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_435_91,(0,0,2):C.UVGC_435_92,(0,0,1):C.UVGC_435_93,(0,1,0):C.UVGC_430_82,(0,1,2):C.UVGC_430_83,(0,1,1):C.UVGC_430_84})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_433_89})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_431_85})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_434_90})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.H3z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_436_94})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_389_22,(0,1,0):C.UVGC_404_28,(0,2,0):C.UVGC_404_28})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.d, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_389_22,(0,1,0):C.UVGC_373_4,(0,2,0):C.UVGC_373_4})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_389_22,(0,1,0):C.UVGC_373_4,(0,2,0):C.UVGC_373_4})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.b, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_407_31,(0,1,0):C.UVGC_408_32})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.c, P.G] ] ],
                couplings = {(0,0,0):C.UVGC_392_24,(0,1,0):C.UVGC_370_2,(0,2,0):C.UVGC_370_2})

V_90 = CTVertex(name = 'V_90',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_392_24,(0,1,0):C.UVGC_423_72,(0,2,0):C.UVGC_423_72})

V_91 = CTVertex(name = 'V_91',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_392_24,(0,1,0):C.UVGC_370_2,(0,2,0):C.UVGC_370_2})

V_92 = CTVertex(name = 'V_92',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                loop_particles = [ [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_427_77,(0,1,0):C.UVGC_428_78})

V_93 = CTVertex(name = 'V_93',
                type = 'UV',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.G], [P.G, P.s] ], [ [P.c, P.G, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_401_25,(0,0,1):C.UVGC_401_26})

V_94 = CTVertex(name = 'V_94',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.G] ], [ [P.b, P.G, P.t] ], [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_426_75,(0,0,2):C.UVGC_426_76,(0,0,1):C.UVGC_401_26})

V_95 = CTVertex(name = 'V_95',
                type = 'UV',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.G], [P.G, P.u] ], [ [P.d, P.G, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_401_25,(0,0,1):C.UVGC_401_26})

V_96 = CTVertex(name = 'V_96',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.b, P.G] ], [ [P.b, P.G, P.t] ], [ [P.G, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_426_75,(0,0,2):C.UVGC_426_76,(0,0,1):C.UVGC_401_26})

V_97 = CTVertex(name = 'V_97',
                type = 'UV',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.d, P.G], [P.G, P.u] ], [ [P.d, P.G, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_401_25,(0,0,1):C.UVGC_401_26})

V_98 = CTVertex(name = 'V_98',
                type = 'UV',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                loop_particles = [ [ [P.c, P.G], [P.G, P.s] ], [ [P.c, P.G, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_401_25,(0,0,1):C.UVGC_401_26})

V_99 = CTVertex(name = 'V_99',
                type = 'UV',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1, L.FF3, L.FF5 ],
                loop_particles = [ [ [P.G, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_391_23,(0,1,0):C.UVGC_369_1,(0,2,0):C.UVGC_369_1})

V_100 = CTVertex(name = 'V_100',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.c, P.G] ] ],
                 couplings = {(0,0,0):C.UVGC_391_23,(0,1,0):C.UVGC_369_1,(0,2,0):C.UVGC_369_1})

V_101 = CTVertex(name = 'V_101',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.G, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_425_74,(0,2,0):C.UVGC_425_74,(0,1,0):C.UVGC_422_71,(0,3,0):C.UVGC_422_71})

V_102 = CTVertex(name = 'V_102',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.d, P.G] ] ],
                 couplings = {(0,0,0):C.UVGC_391_23,(0,1,0):C.UVGC_369_1,(0,2,0):C.UVGC_369_1})

V_103 = CTVertex(name = 'V_103',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.G, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_391_23,(0,1,0):C.UVGC_369_1,(0,2,0):C.UVGC_369_1})

V_104 = CTVertex(name = 'V_104',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.b, P.G] ] ],
                 couplings = {(0,0,0):C.UVGC_406_30,(0,2,0):C.UVGC_406_30,(0,1,0):C.UVGC_403_27,(0,3,0):C.UVGC_403_27})

V_105 = CTVertex(name = 'V_105',
                 type = 'UV',
                 particles = [ P.G, P.G ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.G] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_414_39,(0,0,1):C.UVGC_414_40,(0,0,2):C.UVGC_414_41,(0,0,3):C.UVGC_414_42,(0,1,0):C.UVGC_413_37,(0,1,3):C.UVGC_413_38})

