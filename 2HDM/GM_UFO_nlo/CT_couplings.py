# This file was automatically created by FeynRules 2.1.88
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Tue 3 Feb 2015 18:16:33


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_336_1 = Coupling(name = 'R2GC_336_1',
                      value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_337_2 = Coupling(name = 'R2GC_337_2',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_339_3 = Coupling(name = 'R2GC_339_3',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_344_4 = Coupling(name = 'R2GC_344_4',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_344_5 = Coupling(name = 'R2GC_344_5',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_345_6 = Coupling(name = 'R2GC_345_6',
                      value = '-(ca*complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_345_7 = Coupling(name = 'R2GC_345_7',
                      value = '-(ca*complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_346_8 = Coupling(name = 'R2GC_346_8',
                      value = '-(complex(0,1)*G**2*MB*sa*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_346_9 = Coupling(name = 'R2GC_346_9',
                      value = '-(complex(0,1)*G**2*MT*sa*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_347_10 = Coupling(name = 'R2GC_347_10',
                       value = '-(ca**2*complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_347_11 = Coupling(name = 'R2GC_347_11',
                       value = '-(ca**2*complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_348_12 = Coupling(name = 'R2GC_348_12',
                       value = '-(ch**2*complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_348_13 = Coupling(name = 'R2GC_348_13',
                       value = '-(ch**2*complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_349_14 = Coupling(name = 'R2GC_349_14',
                       value = '-(ca*complex(0,1)*G**2*sa*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_349_15 = Coupling(name = 'R2GC_349_15',
                       value = '-(ca*complex(0,1)*G**2*sa*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_350_16 = Coupling(name = 'R2GC_350_16',
                       value = '-(complex(0,1)*G**2*sa**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_350_17 = Coupling(name = 'R2GC_350_17',
                       value = '-(complex(0,1)*G**2*sa**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_351_18 = Coupling(name = 'R2GC_351_18',
                       value = '(ch*complex(0,1)*G**2*sh*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_351_19 = Coupling(name = 'R2GC_351_19',
                       value = '(ch*complex(0,1)*G**2*sh*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_352_20 = Coupling(name = 'R2GC_352_20',
                       value = '-(complex(0,1)*G**2*sh**2*yb**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_352_21 = Coupling(name = 'R2GC_352_21',
                       value = '-(complex(0,1)*G**2*sh**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_353_22 = Coupling(name = 'R2GC_353_22',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_353_23 = Coupling(name = 'R2GC_353_23',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_354_24 = Coupling(name = 'R2GC_354_24',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_354_25 = Coupling(name = 'R2GC_354_25',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_355_26 = Coupling(name = 'R2GC_355_26',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_355_27 = Coupling(name = 'R2GC_355_27',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_357_28 = Coupling(name = 'R2GC_357_28',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_357_29 = Coupling(name = 'R2GC_357_29',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_358_30 = Coupling(name = 'R2GC_358_30',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_358_31 = Coupling(name = 'R2GC_358_31',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_359_32 = Coupling(name = 'R2GC_359_32',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_359_33 = Coupling(name = 'R2GC_359_33',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_360_34 = Coupling(name = 'R2GC_360_34',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_360_35 = Coupling(name = 'R2GC_360_35',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_361_36 = Coupling(name = 'R2GC_361_36',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_362_37 = Coupling(name = 'R2GC_362_37',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_364_38 = Coupling(name = 'R2GC_364_38',
                       value = '-(ch**2*complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (ch**2*complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_365_39 = Coupling(name = 'R2GC_365_39',
                       value = '(ch*complex(0,1)*G**2*sh*yb**2)/(16.*cmath.pi**2) + (ch*complex(0,1)*G**2*sh*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_366_40 = Coupling(name = 'R2GC_366_40',
                       value = '-(complex(0,1)*G**2*sh**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*sh**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_367_41 = Coupling(name = 'R2GC_367_41',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_381_42 = Coupling(name = 'R2GC_381_42',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_381_43 = Coupling(name = 'R2GC_381_43',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_382_44 = Coupling(name = 'R2GC_382_44',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_382_45 = Coupling(name = 'R2GC_382_45',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_383_46 = Coupling(name = 'R2GC_383_46',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_383_47 = Coupling(name = 'R2GC_383_47',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_384_48 = Coupling(name = 'R2GC_384_48',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_385_49 = Coupling(name = 'R2GC_385_49',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_385_50 = Coupling(name = 'R2GC_385_50',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_387_51 = Coupling(name = 'R2GC_387_51',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_387_52 = Coupling(name = 'R2GC_387_52',
                       value = '(complex(0,1)*G**4)/(4.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_388_53 = Coupling(name = 'R2GC_388_53',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_388_54 = Coupling(name = 'R2GC_388_54',
                       value = '(-23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_389_55 = Coupling(name = 'R2GC_389_55',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_391_56 = Coupling(name = 'R2GC_391_56',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_392_57 = Coupling(name = 'R2GC_392_57',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_401_58 = Coupling(name = 'R2GC_401_58',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_406_59 = Coupling(name = 'R2GC_406_59',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_409_60 = Coupling(name = 'R2GC_409_60',
                       value = '(ca*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_410_61 = Coupling(name = 'R2GC_410_61',
                       value = '-(ch*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_411_62 = Coupling(name = 'R2GC_411_62',
                       value = '(complex(0,1)*G**2*sa*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_412_63 = Coupling(name = 'R2GC_412_63',
                       value = '(G**2*sh*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_413_64 = Coupling(name = 'R2GC_413_64',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_413_65 = Coupling(name = 'R2GC_413_65',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_414_66 = Coupling(name = 'R2GC_414_66',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_415_67 = Coupling(name = 'R2GC_415_67',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_415_68 = Coupling(name = 'R2GC_415_68',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_416_69 = Coupling(name = 'R2GC_416_69',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_416_70 = Coupling(name = 'R2GC_416_70',
                       value = '(19*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_417_71 = Coupling(name = 'R2GC_417_71',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_418_72 = Coupling(name = 'R2GC_418_72',
                       value = '(31*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_419_73 = Coupling(name = 'R2GC_419_73',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_420_74 = Coupling(name = 'R2GC_420_74',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_421_75 = Coupling(name = 'R2GC_421_75',
                       value = '(7*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_425_76 = Coupling(name = 'R2GC_425_76',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_429_77 = Coupling(name = 'R2GC_429_77',
                       value = '(ch*complex(0,1)*G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_430_78 = Coupling(name = 'R2GC_430_78',
                       value = '-(complex(0,1)*G**2*sh*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_431_79 = Coupling(name = 'R2GC_431_79',
                       value = '(ca*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_432_80 = Coupling(name = 'R2GC_432_80',
                       value = '-(ch*complex(0,1)*G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_433_81 = Coupling(name = 'R2GC_433_81',
                       value = '(ch*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_434_82 = Coupling(name = 'R2GC_434_82',
                       value = '(complex(0,1)*G**2*sa*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_435_83 = Coupling(name = 'R2GC_435_83',
                       value = '(complex(0,1)*G**2*sh*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_436_84 = Coupling(name = 'R2GC_436_84',
                       value = '-(G**2*sh*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_369_1 = Coupling(name = 'UVGC_369_1',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_370_2 = Coupling(name = 'UVGC_370_2',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_371_3 = Coupling(name = 'UVGC_371_3',
                      value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_373_4 = Coupling(name = 'UVGC_373_4',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_381_5 = Coupling(name = 'UVGC_381_5',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_381_6 = Coupling(name = 'UVGC_381_6',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_382_7 = Coupling(name = 'UVGC_382_7',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_382_8 = Coupling(name = 'UVGC_382_8',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_384_9 = Coupling(name = 'UVGC_384_9',
                      value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_384_10 = Coupling(name = 'UVGC_384_10',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_385_11 = Coupling(name = 'UVGC_385_11',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_385_12 = Coupling(name = 'UVGC_385_12',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_386_13 = Coupling(name = 'UVGC_386_13',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_386_14 = Coupling(name = 'UVGC_386_14',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_386_15 = Coupling(name = 'UVGC_386_15',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_386_16 = Coupling(name = 'UVGC_386_16',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_386_17 = Coupling(name = 'UVGC_386_17',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_387_18 = Coupling(name = 'UVGC_387_18',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_387_19 = Coupling(name = 'UVGC_387_19',
                       value = {-1:'(47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_388_20 = Coupling(name = 'UVGC_388_20',
                       value = {-1:'(-253*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_388_21 = Coupling(name = 'UVGC_388_21',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_389_22 = Coupling(name = 'UVGC_389_22',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_391_23 = Coupling(name = 'UVGC_391_23',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_392_24 = Coupling(name = 'UVGC_392_24',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_401_25 = Coupling(name = 'UVGC_401_25',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_401_26 = Coupling(name = 'UVGC_401_26',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_403_27 = Coupling(name = 'UVGC_403_27',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_404_28 = Coupling(name = 'UVGC_404_28',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_405_29 = Coupling(name = 'UVGC_405_29',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_406_30 = Coupling(name = 'UVGC_406_30',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_407_31 = Coupling(name = 'UVGC_407_31',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_408_32 = Coupling(name = 'UVGC_408_32',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_409_33 = Coupling(name = 'UVGC_409_33',
                       value = {-1:'( (ca*complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(ca*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (ca*complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*ca*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (ca*complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (ca*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (ca*complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_410_34 = Coupling(name = 'UVGC_410_34',
                       value = {-1:'( -(ch*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (ch*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (ch*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*ch*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (ch*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(ch*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (ch*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_411_35 = Coupling(name = 'UVGC_411_35',
                       value = {-1:'( (complex(0,1)*G**2*sa*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*sa*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*sa*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*sa*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*sa*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*sa*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*sa*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_412_36 = Coupling(name = 'UVGC_412_36',
                       value = {-1:'( (G**2*sh*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*sh*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*sh*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*sh*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*sh*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*sh*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*sh*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_413_37 = Coupling(name = 'UVGC_413_37',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_413_38 = Coupling(name = 'UVGC_413_38',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_414_39 = Coupling(name = 'UVGC_414_39',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_414_40 = Coupling(name = 'UVGC_414_40',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_414_41 = Coupling(name = 'UVGC_414_41',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_414_42 = Coupling(name = 'UVGC_414_42',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_415_43 = Coupling(name = 'UVGC_415_43',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_415_44 = Coupling(name = 'UVGC_415_44',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_415_45 = Coupling(name = 'UVGC_415_45',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_415_46 = Coupling(name = 'UVGC_415_46',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_415_47 = Coupling(name = 'UVGC_415_47',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_416_48 = Coupling(name = 'UVGC_416_48',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_416_49 = Coupling(name = 'UVGC_416_49',
                       value = {-1:'(147*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_416_50 = Coupling(name = 'UVGC_416_50',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_416_51 = Coupling(name = 'UVGC_416_51',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_417_52 = Coupling(name = 'UVGC_417_52',
                       value = {-1:'(147*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_417_53 = Coupling(name = 'UVGC_417_53',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_418_54 = Coupling(name = 'UVGC_418_54',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_418_55 = Coupling(name = 'UVGC_418_55',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_418_56 = Coupling(name = 'UVGC_418_56',
                       value = {-1:'(523*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_418_57 = Coupling(name = 'UVGC_418_57',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_418_58 = Coupling(name = 'UVGC_418_58',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_419_59 = Coupling(name = 'UVGC_419_59',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_419_60 = Coupling(name = 'UVGC_419_60',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_419_61 = Coupling(name = 'UVGC_419_61',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_419_62 = Coupling(name = 'UVGC_419_62',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_419_63 = Coupling(name = 'UVGC_419_63',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_420_64 = Coupling(name = 'UVGC_420_64',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_420_65 = Coupling(name = 'UVGC_420_65',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_421_66 = Coupling(name = 'UVGC_421_66',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_421_67 = Coupling(name = 'UVGC_421_67',
                       value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_421_68 = Coupling(name = 'UVGC_421_68',
                       value = {-1:'(-85*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_421_69 = Coupling(name = 'UVGC_421_69',
                       value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_421_70 = Coupling(name = 'UVGC_421_70',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_422_71 = Coupling(name = 'UVGC_422_71',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_423_72 = Coupling(name = 'UVGC_423_72',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_424_73 = Coupling(name = 'UVGC_424_73',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_425_74 = Coupling(name = 'UVGC_425_74',
                       value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_426_75 = Coupling(name = 'UVGC_426_75',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_426_76 = Coupling(name = 'UVGC_426_76',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_427_77 = Coupling(name = 'UVGC_427_77',
                       value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_428_78 = Coupling(name = 'UVGC_428_78',
                       value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_429_79 = Coupling(name = 'UVGC_429_79',
                       value = {-1:'( (ch*complex(0,1)*G**2*yb)/(12.*cmath.pi**2) if MB else -(ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2) - (3*ch*complex(0,1)*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2) ) - (ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_429_80 = Coupling(name = 'UVGC_429_80',
                       value = {-1:'( (ch*complex(0,1)*G**2*yb)/(12.*cmath.pi**2) if MT else -(ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2) - (ch*complex(0,1)*G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2) ) - (ch*complex(0,1)*G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_429_81 = Coupling(name = 'UVGC_429_81',
                       value = {-1:'(ch*complex(0,1)*G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_430_82 = Coupling(name = 'UVGC_430_82',
                       value = {-1:'( -(complex(0,1)*G**2*sh*yb)/(12.*cmath.pi**2) if MB else (complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2) )',0:'( (-13*complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2) + (3*complex(0,1)*G**2*sh*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_430_83 = Coupling(name = 'UVGC_430_83',
                       value = {-1:'( -(complex(0,1)*G**2*sh*yb)/(12.*cmath.pi**2) if MT else (complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2) + (complex(0,1)*G**2*sh*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2*sh*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_430_84 = Coupling(name = 'UVGC_430_84',
                       value = {-1:'-(complex(0,1)*G**2*sh*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_431_85 = Coupling(name = 'UVGC_431_85',
                       value = {-1:'( (ca*complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(ca*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (ca*complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*ca*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (ca*complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (ca*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (ca*complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_432_86 = Coupling(name = 'UVGC_432_86',
                       value = {-1:'( -(ch*complex(0,1)*G**2*yt)/(12.*cmath.pi**2) if MB else (ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2) + (ch*complex(0,1)*G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2) ) + (ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_432_87 = Coupling(name = 'UVGC_432_87',
                       value = {-1:'( -(ch*complex(0,1)*G**2*yt)/(12.*cmath.pi**2) if MT else (ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2) + (3*ch*complex(0,1)*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2) ) + (ch*complex(0,1)*G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_432_88 = Coupling(name = 'UVGC_432_88',
                       value = {-1:'-(ch*complex(0,1)*G**2*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_433_89 = Coupling(name = 'UVGC_433_89',
                       value = {-1:'( (ch*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(ch*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (ch*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*ch*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (ch*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (ch*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (ch*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_434_90 = Coupling(name = 'UVGC_434_90',
                       value = {-1:'( (complex(0,1)*G**2*sa*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*sa*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*sa*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*sa*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*sa*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*sa*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*sa*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_435_91 = Coupling(name = 'UVGC_435_91',
                       value = {-1:'( (complex(0,1)*G**2*sh*yt)/(12.*cmath.pi**2) if MB else -(complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2) )',0:'( (5*complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2) - (complex(0,1)*G**2*sh*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_435_92 = Coupling(name = 'UVGC_435_92',
                       value = {-1:'( (complex(0,1)*G**2*sh*yt)/(12.*cmath.pi**2) if MT else -(complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2) )',0:'( (13*complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2) - (3*complex(0,1)*G**2*sh*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2*sh*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_435_93 = Coupling(name = 'UVGC_435_93',
                       value = {-1:'(complex(0,1)*G**2*sh*yt)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_436_94 = Coupling(name = 'UVGC_436_94',
                       value = {-1:'( -(G**2*sh*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*sh*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*sh*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*sh*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*sh*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*sh*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*sh*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

