# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 11.0.1 for Linux x86 (64-bit) (September 21, 2016)
# Date: Fri 8 May 2020 00:41:23


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

QNP = CouplingOrder(name = 'QNP',
                    expansion_order = 2,
                    hierarchy = 1)

