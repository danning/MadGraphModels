Requestor: Paul Thompson
Content: Higgs doublet with flavor and CP conservation
JIRA: https://its.cern.ch/jira/browse/AGENE-1074