Requestor: Nicola Orlando
Contents: General Two Higgs Doublet Model for Flavor Changing Neutral Higgs production
Webpage: https://twiki.org/cgi-bin/view/Sandbox/FlavorChangingNeutralHiggs
Source: https://twiki.org/p/pub/Sandbox/FlavorChangingNeutralHiggs/gen2HDM.tar.gz
Paper: https://arxiv.org/pdf/1710.07260.pdf
JIRA: https://its.cern.ch/jira/browse/AGENE-1857
