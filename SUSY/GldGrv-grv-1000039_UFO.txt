Requestor: Neha Santpur
Contents: Higgs to LLP neutralinos to non-pointing photons in gauge mediation
Paper: https://arxiv.org/abs/1805.05957
Source: Private communication with the authors
JIRA: https://its.cern.ch/jira/browse/AGENE-1686