Requestor: Andreas Redelbach
Contents: Bi-linear RPV SUSY made using SPheno (https://arxiv.org/pdf/1104.1573v3.pdf) and Sarah (https://arxiv.org/pdf/1309.7223v2.pdf)
JIRA: https://its.cern.ch/jira/browse/AGENE-1302