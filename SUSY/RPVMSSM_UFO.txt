Requestor: Larry Lee
Content: R-parity violating decay SUSY searches.  SLHA1 compliant version of the FeynRules model, which is equivalent, at http://feynrules.irmp.ucl.ac.be/wiki/RPVMSSM
Update requested by: Simone Amoroso
Paper: https://arxiv.org/abs/1601.03737
Webpage: http://rci.rutgers.edu/~am1788
JIRA: https://its.cern.ch/jira/browse/AGENE-1282