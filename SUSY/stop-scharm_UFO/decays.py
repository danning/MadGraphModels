# This file was automatically created by FeynRules 2.4.62
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Tue 20 Jun 2017 16:47:04


from object_library import all_decays, Decay
import particles as P


Decay_chi = Decay(name = 'Decay_chi',
                  particle = P.chi,
                  partial_widths = {(P.sq1,P.c__tilde__):'((Mchi**2 - Msq1**2)*((8*ct**2*ee**2*Mchi**2)/(3.*cw**2) - (8*ct**2*ee**2*Msq1**2)/(3.*cw**2)))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq1,P.t__tilde__):'(((8*ee**2*Mchi**2*st**2)/(3.*cw**2) - (8*ee**2*Msq1**2*st**2)/(3.*cw**2) + (8*ee**2*MT**2*st**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq1**2 + Msq1**4 - 2*Mchi**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq1__tilde__,P.c):'((Mchi**2 - Msq1**2)*((8*ct**2*ee**2*Mchi**2)/(3.*cw**2) - (8*ct**2*ee**2*Msq1**2)/(3.*cw**2)))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq1__tilde__,P.t):'(((8*ee**2*Mchi**2*st**2)/(3.*cw**2) - (8*ee**2*Msq1**2*st**2)/(3.*cw**2) + (8*ee**2*MT**2*st**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq1**2 + Msq1**4 - 2*Mchi**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq2,P.c__tilde__):'((Mchi**2 - Msq2**2)*((8*ee**2*Mchi**2*st**2)/(3.*cw**2) - (8*ee**2*Msq2**2*st**2)/(3.*cw**2)))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq2,P.t__tilde__):'(((8*ct**2*ee**2*Mchi**2)/(3.*cw**2) - (8*ct**2*ee**2*Msq2**2)/(3.*cw**2) + (8*ct**2*ee**2*MT**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq2**2 + Msq2**4 - 2*Mchi**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq2__tilde__,P.c):'((Mchi**2 - Msq2**2)*((8*ee**2*Mchi**2*st**2)/(3.*cw**2) - (8*ee**2*Msq2**2*st**2)/(3.*cw**2)))/(32.*cmath.pi*abs(Mchi)**3)',
                                    (P.sq2__tilde__,P.t):'(((8*ct**2*ee**2*Mchi**2)/(3.*cw**2) - (8*ct**2*ee**2*Msq2**2)/(3.*cw**2) + (8*ct**2*ee**2*MT**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq2**2 + Msq2**4 - 2*Mchi**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchi)**3)'})

Decay_go = Decay(name = 'Decay_go',
                 particle = P.go,
                 partial_widths = {(P.sq1,P.c__tilde__):'((Mgo**2 - Msq1**2)*(8*ct**2*G**2*Mgo**2 - 8*ct**2*G**2*Msq1**2))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq1,P.t__tilde__):'((8*G**2*Mgo**2*st**2 - 8*G**2*Msq1**2*st**2 + 8*G**2*MT**2*st**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq1**2 + Msq1**4 - 2*Mgo**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq1__tilde__,P.c):'((Mgo**2 - Msq1**2)*(8*ct**2*G**2*Mgo**2 - 8*ct**2*G**2*Msq1**2))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq1__tilde__,P.t):'((8*G**2*Mgo**2*st**2 - 8*G**2*Msq1**2*st**2 + 8*G**2*MT**2*st**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq1**2 + Msq1**4 - 2*Mgo**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq2,P.c__tilde__):'((Mgo**2 - Msq2**2)*(8*G**2*Mgo**2*st**2 - 8*G**2*Msq2**2*st**2))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq2,P.t__tilde__):'((8*ct**2*G**2*Mgo**2 - 8*ct**2*G**2*Msq2**2 + 8*ct**2*G**2*MT**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq2**2 + Msq2**4 - 2*Mgo**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq2__tilde__,P.c):'((Mgo**2 - Msq2**2)*(8*G**2*Mgo**2*st**2 - 8*G**2*Msq2**2*st**2))/(256.*cmath.pi*abs(Mgo)**3)',
                                   (P.sq2__tilde__,P.t):'((8*ct**2*G**2*Mgo**2 - 8*ct**2*G**2*Msq2**2 + 8*ct**2*G**2*MT**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq2**2 + Msq2**4 - 2*Mgo**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(256.*cmath.pi*abs(Mgo)**3)'})

Decay_H = Decay(name = 'Decay_H',
                particle = P.H,
                partial_widths = {(P.t,P.t__tilde__):'((3*MH**2*yt**2 - 12*MT**2*yt**2)*cmath.sqrt(MH**4 - 4*MH**2*MT**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((MH**2*ytau**2 - 4*MTA**2*ytau**2)*cmath.sqrt(MH**4 - 4*MH**2*MTA**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.W__minus__,P.W__plus__):'(((3*ee**4*vev**2)/(4.*sw**4) + (ee**4*MH**4*vev**2)/(16.*MW**4*sw**4) - (ee**4*MH**2*vev**2)/(4.*MW**2*sw**4))*cmath.sqrt(MH**4 - 4*MH**2*MW**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.Z,P.Z):'(((9*ee**4*vev**2)/2. + (3*ee**4*MH**4*vev**2)/(8.*MZ**4) - (3*ee**4*MH**2*vev**2)/(2.*MZ**2) + (3*cw**4*ee**4*vev**2)/(4.*sw**4) + (cw**4*ee**4*MH**4*vev**2)/(16.*MZ**4*sw**4) - (cw**4*ee**4*MH**2*vev**2)/(4.*MZ**2*sw**4) + (3*cw**2*ee**4*vev**2)/sw**2 + (cw**2*ee**4*MH**4*vev**2)/(4.*MZ**4*sw**2) - (cw**2*ee**4*MH**2*vev**2)/(MZ**2*sw**2) + (3*ee**4*sw**2*vev**2)/cw**2 + (ee**4*MH**4*sw**2*vev**2)/(4.*cw**2*MZ**4) - (ee**4*MH**2*sw**2*vev**2)/(cw**2*MZ**2) + (3*ee**4*sw**4*vev**2)/(4.*cw**4) + (ee**4*MH**4*sw**4*vev**2)/(16.*cw**4*MZ**4) - (ee**4*MH**2*sw**4*vev**2)/(4.*cw**4*MZ**2))*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

Decay_sq1 = Decay(name = 'Decay_sq1',
                  particle = P.sq1,
                  partial_widths = {(P.c,P.chi):'((-Mchi**2 + Msq1**2)*((-8*ct**2*ee**2*Mchi**2)/(3.*cw**2) + (8*ct**2*ee**2*Msq1**2)/(3.*cw**2)))/(48.*cmath.pi*abs(Msq1)**3)',
                                    (P.c,P.go):'((-Mgo**2 + Msq1**2)*(-8*ct**2*G**2*Mgo**2 + 8*ct**2*G**2*Msq1**2))/(48.*cmath.pi*abs(Msq1)**3)',
                                    (P.t,P.chi):'(((-8*ee**2*Mchi**2*st**2)/(3.*cw**2) + (8*ee**2*Msq1**2*st**2)/(3.*cw**2) - (8*ee**2*MT**2*st**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq1**2 + Msq1**4 - 2*Mchi**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Msq1)**3)',
                                    (P.t,P.go):'((-8*G**2*Mgo**2*st**2 + 8*G**2*Msq1**2*st**2 - 8*G**2*MT**2*st**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq1**2 + Msq1**4 - 2*Mgo**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Msq1)**3)'})

Decay_sq2 = Decay(name = 'Decay_sq2',
                  particle = P.sq2,
                  partial_widths = {(P.c,P.chi):'((-Mchi**2 + Msq2**2)*((-8*ee**2*Mchi**2*st**2)/(3.*cw**2) + (8*ee**2*Msq2**2*st**2)/(3.*cw**2)))/(48.*cmath.pi*abs(Msq2)**3)',
                                    (P.c,P.go):'((-Mgo**2 + Msq2**2)*(-8*G**2*Mgo**2*st**2 + 8*G**2*Msq2**2*st**2))/(48.*cmath.pi*abs(Msq2)**3)',
                                    (P.t,P.chi):'(((-8*ct**2*ee**2*Mchi**2)/(3.*cw**2) + (8*ct**2*ee**2*Msq2**2)/(3.*cw**2) - (8*ct**2*ee**2*MT**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq2**2 + Msq2**4 - 2*Mchi**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Msq2)**3)',
                                    (P.t,P.go):'((-8*ct**2*G**2*Mgo**2 + 8*ct**2*G**2*Msq2**2 - 8*ct**2*G**2*MT**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq2**2 + Msq2**4 - 2*Mgo**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Msq2)**3)'})

Decay_t = Decay(name = 'Decay_t',
                particle = P.t,
                partial_widths = {(P.sq1,P.chi):'(((8*ee**2*Mchi**2*st**2)/(3.*cw**2) - (8*ee**2*Msq1**2*st**2)/(3.*cw**2) + (8*ee**2*MT**2*st**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq1**2 + Msq1**4 - 2*Mchi**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.sq1,P.go):'((8*G**2*Mgo**2*st**2 - 8*G**2*Msq1**2*st**2 + 8*G**2*MT**2*st**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq1**2 + Msq1**4 - 2*Mgo**2*MT**2 - 2*Msq1**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.sq2,P.chi):'(((8*ct**2*ee**2*Mchi**2)/(3.*cw**2) - (8*ct**2*ee**2*Msq2**2)/(3.*cw**2) + (8*ct**2*ee**2*MT**2)/(3.*cw**2))*cmath.sqrt(Mchi**4 - 2*Mchi**2*Msq2**2 + Msq2**4 - 2*Mchi**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.sq2,P.go):'((8*ct**2*G**2*Mgo**2 - 8*ct**2*G**2*Msq2**2 + 8*ct**2*G**2*MT**2)*cmath.sqrt(Mgo**4 - 2*Mgo**2*Msq2**2 + Msq2**4 - 2*Mgo**2*MT**2 - 2*Msq2**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.W__plus__,P.b):'((MT**2 - MW**2)*((3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2))/(96.*cmath.pi*abs(MT)**3)'})

Decay_ta__minus__ = Decay(name = 'Decay_ta__minus__',
                          particle = P.ta__minus__,
                          partial_widths = {(P.W__minus__,P.vt):'((MTA**2 - MW**2)*((ee**2*MTA**2)/(2.*sw**2) + (ee**2*MTA**4)/(2.*MW**2*sw**2) - (ee**2*MW**2)/sw**2))/(32.*cmath.pi*abs(MTA)**3)'})

Decay_W__plus__ = Decay(name = 'Decay_W__plus__',
                        particle = P.W__plus__,
                        partial_widths = {(P.c,P.s__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.t,P.b__tilde__):'((-MT**2 + MW**2)*((-3*ee**2*MT**2)/(2.*sw**2) - (3*ee**2*MT**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.u,P.d__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.ve,P.e__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vm,P.mu__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vt,P.ta__plus__):'((-MTA**2 + MW**2)*(-(ee**2*MTA**2)/(2.*sw**2) - (ee**2*MTA**4)/(2.*MW**2*sw**2) + (ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)'})

Decay_Z = Decay(name = 'Decay_Z',
                particle = P.Z,
                partial_widths = {(P.b,P.b__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.c,P.c__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.d,P.d__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.e__minus__,P.e__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.mu__minus__,P.mu__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.s,P.s__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.t,P.t__tilde__):'((-11*ee**2*MT**2 - ee**2*MZ**2 - (3*cw**2*ee**2*MT**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MT**2*sw**2)/(6.*cw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MT**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((-5*ee**2*MTA**2 - ee**2*MZ**2 - (cw**2*ee**2*MTA**2)/(2.*sw**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MTA**2*sw**2)/(2.*cw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2))*cmath.sqrt(-4*MTA**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.u,P.u__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ve,P.ve__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vm,P.vm__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vt,P.vt__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.W__minus__,P.W__plus__):'(((-12*cw**2*ee**2*MW**2)/sw**2 - (17*cw**2*ee**2*MZ**2)/sw**2 + (4*cw**2*ee**2*MZ**4)/(MW**2*sw**2) + (cw**2*ee**2*MZ**6)/(4.*MW**4*sw**2))*cmath.sqrt(-4*MW**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)'})

