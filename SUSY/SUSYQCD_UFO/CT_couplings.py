# This file was automatically created by FeynRules 2.4.16
# Mathematica version: 9.0 for Mac OS X x86 (64-bit) (January 24, 2013)
# Date: Fri 21 Aug 2015 09:21:20


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_100_2 = Coupling(name = 'R2GC_100_2',
                      value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_100_3 = Coupling(name = 'R2GC_100_3',
                      value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_101_4 = Coupling(name = 'R2GC_101_4',
                      value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_102_5 = Coupling(name = 'R2GC_102_5',
                      value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_102_6 = Coupling(name = 'R2GC_102_6',
                      value = '(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_103_7 = Coupling(name = 'R2GC_103_7',
                      value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_103_8 = Coupling(name = 'R2GC_103_8',
                      value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_103_9 = Coupling(name = 'R2GC_103_9',
                      value = '(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                      order = {'QCD':4})

R2GC_104_10 = Coupling(name = 'R2GC_104_10',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_105_11 = Coupling(name = 'R2GC_105_11',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_106_12 = Coupling(name = 'R2GC_106_12',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_107_13 = Coupling(name = 'R2GC_107_13',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_107_14 = Coupling(name = 'R2GC_107_14',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_107_15 = Coupling(name = 'R2GC_107_15',
                       value = '(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_108_16 = Coupling(name = 'R2GC_108_16',
                       value = '-G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_108_17 = Coupling(name = 'R2GC_108_17',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_108_18 = Coupling(name = 'R2GC_108_18',
                       value = '-G**3/(8.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_109_19 = Coupling(name = 'R2GC_109_19',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_109_20 = Coupling(name = 'R2GC_109_20',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_109_21 = Coupling(name = 'R2GC_109_21',
                       value = 'G**3/(8.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_110_22 = Coupling(name = 'R2GC_110_22',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_110_23 = Coupling(name = 'R2GC_110_23',
                       value = '(-3*complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_111_24 = Coupling(name = 'R2GC_111_24',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_111_25 = Coupling(name = 'R2GC_111_25',
                       value = '(-9*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_112_26 = Coupling(name = 'R2GC_112_26',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_114_27 = Coupling(name = 'R2GC_114_27',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_114_28 = Coupling(name = 'R2GC_114_28',
                       value = '(23*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_115_29 = Coupling(name = 'R2GC_115_29',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_115_30 = Coupling(name = 'R2GC_115_30',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_115_31 = Coupling(name = 'R2GC_115_31',
                       value = '(5*complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_116_32 = Coupling(name = 'R2GC_116_32',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_117_33 = Coupling(name = 'R2GC_117_33',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_118_34 = Coupling(name = 'R2GC_118_34',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_119_35 = Coupling(name = 'R2GC_119_35',
                       value = '-(ee*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_120_36 = Coupling(name = 'R2GC_120_36',
                       value = '(-4*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_121_37 = Coupling(name = 'R2GC_121_37',
                       value = '(37*ee*complex(0,1)*G**3)/(432.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_122_38 = Coupling(name = 'R2GC_122_38',
                       value = '-(cw*ee*complex(0,1)*G**2)/(18.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(54.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_123_39 = Coupling(name = 'R2GC_123_39',
                       value = '(ee*complex(0,1)*G**2*sw)/(27.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_124_40 = Coupling(name = 'R2GC_124_40',
                       value = '(-2*cw*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*sw) - (2*ee**2*complex(0,1)*G**2*sw)/(81.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_125_41 = Coupling(name = 'R2GC_125_41',
                       value = '(4*ee**2*complex(0,1)*G**2*sw)/(81.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_126_42 = Coupling(name = 'R2GC_126_42',
                       value = '(37*cw*ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw) + (37*ee*complex(0,1)*G**3*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_127_43 = Coupling(name = 'R2GC_127_43',
                       value = '(-37*ee*complex(0,1)*G**3*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_128_44 = Coupling(name = 'R2GC_128_44',
                       value = '(-2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(81.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_129_45 = Coupling(name = 'R2GC_129_45',
                       value = '(-4*ee**2*complex(0,1)*G**2*sw**2)/(81.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_131_46 = Coupling(name = 'R2GC_131_46',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_133_47 = Coupling(name = 'R2GC_133_47',
                       value = '(2*ee*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_134_48 = Coupling(name = 'R2GC_134_48',
                       value = '(-16*ee**2*complex(0,1)*G**2)/(81.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_135_49 = Coupling(name = 'R2GC_135_49',
                       value = '(-37*ee*complex(0,1)*G**3)/(216.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_136_50 = Coupling(name = 'R2GC_136_50',
                       value = '(cw*ee*complex(0,1)*G**2)/(18.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(54.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_137_51 = Coupling(name = 'R2GC_137_51',
                       value = '(-2*ee*complex(0,1)*G**2*sw)/(27.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_138_52 = Coupling(name = 'R2GC_138_52',
                       value = '(-4*cw*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2*sw) + (4*ee**2*complex(0,1)*G**2*sw)/(81.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_139_53 = Coupling(name = 'R2GC_139_53',
                       value = '(16*ee**2*complex(0,1)*G**2*sw)/(81.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_140_54 = Coupling(name = 'R2GC_140_54',
                       value = '(-37*cw*ee*complex(0,1)*G**3)/(288.*cmath.pi**2*sw) + (37*ee*complex(0,1)*G**3*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_141_55 = Coupling(name = 'R2GC_141_55',
                       value = '(37*ee*complex(0,1)*G**3*sw)/(216.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_142_56 = Coupling(name = 'R2GC_142_56',
                       value = '(2*ee**2*complex(0,1)*G**2)/(27.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*sw**2)/(81.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_143_57 = Coupling(name = 'R2GC_143_57',
                       value = '(-16*ee**2*complex(0,1)*G**2*sw**2)/(81.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_158_58 = Coupling(name = 'R2GC_158_58',
                       value = '-(complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_158_59 = Coupling(name = 'R2GC_158_59',
                       value = '-(complex(0,1)*G**2)/(72.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_159_60 = Coupling(name = 'R2GC_159_60',
                       value = '(complex(0,1)*G**3)/(9.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_159_61 = Coupling(name = 'R2GC_159_61',
                       value = '(53*complex(0,1)*G**3)/(576.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_160_62 = Coupling(name = 'R2GC_160_62',
                       value = '(13*complex(0,1)*G**4)/(384.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_63 = Coupling(name = 'R2GC_161_63',
                       value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_64 = Coupling(name = 'R2GC_161_64',
                       value = '(-2*complex(0,1)*G**4)/(9.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_161_65 = Coupling(name = 'R2GC_161_65',
                       value = '(-79*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_162_66 = Coupling(name = 'R2GC_162_66',
                       value = '(-13*complex(0,1)*G**4)/(144.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_162_67 = Coupling(name = 'R2GC_162_67',
                       value = '(55*complex(0,1)*G**4)/(216.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_162_68 = Coupling(name = 'R2GC_162_68',
                       value = '(61*complex(0,1)*G**4)/(1728.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_163_69 = Coupling(name = 'R2GC_163_69',
                       value = '(complex(0,1)*G**2*Mgo**2)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_163_70 = Coupling(name = 'R2GC_163_70',
                       value = '(complex(0,1)*G**2*MsbL**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_169_71 = Coupling(name = 'R2GC_169_71',
                       value = '(complex(0,1)*G**2*MsbR**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_175_72 = Coupling(name = 'R2GC_175_72',
                       value = '(complex(0,1)*G**2*MscL**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_181_73 = Coupling(name = 'R2GC_181_73',
                       value = '(complex(0,1)*G**2*MscR**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_187_74 = Coupling(name = 'R2GC_187_74',
                       value = '(complex(0,1)*G**2*MsdL**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_193_75 = Coupling(name = 'R2GC_193_75',
                       value = '(complex(0,1)*G**2*MsdR**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_200_76 = Coupling(name = 'R2GC_200_76',
                       value = '(3*complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_203_77 = Coupling(name = 'R2GC_203_77',
                       value = '(-3*complex(0,1)*G**3)/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_204_78 = Coupling(name = 'R2GC_204_78',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_208_79 = Coupling(name = 'R2GC_208_79',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_212_80 = Coupling(name = 'R2GC_212_80',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_216_81 = Coupling(name = 'R2GC_216_81',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_247_82 = Coupling(name = 'R2GC_247_82',
                       value = '(complex(0,1)*G**2*MssL**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_248_83 = Coupling(name = 'R2GC_248_83',
                       value = '(complex(0,1)*G**2*MssR**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_265_84 = Coupling(name = 'R2GC_265_84',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_268_85 = Coupling(name = 'R2GC_268_85',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_269_86 = Coupling(name = 'R2GC_269_86',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_292_87 = Coupling(name = 'R2GC_292_87',
                       value = '(complex(0,1)*G**2*MstL**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_292_88 = Coupling(name = 'R2GC_292_88',
                       value = '(complex(0,1)*G**2*Mgo**2)/(6.*cmath.pi**2) + (complex(0,1)*G**2*MT**2)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_293_89 = Coupling(name = 'R2GC_293_89',
                       value = '(complex(0,1)*G**2*MstR**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_302_90 = Coupling(name = 'R2GC_302_90',
                       value = '-(G**2*Mgo*yt)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_303_91 = Coupling(name = 'R2GC_303_91',
                       value = '-(complex(0,1)*G**2*Mgo*yt)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_304_92 = Coupling(name = 'R2GC_304_92',
                       value = '(G**2*Mgo*yt)/(6.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_305_93 = Coupling(name = 'R2GC_305_93',
                       value = '(complex(0,1)*G**2*MT*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_306_94 = Coupling(name = 'R2GC_306_94',
                       value = '(5*complex(0,1)*G**2*yt**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_320_95 = Coupling(name = 'R2GC_320_95',
                       value = '(-2*ee*complex(0,1)*G**2)/(27.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_330_96 = Coupling(name = 'R2GC_330_96',
                       value = '(complex(0,1)*G**2*MsuL**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_331_97 = Coupling(name = 'R2GC_331_97',
                       value = '(complex(0,1)*G**2*MsuR**2)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_333_98 = Coupling(name = 'R2GC_333_98',
                       value = '-(cw*ee*complex(0,1)*G**2)/(18.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(54.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_342_99 = Coupling(name = 'R2GC_342_99',
                       value = '(3*complex(0,1)*G**2*Mgo)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_344_100 = Coupling(name = 'R2GC_344_100',
                        value = '(ee*complex(0,1)*G**2)/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_344_101 = Coupling(name = 'R2GC_344_101',
                        value = '-(ee*complex(0,1)*G**2)/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_344_102 = Coupling(name = 'R2GC_344_102',
                        value = '-(ee*complex(0,1)*G**2)/(48.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_344_103 = Coupling(name = 'R2GC_344_103',
                        value = '(ee*complex(0,1)*G**2)/(48.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_346_104 = Coupling(name = 'R2GC_346_104',
                        value = '(cw*ee*complex(0,1)*G**2)/(64.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(192.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_346_105 = Coupling(name = 'R2GC_346_105',
                        value = '(ee*complex(0,1)*G**2*sw)/(96.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_346_106 = Coupling(name = 'R2GC_346_106',
                        value = '-(cw*ee*complex(0,1)*G**2)/(64.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(192.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_346_107 = Coupling(name = 'R2GC_346_107',
                        value = '-(ee*complex(0,1)*G**2*sw)/(48.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_347_108 = Coupling(name = 'R2GC_347_108',
                        value = '(ee*complex(0,1)*G**2)/(72.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_348_109 = Coupling(name = 'R2GC_348_109',
                        value = '(ee*complex(0,1)*G**2)/(36.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_350_110 = Coupling(name = 'R2GC_350_110',
                        value = '-(ee*complex(0,1)*G**2)/(18.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_351_111 = Coupling(name = 'R2GC_351_111',
                        value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_352_112 = Coupling(name = 'R2GC_352_112',
                        value = '(ee**2*complex(0,1)*G**2*cmath.sqrt(2))/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

R2GC_353_113 = Coupling(name = 'R2GC_353_113',
                        value = '-(ee**2*complex(0,1)*G**2)/(9.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

R2GC_354_114 = Coupling(name = 'R2GC_354_114',
                        value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_355_115 = Coupling(name = 'R2GC_355_115',
                        value = '(ee*complex(0,1)*G**2)/(9.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_356_116 = Coupling(name = 'R2GC_356_116',
                        value = '-(ee**2*complex(0,1)*G**2*cmath.sqrt(2))/(27.*cmath.pi**2*sw)',
                        order = {'QCD':2,'QED':2})

R2GC_357_117 = Coupling(name = 'R2GC_357_117',
                        value = '(-37*ee*complex(0,1)*G**3)/(144.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

R2GC_360_118 = Coupling(name = 'R2GC_360_118',
                        value = '(-11*complex(0,1)*G**4)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_360_119 = Coupling(name = 'R2GC_360_119',
                        value = '(25*complex(0,1)*G**4)/(216.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_360_120 = Coupling(name = 'R2GC_360_120',
                        value = '(5*complex(0,1)*G**4)/(576.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_360_121 = Coupling(name = 'R2GC_360_121',
                        value = '-(complex(0,1)*G**4)/(3456.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_361_122 = Coupling(name = 'R2GC_361_122',
                        value = '(-5*complex(0,1)*G**4)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_361_123 = Coupling(name = 'R2GC_361_123',
                        value = '(-5*complex(0,1)*G**4)/(72.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_364_124 = Coupling(name = 'R2GC_364_124',
                        value = '(complex(0,1)*G**4)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_364_125 = Coupling(name = 'R2GC_364_125',
                        value = '(5*complex(0,1)*G**4)/(432.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_364_126 = Coupling(name = 'R2GC_364_126',
                        value = '(-109*complex(0,1)*G**4)/(3456.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_365_127 = Coupling(name = 'R2GC_365_127',
                        value = '(35*complex(0,1)*G**4)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_365_128 = Coupling(name = 'R2GC_365_128',
                        value = '(29*complex(0,1)*G**4)/(1152.*cmath.pi**2)',
                        order = {'QCD':4})

R2GC_431_129 = Coupling(name = 'R2GC_431_129',
                        value = '-(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_432_130 = Coupling(name = 'R2GC_432_130',
                        value = '(G**2*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_447_131 = Coupling(name = 'R2GC_447_131',
                        value = '-(G**2*Mgo*yt)/(6.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_448_132 = Coupling(name = 'R2GC_448_132',
                        value = '(G**2*Mgo*yt)/(6.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_449_133 = Coupling(name = 'R2GC_449_133',
                        value = '-(G**2*MT*yt)/(6.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_450_134 = Coupling(name = 'R2GC_450_134',
                        value = '(G**2*MT*yt)/(6.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

R2GC_452_135 = Coupling(name = 'R2GC_452_135',
                        value = '(-5*G**2*yt**2)/(18.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_453_136 = Coupling(name = 'R2GC_453_136',
                        value = '(5*complex(0,1)*G**2*yt**2)/(18.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_454_137 = Coupling(name = 'R2GC_454_137',
                        value = '(5*G**2*yt**2)/(18.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

R2GC_525_138 = Coupling(name = 'R2GC_525_138',
                        value = '(3*complex(0,1)*G**3)/(4.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_525_139 = Coupling(name = 'R2GC_525_139',
                        value = '-(complex(0,1)*G**3)/(96.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_525_140 = Coupling(name = 'R2GC_525_140',
                        value = '(3*complex(0,1)*G**3)/(32.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_531_141 = Coupling(name = 'R2GC_531_141',
                        value = '(-3*complex(0,1)*G**3)/(4.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_531_142 = Coupling(name = 'R2GC_531_142',
                        value = '(complex(0,1)*G**3)/(96.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_531_143 = Coupling(name = 'R2GC_531_143',
                        value = '(-3*complex(0,1)*G**3)/(32.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':3})

R2GC_74_144 = Coupling(name = 'R2GC_74_144',
                       value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_75_145 = Coupling(name = 'R2GC_75_145',
                       value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_77_146 = Coupling(name = 'R2GC_77_146',
                       value = '(-3*complex(0,1)*G**2*Mgo**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_77_147 = Coupling(name = 'R2GC_77_147',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_78_148 = Coupling(name = 'R2GC_78_148',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_78_149 = Coupling(name = 'R2GC_78_149',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_79_150 = Coupling(name = 'R2GC_79_150',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_79_151 = Coupling(name = 'R2GC_79_151',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_80_152 = Coupling(name = 'R2GC_80_152',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_80_153 = Coupling(name = 'R2GC_80_153',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_82_154 = Coupling(name = 'R2GC_82_154',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_82_155 = Coupling(name = 'R2GC_82_155',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_83_156 = Coupling(name = 'R2GC_83_156',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_83_157 = Coupling(name = 'R2GC_83_157',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_84_158 = Coupling(name = 'R2GC_84_158',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_84_159 = Coupling(name = 'R2GC_84_159',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_85_160 = Coupling(name = 'R2GC_85_160',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_85_161 = Coupling(name = 'R2GC_85_161',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_86_162 = Coupling(name = 'R2GC_86_162',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_87_163 = Coupling(name = 'R2GC_87_163',
                       value = '-G**3/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_87_164 = Coupling(name = 'R2GC_87_164',
                       value = '(-3*G**3)/(8.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_88_165 = Coupling(name = 'R2GC_88_165',
                       value = '-(complex(0,1)*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_88_166 = Coupling(name = 'R2GC_88_166',
                       value = '(complex(0,1)*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_90_167 = Coupling(name = 'R2GC_90_167',
                       value = '-(ee*complex(0,1)*G**2)/(192.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_90_168 = Coupling(name = 'R2GC_90_168',
                       value = '-(ee*complex(0,1)*G**2)/(96.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_90_169 = Coupling(name = 'R2GC_90_169',
                       value = '(ee*complex(0,1)*G**2)/(48.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_91_170 = Coupling(name = 'R2GC_91_170',
                       value = '-G**3/(32.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_99_171 = Coupling(name = 'R2GC_99_171',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_99_172 = Coupling(name = 'R2GC_99_172',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_99_173 = Coupling(name = 'R2GC_99_173',
                       value = '-G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_100_1 = Coupling(name = 'UVGC_100_1',
                      value = '(3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                      order = {'QCD':4})

UVGC_100_2 = Coupling(name = 'UVGC_100_2',
                      value = '(-3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                      order = {'QCD':4})

UVGC_102_3 = Coupling(name = 'UVGC_102_3',
                      value = '-(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                      order = {'QCD':4})

UVGC_102_4 = Coupling(name = 'UVGC_102_4',
                      value = '(complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                      order = {'QCD':4})

UVGC_103_5 = Coupling(name = 'UVGC_103_5',
                      value = '(-3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                      order = {'QCD':4})

UVGC_103_6 = Coupling(name = 'UVGC_103_6',
                      value = '(3*complex(0,1)*G**4*invFREps)/(256.*cmath.pi**2)',
                      order = {'QCD':4})

UVGC_104_7 = Coupling(name = 'UVGC_104_7',
                      value = '-2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                      order = {'QCD':4})

UVGC_104_8 = Coupling(name = 'UVGC_104_8',
                      value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                      order = {'QCD':4})

UVGC_104_9 = Coupling(name = 'UVGC_104_9',
                      value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                      order = {'QCD':4})

UVGC_104_10 = Coupling(name = 'UVGC_104_10',
                       value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (31*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_11 = Coupling(name = 'UVGC_104_11',
                       value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_12 = Coupling(name = 'UVGC_104_12',
                       value = '-((FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_104_13 = Coupling(name = 'UVGC_104_13',
                       value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_104_14 = Coupling(name = 'UVGC_104_14',
                       value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_15 = Coupling(name = 'UVGC_104_15',
                       value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_16 = Coupling(name = 'UVGC_104_16',
                       value = '-((FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_17 = Coupling(name = 'UVGC_104_17',
                       value = '-((FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_18 = Coupling(name = 'UVGC_104_18',
                       value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_19 = Coupling(name = 'UVGC_104_19',
                       value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_20 = Coupling(name = 'UVGC_104_20',
                       value = '-((FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_21 = Coupling(name = 'UVGC_104_21',
                       value = '-((FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_22 = Coupling(name = 'UVGC_104_22',
                       value = '-((FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_23 = Coupling(name = 'UVGC_104_23',
                       value = '-((FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_24 = Coupling(name = 'UVGC_104_24',
                       value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_25 = Coupling(name = 'UVGC_104_25',
                       value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_104_26 = Coupling(name = 'UVGC_104_26',
                       value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_104_27 = Coupling(name = 'UVGC_104_27',
                       value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_28 = Coupling(name = 'UVGC_105_28',
                       value = '2*FRCTdeltaZxGGxb*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_29 = Coupling(name = 'UVGC_105_29',
                       value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_30 = Coupling(name = 'UVGC_105_30',
                       value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_31 = Coupling(name = 'UVGC_105_31',
                       value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (37*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_32 = Coupling(name = 'UVGC_105_32',
                       value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (3*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_33 = Coupling(name = 'UVGC_105_33',
                       value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_34 = Coupling(name = 'UVGC_105_34',
                       value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_35 = Coupling(name = 'UVGC_105_35',
                       value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_36 = Coupling(name = 'UVGC_105_36',
                       value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_37 = Coupling(name = 'UVGC_105_37',
                       value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_38 = Coupling(name = 'UVGC_105_38',
                       value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_39 = Coupling(name = 'UVGC_105_39',
                       value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_40 = Coupling(name = 'UVGC_105_40',
                       value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_41 = Coupling(name = 'UVGC_105_41',
                       value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_42 = Coupling(name = 'UVGC_105_42',
                       value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_43 = Coupling(name = 'UVGC_105_43',
                       value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_44 = Coupling(name = 'UVGC_105_44',
                       value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_45 = Coupling(name = 'UVGC_105_45',
                       value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_46 = Coupling(name = 'UVGC_105_46',
                       value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

UVGC_105_47 = Coupling(name = 'UVGC_105_47',
                       value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_105_48 = Coupling(name = 'UVGC_105_48',
                       value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2',
                       order = {'QCD':4})

UVGC_106_49 = Coupling(name = 'UVGC_106_49',
                       value = 'FRCTdeltaZxGGxb*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_50 = Coupling(name = 'UVGC_106_50',
                       value = 'FRCTdeltaZxGGxc*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_51 = Coupling(name = 'UVGC_106_51',
                       value = 'FRCTdeltaZxGGxd*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_52 = Coupling(name = 'UVGC_106_52',
                       value = 'FRCTdeltaZxGGxG*complex(0,1) + (11*complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_53 = Coupling(name = 'UVGC_106_53',
                       value = 'FRCTdeltaZxGGxghG*complex(0,1) - (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_54 = Coupling(name = 'UVGC_106_54',
                       value = 'FRCTdeltaZxGGxgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_55 = Coupling(name = 'UVGC_106_55',
                       value = 'FRCTdeltaZxGGxs*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_56 = Coupling(name = 'UVGC_106_56',
                       value = 'FRCTdeltaZxGGxsbL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_57 = Coupling(name = 'UVGC_106_57',
                       value = 'FRCTdeltaZxGGxsbR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_58 = Coupling(name = 'UVGC_106_58',
                       value = 'FRCTdeltaZxGGxscL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_59 = Coupling(name = 'UVGC_106_59',
                       value = 'FRCTdeltaZxGGxscR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_60 = Coupling(name = 'UVGC_106_60',
                       value = 'FRCTdeltaZxGGxsdL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_61 = Coupling(name = 'UVGC_106_61',
                       value = 'FRCTdeltaZxGGxsdR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_62 = Coupling(name = 'UVGC_106_62',
                       value = 'FRCTdeltaZxGGxssL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_63 = Coupling(name = 'UVGC_106_63',
                       value = 'FRCTdeltaZxGGxssR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_64 = Coupling(name = 'UVGC_106_64',
                       value = 'FRCTdeltaZxGGxstL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_65 = Coupling(name = 'UVGC_106_65',
                       value = 'FRCTdeltaZxGGxstR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_66 = Coupling(name = 'UVGC_106_66',
                       value = 'FRCTdeltaZxGGxsuL*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_67 = Coupling(name = 'UVGC_106_67',
                       value = 'FRCTdeltaZxGGxsuR*complex(0,1) - (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_68 = Coupling(name = 'UVGC_106_68',
                       value = 'FRCTdeltaZxGGxt*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_106_69 = Coupling(name = 'UVGC_106_69',
                       value = 'FRCTdeltaZxGGxu*complex(0,1) - (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_70 = Coupling(name = 'UVGC_107_70',
                       value = '-(FRCTdeltaZxGGxb*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_71 = Coupling(name = 'UVGC_107_71',
                       value = '-(FRCTdeltaZxGGxc*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_72 = Coupling(name = 'UVGC_107_72',
                       value = '-(FRCTdeltaZxGGxd*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_73 = Coupling(name = 'UVGC_107_73',
                       value = '-(FRCTdeltaZxGGxG*complex(0,1)) - (19*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_74 = Coupling(name = 'UVGC_107_74',
                       value = '-(FRCTdeltaZxGGxghG*complex(0,1)) - (complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_75 = Coupling(name = 'UVGC_107_75',
                       value = '-(FRCTdeltaZxGGxgo*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_76 = Coupling(name = 'UVGC_107_76',
                       value = '-(FRCTdeltaZxGGxs*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_77 = Coupling(name = 'UVGC_107_77',
                       value = '-(FRCTdeltaZxGGxsbL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_78 = Coupling(name = 'UVGC_107_78',
                       value = '-(FRCTdeltaZxGGxsbR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_79 = Coupling(name = 'UVGC_107_79',
                       value = '-(FRCTdeltaZxGGxscL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_80 = Coupling(name = 'UVGC_107_80',
                       value = '-(FRCTdeltaZxGGxscR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_81 = Coupling(name = 'UVGC_107_81',
                       value = '-(FRCTdeltaZxGGxsdL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_82 = Coupling(name = 'UVGC_107_82',
                       value = '-(FRCTdeltaZxGGxsdR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_83 = Coupling(name = 'UVGC_107_83',
                       value = '-(FRCTdeltaZxGGxssL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_84 = Coupling(name = 'UVGC_107_84',
                       value = '-(FRCTdeltaZxGGxssR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_85 = Coupling(name = 'UVGC_107_85',
                       value = '-(FRCTdeltaZxGGxstL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_86 = Coupling(name = 'UVGC_107_86',
                       value = '-(FRCTdeltaZxGGxstR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_87 = Coupling(name = 'UVGC_107_87',
                       value = '-(FRCTdeltaZxGGxsuL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_88 = Coupling(name = 'UVGC_107_88',
                       value = '-(FRCTdeltaZxGGxsuR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_89 = Coupling(name = 'UVGC_107_89',
                       value = '-(FRCTdeltaZxGGxt*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_107_90 = Coupling(name = 'UVGC_107_90',
                       value = '-(FRCTdeltaZxGGxu*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_108_91 = Coupling(name = 'UVGC_108_91',
                       value = '(3*FRCTdeltaZxGGxb*G)/2. - (G**3*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_92 = Coupling(name = 'UVGC_108_92',
                       value = '(3*FRCTdeltaZxGGxc*G)/2. - (G**3*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_93 = Coupling(name = 'UVGC_108_93',
                       value = '(3*FRCTdeltaZxGGxd*G)/2. - (G**3*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_94 = Coupling(name = 'UVGC_108_94',
                       value = '(3*FRCTdeltaZxGGxG*G)/2. + (15*G**3*invFREps)/(128.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_95 = Coupling(name = 'UVGC_108_95',
                       value = '(3*FRCTdeltaZxGGxghG*G)/2. + (G**3*invFREps)/(128.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_96 = Coupling(name = 'UVGC_108_96',
                       value = '(FRCTdeltaxaSxgo*G)/(2.*aS) + (3*FRCTdeltaZxGGxgo*G)/2. - (G**3*invFREps)/(8.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_97 = Coupling(name = 'UVGC_108_97',
                       value = '(3*FRCTdeltaZxGGxs*G)/2. - (G**3*invFREps)/(24.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_98 = Coupling(name = 'UVGC_108_98',
                       value = '(FRCTdeltaxaSxsbL*G)/(2.*aS) + (3*FRCTdeltaZxGGxsbL*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_99 = Coupling(name = 'UVGC_108_99',
                       value = '(FRCTdeltaxaSxsbR*G)/(2.*aS) + (3*FRCTdeltaZxGGxsbR*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                       order = {'QCD':3})

UVGC_108_100 = Coupling(name = 'UVGC_108_100',
                        value = '(FRCTdeltaxaSxscL*G)/(2.*aS) + (3*FRCTdeltaZxGGxscL*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_101 = Coupling(name = 'UVGC_108_101',
                        value = '(FRCTdeltaxaSxscR*G)/(2.*aS) + (3*FRCTdeltaZxGGxscR*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_102 = Coupling(name = 'UVGC_108_102',
                        value = '(FRCTdeltaxaSxsdL*G)/(2.*aS) + (3*FRCTdeltaZxGGxsdL*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_103 = Coupling(name = 'UVGC_108_103',
                        value = '(FRCTdeltaxaSxsdR*G)/(2.*aS) + (3*FRCTdeltaZxGGxsdR*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_104 = Coupling(name = 'UVGC_108_104',
                        value = '(FRCTdeltaxaSxssL*G)/(2.*aS) + (3*FRCTdeltaZxGGxssL*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_105 = Coupling(name = 'UVGC_108_105',
                        value = '(FRCTdeltaxaSxssR*G)/(2.*aS) + (3*FRCTdeltaZxGGxssR*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_106 = Coupling(name = 'UVGC_108_106',
                        value = '(FRCTdeltaxaSxstL*G)/(2.*aS) + (3*FRCTdeltaZxGGxstL*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_107 = Coupling(name = 'UVGC_108_107',
                        value = '(FRCTdeltaxaSxstR*G)/(2.*aS) + (3*FRCTdeltaZxGGxstR*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_108 = Coupling(name = 'UVGC_108_108',
                        value = '(FRCTdeltaxaSxsuL*G)/(2.*aS) + (3*FRCTdeltaZxGGxsuL*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_109 = Coupling(name = 'UVGC_108_109',
                        value = '(FRCTdeltaxaSxsuR*G)/(2.*aS) + (3*FRCTdeltaZxGGxsuR*G)/2. - (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_110 = Coupling(name = 'UVGC_108_110',
                        value = '(FRCTdeltaxaSxt*G)/(2.*aS) + (3*FRCTdeltaZxGGxt*G)/2. - (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_108_111 = Coupling(name = 'UVGC_108_111',
                        value = '(3*FRCTdeltaZxGGxu*G)/2. - (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_112 = Coupling(name = 'UVGC_109_112',
                        value = '(-3*FRCTdeltaZxGGxb*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_113 = Coupling(name = 'UVGC_109_113',
                        value = '(-3*FRCTdeltaZxGGxc*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_114 = Coupling(name = 'UVGC_109_114',
                        value = '(-3*FRCTdeltaZxGGxd*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_115 = Coupling(name = 'UVGC_109_115',
                        value = '(-3*FRCTdeltaZxGGxG*G)/2. - (15*G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_116 = Coupling(name = 'UVGC_109_116',
                        value = '(-3*FRCTdeltaZxGGxghG*G)/2. - (G**3*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_117 = Coupling(name = 'UVGC_109_117',
                        value = '-(FRCTdeltaxaSxgo*G)/(2.*aS) - (3*FRCTdeltaZxGGxgo*G)/2. + (G**3*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_118 = Coupling(name = 'UVGC_109_118',
                        value = '(-3*FRCTdeltaZxGGxs*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_119 = Coupling(name = 'UVGC_109_119',
                        value = '-(FRCTdeltaxaSxsbL*G)/(2.*aS) - (3*FRCTdeltaZxGGxsbL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_120 = Coupling(name = 'UVGC_109_120',
                        value = '-(FRCTdeltaxaSxsbR*G)/(2.*aS) - (3*FRCTdeltaZxGGxsbR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_121 = Coupling(name = 'UVGC_109_121',
                        value = '-(FRCTdeltaxaSxscL*G)/(2.*aS) - (3*FRCTdeltaZxGGxscL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_122 = Coupling(name = 'UVGC_109_122',
                        value = '-(FRCTdeltaxaSxscR*G)/(2.*aS) - (3*FRCTdeltaZxGGxscR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_123 = Coupling(name = 'UVGC_109_123',
                        value = '-(FRCTdeltaxaSxsdL*G)/(2.*aS) - (3*FRCTdeltaZxGGxsdL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_124 = Coupling(name = 'UVGC_109_124',
                        value = '-(FRCTdeltaxaSxsdR*G)/(2.*aS) - (3*FRCTdeltaZxGGxsdR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_125 = Coupling(name = 'UVGC_109_125',
                        value = '-(FRCTdeltaxaSxssL*G)/(2.*aS) - (3*FRCTdeltaZxGGxssL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_126 = Coupling(name = 'UVGC_109_126',
                        value = '-(FRCTdeltaxaSxssR*G)/(2.*aS) - (3*FRCTdeltaZxGGxssR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_127 = Coupling(name = 'UVGC_109_127',
                        value = '-(FRCTdeltaxaSxstL*G)/(2.*aS) - (3*FRCTdeltaZxGGxstL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_128 = Coupling(name = 'UVGC_109_128',
                        value = '-(FRCTdeltaxaSxstR*G)/(2.*aS) - (3*FRCTdeltaZxGGxstR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_129 = Coupling(name = 'UVGC_109_129',
                        value = '-(FRCTdeltaxaSxsuL*G)/(2.*aS) - (3*FRCTdeltaZxGGxsuL*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_130 = Coupling(name = 'UVGC_109_130',
                        value = '-(FRCTdeltaxaSxsuR*G)/(2.*aS) - (3*FRCTdeltaZxGGxsuR*G)/2. + (G**3*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_131 = Coupling(name = 'UVGC_109_131',
                        value = '-(FRCTdeltaxaSxt*G)/(2.*aS) - (3*FRCTdeltaZxGGxt*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_109_132 = Coupling(name = 'UVGC_109_132',
                        value = '(-3*FRCTdeltaZxGGxu*G)/2. + (G**3*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_110_133 = Coupling(name = 'UVGC_110_133',
                        value = '-(complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_110_134 = Coupling(name = 'UVGC_110_134',
                        value = '-(complex(0,1)*G**4*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_110_135 = Coupling(name = 'UVGC_110_135',
                        value = '(complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_111_136 = Coupling(name = 'UVGC_111_136',
                        value = '(5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_137 = Coupling(name = 'UVGC_112_137',
                        value = '2*FRCTdeltaZxGGxb*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_138 = Coupling(name = 'UVGC_112_138',
                        value = '2*FRCTdeltaZxGGxc*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_139 = Coupling(name = 'UVGC_112_139',
                        value = '2*FRCTdeltaZxGGxd*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_140 = Coupling(name = 'UVGC_112_140',
                        value = '2*FRCTdeltaZxGGxG*complex(0,1)*G**2 - (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_141 = Coupling(name = 'UVGC_112_141',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_142 = Coupling(name = 'UVGC_112_142',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_143 = Coupling(name = 'UVGC_112_143',
                        value = '2*FRCTdeltaZxGGxs*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_144 = Coupling(name = 'UVGC_112_144',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_145 = Coupling(name = 'UVGC_112_145',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_146 = Coupling(name = 'UVGC_112_146',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_147 = Coupling(name = 'UVGC_112_147',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_148 = Coupling(name = 'UVGC_112_148',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_149 = Coupling(name = 'UVGC_112_149',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_150 = Coupling(name = 'UVGC_112_150',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_151 = Coupling(name = 'UVGC_112_151',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_152 = Coupling(name = 'UVGC_112_152',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_153 = Coupling(name = 'UVGC_112_153',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_154 = Coupling(name = 'UVGC_112_154',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_155 = Coupling(name = 'UVGC_112_155',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_156 = Coupling(name = 'UVGC_112_156',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_112_157 = Coupling(name = 'UVGC_112_157',
                        value = '2*FRCTdeltaZxGGxu*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_113_158 = Coupling(name = 'UVGC_113_158',
                        value = '2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_159 = Coupling(name = 'UVGC_114_159',
                        value = '-2*FRCTdeltaZxGGxb*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_160 = Coupling(name = 'UVGC_114_160',
                        value = '-2*FRCTdeltaZxGGxc*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_161 = Coupling(name = 'UVGC_114_161',
                        value = '-2*FRCTdeltaZxGGxd*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_162 = Coupling(name = 'UVGC_114_162',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_163 = Coupling(name = 'UVGC_114_163',
                        value = '-((FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxgo*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(4.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_164 = Coupling(name = 'UVGC_114_164',
                        value = '-2*FRCTdeltaZxGGxs*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_165 = Coupling(name = 'UVGC_114_165',
                        value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_166 = Coupling(name = 'UVGC_114_166',
                        value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_167 = Coupling(name = 'UVGC_114_167',
                        value = '-((FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_168 = Coupling(name = 'UVGC_114_168',
                        value = '-((FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_169 = Coupling(name = 'UVGC_114_169',
                        value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_170 = Coupling(name = 'UVGC_114_170',
                        value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_171 = Coupling(name = 'UVGC_114_171',
                        value = '-((FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_172 = Coupling(name = 'UVGC_114_172',
                        value = '-((FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_173 = Coupling(name = 'UVGC_114_173',
                        value = '-((FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_174 = Coupling(name = 'UVGC_114_174',
                        value = '-((FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_175 = Coupling(name = 'UVGC_114_175',
                        value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_176 = Coupling(name = 'UVGC_114_176',
                        value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_177 = Coupling(name = 'UVGC_114_177',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxt*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_114_178 = Coupling(name = 'UVGC_114_178',
                        value = '-2*FRCTdeltaZxGGxu*complex(0,1)*G**2 + (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_179 = Coupling(name = 'UVGC_115_179',
                        value = '-2*FRCTdeltaZxGGxG*complex(0,1)*G**2 + (7*complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_180 = Coupling(name = 'UVGC_115_180',
                        value = '-2*FRCTdeltaZxGGxghG*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(128.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_181 = Coupling(name = 'UVGC_115_181',
                        value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_182 = Coupling(name = 'UVGC_115_182',
                        value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsbR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_183 = Coupling(name = 'UVGC_115_183',
                        value = '-((FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_184 = Coupling(name = 'UVGC_115_184',
                        value = '-((FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxscR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_185 = Coupling(name = 'UVGC_115_185',
                        value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_186 = Coupling(name = 'UVGC_115_186',
                        value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsdR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_187 = Coupling(name = 'UVGC_115_187',
                        value = '-((FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_188 = Coupling(name = 'UVGC_115_188',
                        value = '-((FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxssR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_189 = Coupling(name = 'UVGC_115_189',
                        value = '-((FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_190 = Coupling(name = 'UVGC_115_190',
                        value = '-((FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxstR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_191 = Coupling(name = 'UVGC_115_191',
                        value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuL*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_115_192 = Coupling(name = 'UVGC_115_192',
                        value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS) - 2*FRCTdeltaZxGGxsuR*complex(0,1)*G**2 + (5*complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_116_193 = Coupling(name = 'UVGC_116_193',
                        value = '(complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_117_194 = Coupling(name = 'UVGC_117_194',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_118_195 = Coupling(name = 'UVGC_118_195',
                        value = '(-13*complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_119_196 = Coupling(name = 'UVGC_119_196',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_120_197 = Coupling(name = 'UVGC_120_197',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_121_198 = Coupling(name = 'UVGC_121_198',
                        value = '(ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_122_199 = Coupling(name = 'UVGC_122_199',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_123_200 = Coupling(name = 'UVGC_123_200',
                        value = '(ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_124_201 = Coupling(name = 'UVGC_124_201',
                        value = '-(cw*ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*invFREps*sw)/(54.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_125_202 = Coupling(name = 'UVGC_125_202',
                        value = '(ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_126_203 = Coupling(name = 'UVGC_126_203',
                        value = '(cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_127_204 = Coupling(name = 'UVGC_127_204',
                        value = '-(ee*complex(0,1)*G**3*invFREps*sw)/(9.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_128_205 = Coupling(name = 'UVGC_128_205',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_129_206 = Coupling(name = 'UVGC_129_206',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_133_207 = Coupling(name = 'UVGC_133_207',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_134_208 = Coupling(name = 'UVGC_134_208',
                        value = '(-4*ee**2*complex(0,1)*G**2*invFREps)/(27.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_135_209 = Coupling(name = 'UVGC_135_209',
                        value = '(-2*ee*complex(0,1)*G**3*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_136_210 = Coupling(name = 'UVGC_136_210',
                        value = '(cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_137_211 = Coupling(name = 'UVGC_137_211',
                        value = '-(ee*complex(0,1)*G**2*invFREps*sw)/(9.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_138_212 = Coupling(name = 'UVGC_138_212',
                        value = '-(cw*ee**2*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2*sw) + (ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_139_213 = Coupling(name = 'UVGC_139_213',
                        value = '(4*ee**2*complex(0,1)*G**2*invFREps*sw)/(27.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_140_214 = Coupling(name = 'UVGC_140_214',
                        value = '-(cw*ee*complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_141_215 = Coupling(name = 'UVGC_141_215',
                        value = '(2*ee*complex(0,1)*G**3*invFREps*sw)/(9.*cw*cmath.pi**2)',
                        order = {'QCD':3,'QED':1})

UVGC_142_216 = Coupling(name = 'UVGC_142_216',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2) - (cw**2*ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2) - (ee**2*complex(0,1)*G**2*invFREps*sw**2)/(108.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_143_217 = Coupling(name = 'UVGC_143_217',
                        value = '(-4*ee**2*complex(0,1)*G**2*invFREps*sw**2)/(27.*cw**2*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_158_218 = Coupling(name = 'UVGC_158_218',
                        value = 'FRCTdeltaZxsbLsbLxbgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_158_219 = Coupling(name = 'UVGC_158_219',
                        value = 'FRCTdeltaZxsbLsbLxGsbL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_159_220 = Coupling(name = 'UVGC_159_220',
                        value = '-(FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_221 = Coupling(name = 'UVGC_159_221',
                        value = '-(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_222 = Coupling(name = 'UVGC_159_222',
                        value = '-(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_223 = Coupling(name = 'UVGC_159_223',
                        value = '-(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_224 = Coupling(name = 'UVGC_159_224',
                        value = '-(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_225 = Coupling(name = 'UVGC_159_225',
                        value = '-(FRCTdeltaxaSxgo*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxgo*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_226 = Coupling(name = 'UVGC_159_226',
                        value = '-(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_227 = Coupling(name = 'UVGC_159_227',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsbL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_228 = Coupling(name = 'UVGC_159_228',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsbR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_229 = Coupling(name = 'UVGC_159_229',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxscL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_230 = Coupling(name = 'UVGC_159_230',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxscR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_231 = Coupling(name = 'UVGC_159_231',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsdL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_232 = Coupling(name = 'UVGC_159_232',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsdR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_233 = Coupling(name = 'UVGC_159_233',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxssL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_234 = Coupling(name = 'UVGC_159_234',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxssR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_235 = Coupling(name = 'UVGC_159_235',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxstL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_236 = Coupling(name = 'UVGC_159_236',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxstR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_237 = Coupling(name = 'UVGC_159_237',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsuL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_238 = Coupling(name = 'UVGC_159_238',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxsuR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_239 = Coupling(name = 'UVGC_159_239',
                        value = '-(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) - (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_240 = Coupling(name = 'UVGC_159_240',
                        value = '-(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_159_241 = Coupling(name = 'UVGC_159_241',
                        value = '-(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_159_242 = Coupling(name = 'UVGC_159_242',
                        value = '-(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_160_243 = Coupling(name = 'UVGC_160_243',
                        value = '(-3*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_160_244 = Coupling(name = 'UVGC_160_244',
                        value = '(3*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_161_245 = Coupling(name = 'UVGC_161_245',
                        value = 'FRCTdeltaZxGGxb*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_246 = Coupling(name = 'UVGC_161_246',
                        value = 'FRCTdeltaZxGGxc*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_247 = Coupling(name = 'UVGC_161_247',
                        value = 'FRCTdeltaZxGGxd*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_248 = Coupling(name = 'UVGC_161_248',
                        value = 'FRCTdeltaZxGGxG*complex(0,1)*G**2 - (9*complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_161_249 = Coupling(name = 'UVGC_161_249',
                        value = 'FRCTdeltaZxGGxghG*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_250 = Coupling(name = 'UVGC_161_250',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxgo*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_251 = Coupling(name = 'UVGC_161_251',
                        value = 'FRCTdeltaZxGGxs*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_252 = Coupling(name = 'UVGC_161_252',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsbL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_253 = Coupling(name = 'UVGC_161_253',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsbR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_254 = Coupling(name = 'UVGC_161_254',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxscL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_255 = Coupling(name = 'UVGC_161_255',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxscR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_256 = Coupling(name = 'UVGC_161_256',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsdL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_257 = Coupling(name = 'UVGC_161_257',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsdR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_258 = Coupling(name = 'UVGC_161_258',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxssL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_259 = Coupling(name = 'UVGC_161_259',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxssR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_260 = Coupling(name = 'UVGC_161_260',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxstL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_261 = Coupling(name = 'UVGC_161_261',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxstR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_262 = Coupling(name = 'UVGC_161_262',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsuL*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_263 = Coupling(name = 'UVGC_161_263',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxsuR*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_264 = Coupling(name = 'UVGC_161_264',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/aS + FRCTdeltaZxGGxt*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_265 = Coupling(name = 'UVGC_161_265',
                        value = 'FRCTdeltaZxGGxu*complex(0,1)*G**2',
                        order = {'QCD':4})

UVGC_161_266 = Coupling(name = 'UVGC_161_266',
                        value = 'FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_161_267 = Coupling(name = 'UVGC_161_267',
                        value = 'FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_268 = Coupling(name = 'UVGC_162_268',
                        value = '(-13*complex(0,1)*G**4*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_269 = Coupling(name = 'UVGC_162_269',
                        value = '-(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(3.*aS)',
                        order = {'QCD':4})

UVGC_162_270 = Coupling(name = 'UVGC_162_270',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_271 = Coupling(name = 'UVGC_162_271',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_272 = Coupling(name = 'UVGC_162_272',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_273 = Coupling(name = 'UVGC_162_273',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_274 = Coupling(name = 'UVGC_162_274',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_275 = Coupling(name = 'UVGC_162_275',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_276 = Coupling(name = 'UVGC_162_276',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_277 = Coupling(name = 'UVGC_162_277',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_278 = Coupling(name = 'UVGC_162_278',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_279 = Coupling(name = 'UVGC_162_279',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_280 = Coupling(name = 'UVGC_162_280',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_281 = Coupling(name = 'UVGC_162_281',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_282 = Coupling(name = 'UVGC_162_282',
                        value = '-(FRCTdeltaxaSxt*complex(0,1)*G**2)/(3.*aS)',
                        order = {'QCD':4})

UVGC_162_283 = Coupling(name = 'UVGC_162_283',
                        value = '(-2*FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_162_284 = Coupling(name = 'UVGC_162_284',
                        value = '(-2*FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_163_285 = Coupling(name = 'UVGC_163_285',
                        value = '-2*FRCTdeltaxMsbLxsbL*complex(0,1)*MsbL - (complex(0,1)*G**2*invFREps*MsbL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_163_286 = Coupling(name = 'UVGC_163_286',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsbLxbgo*complex(0,1)*MsbL - FRCTdeltaZxsbLsbLxbgo*complex(0,1)*MsbL**2',
                        order = {'QCD':2})

UVGC_163_287 = Coupling(name = 'UVGC_163_287',
                        value = '-2*FRCTdeltaxMsbLxGsbL*complex(0,1)*MsbL - FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*MsbL**2 + (complex(0,1)*G**2*invFREps*MsbL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_164_288 = Coupling(name = 'UVGC_164_288',
                        value = 'FRCTdeltaZxsbRsbRxbgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_164_289 = Coupling(name = 'UVGC_164_289',
                        value = 'FRCTdeltaZxsbRsbRxGsbR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_165_290 = Coupling(name = 'UVGC_165_290',
                        value = '-(FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_165_291 = Coupling(name = 'UVGC_165_291',
                        value = '-(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_167_292 = Coupling(name = 'UVGC_167_292',
                        value = 'FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_167_293 = Coupling(name = 'UVGC_167_293',
                        value = 'FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_168_294 = Coupling(name = 'UVGC_168_294',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(3.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_168_295 = Coupling(name = 'UVGC_168_295',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_168_296 = Coupling(name = 'UVGC_168_296',
                        value = '(-2*FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_168_297 = Coupling(name = 'UVGC_168_297',
                        value = '(-2*FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_169_298 = Coupling(name = 'UVGC_169_298',
                        value = '-2*FRCTdeltaxMsbRxsbR*complex(0,1)*MsbR - (complex(0,1)*G**2*invFREps*MsbR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_169_299 = Coupling(name = 'UVGC_169_299',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsbRxbgo*complex(0,1)*MsbR - FRCTdeltaZxsbRsbRxbgo*complex(0,1)*MsbR**2',
                        order = {'QCD':2})

UVGC_169_300 = Coupling(name = 'UVGC_169_300',
                        value = '-2*FRCTdeltaxMsbRxGsbR*complex(0,1)*MsbR - FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*MsbR**2 + (complex(0,1)*G**2*invFREps*MsbR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_170_301 = Coupling(name = 'UVGC_170_301',
                        value = 'FRCTdeltaZxscLscLxcgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_170_302 = Coupling(name = 'UVGC_170_302',
                        value = 'FRCTdeltaZxscLscLxGscL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_171_303 = Coupling(name = 'UVGC_171_303',
                        value = '-(FRCTdeltaZxscLscLxcgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_171_304 = Coupling(name = 'UVGC_171_304',
                        value = '-(FRCTdeltaZxscLscLxGscL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_173_305 = Coupling(name = 'UVGC_173_305',
                        value = 'FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_173_306 = Coupling(name = 'UVGC_173_306',
                        value = 'FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_174_307 = Coupling(name = 'UVGC_174_307',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_174_308 = Coupling(name = 'UVGC_174_308',
                        value = '(-2*FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_174_309 = Coupling(name = 'UVGC_174_309',
                        value = '(-2*FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_175_310 = Coupling(name = 'UVGC_175_310',
                        value = '-2*FRCTdeltaxMscLxscL*complex(0,1)*MscL - (complex(0,1)*G**2*invFREps*MscL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_175_311 = Coupling(name = 'UVGC_175_311',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMscLxcgo*complex(0,1)*MscL - FRCTdeltaZxscLscLxcgo*complex(0,1)*MscL**2',
                        order = {'QCD':2})

UVGC_175_312 = Coupling(name = 'UVGC_175_312',
                        value = '-2*FRCTdeltaxMscLxGscL*complex(0,1)*MscL - FRCTdeltaZxscLscLxGscL*complex(0,1)*MscL**2 + (complex(0,1)*G**2*invFREps*MscL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_176_313 = Coupling(name = 'UVGC_176_313',
                        value = 'FRCTdeltaZxscRscRxcgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_176_314 = Coupling(name = 'UVGC_176_314',
                        value = 'FRCTdeltaZxscRscRxGscR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_177_315 = Coupling(name = 'UVGC_177_315',
                        value = '-(FRCTdeltaZxscRscRxcgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_177_316 = Coupling(name = 'UVGC_177_316',
                        value = '-(FRCTdeltaZxscRscRxGscR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_179_317 = Coupling(name = 'UVGC_179_317',
                        value = 'FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_179_318 = Coupling(name = 'UVGC_179_318',
                        value = 'FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_180_319 = Coupling(name = 'UVGC_180_319',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_180_320 = Coupling(name = 'UVGC_180_320',
                        value = '(-2*FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_180_321 = Coupling(name = 'UVGC_180_321',
                        value = '(-2*FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_181_322 = Coupling(name = 'UVGC_181_322',
                        value = '-2*FRCTdeltaxMscRxscR*complex(0,1)*MscR - (complex(0,1)*G**2*invFREps*MscR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_181_323 = Coupling(name = 'UVGC_181_323',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMscRxcgo*complex(0,1)*MscR - FRCTdeltaZxscRscRxcgo*complex(0,1)*MscR**2',
                        order = {'QCD':2})

UVGC_181_324 = Coupling(name = 'UVGC_181_324',
                        value = '-2*FRCTdeltaxMscRxGscR*complex(0,1)*MscR - FRCTdeltaZxscRscRxGscR*complex(0,1)*MscR**2 + (complex(0,1)*G**2*invFREps*MscR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_182_325 = Coupling(name = 'UVGC_182_325',
                        value = 'FRCTdeltaZxsdLsdLxdgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_182_326 = Coupling(name = 'UVGC_182_326',
                        value = 'FRCTdeltaZxsdLsdLxGsdL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_183_327 = Coupling(name = 'UVGC_183_327',
                        value = '-(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_183_328 = Coupling(name = 'UVGC_183_328',
                        value = '-(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_185_329 = Coupling(name = 'UVGC_185_329',
                        value = 'FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_185_330 = Coupling(name = 'UVGC_185_330',
                        value = 'FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_186_331 = Coupling(name = 'UVGC_186_331',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_186_332 = Coupling(name = 'UVGC_186_332',
                        value = '(-2*FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_186_333 = Coupling(name = 'UVGC_186_333',
                        value = '(-2*FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_187_334 = Coupling(name = 'UVGC_187_334',
                        value = '-2*FRCTdeltaxMsdLxsdL*complex(0,1)*MsdL - (complex(0,1)*G**2*invFREps*MsdL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_187_335 = Coupling(name = 'UVGC_187_335',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsdLxdgo*complex(0,1)*MsdL - FRCTdeltaZxsdLsdLxdgo*complex(0,1)*MsdL**2',
                        order = {'QCD':2})

UVGC_187_336 = Coupling(name = 'UVGC_187_336',
                        value = '-2*FRCTdeltaxMsdLxGsdL*complex(0,1)*MsdL - FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*MsdL**2 + (complex(0,1)*G**2*invFREps*MsdL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_188_337 = Coupling(name = 'UVGC_188_337',
                        value = 'FRCTdeltaZxsdRsdRxdgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_188_338 = Coupling(name = 'UVGC_188_338',
                        value = 'FRCTdeltaZxsdRsdRxGsdR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_189_339 = Coupling(name = 'UVGC_189_339',
                        value = '-(FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_189_340 = Coupling(name = 'UVGC_189_340',
                        value = '-(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_191_341 = Coupling(name = 'UVGC_191_341',
                        value = 'FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_191_342 = Coupling(name = 'UVGC_191_342',
                        value = 'FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_192_343 = Coupling(name = 'UVGC_192_343',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_192_344 = Coupling(name = 'UVGC_192_344',
                        value = '(-2*FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_192_345 = Coupling(name = 'UVGC_192_345',
                        value = '(-2*FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_193_346 = Coupling(name = 'UVGC_193_346',
                        value = '-2*FRCTdeltaxMsdRxsdR*complex(0,1)*MsdR - (complex(0,1)*G**2*invFREps*MsdR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_193_347 = Coupling(name = 'UVGC_193_347',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsdRxdgo*complex(0,1)*MsdR - FRCTdeltaZxsdRsdRxdgo*complex(0,1)*MsdR**2',
                        order = {'QCD':2})

UVGC_193_348 = Coupling(name = 'UVGC_193_348',
                        value = '-2*FRCTdeltaxMsdRxGsdR*complex(0,1)*MsdR - FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*MsdR**2 + (complex(0,1)*G**2*invFREps*MsdR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_200_349 = Coupling(name = 'UVGC_200_349',
                        value = '(3*complex(0,1)*G**2*invFREps)/(16.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_201_350 = Coupling(name = 'UVGC_201_350',
                        value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_201_351 = Coupling(name = 'UVGC_201_351',
                        value = '-(ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_202_352 = Coupling(name = 'UVGC_202_352',
                        value = '-(FRCTdeltaZxbbLxbG*complex(0,1))',
                        order = {'QCD':2})

UVGC_202_353 = Coupling(name = 'UVGC_202_353',
                        value = '-(FRCTdeltaZxbbLxgosbL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_203_354 = Coupling(name = 'UVGC_203_354',
                        value = '(FRCTdeltaZxGGxb*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_355 = Coupling(name = 'UVGC_203_355',
                        value = '(FRCTdeltaZxGGxc*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_356 = Coupling(name = 'UVGC_203_356',
                        value = '(FRCTdeltaZxGGxd*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_357 = Coupling(name = 'UVGC_203_357',
                        value = '(FRCTdeltaZxGGxG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_358 = Coupling(name = 'UVGC_203_358',
                        value = '(FRCTdeltaZxGGxghG*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_359 = Coupling(name = 'UVGC_203_359',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxgo*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_360 = Coupling(name = 'UVGC_203_360',
                        value = '(FRCTdeltaZxGGxs*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_361 = Coupling(name = 'UVGC_203_361',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsbL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_362 = Coupling(name = 'UVGC_203_362',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsbR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_363 = Coupling(name = 'UVGC_203_363',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxscL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_364 = Coupling(name = 'UVGC_203_364',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxscR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_365 = Coupling(name = 'UVGC_203_365',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsdL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_366 = Coupling(name = 'UVGC_203_366',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsdR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_367 = Coupling(name = 'UVGC_203_367',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxssL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_368 = Coupling(name = 'UVGC_203_368',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxssR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_369 = Coupling(name = 'UVGC_203_369',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxstL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_370 = Coupling(name = 'UVGC_203_370',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxstR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_371 = Coupling(name = 'UVGC_203_371',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsuL*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_372 = Coupling(name = 'UVGC_203_372',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxsuR*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_373 = Coupling(name = 'UVGC_203_373',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G)/(2.*aS) + (FRCTdeltaZxGGxt*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_374 = Coupling(name = 'UVGC_203_374',
                        value = '(FRCTdeltaZxGGxu*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_203_375 = Coupling(name = 'UVGC_203_375',
                        value = 'FRCTdeltaZxbbLxbG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_203_376 = Coupling(name = 'UVGC_203_376',
                        value = 'FRCTdeltaZxbbLxgosbL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_204_377 = Coupling(name = 'UVGC_204_377',
                        value = '-(cw*ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxbbLxbG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_204_378 = Coupling(name = 'UVGC_204_378',
                        value = '-(cw*ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxbbLxgosbL*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_205_379 = Coupling(name = 'UVGC_205_379',
                        value = '-(ee*FRCTdeltaZxbbRxbG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_205_380 = Coupling(name = 'UVGC_205_380',
                        value = '-(ee*FRCTdeltaZxbbRxgosbR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_206_381 = Coupling(name = 'UVGC_206_381',
                        value = '-(FRCTdeltaZxbbRxbG*complex(0,1))',
                        order = {'QCD':2})

UVGC_206_382 = Coupling(name = 'UVGC_206_382',
                        value = '-(FRCTdeltaZxbbRxgosbR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_207_383 = Coupling(name = 'UVGC_207_383',
                        value = 'FRCTdeltaZxbbRxbG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_207_384 = Coupling(name = 'UVGC_207_384',
                        value = 'FRCTdeltaZxbbRxgosbR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_208_385 = Coupling(name = 'UVGC_208_385',
                        value = '(ee*FRCTdeltaZxbbRxbG*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_208_386 = Coupling(name = 'UVGC_208_386',
                        value = '(ee*FRCTdeltaZxbbRxgosbR*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_209_387 = Coupling(name = 'UVGC_209_387',
                        value = '(2*ee*FRCTdeltaZxccLxcG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_209_388 = Coupling(name = 'UVGC_209_388',
                        value = '(2*ee*FRCTdeltaZxccLxgoscL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_210_389 = Coupling(name = 'UVGC_210_389',
                        value = '-(FRCTdeltaZxccLxcG*complex(0,1))',
                        order = {'QCD':2})

UVGC_210_390 = Coupling(name = 'UVGC_210_390',
                        value = '-(FRCTdeltaZxccLxgoscL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_211_391 = Coupling(name = 'UVGC_211_391',
                        value = 'FRCTdeltaZxccLxcG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_211_392 = Coupling(name = 'UVGC_211_392',
                        value = 'FRCTdeltaZxccLxgoscL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_212_393 = Coupling(name = 'UVGC_212_393',
                        value = '(cw*ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxccLxcG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_212_394 = Coupling(name = 'UVGC_212_394',
                        value = '(cw*ee*FRCTdeltaZxccLxgoscL*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxccLxgoscL*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_213_395 = Coupling(name = 'UVGC_213_395',
                        value = '(2*ee*FRCTdeltaZxccRxcG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_213_396 = Coupling(name = 'UVGC_213_396',
                        value = '(2*ee*FRCTdeltaZxccRxgoscR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_214_397 = Coupling(name = 'UVGC_214_397',
                        value = '-(FRCTdeltaZxccRxcG*complex(0,1))',
                        order = {'QCD':2})

UVGC_214_398 = Coupling(name = 'UVGC_214_398',
                        value = '-(FRCTdeltaZxccRxgoscR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_215_399 = Coupling(name = 'UVGC_215_399',
                        value = 'FRCTdeltaZxccRxcG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_215_400 = Coupling(name = 'UVGC_215_400',
                        value = 'FRCTdeltaZxccRxgoscR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_216_401 = Coupling(name = 'UVGC_216_401',
                        value = '(-2*ee*FRCTdeltaZxccRxcG*complex(0,1)*sw)/(3.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_216_402 = Coupling(name = 'UVGC_216_402',
                        value = '(-2*ee*FRCTdeltaZxccRxgoscR*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_217_403 = Coupling(name = 'UVGC_217_403',
                        value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_217_404 = Coupling(name = 'UVGC_217_404',
                        value = '-(ee*FRCTdeltaZxddLxgosdL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_218_405 = Coupling(name = 'UVGC_218_405',
                        value = '-(FRCTdeltaZxddLxdG*complex(0,1))',
                        order = {'QCD':2})

UVGC_218_406 = Coupling(name = 'UVGC_218_406',
                        value = '-(FRCTdeltaZxddLxgosdL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_219_407 = Coupling(name = 'UVGC_219_407',
                        value = 'FRCTdeltaZxddLxdG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_219_408 = Coupling(name = 'UVGC_219_408',
                        value = 'FRCTdeltaZxddLxgosdL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_220_409 = Coupling(name = 'UVGC_220_409',
                        value = '-(cw*ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxddLxdG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_220_410 = Coupling(name = 'UVGC_220_410',
                        value = '-(cw*ee*FRCTdeltaZxddLxgosdL*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxddLxgosdL*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_221_411 = Coupling(name = 'UVGC_221_411',
                        value = '-(ee*FRCTdeltaZxddRxdG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_221_412 = Coupling(name = 'UVGC_221_412',
                        value = '-(ee*FRCTdeltaZxddRxgosdR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_222_413 = Coupling(name = 'UVGC_222_413',
                        value = '-(FRCTdeltaZxddRxdG*complex(0,1))',
                        order = {'QCD':2})

UVGC_222_414 = Coupling(name = 'UVGC_222_414',
                        value = '-(FRCTdeltaZxddRxgosdR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_223_415 = Coupling(name = 'UVGC_223_415',
                        value = 'FRCTdeltaZxddRxdG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_223_416 = Coupling(name = 'UVGC_223_416',
                        value = 'FRCTdeltaZxddRxgosdR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_224_417 = Coupling(name = 'UVGC_224_417',
                        value = '(ee*FRCTdeltaZxddRxdG*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_224_418 = Coupling(name = 'UVGC_224_418',
                        value = '(ee*FRCTdeltaZxddRxgosdR*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_225_419 = Coupling(name = 'UVGC_225_419',
                        value = '-(FRCTdeltaZxssLxsG*complex(0,1))',
                        order = {'QCD':2})

UVGC_225_420 = Coupling(name = 'UVGC_225_420',
                        value = '-(FRCTdeltaZxssLxgossL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_226_421 = Coupling(name = 'UVGC_226_421',
                        value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_226_422 = Coupling(name = 'UVGC_226_422',
                        value = '-(ee*FRCTdeltaZxssLxgossL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_227_423 = Coupling(name = 'UVGC_227_423',
                        value = '-(FRCTdeltaZxssRxsG*complex(0,1))',
                        order = {'QCD':2})

UVGC_227_424 = Coupling(name = 'UVGC_227_424',
                        value = '-(FRCTdeltaZxssRxgossR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_228_425 = Coupling(name = 'UVGC_228_425',
                        value = '-(ee*FRCTdeltaZxssRxsG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_228_426 = Coupling(name = 'UVGC_228_426',
                        value = '-(ee*FRCTdeltaZxssRxgossR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_229_427 = Coupling(name = 'UVGC_229_427',
                        value = 'FRCTdeltaZxssLxsG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_229_428 = Coupling(name = 'UVGC_229_428',
                        value = 'FRCTdeltaZxssLxgossL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_230_429 = Coupling(name = 'UVGC_230_429',
                        value = 'FRCTdeltaZxssRxsG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_230_430 = Coupling(name = 'UVGC_230_430',
                        value = 'FRCTdeltaZxssRxgossR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_234_431 = Coupling(name = 'UVGC_234_431',
                        value = '-(cw*ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxssLxsG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_234_432 = Coupling(name = 'UVGC_234_432',
                        value = '-(cw*ee*FRCTdeltaZxssLxgossL*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxssLxgossL*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_235_433 = Coupling(name = 'UVGC_235_433',
                        value = '(ee*FRCTdeltaZxssRxsG*complex(0,1)*sw)/(3.*cw) - (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_235_434 = Coupling(name = 'UVGC_235_434',
                        value = '(ee*FRCTdeltaZxssRxgossR*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_236_435 = Coupling(name = 'UVGC_236_435',
                        value = 'FRCTdeltaZxssLssLxGssL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_236_436 = Coupling(name = 'UVGC_236_436',
                        value = 'FRCTdeltaZxssLssLxsgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_237_437 = Coupling(name = 'UVGC_237_437',
                        value = 'FRCTdeltaZxssRssRxGssR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_237_438 = Coupling(name = 'UVGC_237_438',
                        value = 'FRCTdeltaZxssRssRxsgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_240_439 = Coupling(name = 'UVGC_240_439',
                        value = '-(FRCTdeltaZxssLssLxGssL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_240_440 = Coupling(name = 'UVGC_240_440',
                        value = '-(FRCTdeltaZxssLssLxsgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_241_441 = Coupling(name = 'UVGC_241_441',
                        value = '-(FRCTdeltaZxssRssRxGssR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_241_442 = Coupling(name = 'UVGC_241_442',
                        value = '-(FRCTdeltaZxssRssRxsgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_243_443 = Coupling(name = 'UVGC_243_443',
                        value = 'FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_243_444 = Coupling(name = 'UVGC_243_444',
                        value = 'FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_244_445 = Coupling(name = 'UVGC_244_445',
                        value = 'FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_244_446 = Coupling(name = 'UVGC_244_446',
                        value = 'FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_245_447 = Coupling(name = 'UVGC_245_447',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_245_448 = Coupling(name = 'UVGC_245_448',
                        value = '(-2*FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_245_449 = Coupling(name = 'UVGC_245_449',
                        value = '(-2*FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_246_450 = Coupling(name = 'UVGC_246_450',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_246_451 = Coupling(name = 'UVGC_246_451',
                        value = '(-2*FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_246_452 = Coupling(name = 'UVGC_246_452',
                        value = '(-2*FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_247_453 = Coupling(name = 'UVGC_247_453',
                        value = '-2*FRCTdeltaxMssLxssL*complex(0,1)*MssL - (complex(0,1)*G**2*invFREps*MssL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_247_454 = Coupling(name = 'UVGC_247_454',
                        value = '-2*FRCTdeltaxMssLxGssL*complex(0,1)*MssL - FRCTdeltaZxssLssLxGssL*complex(0,1)*MssL**2 + (complex(0,1)*G**2*invFREps*MssL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_247_455 = Coupling(name = 'UVGC_247_455',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMssLxsgo*complex(0,1)*MssL - FRCTdeltaZxssLssLxsgo*complex(0,1)*MssL**2',
                        order = {'QCD':2})

UVGC_248_456 = Coupling(name = 'UVGC_248_456',
                        value = '-2*FRCTdeltaxMssRxssR*complex(0,1)*MssR - (complex(0,1)*G**2*invFREps*MssR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_248_457 = Coupling(name = 'UVGC_248_457',
                        value = '-2*FRCTdeltaxMssRxGssR*complex(0,1)*MssR - FRCTdeltaZxssRssRxGssR*complex(0,1)*MssR**2 + (complex(0,1)*G**2*invFREps*MssR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_248_458 = Coupling(name = 'UVGC_248_458',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMssRxsgo*complex(0,1)*MssR - FRCTdeltaZxssRssRxsgo*complex(0,1)*MssR**2',
                        order = {'QCD':2})

UVGC_257_459 = Coupling(name = 'UVGC_257_459',
                        value = '-(FRCTdeltaZxttLxtG*complex(0,1))',
                        order = {'QCD':2})

UVGC_257_460 = Coupling(name = 'UVGC_257_460',
                        value = '-(FRCTdeltaZxttLxgostL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_257_461 = Coupling(name = 'UVGC_257_461',
                        value = '-(FRCTdeltaZxttLxgostR*complex(0,1))',
                        order = {'QCD':2})

UVGC_258_462 = Coupling(name = 'UVGC_258_462',
                        value = '(2*ee*FRCTdeltaZxttLxtG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_258_463 = Coupling(name = 'UVGC_258_463',
                        value = '(2*ee*FRCTdeltaZxttLxgostL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_258_464 = Coupling(name = 'UVGC_258_464',
                        value = '(2*ee*FRCTdeltaZxttLxgostR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_259_465 = Coupling(name = 'UVGC_259_465',
                        value = '-(FRCTdeltaZxttRxtG*complex(0,1))',
                        order = {'QCD':2})

UVGC_259_466 = Coupling(name = 'UVGC_259_466',
                        value = '-(FRCTdeltaZxttRxgostL*complex(0,1))',
                        order = {'QCD':2})

UVGC_259_467 = Coupling(name = 'UVGC_259_467',
                        value = '-(FRCTdeltaZxttRxgostR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_260_468 = Coupling(name = 'UVGC_260_468',
                        value = '(2*ee*FRCTdeltaZxttRxtG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_260_469 = Coupling(name = 'UVGC_260_469',
                        value = '(2*ee*FRCTdeltaZxttRxgostL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_260_470 = Coupling(name = 'UVGC_260_470',
                        value = '(2*ee*FRCTdeltaZxttRxgostR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_261_471 = Coupling(name = 'UVGC_261_471',
                        value = 'FRCTdeltaZxttLxtG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_261_472 = Coupling(name = 'UVGC_261_472',
                        value = 'FRCTdeltaZxttLxgostL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_261_473 = Coupling(name = 'UVGC_261_473',
                        value = 'FRCTdeltaZxttLxgostR*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_262_474 = Coupling(name = 'UVGC_262_474',
                        value = 'FRCTdeltaZxttRxtG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_262_475 = Coupling(name = 'UVGC_262_475',
                        value = 'FRCTdeltaZxttRxgostL*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_262_476 = Coupling(name = 'UVGC_262_476',
                        value = 'FRCTdeltaZxttRxgostR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_265_477 = Coupling(name = 'UVGC_265_477',
                        value = '-(FRCTdeltaxMTxtG*complex(0,1)) - (FRCTdeltaZxttLxtG*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxtG*complex(0,1)*MT)/2. + (complex(0,1)*G**2*invFREps*MT)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_265_478 = Coupling(name = 'UVGC_265_478',
                        value = '-(FRCTdeltaxMTxgostL*complex(0,1)) - (FRCTdeltaZxttLxgostL*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxgostL*complex(0,1)*MT)/2.',
                        order = {'QCD':2})

UVGC_265_479 = Coupling(name = 'UVGC_265_479',
                        value = '-(FRCTdeltaxMTxgostR*complex(0,1)) - (FRCTdeltaZxttLxgostR*complex(0,1)*MT)/2. - (FRCTdeltaZxttRxgostR*complex(0,1)*MT)/2.',
                        order = {'QCD':2})

UVGC_266_480 = Coupling(name = 'UVGC_266_480',
                        value = '(cw*ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxttLxtG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_266_481 = Coupling(name = 'UVGC_266_481',
                        value = '(cw*ee*FRCTdeltaZxttLxgostL*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxttLxgostL*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_266_482 = Coupling(name = 'UVGC_266_482',
                        value = '(cw*ee*FRCTdeltaZxttLxgostR*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxttLxgostR*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_267_483 = Coupling(name = 'UVGC_267_483',
                        value = '(-2*ee*FRCTdeltaZxttRxtG*complex(0,1)*sw)/(3.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_267_484 = Coupling(name = 'UVGC_267_484',
                        value = '(-2*ee*FRCTdeltaZxttRxgostL*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_267_485 = Coupling(name = 'UVGC_267_485',
                        value = '(-2*ee*FRCTdeltaZxttRxgostR*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_268_486 = Coupling(name = 'UVGC_268_486',
                        value = '-(FRCTdeltaZxttLxtG*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxtG*complex(0,1)*yt)/(2.*cmath.sqrt(2)) + (complex(0,1)*G**2*invFREps*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (FRCTdeltaxMTxtG*complex(0,1)*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_268_487 = Coupling(name = 'UVGC_268_487',
                        value = '-(FRCTdeltaZxttLxgostL*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxgostL*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaxMTxgostL*complex(0,1)*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_268_488 = Coupling(name = 'UVGC_268_488',
                        value = '-(FRCTdeltaZxttLxgostR*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxgostR*complex(0,1)*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaxMTxgostR*complex(0,1)*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_269_489 = Coupling(name = 'UVGC_269_489',
                        value = '-(FRCTdeltaZxttLxtG*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxtG*yt)/(2.*cmath.sqrt(2)) + (G**2*invFREps*yt)/(3.*cmath.pi**2*cmath.sqrt(2)) - (FRCTdeltaxMTxtG*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_269_490 = Coupling(name = 'UVGC_269_490',
                        value = '-(FRCTdeltaZxttLxgostL*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxgostL*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaxMTxgostL*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_269_491 = Coupling(name = 'UVGC_269_491',
                        value = '-(FRCTdeltaZxttLxgostR*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaZxttRxgostR*yt)/(2.*cmath.sqrt(2)) - (FRCTdeltaxMTxgostR*yt)/(MT*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_270_492 = Coupling(name = 'UVGC_270_492',
                        value = '(FRCTdeltaZxstLstRxtgo*complex(0,1))/2. + (FRCTdeltaZxstRstLxtgo*complex(0,1))/2.',
                        order = {'QCD':2})

UVGC_271_493 = Coupling(name = 'UVGC_271_493',
                        value = '-(FRCTdeltaZxstLstRxtgo*complex(0,1)*G)/2. - (FRCTdeltaZxstRstLxtgo*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_272_494 = Coupling(name = 'UVGC_272_494',
                        value = '(FRCTdeltaZxstLstRxtgo*complex(0,1)*G)/2. + (FRCTdeltaZxstRstLxtgo*complex(0,1)*G)/2.',
                        order = {'QCD':3})

UVGC_273_495 = Coupling(name = 'UVGC_273_495',
                        value = '(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/12. - (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/12.',
                        order = {'QCD':4})

UVGC_274_496 = Coupling(name = 'UVGC_274_496',
                        value = '-(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/12. + (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/12.',
                        order = {'QCD':4})

UVGC_275_497 = Coupling(name = 'UVGC_275_497',
                        value = '(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/6. - (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_276_498 = Coupling(name = 'UVGC_276_498',
                        value = '-(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/6. + (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_277_499 = Coupling(name = 'UVGC_277_499',
                        value = '(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/4. - (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/4.',
                        order = {'QCD':4})

UVGC_278_500 = Coupling(name = 'UVGC_278_500',
                        value = '-(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/4. + (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/4.',
                        order = {'QCD':4})

UVGC_279_501 = Coupling(name = 'UVGC_279_501',
                        value = '(FRCTdeltaZxstLstRxtgo*complex(0,1)*G**2)/2. + (FRCTdeltaZxstRstLxtgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_280_502 = Coupling(name = 'UVGC_280_502',
                        value = 'FRCTdeltaZxstLstLxGstL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_280_503 = Coupling(name = 'UVGC_280_503',
                        value = 'FRCTdeltaZxstLstLxtgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_281_504 = Coupling(name = 'UVGC_281_504',
                        value = 'FRCTdeltaZxstRstRxGstR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_281_505 = Coupling(name = 'UVGC_281_505',
                        value = 'FRCTdeltaZxstRstRxtgo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_284_506 = Coupling(name = 'UVGC_284_506',
                        value = '-(FRCTdeltaZxstLstLxGstL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_284_507 = Coupling(name = 'UVGC_284_507',
                        value = '-(FRCTdeltaZxstLstLxtgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_285_508 = Coupling(name = 'UVGC_285_508',
                        value = '-(FRCTdeltaZxstRstRxGstR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_285_509 = Coupling(name = 'UVGC_285_509',
                        value = '-(FRCTdeltaZxstRstRxtgo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_287_510 = Coupling(name = 'UVGC_287_510',
                        value = 'FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_287_511 = Coupling(name = 'UVGC_287_511',
                        value = 'FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_288_512 = Coupling(name = 'UVGC_288_512',
                        value = 'FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_288_513 = Coupling(name = 'UVGC_288_513',
                        value = 'FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_289_514 = Coupling(name = 'UVGC_289_514',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_289_515 = Coupling(name = 'UVGC_289_515',
                        value = '(-2*FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_289_516 = Coupling(name = 'UVGC_289_516',
                        value = '(-2*FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_290_517 = Coupling(name = 'UVGC_290_517',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_290_518 = Coupling(name = 'UVGC_290_518',
                        value = '(-2*FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_290_519 = Coupling(name = 'UVGC_290_519',
                        value = '(-2*FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_291_520 = Coupling(name = 'UVGC_291_520',
                        value = '-(FRCTdeltaxMstLRxtgo*complex(0,1)) - (FRCTdeltaZxstLstRxtgo*complex(0,1)*MstL**2)/2. - (FRCTdeltaZxstRstLxtgo*complex(0,1)*MstR**2)/2. - (complex(0,1)*G**2*invFREps*Mgo*MT)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_292_521 = Coupling(name = 'UVGC_292_521',
                        value = '-2*FRCTdeltaxMstLxstL*complex(0,1)*MstL - (complex(0,1)*G**2*invFREps*MstL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_292_522 = Coupling(name = 'UVGC_292_522',
                        value = '-2*FRCTdeltaxMstLxGstL*complex(0,1)*MstL - FRCTdeltaZxstLstLxGstL*complex(0,1)*MstL**2 + (complex(0,1)*G**2*invFREps*MstL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_292_523 = Coupling(name = 'UVGC_292_523',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMstLxtgo*complex(0,1)*MstL - FRCTdeltaZxstLstLxtgo*complex(0,1)*MstL**2 + (complex(0,1)*G**2*invFREps*MT**2)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_293_524 = Coupling(name = 'UVGC_293_524',
                        value = '-2*FRCTdeltaxMstRxstR*complex(0,1)*MstR - (complex(0,1)*G**2*invFREps*MstR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_293_525 = Coupling(name = 'UVGC_293_525',
                        value = '-2*FRCTdeltaxMstRxGstR*complex(0,1)*MstR - FRCTdeltaZxstRstRxGstR*complex(0,1)*MstR**2 + (complex(0,1)*G**2*invFREps*MstR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_293_526 = Coupling(name = 'UVGC_293_526',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMstRxtgo*complex(0,1)*MstR - FRCTdeltaZxstRstRxtgo*complex(0,1)*MstR**2 + (complex(0,1)*G**2*invFREps*MT**2)/(3.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_302_527 = Coupling(name = 'UVGC_302_527',
                        value = '-(G**2*invFREps*Mgo*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_303_528 = Coupling(name = 'UVGC_303_528',
                        value = '-(complex(0,1)*G**2*invFREps*Mgo*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_304_529 = Coupling(name = 'UVGC_304_529',
                        value = '(G**2*invFREps*Mgo*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_305_530 = Coupling(name = 'UVGC_305_530',
                        value = '(complex(0,1)*G**2*invFREps*MT*yt*cmath.sqrt(2))/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_306_531 = Coupling(name = 'UVGC_306_531',
                        value = '(complex(0,1)*G**2*invFREps*yt**2)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':2})

UVGC_307_532 = Coupling(name = 'UVGC_307_532',
                        value = '-(FRCTdeltaZxuuLxuG*complex(0,1))',
                        order = {'QCD':2})

UVGC_307_533 = Coupling(name = 'UVGC_307_533',
                        value = '-(FRCTdeltaZxuuLxgosuL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_308_534 = Coupling(name = 'UVGC_308_534',
                        value = '(2*ee*FRCTdeltaZxuuLxuG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_308_535 = Coupling(name = 'UVGC_308_535',
                        value = '(2*ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_309_536 = Coupling(name = 'UVGC_309_536',
                        value = '-(FRCTdeltaZxuuRxuG*complex(0,1))',
                        order = {'QCD':2})

UVGC_309_537 = Coupling(name = 'UVGC_309_537',
                        value = '-(FRCTdeltaZxuuRxgosuR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_310_538 = Coupling(name = 'UVGC_310_538',
                        value = '(2*ee*FRCTdeltaZxuuRxuG*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_310_539 = Coupling(name = 'UVGC_310_539',
                        value = '(2*ee*FRCTdeltaZxuuRxgosuR*complex(0,1))/3.',
                        order = {'QCD':2,'QED':1})

UVGC_311_540 = Coupling(name = 'UVGC_311_540',
                        value = 'FRCTdeltaZxuuLxuG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_311_541 = Coupling(name = 'UVGC_311_541',
                        value = 'FRCTdeltaZxuuLxgosuL*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_312_542 = Coupling(name = 'UVGC_312_542',
                        value = 'FRCTdeltaZxuuRxuG*complex(0,1)*G',
                        order = {'QCD':3})

UVGC_312_543 = Coupling(name = 'UVGC_312_543',
                        value = 'FRCTdeltaZxuuRxgosuR*complex(0,1)*G - (complex(0,1)*G**3*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_316_544 = Coupling(name = 'UVGC_316_544',
                        value = '(cw*ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*G**2*invFREps)/(24.*cmath.pi**2*sw) - (ee*FRCTdeltaZxuuLxuG*complex(0,1)*sw)/(6.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(72.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_316_545 = Coupling(name = 'UVGC_316_545',
                        value = '(cw*ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/(2.*sw) - (ee*FRCTdeltaZxuuLxgosuL*complex(0,1)*sw)/(6.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_317_546 = Coupling(name = 'UVGC_317_546',
                        value = '(-2*ee*FRCTdeltaZxuuRxuG*complex(0,1)*sw)/(3.*cw) + (ee*complex(0,1)*G**2*invFREps*sw)/(18.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_317_547 = Coupling(name = 'UVGC_317_547',
                        value = '(-2*ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*sw)/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_318_548 = Coupling(name = 'UVGC_318_548',
                        value = 'FRCTdeltaZxsuLsuLxGsuL*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_318_549 = Coupling(name = 'UVGC_318_549',
                        value = 'FRCTdeltaZxsuLsuLxugo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_319_550 = Coupling(name = 'UVGC_319_550',
                        value = 'FRCTdeltaZxsuRsuRxGsuR*complex(0,1) + (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_319_551 = Coupling(name = 'UVGC_319_551',
                        value = 'FRCTdeltaZxsuRsuRxugo*complex(0,1) - (complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_320_552 = Coupling(name = 'UVGC_320_552',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(9.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_323_553 = Coupling(name = 'UVGC_323_553',
                        value = '-(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_323_554 = Coupling(name = 'UVGC_323_554',
                        value = '-(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_324_555 = Coupling(name = 'UVGC_324_555',
                        value = '-(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_324_556 = Coupling(name = 'UVGC_324_556',
                        value = '-(FRCTdeltaZxsuRsuRxugo*complex(0,1)*G) + (complex(0,1)*G**3*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_326_557 = Coupling(name = 'UVGC_326_557',
                        value = 'FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_326_558 = Coupling(name = 'UVGC_326_558',
                        value = 'FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_327_559 = Coupling(name = 'UVGC_327_559',
                        value = 'FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2 - (13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_327_560 = Coupling(name = 'UVGC_327_560',
                        value = 'FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2 - (complex(0,1)*G**4*invFREps)/(6.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_328_561 = Coupling(name = 'UVGC_328_561',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_328_562 = Coupling(name = 'UVGC_328_562',
                        value = '(-2*FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_328_563 = Coupling(name = 'UVGC_328_563',
                        value = '(-2*FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_329_564 = Coupling(name = 'UVGC_329_564',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(3.*aS) - (7*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_329_565 = Coupling(name = 'UVGC_329_565',
                        value = '(-2*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/3. + (29*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_329_566 = Coupling(name = 'UVGC_329_566',
                        value = '(-2*FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/3. + (11*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_330_567 = Coupling(name = 'UVGC_330_567',
                        value = '-2*FRCTdeltaxMsuLxsuL*complex(0,1)*MsuL - (complex(0,1)*G**2*invFREps*MsuL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_330_568 = Coupling(name = 'UVGC_330_568',
                        value = '-2*FRCTdeltaxMsuLxGsuL*complex(0,1)*MsuL - FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*MsuL**2 + (complex(0,1)*G**2*invFREps*MsuL**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_330_569 = Coupling(name = 'UVGC_330_569',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsuLxugo*complex(0,1)*MsuL - FRCTdeltaZxsuLsuLxugo*complex(0,1)*MsuL**2',
                        order = {'QCD':2})

UVGC_331_570 = Coupling(name = 'UVGC_331_570',
                        value = '-2*FRCTdeltaxMsuRxsuR*complex(0,1)*MsuR - (complex(0,1)*G**2*invFREps*MsuR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_331_571 = Coupling(name = 'UVGC_331_571',
                        value = '-2*FRCTdeltaxMsuRxGsuR*complex(0,1)*MsuR - FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*MsuR**2 + (complex(0,1)*G**2*invFREps*MsuR**2)/(12.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_331_572 = Coupling(name = 'UVGC_331_572',
                        value = '(complex(0,1)*G**2*invFREps*Mgo**2)/(3.*cmath.pi**2) - 2*FRCTdeltaxMsuRxugo*complex(0,1)*MsuR - FRCTdeltaZxsuRsuRxugo*complex(0,1)*MsuR**2',
                        order = {'QCD':2})

UVGC_333_573 = Coupling(name = 'UVGC_333_573',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*invFREps*sw)/(36.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_341_574 = Coupling(name = 'UVGC_341_574',
                        value = '-(FRCTdeltaxMgoxbsbL*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_575 = Coupling(name = 'UVGC_341_575',
                        value = '-(FRCTdeltaxMgoxbsbR*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_576 = Coupling(name = 'UVGC_341_576',
                        value = '-(FRCTdeltaxMgoxcscL*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_577 = Coupling(name = 'UVGC_341_577',
                        value = '-(FRCTdeltaxMgoxcscR*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_578 = Coupling(name = 'UVGC_341_578',
                        value = '-(FRCTdeltaxMgoxdsdL*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_579 = Coupling(name = 'UVGC_341_579',
                        value = '-(FRCTdeltaxMgoxdsdR*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_580 = Coupling(name = 'UVGC_341_580',
                        value = '-(FRCTdeltaxMgoxgoG*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_581 = Coupling(name = 'UVGC_341_581',
                        value = '-(FRCTdeltaxMgoxsssL*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_582 = Coupling(name = 'UVGC_341_582',
                        value = '-(FRCTdeltaxMgoxsssR*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_583 = Coupling(name = 'UVGC_341_583',
                        value = '-(FRCTdeltaxMgoxtstL*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_584 = Coupling(name = 'UVGC_341_584',
                        value = '-(FRCTdeltaxMgoxtstR*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_585 = Coupling(name = 'UVGC_341_585',
                        value = '-(FRCTdeltaxMgoxusuL*complex(0,1))',
                        order = {'QCD':2})

UVGC_341_586 = Coupling(name = 'UVGC_341_586',
                        value = '-(FRCTdeltaxMgoxusuR*complex(0,1))',
                        order = {'QCD':2})

UVGC_342_587 = Coupling(name = 'UVGC_342_587',
                        value = '-(FRCTdeltaZxgogoLxbsbL*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_588 = Coupling(name = 'UVGC_342_588',
                        value = '-(FRCTdeltaZxgogoLxbsbR*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_589 = Coupling(name = 'UVGC_342_589',
                        value = '-(FRCTdeltaZxgogoLxcscL*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_590 = Coupling(name = 'UVGC_342_590',
                        value = '-(FRCTdeltaZxgogoLxcscR*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_591 = Coupling(name = 'UVGC_342_591',
                        value = '-(FRCTdeltaZxgogoLxdsdL*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_592 = Coupling(name = 'UVGC_342_592',
                        value = '-(FRCTdeltaZxgogoLxdsdR*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_593 = Coupling(name = 'UVGC_342_593',
                        value = '-(FRCTdeltaZxgogoLxgoG*complex(0,1)*Mgo) + (3*complex(0,1)*G**2*invFREps*Mgo)/(4.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_342_594 = Coupling(name = 'UVGC_342_594',
                        value = '-(FRCTdeltaZxgogoLxsssL*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_595 = Coupling(name = 'UVGC_342_595',
                        value = '-(FRCTdeltaZxgogoLxsssR*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_596 = Coupling(name = 'UVGC_342_596',
                        value = '-(FRCTdeltaZxgogoLxtstL*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_597 = Coupling(name = 'UVGC_342_597',
                        value = '-(FRCTdeltaZxgogoLxtstR*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_598 = Coupling(name = 'UVGC_342_598',
                        value = '-(FRCTdeltaZxgogoLxusuL*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_342_599 = Coupling(name = 'UVGC_342_599',
                        value = '-(FRCTdeltaZxgogoLxusuR*complex(0,1)*Mgo)',
                        order = {'QCD':2})

UVGC_343_600 = Coupling(name = 'UVGC_343_600',
                        value = '-(FRCTdeltaZxgogoLxbsbL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_601 = Coupling(name = 'UVGC_343_601',
                        value = '-(FRCTdeltaZxgogoLxbsbR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_602 = Coupling(name = 'UVGC_343_602',
                        value = '-(FRCTdeltaZxgogoLxcscL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_603 = Coupling(name = 'UVGC_343_603',
                        value = '-(FRCTdeltaZxgogoLxcscR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_604 = Coupling(name = 'UVGC_343_604',
                        value = '-(FRCTdeltaZxgogoLxdsdL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_605 = Coupling(name = 'UVGC_343_605',
                        value = '-(FRCTdeltaZxgogoLxdsdR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_606 = Coupling(name = 'UVGC_343_606',
                        value = '-(FRCTdeltaZxgogoLxgoG*complex(0,1))',
                        order = {'QCD':2})

UVGC_343_607 = Coupling(name = 'UVGC_343_607',
                        value = '-(FRCTdeltaZxgogoLxsssL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_608 = Coupling(name = 'UVGC_343_608',
                        value = '-(FRCTdeltaZxgogoLxsssR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_609 = Coupling(name = 'UVGC_343_609',
                        value = '-(FRCTdeltaZxgogoLxtstL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_610 = Coupling(name = 'UVGC_343_610',
                        value = '-(FRCTdeltaZxgogoLxtstR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_611 = Coupling(name = 'UVGC_343_611',
                        value = '-(FRCTdeltaZxgogoLxusuL*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_343_612 = Coupling(name = 'UVGC_343_612',
                        value = '-(FRCTdeltaZxgogoLxusuR*complex(0,1)) + (complex(0,1)*G**2*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':2})

UVGC_344_613 = Coupling(name = 'UVGC_344_613',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_344_614 = Coupling(name = 'UVGC_344_614',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_344_615 = Coupling(name = 'UVGC_344_615',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_344_616 = Coupling(name = 'UVGC_344_616',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_345_617 = Coupling(name = 'UVGC_345_617',
                        value = '(FRCTdeltaZxGGxb*G)/2.',
                        order = {'QCD':3})

UVGC_345_618 = Coupling(name = 'UVGC_345_618',
                        value = '(FRCTdeltaZxGGxc*G)/2.',
                        order = {'QCD':3})

UVGC_345_619 = Coupling(name = 'UVGC_345_619',
                        value = '(FRCTdeltaZxGGxd*G)/2.',
                        order = {'QCD':3})

UVGC_345_620 = Coupling(name = 'UVGC_345_620',
                        value = '(FRCTdeltaZxGGxG*G)/2.',
                        order = {'QCD':3})

UVGC_345_621 = Coupling(name = 'UVGC_345_621',
                        value = '(FRCTdeltaZxGGxghG*G)/2.',
                        order = {'QCD':3})

UVGC_345_622 = Coupling(name = 'UVGC_345_622',
                        value = '(FRCTdeltaxaSxgo*G)/(2.*aS) + (FRCTdeltaZxGGxgo*G)/2.',
                        order = {'QCD':3})

UVGC_345_623 = Coupling(name = 'UVGC_345_623',
                        value = '(FRCTdeltaZxGGxs*G)/2.',
                        order = {'QCD':3})

UVGC_345_624 = Coupling(name = 'UVGC_345_624',
                        value = '(FRCTdeltaxaSxsbL*G)/(2.*aS) + (FRCTdeltaZxGGxsbL*G)/2.',
                        order = {'QCD':3})

UVGC_345_625 = Coupling(name = 'UVGC_345_625',
                        value = '(FRCTdeltaxaSxsbR*G)/(2.*aS) + (FRCTdeltaZxGGxsbR*G)/2.',
                        order = {'QCD':3})

UVGC_345_626 = Coupling(name = 'UVGC_345_626',
                        value = '(FRCTdeltaxaSxscL*G)/(2.*aS) + (FRCTdeltaZxGGxscL*G)/2.',
                        order = {'QCD':3})

UVGC_345_627 = Coupling(name = 'UVGC_345_627',
                        value = '(FRCTdeltaxaSxscR*G)/(2.*aS) + (FRCTdeltaZxGGxscR*G)/2.',
                        order = {'QCD':3})

UVGC_345_628 = Coupling(name = 'UVGC_345_628',
                        value = '(FRCTdeltaxaSxsdL*G)/(2.*aS) + (FRCTdeltaZxGGxsdL*G)/2.',
                        order = {'QCD':3})

UVGC_345_629 = Coupling(name = 'UVGC_345_629',
                        value = '(FRCTdeltaxaSxsdR*G)/(2.*aS) + (FRCTdeltaZxGGxsdR*G)/2.',
                        order = {'QCD':3})

UVGC_345_630 = Coupling(name = 'UVGC_345_630',
                        value = '(FRCTdeltaxaSxssL*G)/(2.*aS) + (FRCTdeltaZxGGxssL*G)/2.',
                        order = {'QCD':3})

UVGC_345_631 = Coupling(name = 'UVGC_345_631',
                        value = '(FRCTdeltaxaSxssR*G)/(2.*aS) + (FRCTdeltaZxGGxssR*G)/2.',
                        order = {'QCD':3})

UVGC_345_632 = Coupling(name = 'UVGC_345_632',
                        value = '(FRCTdeltaxaSxstL*G)/(2.*aS) + (FRCTdeltaZxGGxstL*G)/2.',
                        order = {'QCD':3})

UVGC_345_633 = Coupling(name = 'UVGC_345_633',
                        value = '(FRCTdeltaxaSxstR*G)/(2.*aS) + (FRCTdeltaZxGGxstR*G)/2.',
                        order = {'QCD':3})

UVGC_345_634 = Coupling(name = 'UVGC_345_634',
                        value = '(FRCTdeltaxaSxsuL*G)/(2.*aS) + (FRCTdeltaZxGGxsuL*G)/2.',
                        order = {'QCD':3})

UVGC_345_635 = Coupling(name = 'UVGC_345_635',
                        value = '(FRCTdeltaxaSxsuR*G)/(2.*aS) + (FRCTdeltaZxGGxsuR*G)/2.',
                        order = {'QCD':3})

UVGC_345_636 = Coupling(name = 'UVGC_345_636',
                        value = '(FRCTdeltaxaSxt*G)/(2.*aS) + (FRCTdeltaZxGGxt*G)/2.',
                        order = {'QCD':3})

UVGC_345_637 = Coupling(name = 'UVGC_345_637',
                        value = '(FRCTdeltaZxGGxu*G)/2.',
                        order = {'QCD':3})

UVGC_345_638 = Coupling(name = 'UVGC_345_638',
                        value = 'FRCTdeltaZxgogoLxbsbL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_639 = Coupling(name = 'UVGC_345_639',
                        value = 'FRCTdeltaZxgogoLxbsbR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_640 = Coupling(name = 'UVGC_345_640',
                        value = 'FRCTdeltaZxgogoLxcscL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_641 = Coupling(name = 'UVGC_345_641',
                        value = 'FRCTdeltaZxgogoLxcscR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_642 = Coupling(name = 'UVGC_345_642',
                        value = 'FRCTdeltaZxgogoLxdsdL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_643 = Coupling(name = 'UVGC_345_643',
                        value = 'FRCTdeltaZxgogoLxdsdR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_644 = Coupling(name = 'UVGC_345_644',
                        value = 'FRCTdeltaZxgogoLxgoG*G - (3*G**3*invFREps)/(8.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_645 = Coupling(name = 'UVGC_345_645',
                        value = 'FRCTdeltaZxgogoLxsssL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_646 = Coupling(name = 'UVGC_345_646',
                        value = 'FRCTdeltaZxgogoLxsssR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_647 = Coupling(name = 'UVGC_345_647',
                        value = 'FRCTdeltaZxgogoLxtstL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_648 = Coupling(name = 'UVGC_345_648',
                        value = 'FRCTdeltaZxgogoLxtstR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_649 = Coupling(name = 'UVGC_345_649',
                        value = 'FRCTdeltaZxgogoLxusuL*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_345_650 = Coupling(name = 'UVGC_345_650',
                        value = 'FRCTdeltaZxgogoLxusuR*G - (G**3*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':3})

UVGC_346_651 = Coupling(name = 'UVGC_346_651',
                        value = '(cw*ee*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*invFREps*sw)/(192.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_346_652 = Coupling(name = 'UVGC_346_652',
                        value = '(ee*complex(0,1)*G**2*invFREps*sw)/(96.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_346_653 = Coupling(name = 'UVGC_346_653',
                        value = '-(cw*ee*complex(0,1)*G**2*invFREps)/(64.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*invFREps*sw)/(192.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_346_654 = Coupling(name = 'UVGC_346_654',
                        value = '-(ee*complex(0,1)*G**2*invFREps*sw)/(48.*cw*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_347_655 = Coupling(name = 'UVGC_347_655',
                        value = '-(ee*FRCTdeltaZxbbLxbG*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_347_656 = Coupling(name = 'UVGC_347_656',
                        value = '-(ee*FRCTdeltaZxsbLsbLxbgo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_347_657 = Coupling(name = 'UVGC_347_657',
                        value = '-(ee*FRCTdeltaZxsbLsbLxGsbL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_347_658 = Coupling(name = 'UVGC_347_658',
                        value = '-(ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_347_659 = Coupling(name = 'UVGC_347_659',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(36.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_348_660 = Coupling(name = 'UVGC_348_660',
                        value = '-(ee*FRCTdeltaZxbbRxbG*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_348_661 = Coupling(name = 'UVGC_348_661',
                        value = '-(ee*FRCTdeltaZxsbRsbRxbgo*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_348_662 = Coupling(name = 'UVGC_348_662',
                        value = '-(ee*FRCTdeltaZxsbRsbRxGsbR*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_348_663 = Coupling(name = 'UVGC_348_663',
                        value = '-(ee*FRCTdeltaZxbbRxgosbR*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_348_664 = Coupling(name = 'UVGC_348_664',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(18.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_349_665 = Coupling(name = 'UVGC_349_665',
                        value = '-(ee*FRCTdeltaZxccLxcG*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_349_666 = Coupling(name = 'UVGC_349_666',
                        value = '-(ee*FRCTdeltaZxscLscLxcgo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_349_667 = Coupling(name = 'UVGC_349_667',
                        value = '-(ee*FRCTdeltaZxscLscLxGscL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_349_668 = Coupling(name = 'UVGC_349_668',
                        value = '-(ee*FRCTdeltaZxccLxgoscL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_350_669 = Coupling(name = 'UVGC_350_669',
                        value = '(ee*FRCTdeltaZxccRxcG*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_350_670 = Coupling(name = 'UVGC_350_670',
                        value = '(ee*FRCTdeltaZxscRscRxcgo*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_350_671 = Coupling(name = 'UVGC_350_671',
                        value = '(ee*FRCTdeltaZxscRscRxGscR*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_350_672 = Coupling(name = 'UVGC_350_672',
                        value = '(ee*FRCTdeltaZxccRxgoscR*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_350_673 = Coupling(name = 'UVGC_350_673',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(9.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_351_674 = Coupling(name = 'UVGC_351_674',
                        value = '(ee*FRCTdeltaZxccLxcG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_351_675 = Coupling(name = 'UVGC_351_675',
                        value = '(ee*FRCTdeltaZxssLxsG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_351_676 = Coupling(name = 'UVGC_351_676',
                        value = '(ee*FRCTdeltaZxccLxgoscL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_351_677 = Coupling(name = 'UVGC_351_677',
                        value = '(ee*FRCTdeltaZxssLxgossL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_351_678 = Coupling(name = 'UVGC_351_678',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_352_679 = Coupling(name = 'UVGC_352_679',
                        value = '(ee**2*complex(0,1)*G**2*invFREps)/(18.*cw*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_353_680 = Coupling(name = 'UVGC_353_680',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(12.*cmath.pi**2*sw**2)',
                        order = {'QCD':2,'QED':2})

UVGC_354_681 = Coupling(name = 'UVGC_354_681',
                        value = '-(ee*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_355_682 = Coupling(name = 'UVGC_355_682',
                        value = '(ee*complex(0,1)*G**2*invFREps)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_356_683 = Coupling(name = 'UVGC_356_683',
                        value = '-(ee**2*complex(0,1)*G**2*invFREps)/(18.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_357_684 = Coupling(name = 'UVGC_357_684',
                        value = '-(ee*complex(0,1)*G**3*invFREps)/(3.*cmath.pi**2*sw*cmath.sqrt(2))',
                        order = {'QCD':3,'QED':1})

UVGC_358_685 = Coupling(name = 'UVGC_358_685',
                        value = '-(ee*FRCTdeltaZxddLxdG*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_358_686 = Coupling(name = 'UVGC_358_686',
                        value = '-(ee*FRCTdeltaZxsdLsdLxdgo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_358_687 = Coupling(name = 'UVGC_358_687',
                        value = '-(ee*FRCTdeltaZxsdLsdLxGsdL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_358_688 = Coupling(name = 'UVGC_358_688',
                        value = '-(ee*FRCTdeltaZxddLxgosdL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_359_689 = Coupling(name = 'UVGC_359_689',
                        value = '-(ee*FRCTdeltaZxddRxdG*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_359_690 = Coupling(name = 'UVGC_359_690',
                        value = '-(ee*FRCTdeltaZxsdRsdRxdgo*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_359_691 = Coupling(name = 'UVGC_359_691',
                        value = '-(ee*FRCTdeltaZxsdRsdRxGsdR*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_359_692 = Coupling(name = 'UVGC_359_692',
                        value = '-(ee*FRCTdeltaZxddRxgosdR*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_360_693 = Coupling(name = 'UVGC_360_693',
                        value = '(-11*complex(0,1)*G**4*invFREps)/(144.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_694 = Coupling(name = 'UVGC_360_694',
                        value = '-(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(6.*aS)',
                        order = {'QCD':4})

UVGC_360_695 = Coupling(name = 'UVGC_360_695',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_696 = Coupling(name = 'UVGC_360_696',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_697 = Coupling(name = 'UVGC_360_697',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_698 = Coupling(name = 'UVGC_360_698',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_699 = Coupling(name = 'UVGC_360_699',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_700 = Coupling(name = 'UVGC_360_700',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_701 = Coupling(name = 'UVGC_360_701',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_702 = Coupling(name = 'UVGC_360_702',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_703 = Coupling(name = 'UVGC_360_703',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_704 = Coupling(name = 'UVGC_360_704',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_705 = Coupling(name = 'UVGC_360_705',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_706 = Coupling(name = 'UVGC_360_706',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_707 = Coupling(name = 'UVGC_360_707',
                        value = '-(FRCTdeltaxaSxt*complex(0,1)*G**2)/(6.*aS)',
                        order = {'QCD':4})

UVGC_360_708 = Coupling(name = 'UVGC_360_708',
                        value = '-(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/6. - (FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_709 = Coupling(name = 'UVGC_360_709',
                        value = '-(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_710 = Coupling(name = 'UVGC_360_710',
                        value = '-(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_711 = Coupling(name = 'UVGC_360_711',
                        value = '(-11*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_360_712 = Coupling(name = 'UVGC_360_712',
                        value = '(7*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_713 = Coupling(name = 'UVGC_361_713',
                        value = '(-5*complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_714 = Coupling(name = 'UVGC_361_714',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(2.*aS)',
                        order = {'QCD':4})

UVGC_361_715 = Coupling(name = 'UVGC_361_715',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_716 = Coupling(name = 'UVGC_361_716',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_717 = Coupling(name = 'UVGC_361_717',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_718 = Coupling(name = 'UVGC_361_718',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_719 = Coupling(name = 'UVGC_361_719',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_720 = Coupling(name = 'UVGC_361_720',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_721 = Coupling(name = 'UVGC_361_721',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_722 = Coupling(name = 'UVGC_361_722',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_723 = Coupling(name = 'UVGC_361_723',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_724 = Coupling(name = 'UVGC_361_724',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_725 = Coupling(name = 'UVGC_361_725',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_726 = Coupling(name = 'UVGC_361_726',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_727 = Coupling(name = 'UVGC_361_727',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/(2.*aS)',
                        order = {'QCD':4})

UVGC_361_728 = Coupling(name = 'UVGC_361_728',
                        value = '(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/2. + (FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/2. - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_729 = Coupling(name = 'UVGC_361_729',
                        value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_730 = Coupling(name = 'UVGC_361_730',
                        value = '(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_731 = Coupling(name = 'UVGC_361_731',
                        value = '(-5*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_361_732 = Coupling(name = 'UVGC_361_732',
                        value = '(-23*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_362_733 = Coupling(name = 'UVGC_362_733',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_362_734 = Coupling(name = 'UVGC_362_734',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_362_735 = Coupling(name = 'UVGC_362_735',
                        value = '-(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_362_736 = Coupling(name = 'UVGC_362_736',
                        value = '-(FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_362_737 = Coupling(name = 'UVGC_362_737',
                        value = '-(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_362_738 = Coupling(name = 'UVGC_362_738',
                        value = '(5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_363_739 = Coupling(name = 'UVGC_363_739',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_363_740 = Coupling(name = 'UVGC_363_740',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_363_741 = Coupling(name = 'UVGC_363_741',
                        value = '(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_363_742 = Coupling(name = 'UVGC_363_742',
                        value = '(FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_363_743 = Coupling(name = 'UVGC_363_743',
                        value = '(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_363_744 = Coupling(name = 'UVGC_363_744',
                        value = '-(complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_745 = Coupling(name = 'UVGC_364_745',
                        value = '(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(6.*aS)',
                        order = {'QCD':4})

UVGC_364_746 = Coupling(name = 'UVGC_364_746',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_747 = Coupling(name = 'UVGC_364_747',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_748 = Coupling(name = 'UVGC_364_748',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_749 = Coupling(name = 'UVGC_364_749',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_750 = Coupling(name = 'UVGC_364_750',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_751 = Coupling(name = 'UVGC_364_751',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_752 = Coupling(name = 'UVGC_364_752',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_753 = Coupling(name = 'UVGC_364_753',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_754 = Coupling(name = 'UVGC_364_754',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_755 = Coupling(name = 'UVGC_364_755',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_756 = Coupling(name = 'UVGC_364_756',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_757 = Coupling(name = 'UVGC_364_757',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_758 = Coupling(name = 'UVGC_364_758',
                        value = '(FRCTdeltaxaSxt*complex(0,1)*G**2)/(6.*aS)',
                        order = {'QCD':4})

UVGC_364_759 = Coupling(name = 'UVGC_364_759',
                        value = '(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_364_760 = Coupling(name = 'UVGC_364_760',
                        value = '(FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_364_761 = Coupling(name = 'UVGC_364_761',
                        value = '(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_762 = Coupling(name = 'UVGC_364_762',
                        value = '(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_763 = Coupling(name = 'UVGC_364_763',
                        value = '(complex(0,1)*G**4*invFREps)/(72.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_364_764 = Coupling(name = 'UVGC_364_764',
                        value = '(-29*complex(0,1)*G**4*invFREps)/(576.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_765 = Coupling(name = 'UVGC_365_765',
                        value = '-(FRCTdeltaxaSxgo*complex(0,1)*G**2)/(2.*aS)',
                        order = {'QCD':4})

UVGC_365_766 = Coupling(name = 'UVGC_365_766',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_767 = Coupling(name = 'UVGC_365_767',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_768 = Coupling(name = 'UVGC_365_768',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_769 = Coupling(name = 'UVGC_365_769',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_770 = Coupling(name = 'UVGC_365_770',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_771 = Coupling(name = 'UVGC_365_771',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_772 = Coupling(name = 'UVGC_365_772',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_773 = Coupling(name = 'UVGC_365_773',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_774 = Coupling(name = 'UVGC_365_774',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_775 = Coupling(name = 'UVGC_365_775',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_776 = Coupling(name = 'UVGC_365_776',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_777 = Coupling(name = 'UVGC_365_777',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_778 = Coupling(name = 'UVGC_365_778',
                        value = '-(FRCTdeltaxaSxt*complex(0,1)*G**2)/(2.*aS)',
                        order = {'QCD':4})

UVGC_365_779 = Coupling(name = 'UVGC_365_779',
                        value = '-(FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_365_780 = Coupling(name = 'UVGC_365_780',
                        value = '-(FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_365_781 = Coupling(name = 'UVGC_365_781',
                        value = '-(FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_782 = Coupling(name = 'UVGC_365_782',
                        value = '-(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_783 = Coupling(name = 'UVGC_365_783',
                        value = '(7*complex(0,1)*G**4*invFREps)/(24.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_365_784 = Coupling(name = 'UVGC_365_784',
                        value = '(13*complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_366_785 = Coupling(name = 'UVGC_366_785',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_366_786 = Coupling(name = 'UVGC_366_786',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_366_787 = Coupling(name = 'UVGC_366_787',
                        value = '-(FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_366_788 = Coupling(name = 'UVGC_366_788',
                        value = '-(FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_366_789 = Coupling(name = 'UVGC_366_789',
                        value = '-(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_367_790 = Coupling(name = 'UVGC_367_790',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_367_791 = Coupling(name = 'UVGC_367_791',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_367_792 = Coupling(name = 'UVGC_367_792',
                        value = '(FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_367_793 = Coupling(name = 'UVGC_367_793',
                        value = '(FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_367_794 = Coupling(name = 'UVGC_367_794',
                        value = '(FRCTdeltaZxscLscLxGscL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_368_795 = Coupling(name = 'UVGC_368_795',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_368_796 = Coupling(name = 'UVGC_368_796',
                        value = '-(FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/6. - (FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_368_797 = Coupling(name = 'UVGC_368_797',
                        value = '-(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_369_798 = Coupling(name = 'UVGC_369_798',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_369_799 = Coupling(name = 'UVGC_369_799',
                        value = '(FRCTdeltaZxscLscLxcgo*complex(0,1)*G**2)/2. + (FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/2. - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_369_800 = Coupling(name = 'UVGC_369_800',
                        value = '(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_370_801 = Coupling(name = 'UVGC_370_801',
                        value = '(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_370_802 = Coupling(name = 'UVGC_370_802',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_370_803 = Coupling(name = 'UVGC_370_803',
                        value = '(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_370_804 = Coupling(name = 'UVGC_370_804',
                        value = '(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_371_805 = Coupling(name = 'UVGC_371_805',
                        value = '-(FRCTdeltaxaSxsbL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_371_806 = Coupling(name = 'UVGC_371_806',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_371_807 = Coupling(name = 'UVGC_371_807',
                        value = '-(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_371_808 = Coupling(name = 'UVGC_371_808',
                        value = '-(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_374_809 = Coupling(name = 'UVGC_374_809',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_374_810 = Coupling(name = 'UVGC_374_810',
                        value = '(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_374_811 = Coupling(name = 'UVGC_374_811',
                        value = '(FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_375_812 = Coupling(name = 'UVGC_375_812',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_375_813 = Coupling(name = 'UVGC_375_813',
                        value = '-(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_375_814 = Coupling(name = 'UVGC_375_814',
                        value = '-(FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_376_815 = Coupling(name = 'UVGC_376_815',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_376_816 = Coupling(name = 'UVGC_376_816',
                        value = '-(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_376_817 = Coupling(name = 'UVGC_376_817',
                        value = '-(FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_377_818 = Coupling(name = 'UVGC_377_818',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_377_819 = Coupling(name = 'UVGC_377_819',
                        value = '(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_377_820 = Coupling(name = 'UVGC_377_820',
                        value = '(FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_378_821 = Coupling(name = 'UVGC_378_821',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_378_822 = Coupling(name = 'UVGC_378_822',
                        value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_378_823 = Coupling(name = 'UVGC_378_823',
                        value = '(FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_379_824 = Coupling(name = 'UVGC_379_824',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_379_825 = Coupling(name = 'UVGC_379_825',
                        value = '-(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_379_826 = Coupling(name = 'UVGC_379_826',
                        value = '-(FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_380_827 = Coupling(name = 'UVGC_380_827',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_380_828 = Coupling(name = 'UVGC_380_828',
                        value = '-(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_380_829 = Coupling(name = 'UVGC_380_829',
                        value = '-(FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_381_830 = Coupling(name = 'UVGC_381_830',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_381_831 = Coupling(name = 'UVGC_381_831',
                        value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_381_832 = Coupling(name = 'UVGC_381_832',
                        value = '(FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_382_833 = Coupling(name = 'UVGC_382_833',
                        value = '-(FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_383_834 = Coupling(name = 'UVGC_383_834',
                        value = '(FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_384_835 = Coupling(name = 'UVGC_384_835',
                        value = '(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_384_836 = Coupling(name = 'UVGC_384_836',
                        value = '(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(192.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_384_837 = Coupling(name = 'UVGC_384_837',
                        value = '(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_384_838 = Coupling(name = 'UVGC_384_838',
                        value = '(FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_384_839 = Coupling(name = 'UVGC_384_839',
                        value = '(FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_384_840 = Coupling(name = 'UVGC_384_840',
                        value = '(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_384_841 = Coupling(name = 'UVGC_384_841',
                        value = '(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_385_842 = Coupling(name = 'UVGC_385_842',
                        value = '-(FRCTdeltaxaSxsbR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_385_843 = Coupling(name = 'UVGC_385_843',
                        value = '-(FRCTdeltaxaSxscL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(64.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_385_844 = Coupling(name = 'UVGC_385_844',
                        value = '-(FRCTdeltaxaSxscR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_385_845 = Coupling(name = 'UVGC_385_845',
                        value = '-(FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_385_846 = Coupling(name = 'UVGC_385_846',
                        value = '-(FRCTdeltaZxscRscRxcgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_385_847 = Coupling(name = 'UVGC_385_847',
                        value = '-(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_385_848 = Coupling(name = 'UVGC_385_848',
                        value = '-(FRCTdeltaZxscRscRxGscR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_386_849 = Coupling(name = 'UVGC_386_849',
                        value = '-(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_386_850 = Coupling(name = 'UVGC_386_850',
                        value = '-(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_386_851 = Coupling(name = 'UVGC_386_851',
                        value = '-(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_387_852 = Coupling(name = 'UVGC_387_852',
                        value = '(FRCTdeltaxaSxsdL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_387_853 = Coupling(name = 'UVGC_387_853',
                        value = '(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_387_854 = Coupling(name = 'UVGC_387_854',
                        value = '(FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_388_855 = Coupling(name = 'UVGC_388_855',
                        value = '(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_388_856 = Coupling(name = 'UVGC_388_856',
                        value = '(FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_388_857 = Coupling(name = 'UVGC_388_857',
                        value = '(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_389_858 = Coupling(name = 'UVGC_389_858',
                        value = '-(FRCTdeltaxaSxsdR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_389_859 = Coupling(name = 'UVGC_389_859',
                        value = '-(FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_389_860 = Coupling(name = 'UVGC_389_860',
                        value = '-(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_390_861 = Coupling(name = 'UVGC_390_861',
                        value = '-(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_390_862 = Coupling(name = 'UVGC_390_862',
                        value = '-(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_390_863 = Coupling(name = 'UVGC_390_863',
                        value = '-(FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_391_864 = Coupling(name = 'UVGC_391_864',
                        value = '(FRCTdeltaxaSxssL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_391_865 = Coupling(name = 'UVGC_391_865',
                        value = '(FRCTdeltaZxssLssLxGssL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_391_866 = Coupling(name = 'UVGC_391_866',
                        value = '(FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_392_867 = Coupling(name = 'UVGC_392_867',
                        value = '(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_392_868 = Coupling(name = 'UVGC_392_868',
                        value = '(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_392_869 = Coupling(name = 'UVGC_392_869',
                        value = '(FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_393_870 = Coupling(name = 'UVGC_393_870',
                        value = '-(FRCTdeltaxaSxssR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_393_871 = Coupling(name = 'UVGC_393_871',
                        value = '-(FRCTdeltaZxssRssRxGssR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_393_872 = Coupling(name = 'UVGC_393_872',
                        value = '-(FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_394_873 = Coupling(name = 'UVGC_394_873',
                        value = '(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_394_874 = Coupling(name = 'UVGC_394_874',
                        value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_394_875 = Coupling(name = 'UVGC_394_875',
                        value = '(FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_395_876 = Coupling(name = 'UVGC_395_876',
                        value = '-(FRCTdeltaxaSxstR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_395_877 = Coupling(name = 'UVGC_395_877',
                        value = '-(FRCTdeltaZxstRstRxGstR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_395_878 = Coupling(name = 'UVGC_395_878',
                        value = '-(FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_400_879 = Coupling(name = 'UVGC_400_879',
                        value = '-(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/6. - (FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_401_880 = Coupling(name = 'UVGC_401_880',
                        value = '(FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G**2)/2. + (FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G**2)/2. - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_412_881 = Coupling(name = 'UVGC_412_881',
                        value = '-(FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/6. - (FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_413_882 = Coupling(name = 'UVGC_413_882',
                        value = '(FRCTdeltaZxssLssLxsgo*complex(0,1)*G**2)/2. + (FRCTdeltaZxssRssRxsgo*complex(0,1)*G**2)/2. - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_883 = Coupling(name = 'UVGC_414_883',
                        value = '-(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_884 = Coupling(name = 'UVGC_414_884',
                        value = '-(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_414_885 = Coupling(name = 'UVGC_414_885',
                        value = '-(FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_415_886 = Coupling(name = 'UVGC_415_886',
                        value = '(FRCTdeltaxaSxstL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_415_887 = Coupling(name = 'UVGC_415_887',
                        value = '(FRCTdeltaZxstLstLxGstL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_415_888 = Coupling(name = 'UVGC_415_888',
                        value = '(FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_416_889 = Coupling(name = 'UVGC_416_889',
                        value = '-(FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/6. - (FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_417_890 = Coupling(name = 'UVGC_417_890',
                        value = '(FRCTdeltaZxstLstLxtgo*complex(0,1)*G**2)/2. + (FRCTdeltaZxstRstRxtgo*complex(0,1)*G**2)/2. - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_418_891 = Coupling(name = 'UVGC_418_891',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_418_892 = Coupling(name = 'UVGC_418_892',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS) - (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_418_893 = Coupling(name = 'UVGC_418_893',
                        value = '-(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_418_894 = Coupling(name = 'UVGC_418_894',
                        value = '-(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_418_895 = Coupling(name = 'UVGC_418_895',
                        value = '-(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/6. - (FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/6. + (5*complex(0,1)*G**4*invFREps)/(36.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_896 = Coupling(name = 'UVGC_419_896',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_897 = Coupling(name = 'UVGC_419_897',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS) + (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_898 = Coupling(name = 'UVGC_419_898',
                        value = '(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_899 = Coupling(name = 'UVGC_419_899',
                        value = '(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(32.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_419_900 = Coupling(name = 'UVGC_419_900',
                        value = '(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/2. + (FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/2. - (complex(0,1)*G**4*invFREps)/(12.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_428_901 = Coupling(name = 'UVGC_428_901',
                        value = '-(ee*FRCTdeltaZxssLxsG*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_428_902 = Coupling(name = 'UVGC_428_902',
                        value = '-(ee*FRCTdeltaZxssLssLxGssL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_428_903 = Coupling(name = 'UVGC_428_903',
                        value = '-(ee*FRCTdeltaZxssLssLxsgo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_428_904 = Coupling(name = 'UVGC_428_904',
                        value = '-(ee*FRCTdeltaZxssLxgossL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_429_905 = Coupling(name = 'UVGC_429_905',
                        value = '-(ee*FRCTdeltaZxssRxsG*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_429_906 = Coupling(name = 'UVGC_429_906',
                        value = '-(ee*FRCTdeltaZxssRssRxGssR*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_429_907 = Coupling(name = 'UVGC_429_907',
                        value = '-(ee*FRCTdeltaZxssRssRxsgo*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_429_908 = Coupling(name = 'UVGC_429_908',
                        value = '-(ee*FRCTdeltaZxssRxgossR*complex(0,1))/(3.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_430_909 = Coupling(name = 'UVGC_430_909',
                        value = '(ee*FRCTdeltaZxbbLxbG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_430_910 = Coupling(name = 'UVGC_430_910',
                        value = '(ee*FRCTdeltaZxttLxtG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_430_911 = Coupling(name = 'UVGC_430_911',
                        value = '(ee*FRCTdeltaZxbbLxgosbL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_430_912 = Coupling(name = 'UVGC_430_912',
                        value = '(ee*FRCTdeltaZxttLxgostL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_430_913 = Coupling(name = 'UVGC_430_913',
                        value = '(ee*FRCTdeltaZxttLxgostR*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_431_914 = Coupling(name = 'UVGC_431_914',
                        value = '(FRCTdeltaZxbbLxbG*yt)/2.',
                        order = {'QCD':2,'QED':1})

UVGC_431_915 = Coupling(name = 'UVGC_431_915',
                        value = '(FRCTdeltaZxttRxtG*yt)/2. + (FRCTdeltaxMTxtG*yt)/MT',
                        order = {'QCD':2,'QED':1})

UVGC_431_916 = Coupling(name = 'UVGC_431_916',
                        value = '(FRCTdeltaZxbbLxgosbL*yt)/2.',
                        order = {'QCD':2,'QED':1})

UVGC_431_917 = Coupling(name = 'UVGC_431_917',
                        value = '(FRCTdeltaZxttRxgostL*yt)/2. + (FRCTdeltaxMTxgostL*yt)/MT',
                        order = {'QCD':2,'QED':1})

UVGC_431_918 = Coupling(name = 'UVGC_431_918',
                        value = '(FRCTdeltaZxttRxgostR*yt)/2. + (FRCTdeltaxMTxgostR*yt)/MT',
                        order = {'QCD':2,'QED':1})

UVGC_431_919 = Coupling(name = 'UVGC_431_919',
                        value = '-(G**2*invFREps*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_432_920 = Coupling(name = 'UVGC_432_920',
                        value = '-(FRCTdeltaZxbbLxbG*yt)/2.',
                        order = {'QCD':2,'QED':1})

UVGC_432_921 = Coupling(name = 'UVGC_432_921',
                        value = '-(FRCTdeltaZxttRxtG*yt)/2. - (FRCTdeltaxMTxtG*yt)/MT',
                        order = {'QCD':2,'QED':1})

UVGC_432_922 = Coupling(name = 'UVGC_432_922',
                        value = '-(FRCTdeltaZxbbLxgosbL*yt)/2.',
                        order = {'QCD':2,'QED':1})

UVGC_432_923 = Coupling(name = 'UVGC_432_923',
                        value = '-(FRCTdeltaZxttRxgostL*yt)/2. - (FRCTdeltaxMTxgostL*yt)/MT',
                        order = {'QCD':2,'QED':1})

UVGC_432_924 = Coupling(name = 'UVGC_432_924',
                        value = '-(FRCTdeltaZxttRxgostR*yt)/2. - (FRCTdeltaxMTxgostR*yt)/MT',
                        order = {'QCD':2,'QED':1})

UVGC_432_925 = Coupling(name = 'UVGC_432_925',
                        value = '(G**2*invFREps*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_447_926 = Coupling(name = 'UVGC_447_926',
                        value = '-(G**2*invFREps*Mgo*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_448_927 = Coupling(name = 'UVGC_448_927',
                        value = '(G**2*invFREps*Mgo*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_449_928 = Coupling(name = 'UVGC_449_928',
                        value = '-(G**2*invFREps*MT*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_450_929 = Coupling(name = 'UVGC_450_929',
                        value = '(G**2*invFREps*MT*yt)/(3.*cmath.pi**2)',
                        order = {'QCD':2,'QED':1})

UVGC_452_930 = Coupling(name = 'UVGC_452_930',
                        value = '-(G**2*invFREps*yt**2)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_453_931 = Coupling(name = 'UVGC_453_931',
                        value = '(complex(0,1)*G**2*invFREps*yt**2)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_454_932 = Coupling(name = 'UVGC_454_932',
                        value = '(G**2*invFREps*yt**2)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':2})

UVGC_463_933 = Coupling(name = 'UVGC_463_933',
                        value = '-(ee*FRCTdeltaZxstLstLxGstL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_463_934 = Coupling(name = 'UVGC_463_934',
                        value = '-(ee*FRCTdeltaZxttLxtG*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_463_935 = Coupling(name = 'UVGC_463_935',
                        value = '-(ee*FRCTdeltaZxttLxgostL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_463_936 = Coupling(name = 'UVGC_463_936',
                        value = '-(ee*FRCTdeltaZxttLxgostR*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_463_937 = Coupling(name = 'UVGC_463_937',
                        value = '-(ee*FRCTdeltaZxstLstLxtgo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_464_938 = Coupling(name = 'UVGC_464_938',
                        value = '(ee*FRCTdeltaZxstRstRxGstR*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_464_939 = Coupling(name = 'UVGC_464_939',
                        value = '(ee*FRCTdeltaZxttRxtG*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_464_940 = Coupling(name = 'UVGC_464_940',
                        value = '(ee*FRCTdeltaZxttRxgostL*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_464_941 = Coupling(name = 'UVGC_464_941',
                        value = '(ee*FRCTdeltaZxttRxgostR*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_464_942 = Coupling(name = 'UVGC_464_942',
                        value = '(ee*FRCTdeltaZxstRstRxtgo*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_473_943 = Coupling(name = 'UVGC_473_943',
                        value = '(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_473_944 = Coupling(name = 'UVGC_473_944',
                        value = '(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_473_945 = Coupling(name = 'UVGC_473_945',
                        value = '(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_474_946 = Coupling(name = 'UVGC_474_946',
                        value = '(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(6.*aS) + (complex(0,1)*G**4*invFREps)/(288.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_474_947 = Coupling(name = 'UVGC_474_947',
                        value = '(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/6. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_474_948 = Coupling(name = 'UVGC_474_948',
                        value = '(FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_475_949 = Coupling(name = 'UVGC_475_949',
                        value = '(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_476_950 = Coupling(name = 'UVGC_476_950',
                        value = '(FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_477_951 = Coupling(name = 'UVGC_477_951',
                        value = '-(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_478_952 = Coupling(name = 'UVGC_478_952',
                        value = '-(FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/6.',
                        order = {'QCD':4})

UVGC_479_953 = Coupling(name = 'UVGC_479_953',
                        value = '-(FRCTdeltaxaSxsuL*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_479_954 = Coupling(name = 'UVGC_479_954',
                        value = '-(FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_479_955 = Coupling(name = 'UVGC_479_955',
                        value = '-(FRCTdeltaZxsuLsuLxugo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_480_956 = Coupling(name = 'UVGC_480_956',
                        value = '-(FRCTdeltaxaSxsuR*complex(0,1)*G**2)/(2.*aS) - (complex(0,1)*G**4*invFREps)/(96.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_480_957 = Coupling(name = 'UVGC_480_957',
                        value = '-(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G**2)/2. + (complex(0,1)*G**4*invFREps)/(48.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_480_958 = Coupling(name = 'UVGC_480_958',
                        value = '-(FRCTdeltaZxsuRsuRxugo*complex(0,1)*G**2)/2.',
                        order = {'QCD':4})

UVGC_489_959 = Coupling(name = 'UVGC_489_959',
                        value = '(ee*FRCTdeltaZxddLxdG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_489_960 = Coupling(name = 'UVGC_489_960',
                        value = '(ee*FRCTdeltaZxuuLxuG*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_489_961 = Coupling(name = 'UVGC_489_961',
                        value = '(ee*FRCTdeltaZxddLxgosdL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_489_962 = Coupling(name = 'UVGC_489_962',
                        value = '(ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/(2.*sw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_504_963 = Coupling(name = 'UVGC_504_963',
                        value = '-(ee*FRCTdeltaZxsuLsuLxGsuL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_504_964 = Coupling(name = 'UVGC_504_964',
                        value = '-(ee*FRCTdeltaZxuuLxuG*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_504_965 = Coupling(name = 'UVGC_504_965',
                        value = '-(ee*FRCTdeltaZxuuLxgosuL*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_504_966 = Coupling(name = 'UVGC_504_966',
                        value = '-(ee*FRCTdeltaZxsuLsuLxugo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_505_967 = Coupling(name = 'UVGC_505_967',
                        value = '(ee*FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_505_968 = Coupling(name = 'UVGC_505_968',
                        value = '(ee*FRCTdeltaZxuuRxuG*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_505_969 = Coupling(name = 'UVGC_505_969',
                        value = '(ee*FRCTdeltaZxuuRxgosuR*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_505_970 = Coupling(name = 'UVGC_505_970',
                        value = '(ee*FRCTdeltaZxsuRsuRxugo*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_525_971 = Coupling(name = 'UVGC_525_971',
                        value = '-((FRCTdeltaxaSxgo*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_972 = Coupling(name = 'UVGC_525_972',
                        value = '-((FRCTdeltaxaSxsbL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_973 = Coupling(name = 'UVGC_525_973',
                        value = '-((FRCTdeltaxaSxsbR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_974 = Coupling(name = 'UVGC_525_974',
                        value = '-((FRCTdeltaxaSxscL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_975 = Coupling(name = 'UVGC_525_975',
                        value = '-((FRCTdeltaxaSxscR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_976 = Coupling(name = 'UVGC_525_976',
                        value = '-((FRCTdeltaxaSxsdL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_977 = Coupling(name = 'UVGC_525_977',
                        value = '-((FRCTdeltaxaSxsdR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_978 = Coupling(name = 'UVGC_525_978',
                        value = '-((FRCTdeltaxaSxssL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_979 = Coupling(name = 'UVGC_525_979',
                        value = '-((FRCTdeltaxaSxssR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_980 = Coupling(name = 'UVGC_525_980',
                        value = '-((FRCTdeltaxaSxstL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_981 = Coupling(name = 'UVGC_525_981',
                        value = '-((FRCTdeltaxaSxstR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_982 = Coupling(name = 'UVGC_525_982',
                        value = '-((FRCTdeltaxaSxsuL*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_983 = Coupling(name = 'UVGC_525_983',
                        value = '-((FRCTdeltaxaSxsuR*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_984 = Coupling(name = 'UVGC_525_984',
                        value = '-((FRCTdeltaxaSxt*complex(0,1)*G)/(aS*cmath.sqrt(2)))',
                        order = {'QCD':3})

UVGC_525_985 = Coupling(name = 'UVGC_525_985',
                        value = '-((FRCTdeltaZxbbLxbG*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_986 = Coupling(name = 'UVGC_525_986',
                        value = '-((FRCTdeltaZxsbLsbLxbgo*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_987 = Coupling(name = 'UVGC_525_987',
                        value = '-((FRCTdeltaZxgogoLxbsbL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_988 = Coupling(name = 'UVGC_525_988',
                        value = '-((FRCTdeltaZxgogoLxbsbR*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_989 = Coupling(name = 'UVGC_525_989',
                        value = '-((FRCTdeltaZxgogoLxcscL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_990 = Coupling(name = 'UVGC_525_990',
                        value = '-((FRCTdeltaZxgogoLxcscR*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_991 = Coupling(name = 'UVGC_525_991',
                        value = '-((FRCTdeltaZxgogoLxdsdL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_992 = Coupling(name = 'UVGC_525_992',
                        value = '-((FRCTdeltaZxgogoLxdsdR*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_993 = Coupling(name = 'UVGC_525_993',
                        value = '-((FRCTdeltaZxgogoLxgoG*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_994 = Coupling(name = 'UVGC_525_994',
                        value = '-((FRCTdeltaZxsbLsbLxGsbL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_995 = Coupling(name = 'UVGC_525_995',
                        value = '-((FRCTdeltaZxbbLxgosbL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_996 = Coupling(name = 'UVGC_525_996',
                        value = '-((FRCTdeltaZxgogoLxsssL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_997 = Coupling(name = 'UVGC_525_997',
                        value = '-((FRCTdeltaZxgogoLxsssR*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_998 = Coupling(name = 'UVGC_525_998',
                        value = '-((FRCTdeltaZxgogoLxtstL*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_999 = Coupling(name = 'UVGC_525_999',
                        value = '-((FRCTdeltaZxgogoLxtstR*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_525_1000 = Coupling(name = 'UVGC_525_1000',
                         value = '-((FRCTdeltaZxgogoLxusuL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_525_1001 = Coupling(name = 'UVGC_525_1001',
                         value = '-((FRCTdeltaZxgogoLxusuR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_525_1002 = Coupling(name = 'UVGC_525_1002',
                         value = '(3*complex(0,1)*G**3*invFREps)/(4.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_525_1003 = Coupling(name = 'UVGC_525_1003',
                         value = '-(complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_525_1004 = Coupling(name = 'UVGC_525_1004',
                         value = '(3*complex(0,1)*G**3*invFREps)/(16.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_526_1005 = Coupling(name = 'UVGC_526_1005',
                         value = '-((FRCTdeltaZxccLxcG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_526_1006 = Coupling(name = 'UVGC_526_1006',
                         value = '-((FRCTdeltaZxscLscLxcgo*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_526_1007 = Coupling(name = 'UVGC_526_1007',
                         value = '-((FRCTdeltaZxscLscLxGscL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_526_1008 = Coupling(name = 'UVGC_526_1008',
                         value = '-((FRCTdeltaZxccLxgoscL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_527_1009 = Coupling(name = 'UVGC_527_1009',
                         value = '-((FRCTdeltaZxddLxdG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_527_1010 = Coupling(name = 'UVGC_527_1010',
                         value = '-((FRCTdeltaZxsdLsdLxdgo*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_527_1011 = Coupling(name = 'UVGC_527_1011',
                         value = '-((FRCTdeltaZxsdLsdLxGsdL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_527_1012 = Coupling(name = 'UVGC_527_1012',
                         value = '-((FRCTdeltaZxddLxgosdL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_528_1013 = Coupling(name = 'UVGC_528_1013',
                         value = '-((FRCTdeltaZxssLxsG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_528_1014 = Coupling(name = 'UVGC_528_1014',
                         value = '-((FRCTdeltaZxssLssLxGssL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_528_1015 = Coupling(name = 'UVGC_528_1015',
                         value = '-((FRCTdeltaZxssLssLxsgo*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_528_1016 = Coupling(name = 'UVGC_528_1016',
                         value = '-((FRCTdeltaZxssLxgossL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_529_1017 = Coupling(name = 'UVGC_529_1017',
                         value = '-((FRCTdeltaZxstLstLxGstL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_529_1018 = Coupling(name = 'UVGC_529_1018',
                         value = '-((FRCTdeltaZxttLxtG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_529_1019 = Coupling(name = 'UVGC_529_1019',
                         value = '-((FRCTdeltaZxttLxgostL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_529_1020 = Coupling(name = 'UVGC_529_1020',
                         value = '-((FRCTdeltaZxttLxgostR*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_529_1021 = Coupling(name = 'UVGC_529_1021',
                         value = '-((FRCTdeltaZxstLstLxtgo*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_530_1022 = Coupling(name = 'UVGC_530_1022',
                         value = '-((FRCTdeltaZxsuLsuLxGsuL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_530_1023 = Coupling(name = 'UVGC_530_1023',
                         value = '-((FRCTdeltaZxuuLxuG*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_530_1024 = Coupling(name = 'UVGC_530_1024',
                         value = '-((FRCTdeltaZxuuLxgosuL*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_530_1025 = Coupling(name = 'UVGC_530_1025',
                         value = '-((FRCTdeltaZxsuLsuLxugo*complex(0,1)*G)/cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1026 = Coupling(name = 'UVGC_531_1026',
                         value = '(FRCTdeltaxaSxgo*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1027 = Coupling(name = 'UVGC_531_1027',
                         value = '(FRCTdeltaxaSxsbL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1028 = Coupling(name = 'UVGC_531_1028',
                         value = '(FRCTdeltaxaSxsbR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1029 = Coupling(name = 'UVGC_531_1029',
                         value = '(FRCTdeltaxaSxscL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1030 = Coupling(name = 'UVGC_531_1030',
                         value = '(FRCTdeltaxaSxscR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1031 = Coupling(name = 'UVGC_531_1031',
                         value = '(FRCTdeltaxaSxsdL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1032 = Coupling(name = 'UVGC_531_1032',
                         value = '(FRCTdeltaxaSxsdR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1033 = Coupling(name = 'UVGC_531_1033',
                         value = '(FRCTdeltaxaSxssL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1034 = Coupling(name = 'UVGC_531_1034',
                         value = '(FRCTdeltaxaSxssR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1035 = Coupling(name = 'UVGC_531_1035',
                         value = '(FRCTdeltaxaSxstL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1036 = Coupling(name = 'UVGC_531_1036',
                         value = '(FRCTdeltaxaSxstR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1037 = Coupling(name = 'UVGC_531_1037',
                         value = '(FRCTdeltaxaSxsuL*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1038 = Coupling(name = 'UVGC_531_1038',
                         value = '(FRCTdeltaxaSxsuR*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1039 = Coupling(name = 'UVGC_531_1039',
                         value = '(FRCTdeltaxaSxt*complex(0,1)*G)/(aS*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1040 = Coupling(name = 'UVGC_531_1040',
                         value = '(FRCTdeltaZxbbRxbG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1041 = Coupling(name = 'UVGC_531_1041',
                         value = '(FRCTdeltaZxsbRsbRxbgo*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1042 = Coupling(name = 'UVGC_531_1042',
                         value = '(FRCTdeltaZxgogoLxbsbL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1043 = Coupling(name = 'UVGC_531_1043',
                         value = '(FRCTdeltaZxgogoLxbsbR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1044 = Coupling(name = 'UVGC_531_1044',
                         value = '(FRCTdeltaZxgogoLxcscL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1045 = Coupling(name = 'UVGC_531_1045',
                         value = '(FRCTdeltaZxgogoLxcscR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1046 = Coupling(name = 'UVGC_531_1046',
                         value = '(FRCTdeltaZxgogoLxdsdL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1047 = Coupling(name = 'UVGC_531_1047',
                         value = '(FRCTdeltaZxgogoLxdsdR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1048 = Coupling(name = 'UVGC_531_1048',
                         value = '(FRCTdeltaZxgogoLxgoG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1049 = Coupling(name = 'UVGC_531_1049',
                         value = '(FRCTdeltaZxsbRsbRxGsbR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1050 = Coupling(name = 'UVGC_531_1050',
                         value = '(FRCTdeltaZxbbRxgosbR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1051 = Coupling(name = 'UVGC_531_1051',
                         value = '(FRCTdeltaZxgogoLxsssL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1052 = Coupling(name = 'UVGC_531_1052',
                         value = '(FRCTdeltaZxgogoLxsssR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1053 = Coupling(name = 'UVGC_531_1053',
                         value = '(FRCTdeltaZxgogoLxtstL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1054 = Coupling(name = 'UVGC_531_1054',
                         value = '(FRCTdeltaZxgogoLxtstR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1055 = Coupling(name = 'UVGC_531_1055',
                         value = '(FRCTdeltaZxgogoLxusuL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1056 = Coupling(name = 'UVGC_531_1056',
                         value = '(FRCTdeltaZxgogoLxusuR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_531_1057 = Coupling(name = 'UVGC_531_1057',
                         value = '(-3*complex(0,1)*G**3*invFREps)/(4.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1058 = Coupling(name = 'UVGC_531_1058',
                         value = '(complex(0,1)*G**3*invFREps)/(48.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_531_1059 = Coupling(name = 'UVGC_531_1059',
                         value = '(-3*complex(0,1)*G**3*invFREps)/(16.*cmath.pi**2*cmath.sqrt(2))',
                         order = {'QCD':3})

UVGC_532_1060 = Coupling(name = 'UVGC_532_1060',
                         value = '(FRCTdeltaZxccRxcG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_532_1061 = Coupling(name = 'UVGC_532_1061',
                         value = '(FRCTdeltaZxscRscRxcgo*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_532_1062 = Coupling(name = 'UVGC_532_1062',
                         value = '(FRCTdeltaZxscRscRxGscR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_532_1063 = Coupling(name = 'UVGC_532_1063',
                         value = '(FRCTdeltaZxccRxgoscR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_533_1064 = Coupling(name = 'UVGC_533_1064',
                         value = '(FRCTdeltaZxddRxdG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_533_1065 = Coupling(name = 'UVGC_533_1065',
                         value = '(FRCTdeltaZxsdRsdRxdgo*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_533_1066 = Coupling(name = 'UVGC_533_1066',
                         value = '(FRCTdeltaZxsdRsdRxGsdR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_533_1067 = Coupling(name = 'UVGC_533_1067',
                         value = '(FRCTdeltaZxddRxgosdR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_534_1068 = Coupling(name = 'UVGC_534_1068',
                         value = '(FRCTdeltaZxssRxsG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_534_1069 = Coupling(name = 'UVGC_534_1069',
                         value = '(FRCTdeltaZxssRssRxGssR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_534_1070 = Coupling(name = 'UVGC_534_1070',
                         value = '(FRCTdeltaZxssRssRxsgo*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_534_1071 = Coupling(name = 'UVGC_534_1071',
                         value = '(FRCTdeltaZxssRxgossR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_535_1072 = Coupling(name = 'UVGC_535_1072',
                         value = '(FRCTdeltaZxstRstRxGstR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_535_1073 = Coupling(name = 'UVGC_535_1073',
                         value = '(FRCTdeltaZxttRxtG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_535_1074 = Coupling(name = 'UVGC_535_1074',
                         value = '(FRCTdeltaZxttRxgostL*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_535_1075 = Coupling(name = 'UVGC_535_1075',
                         value = '(FRCTdeltaZxttRxgostR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_535_1076 = Coupling(name = 'UVGC_535_1076',
                         value = '(FRCTdeltaZxstRstRxtgo*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_536_1077 = Coupling(name = 'UVGC_536_1077',
                         value = '(FRCTdeltaZxsuRsuRxGsuR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_536_1078 = Coupling(name = 'UVGC_536_1078',
                         value = '(FRCTdeltaZxuuRxuG*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_536_1079 = Coupling(name = 'UVGC_536_1079',
                         value = '(FRCTdeltaZxuuRxgosuR*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_536_1080 = Coupling(name = 'UVGC_536_1080',
                         value = '(FRCTdeltaZxsuRsuRxugo*complex(0,1)*G)/cmath.sqrt(2)',
                         order = {'QCD':3})

UVGC_95_1081 = Coupling(name = 'UVGC_95_1081',
                        value = '-(ee*FRCTdeltaZxstLstRxtgo*complex(0,1))/(6.*cw*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

UVGC_96_1082 = Coupling(name = 'UVGC_96_1082',
                        value = '(ee*FRCTdeltaZxstRstLxtgo*complex(0,1)*cmath.sqrt(2))/(3.*cw)',
                        order = {'QCD':2,'QED':1})

UVGC_97_1083 = Coupling(name = 'UVGC_97_1083',
                        value = '-((FRCTdeltaZxstLstRxtgo*complex(0,1)*G)/cmath.sqrt(2))',
                        order = {'QCD':3})

UVGC_98_1084 = Coupling(name = 'UVGC_98_1084',
                        value = '(FRCTdeltaZxstRstLxtgo*complex(0,1)*G)/cmath.sqrt(2)',
                        order = {'QCD':3})

UVGC_99_1085 = Coupling(name = 'UVGC_99_1085',
                        value = '(3*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVGC_99_1086 = Coupling(name = 'UVGC_99_1086',
                        value = '(-3*G**4*invFREps)/(512.*cmath.pi**2)',
                        order = {'QCD':4})

UVloopGC_522_1 = Coupling(name = 'UVloopGC_522_1',
                          value = '(aS*complex(0,1)*G**2)/(8.*cmath.pi)',
                          order = {'QCD':4})

UVloopGC_523_2 = Coupling(name = 'UVloopGC_523_2',
                          value = '(aS*complex(0,1)*G**2)/(4.*cmath.pi)',
                          order = {'QCD':4})

UVloopGC_524_3 = Coupling(name = 'UVloopGC_524_3',
                          value = '(3*aS*complex(0,1)*G**2)/(8.*cmath.pi)',
                          order = {'QCD':4})

UVloopGC_525_4 = Coupling(name = 'UVloopGC_525_4',
                          value = '-(aS*complex(0,1)*G*cmath.sqrt(2))/(3.*cmath.pi)',
                          order = {'QCD':3})

UVloopGC_531_5 = Coupling(name = 'UVloopGC_531_5',
                          value = '(aS*complex(0,1)*G*cmath.sqrt(2))/(3.*cmath.pi)',
                          order = {'QCD':3})

