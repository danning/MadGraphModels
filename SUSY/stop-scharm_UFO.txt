Requestor: Alvaro Lopez Solis
Content: simplified model to study minimal flavour-violations in SUSY in final states of the type tj+MET
Webpage: https://phystev.cnrs.fr/wiki/2017:groups:np:qfvlhc
Paper: https://arxiv.org/abs/1808.07488
Source: https://phystev.cnrs.fr/wiki/_media/2017:groups:np:st-sch.ufo.tgz