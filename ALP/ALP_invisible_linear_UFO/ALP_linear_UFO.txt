Requestor: Rachel Rosten
Contents: Axion-like particle, 4 linear coupling operators
Webpage: http://feynrules.irmp.ucl.ac.be/wiki/ALPsEFT
Paper: https://arxiv.org/abs/1701.05379
JIRA: https://its.cern.ch/jira/browse/AGENE-1425