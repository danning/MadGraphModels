# This file was automatically created by FeynRules 2.3.32
# Mathematica version: 11.0.1 for Microsoft Windows (64-bit) (September 20, 2016)
# Date: Sun 7 Apr 2019 23:02:46


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

