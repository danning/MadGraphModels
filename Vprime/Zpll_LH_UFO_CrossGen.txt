Requestor: Volker Austrup and Roy Brener
Contents: Z' coupling to s, b, t, and e or mu
Source: based on model by Andreas Crivellin (private communication)
Presentation: https://indico.cern.ch/event/961693/contributions/4045758/attachments/2115785/3559933/CERNZp2020.pdf
