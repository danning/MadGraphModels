# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.2.0 for Microsoft Windows (64-bit) (September 11, 2017)
# Date: Tue 12 Dec 2017 14:34:56



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZptarget = Parameter(name = 'MZptarget',
                      nature = 'external',
                      type = 'real',
                      value = 300,
                      texname = 'M_{\\text{zptarget}}',
                      lhablock = 'FRBlock',
                      lhacode = [ 1 ])

Mn1 = Parameter(name = 'Mn1',
                nature = 'external',
                type = 'real',
                value = 250,
                texname = 'M_{\\text{n1}}',
                lhablock = 'FRBlock',
                lhacode = [ 2 ])

xi = Parameter(name = 'xi',
               nature = 'external',
               type = 'real',
               value = 0.,
               texname = '\\text{Chi}',
               lhablock = 'FRBlock',
               lhacode = [ 3 ])

QS0 = Parameter(name = 'QS0',
                nature = 'external',
                type = 'real',
                value = 2.,
                texname = 'Q_{\\text{S0}}',
                lhablock = 'FRBlock',
                lhacode = [ 4 ])

QDM = Parameter(name = 'QDM',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'Q_{\\text{DM}}',
                lhablock = 'FRBlock',
                lhacode = [ 5 ])

gzp = Parameter(name = 'gzp',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = 'g_{\\text{zp}}',
                lhablock = 'FRBlock',
                lhacode = [ 6 ])

chi = Parameter(name = 'chi',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{cos(hi)}',
                lhablock = 'FRBlock',
                lhacode = [ 7 ])

MStarget = Parameter(name = 'MStarget',
                     nature = 'external',
                     type = 'real',
                     value = 200,
                     texname = '\\text{MStarget}',
                     lhablock = 'FRBlock',
                     lhacode = [ 8 ])

MHtarget = Parameter(name = 'MHtarget',
                     nature = 'external',
                     type = 'real',
                     value = 125,
                     texname = '\\text{MHtarget}',
                     lhablock = 'FRBlock',
                     lhacode = [ 9 ])

lambHS = Parameter(name = 'lambHS',
                   nature = 'external',
                   type = 'real',
                   value = 0.01,
                   texname = '\\text{lambdaHS}',
                   lhablock = 'FRBlock',
                   lhacode = [ 10 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WS0 = Parameter(name = 'WS0',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WS0}',
                lhablock = 'DECAY',
                lhacode = [ 200002100 ])

WZp = Parameter(name = 'WZp',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WZp}',
                lhablock = 'DECAY',
                lhacode = [ 32 ])

cxi = Parameter(name = 'cxi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.cos(xi)',
                texname = '\\text{Cos(xi)}')

sxi = Parameter(name = 'sxi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sin(xi)',
                texname = '\\text{Sin(xi)}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

ozp = Parameter(name = 'ozp',
                nature = 'internal',
                type = 'real',
                value = '1',
                texname = '\\text{orderzp}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

txi = Parameter(name = 'txi',
                nature = 'internal',
                type = 'real',
                value = 'sxi/cxi',
                texname = '\\text{Tan(xi)}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

VS = Parameter(name = 'VS',
               nature = 'internal',
               type = 'real',
               value = '(cxi*MZptarget*cmath.sqrt(2)*cmath.sqrt(-MZ**2 + MZptarget**2 - MZ**2*sw**2*txi**2))/(gzp*QS0*cmath.sqrt(-MZ**2 + MZptarget**2))',
               texname = '\\text{vev}_{\\text{S0}}')

xs = Parameter(name = 'xs',
               nature = 'internal',
               type = 'real',
               value = 'VS**2/vev**2',
               texname = '\\text{xs}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

lambS = Parameter(name = 'lambS',
                  nature = 'internal',
                  type = 'real',
                  value = '(MHtarget**2 + MStarget**2 + cmath.sqrt((-MHtarget**2 + MStarget**2)**2 - 4*lambHS**2*vev**2*VS**2))/(2.*VS**2)',
                  texname = '\\text{$\\#$lambda}_{\\text{S0}}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

MZp = Parameter(name = 'MZp',
                nature = 'external',
                type = 'real',
#                value = 'cmath.sqrt(MZ**2*(1 + sw**2*txi**2) + (gzp**2*QS0**2*vev**2*xs)/(2.*cxi**2) + cmath.sqrt((-2*gzp**2*MZ**2*QS0**2*vev**2*xs)/cxi**2 + (MZ**2*(1 + sw**2*txi**2) + (gzp**2*QS0**2*vev**2*xs)/(2.*cxi**2))**2))/cmath.sqrt(2)',
		value = 100,
                texname = 'M_{\\text{zp}}',
		lhablock = 'MASS',
                lhacode = [ 32 ])

zi = Parameter(name = 'zi',
               nature = 'internal',
               type = 'real',
               value = 'cmath.atan((2*MZ**2*sw*txi)/(MZ**2*(1 - sw**2*txi**2) - (gzp**2*QS0**2*vev**2*xs)/(2.*cxi**2)))/2.',
               texname = '\\text{zi}')

lambH = Parameter(name = 'lambH',
                  nature = 'internal',
                  type = 'real',
                  value = '(MHtarget**2 + MStarget**2)/vev**2 - (lambS*VS**2)/vev**2',
                  texname = '\\text{$\\#$lambda}_H')

alp = Parameter(name = 'alp',
                nature = 'internal',
                type = 'real',
                value = 'cmath.atan((2*lambHS*vev*VS)/(lambH*vev**2 - lambS*VS**2))/2.',
                texname = '\\text{alpha}')

czi = Parameter(name = 'czi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.cos(zi)',
                texname = '\\text{Cos(zi)}')

MS0 = Parameter(name = 'MS0',
                nature = 'external',
                type = 'real',
#                value = 'cmath.sqrt(lambH*vev**2 + lambS*VS**2 + cmath.sqrt(4*lambHS**2*vev**2*VS**2 + (lambH*vev**2 - lambS*VS**2)**2))/cmath.sqrt(2)',
		value = 1000,
		texname = 'M_{\\text{S0}}',
		lhablock = 'MASS',
                lhacode = [ 200002100 ])

szi = Parameter(name = 'szi',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sin(zi)',
                texname = '\\text{Sin(zi)}')

cal = Parameter(name = 'cal',
                nature = 'internal',
                type = 'real',
                value = 'cmath.cos(alp)',
                texname = '\\text{Cos(alpha)}')

sal = Parameter(name = 'sal',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sin(alp)',
                texname = '\\text{Sin(alpha)}')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1a33}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2a33}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3a33}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4a33}')

