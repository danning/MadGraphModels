# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 10.4.1 for Mac OS X x86 (64-bit) (April 11, 2016)
# Date: Fri 8 Jun 2018 02:11:15


from object_library import all_orders, CouplingOrder


NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

