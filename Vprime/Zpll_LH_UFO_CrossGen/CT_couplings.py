# This file was automatically created by FeynRules 2.3.40
# Mathematica version: 12.1.1 for Mac OS X x86 (64-bit) (June 19, 2020)
# Date: Tue 22 Dec 2020 17:33:58


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



