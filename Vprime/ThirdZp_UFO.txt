Requestor: Hector De La Torre Perez
Content: 3rd generation Z' for bbZ'>bbbb analysis
Source: Admir Greljo (private communication)
Paper1: https://arxiv.org/abs/1506.01705
Paper2: https://arxiv.org/abs/1609.07138
JIRA: https://its.cern.ch/jira/browse/AGENE-1645