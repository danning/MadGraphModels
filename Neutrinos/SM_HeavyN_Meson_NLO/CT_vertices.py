# This file was automatically created by FeynRules 2.3.36
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Tue 25 Apr 2023 15:27:22


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1, L.VVV2 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_665_75,(0,1,1):C.R2GC_582_1})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,1,0):C.R2GC_614_42,(2,1,1):C.R2GC_614_43,(0,1,0):C.R2GC_614_42,(0,1,1):C.R2GC_614_43,(6,1,0):C.R2GC_617_47,(6,1,1):C.R2GC_670_81,(4,1,0):C.R2GC_612_38,(4,1,1):C.R2GC_612_39,(3,1,0):C.R2GC_612_38,(3,1,1):C.R2GC_612_39,(8,1,0):C.R2GC_613_40,(8,1,1):C.R2GC_613_41,(7,1,0):C.R2GC_618_48,(7,1,1):C.R2GC_669_80,(5,1,0):C.R2GC_612_38,(5,1,1):C.R2GC_612_39,(1,1,0):C.R2GC_612_38,(1,1,1):C.R2GC_612_39,(11,0,0):C.R2GC_616_45,(11,0,1):C.R2GC_616_46,(10,0,0):C.R2GC_616_45,(10,0,1):C.R2GC_616_46,(9,0,1):C.R2GC_615_44,(2,2,0):C.R2GC_614_42,(2,2,1):C.R2GC_614_43,(0,2,0):C.R2GC_614_42,(0,2,1):C.R2GC_614_43,(4,2,0):C.R2GC_612_38,(4,2,1):C.R2GC_612_39,(3,2,0):C.R2GC_612_38,(3,2,1):C.R2GC_612_39,(8,2,0):C.R2GC_613_40,(8,2,1):C.R2GC_669_80,(6,2,0):C.R2GC_666_76,(6,2,1):C.R2GC_666_77,(7,2,0):C.R2GC_618_48,(7,2,1):C.R2GC_613_41,(5,2,0):C.R2GC_612_38,(5,2,1):C.R2GC_612_39,(1,2,0):C.R2GC_612_38,(1,2,1):C.R2GC_612_39,(2,3,0):C.R2GC_614_42,(2,3,1):C.R2GC_614_43,(0,3,0):C.R2GC_614_42,(0,3,1):C.R2GC_614_43,(4,3,0):C.R2GC_612_38,(4,3,1):C.R2GC_612_39,(3,3,0):C.R2GC_612_38,(3,3,1):C.R2GC_612_39,(8,3,0):C.R2GC_613_40,(8,3,1):C.R2GC_667_79,(6,3,0):C.R2GC_617_47,(7,3,0):C.R2GC_667_78,(7,3,1):C.R2GC_667_79,(5,3,0):C.R2GC_612_38,(5,3,1):C.R2GC_612_39,(1,3,0):C.R2GC_612_38,(1,3,1):C.R2GC_612_39})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.c__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.c, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_662_71})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_692_97,(0,1,0):C.R2GC_693_98})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.u__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.b, P.g, P.u] ] ],
               couplings = {(0,0,0):C.R2GC_660_69})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_656_65})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.b__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2 ],
               loop_particles = [ [ [P.b, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_655_64})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.b__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3, L.FFS4 ],
               loop_particles = [ [ [P.b, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_686_91,(0,1,0):C.R2GC_681_86})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.d__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS3 ],
               loop_particles = [ [ [P.d, P.g, P.t] ] ],
               couplings = {(0,0,0):C.R2GC_684_89})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_685_90})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_682_87})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_683_88})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_658_67})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_657_66})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_688_93})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_690_95})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_622_52})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_622_52})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_622_52})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_619_49})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_619_49})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_619_49})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_620_50})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_620_50})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_620_50})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_620_50})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_620_50})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_620_50})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_652_63})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_639_53})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_641_55})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_678_85})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_676_83})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_677_84})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_651_62})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_643_57})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_645_59})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_661_70})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_691_96})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_659_68})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_640_54})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_687_92})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_644_58})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_642_56})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_689_94})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_646_60})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_596_22,(0,1,0):C.R2GC_583_2})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_596_22,(0,1,0):C.R2GC_583_2})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_596_22,(0,1,0):C.R2GC_583_2})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_597_23,(0,1,0):C.R2GC_584_3})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_597_23,(0,1,0):C.R2GC_584_3})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_597_23,(0,1,0):C.R2GC_584_3})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.b__tilde__, P.b ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_650_61,(0,2,0):C.R2GC_650_61,(0,1,0):C.R2GC_621_51,(0,3,0):C.R2GC_621_51})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.c__tilde__, P.c ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_621_51})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.d__tilde__, P.d ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_621_51})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.s__tilde__, P.s ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_621_51})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.t__tilde__, P.t ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_675_82,(0,2,0):C.R2GC_675_82,(0,1,0):C.R2GC_621_51,(0,3,0):C.R2GC_621_51})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.u__tilde__, P.u ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FF1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_621_51})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.g, P.g ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VV1, L.VV2, L.VV3 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.R2GC_664_74,(0,1,0):C.R2GC_587_4,(0,1,3):C.R2GC_587_5,(0,2,1):C.R2GC_663_72,(0,2,2):C.R2GC_663_73})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.g, P.g, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_588_6,(0,0,1):C.R2GC_588_7})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.Z ],
                color = [ 'Identity(2,3)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_592_14,(0,0,1):C.R2GC_592_15,(0,1,0):C.R2GC_592_14,(0,1,1):C.R2GC_592_15,(0,2,0):C.R2GC_592_14,(0,2,1):C.R2GC_592_15})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.g, P.g, P.Z, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_595_20,(0,0,1):C.R2GC_595_21,(0,1,0):C.R2GC_595_20,(0,1,1):C.R2GC_595_21,(0,2,0):C.R2GC_595_20,(0,2,1):C.R2GC_595_21})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_601_29,(0,0,1):C.R2GC_601_30,(0,0,2):C.R2GC_601_31,(0,0,3):C.R2GC_601_32,(0,0,4):C.R2GC_601_33,(0,0,5):C.R2GC_601_34,(0,0,6):C.R2GC_601_35,(0,0,7):C.R2GC_601_36,(0,0,8):C.R2GC_601_37,(0,1,0):C.R2GC_601_29,(0,1,1):C.R2GC_601_30,(0,1,2):C.R2GC_601_31,(0,1,3):C.R2GC_601_32,(0,1,4):C.R2GC_601_33,(0,1,5):C.R2GC_601_34,(0,1,6):C.R2GC_601_35,(0,1,7):C.R2GC_601_36,(0,1,8):C.R2GC_601_37,(0,2,0):C.R2GC_601_29,(0,2,1):C.R2GC_601_30,(0,2,2):C.R2GC_601_31,(0,2,3):C.R2GC_601_32,(0,2,4):C.R2GC_601_33,(0,2,5):C.R2GC_601_34,(0,2,6):C.R2GC_601_35,(0,2,7):C.R2GC_601_36,(0,2,8):C.R2GC_601_37})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.a, P.a, P.g, P.g ],
                color = [ 'Identity(3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_590_10,(0,0,1):C.R2GC_590_11,(0,1,0):C.R2GC_590_10,(0,1,1):C.R2GC_590_11,(0,2,0):C.R2GC_590_10,(0,2,1):C.R2GC_590_11})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.g, P.g, P.g, P.Z ],
                color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(1,0,0):C.R2GC_594_18,(1,0,1):C.R2GC_594_19,(0,1,0):C.R2GC_593_16,(0,1,1):C.R2GC_593_17,(0,2,0):C.R2GC_593_16,(0,2,1):C.R2GC_593_17,(0,3,0):C.R2GC_593_16,(0,3,1):C.R2GC_593_17})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.a, P.g, P.g, P.g ],
                color = [ 'd(2,3,4)' ],
                lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                couplings = {(0,0,0):C.R2GC_591_12,(0,0,1):C.R2GC_591_13,(0,1,0):C.R2GC_591_12,(0,1,1):C.R2GC_591_13,(0,2,0):C.R2GC_591_12,(0,2,1):C.R2GC_591_13})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.g, P.g, P.H, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_589_8,(0,0,1):C.R2GC_589_9})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.g, P.g, P.G0, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.R2GC_589_8,(0,0,1):C.R2GC_589_9})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.VVSS1 ],
                loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_599_24,(0,0,1):C.R2GC_599_25,(0,0,2):C.R2GC_599_26,(0,0,3):C.R2GC_599_27,(0,0,4):C.R2GC_599_28})

V_70 = CTVertex(name = 'V_70',
                type = 'UV',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV1, L.VVV3, L.VVV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,0):C.UVGC_665_80,(0,0,1):C.UVGC_665_81,(0,0,4):C.UVGC_665_82,(0,1,2):C.UVGC_602_1,(0,2,3):C.UVGC_603_2})

V_71 = CTVertex(name = 'V_71',
                type = 'UV',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                lorentz = [ L.VVVV10, L.VVVV2, L.VVVV3, L.VVVV5 ],
                loop_particles = [ [ [P.b] ], [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(2,1,3):C.UVGC_613_9,(2,1,4):C.UVGC_613_8,(0,1,3):C.UVGC_613_9,(0,1,4):C.UVGC_613_8,(6,1,0):C.UVGC_669_93,(6,1,2):C.UVGC_669_94,(6,1,3):C.UVGC_670_98,(6,1,4):C.UVGC_670_99,(6,1,5):C.UVGC_669_97,(4,1,3):C.UVGC_612_6,(4,1,4):C.UVGC_612_7,(3,1,3):C.UVGC_612_6,(3,1,4):C.UVGC_612_7,(8,1,3):C.UVGC_613_8,(8,1,4):C.UVGC_613_9,(7,1,0):C.UVGC_669_93,(7,1,2):C.UVGC_669_94,(7,1,3):C.UVGC_669_95,(7,1,4):C.UVGC_669_96,(7,1,5):C.UVGC_669_97,(5,1,3):C.UVGC_612_6,(5,1,4):C.UVGC_612_7,(1,1,3):C.UVGC_612_6,(1,1,4):C.UVGC_612_7,(11,0,3):C.UVGC_616_12,(11,0,4):C.UVGC_616_13,(10,0,3):C.UVGC_616_12,(10,0,4):C.UVGC_616_13,(9,0,3):C.UVGC_615_10,(9,0,4):C.UVGC_615_11,(2,2,3):C.UVGC_613_9,(2,2,4):C.UVGC_613_8,(0,2,3):C.UVGC_613_9,(0,2,4):C.UVGC_613_8,(4,2,3):C.UVGC_612_6,(4,2,4):C.UVGC_612_7,(3,2,3):C.UVGC_612_6,(3,2,4):C.UVGC_612_7,(8,2,0):C.UVGC_671_100,(8,2,2):C.UVGC_671_101,(8,2,3):C.UVGC_669_95,(8,2,4):C.UVGC_671_102,(8,2,5):C.UVGC_671_103,(6,2,0):C.UVGC_666_83,(6,2,3):C.UVGC_666_84,(6,2,4):C.UVGC_666_85,(6,2,5):C.UVGC_666_86,(7,2,1):C.UVGC_617_14,(7,2,3):C.UVGC_613_8,(7,2,4):C.UVGC_618_15,(5,2,3):C.UVGC_612_6,(5,2,4):C.UVGC_612_7,(1,2,3):C.UVGC_612_6,(1,2,4):C.UVGC_612_7,(2,3,3):C.UVGC_613_9,(2,3,4):C.UVGC_613_8,(0,3,3):C.UVGC_613_9,(0,3,4):C.UVGC_613_8,(4,3,3):C.UVGC_612_6,(4,3,4):C.UVGC_612_7,(3,3,3):C.UVGC_612_6,(3,3,4):C.UVGC_612_7,(8,3,0):C.UVGC_668_89,(8,3,2):C.UVGC_668_90,(8,3,3):C.UVGC_667_87,(8,3,4):C.UVGC_668_91,(8,3,5):C.UVGC_668_92,(6,3,1):C.UVGC_617_14,(6,3,4):C.UVGC_615_10,(7,3,0):C.UVGC_666_83,(7,3,3):C.UVGC_667_87,(7,3,4):C.UVGC_667_88,(7,3,5):C.UVGC_666_86,(5,3,3):C.UVGC_612_6,(5,3,4):C.UVGC_612_7,(1,3,3):C.UVGC_612_6,(1,3,4):C.UVGC_612_7})

V_72 = CTVertex(name = 'V_72',
                type = 'UV',
                particles = [ P.c__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_662_71,(0,0,2):C.UVGC_662_72,(0,0,0):C.UVGC_662_73})

V_73 = CTVertex(name = 'V_73',
                type = 'UV',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_692_148,(0,0,2):C.UVGC_692_149,(0,0,1):C.UVGC_692_150,(0,1,0):C.UVGC_693_151,(0,1,2):C.UVGC_693_152,(0,1,1):C.UVGC_693_153})

V_74 = CTVertex(name = 'V_74',
                type = 'UV',
                particles = [ P.u__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_660_65,(0,0,2):C.UVGC_660_66,(0,0,1):C.UVGC_660_67})

V_75 = CTVertex(name = 'V_75',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_656_55})

V_76 = CTVertex(name = 'V_76',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_655_54})

V_77 = CTVertex(name = 'V_77',
                type = 'UV',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_686_130,(0,0,2):C.UVGC_686_131,(0,0,1):C.UVGC_686_132,(0,1,0):C.UVGC_681_119,(0,1,2):C.UVGC_681_120,(0,1,1):C.UVGC_681_121})

V_78 = CTVertex(name = 'V_78',
                type = 'UV',
                particles = [ P.d__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_684_124,(0,0,2):C.UVGC_684_125,(0,0,1):C.UVGC_684_126})

V_79 = CTVertex(name = 'V_79',
                type = 'UV',
                particles = [ P.s__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS3 ],
                loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_685_127,(0,0,2):C.UVGC_685_128,(0,0,1):C.UVGC_685_129})

V_80 = CTVertex(name = 'V_80',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_682_122})

V_81 = CTVertex(name = 'V_81',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.H ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_683_123})

V_82 = CTVertex(name = 'V_82',
                type = 'UV',
                particles = [ P.b__tilde__, P.c, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_658_59,(0,0,2):C.UVGC_658_60,(0,0,0):C.UVGC_658_61})

V_83 = CTVertex(name = 'V_83',
                type = 'UV',
                particles = [ P.b__tilde__, P.u, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_657_56,(0,0,2):C.UVGC_657_57,(0,0,1):C.UVGC_657_58})

V_84 = CTVertex(name = 'V_84',
                type = 'UV',
                particles = [ P.t__tilde__, P.d, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_688_136,(0,0,2):C.UVGC_688_137,(0,0,1):C.UVGC_688_138})

V_85 = CTVertex(name = 'V_85',
                type = 'UV',
                particles = [ P.t__tilde__, P.s, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_690_142,(0,0,2):C.UVGC_690_143,(0,0,1):C.UVGC_690_144})

V_86 = CTVertex(name = 'V_86',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_622_19,(0,1,0):C.UVGC_605_4,(0,2,0):C.UVGC_605_4})

V_87 = CTVertex(name = 'V_87',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.UVGC_622_19,(0,1,0):C.UVGC_673_105,(0,2,0):C.UVGC_673_105})

V_88 = CTVertex(name = 'V_88',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.UVGC_622_19,(0,1,0):C.UVGC_605_4,(0,2,0):C.UVGC_605_4})

V_89 = CTVertex(name = 'V_89',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_619_16,(0,1,0):C.UVGC_648_43,(0,2,0):C.UVGC_648_43})

V_90 = CTVertex(name = 'V_90',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.UVGC_619_16,(0,1,0):C.UVGC_607_5,(0,2,0):C.UVGC_607_5})

V_91 = CTVertex(name = 'V_91',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.UVGC_619_16,(0,1,0):C.UVGC_607_5,(0,2,0):C.UVGC_607_5})

V_92 = CTVertex(name = 'V_92',
                type = 'UV',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.UVGC_620_17,(0,1,0):C.UVGC_623_20,(0,1,1):C.UVGC_623_21,(0,1,3):C.UVGC_623_22,(0,1,4):C.UVGC_623_23,(0,1,5):C.UVGC_623_24,(0,1,2):C.UVGC_623_25,(0,2,0):C.UVGC_623_20,(0,2,1):C.UVGC_623_21,(0,2,3):C.UVGC_623_22,(0,2,4):C.UVGC_623_23,(0,2,5):C.UVGC_623_24,(0,2,2):C.UVGC_623_25})

V_93 = CTVertex(name = 'V_93',
                type = 'UV',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ], [ [P.t] ] ],
                couplings = {(0,0,4):C.UVGC_620_17,(0,1,0):C.UVGC_623_20,(0,1,1):C.UVGC_623_21,(0,1,2):C.UVGC_623_22,(0,1,3):C.UVGC_623_23,(0,1,5):C.UVGC_623_24,(0,1,4):C.UVGC_674_106,(0,2,0):C.UVGC_623_20,(0,2,1):C.UVGC_623_21,(0,2,2):C.UVGC_623_22,(0,2,3):C.UVGC_623_23,(0,2,5):C.UVGC_623_24,(0,2,4):C.UVGC_674_106})

V_94 = CTVertex(name = 'V_94',
                type = 'UV',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ], [ [P.t] ] ],
                couplings = {(0,0,4):C.UVGC_620_17,(0,1,0):C.UVGC_623_20,(0,1,1):C.UVGC_623_21,(0,1,2):C.UVGC_623_22,(0,1,3):C.UVGC_623_23,(0,1,5):C.UVGC_623_24,(0,1,4):C.UVGC_623_25,(0,2,0):C.UVGC_623_20,(0,2,1):C.UVGC_623_21,(0,2,2):C.UVGC_623_22,(0,2,3):C.UVGC_623_23,(0,2,5):C.UVGC_623_24,(0,2,4):C.UVGC_623_25})

V_95 = CTVertex(name = 'V_95',
                type = 'UV',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b] ], [ [P.b, P.g] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,1):C.UVGC_620_17,(0,1,0):C.UVGC_623_20,(0,1,2):C.UVGC_623_21,(0,1,3):C.UVGC_623_22,(0,1,4):C.UVGC_623_23,(0,1,5):C.UVGC_623_24,(0,1,1):C.UVGC_649_44,(0,2,0):C.UVGC_623_20,(0,2,2):C.UVGC_623_21,(0,2,3):C.UVGC_623_22,(0,2,4):C.UVGC_623_23,(0,2,5):C.UVGC_623_24,(0,2,1):C.UVGC_649_44})

V_96 = CTVertex(name = 'V_96',
                type = 'UV',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                couplings = {(0,0,2):C.UVGC_620_17,(0,1,0):C.UVGC_623_20,(0,1,1):C.UVGC_623_21,(0,1,3):C.UVGC_623_22,(0,1,4):C.UVGC_623_23,(0,1,5):C.UVGC_623_24,(0,1,2):C.UVGC_623_25,(0,2,0):C.UVGC_623_20,(0,2,1):C.UVGC_623_21,(0,2,3):C.UVGC_623_22,(0,2,4):C.UVGC_623_23,(0,2,5):C.UVGC_623_24,(0,2,2):C.UVGC_623_25})

V_97 = CTVertex(name = 'V_97',
                type = 'UV',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b] ], [ [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ], [ [P.t] ] ],
                couplings = {(0,0,4):C.UVGC_620_17,(0,1,0):C.UVGC_623_20,(0,1,1):C.UVGC_623_21,(0,1,2):C.UVGC_623_22,(0,1,3):C.UVGC_623_23,(0,1,5):C.UVGC_623_24,(0,1,4):C.UVGC_623_25,(0,2,0):C.UVGC_623_20,(0,2,1):C.UVGC_623_21,(0,2,2):C.UVGC_623_22,(0,2,3):C.UVGC_623_23,(0,2,5):C.UVGC_623_24,(0,2,4):C.UVGC_623_25})

V_98 = CTVertex(name = 'V_98',
                type = 'UV',
                particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_652_49,(0,0,2):C.UVGC_652_50,(0,0,0):C.UVGC_652_51})

V_99 = CTVertex(name = 'V_99',
                type = 'UV',
                particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3 ],
                loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                couplings = {(0,0,1):C.UVGC_639_26,(0,0,0):C.UVGC_639_27})

V_100 = CTVertex(name = 'V_100',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_641_30,(0,0,1):C.UVGC_641_31})

V_101 = CTVertex(name = 'V_101',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_678_114,(0,0,2):C.UVGC_678_115,(0,0,1):C.UVGC_678_116})

V_102 = CTVertex(name = 'V_102',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_676_108,(0,0,2):C.UVGC_676_109,(0,0,1):C.UVGC_676_110})

V_103 = CTVertex(name = 'V_103',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_677_111,(0,0,2):C.UVGC_677_112,(0,0,1):C.UVGC_677_113})

V_104 = CTVertex(name = 'V_104',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_651_46,(0,0,2):C.UVGC_651_47,(0,0,1):C.UVGC_651_48})

V_105 = CTVertex(name = 'V_105',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_643_34,(0,0,1):C.UVGC_643_35})

V_106 = CTVertex(name = 'V_106',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_645_38,(0,0,1):C.UVGC_645_39})

V_107 = CTVertex(name = 'V_107',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g] ], [ [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_661_68,(0,0,2):C.UVGC_661_69,(0,0,0):C.UVGC_661_70})

V_108 = CTVertex(name = 'V_108',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_691_145,(0,0,2):C.UVGC_691_146,(0,0,1):C.UVGC_691_147})

V_109 = CTVertex(name = 'V_109',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.u] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_659_62,(0,0,2):C.UVGC_659_63,(0,0,1):C.UVGC_659_64})

V_110 = CTVertex(name = 'V_110',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_640_28,(0,0,0):C.UVGC_640_29})

V_111 = CTVertex(name = 'V_111',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_687_133,(0,0,2):C.UVGC_687_134,(0,0,1):C.UVGC_687_135})

V_112 = CTVertex(name = 'V_112',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_644_36,(0,0,1):C.UVGC_644_37})

V_113 = CTVertex(name = 'V_113',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_642_32,(0,0,1):C.UVGC_642_33})

V_114 = CTVertex(name = 'V_114',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_689_139,(0,0,2):C.UVGC_689_140,(0,0,1):C.UVGC_689_141})

V_115 = CTVertex(name = 'V_115',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_646_40,(0,0,1):C.UVGC_646_41})

V_116 = CTVertex(name = 'V_116',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_679_117,(0,1,0):C.UVGC_680_118})

V_117 = CTVertex(name = 'V_117',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_653_52,(0,1,0):C.UVGC_654_53})

V_118 = CTVertex(name = 'V_118',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_650_45,(0,2,0):C.UVGC_650_45,(0,1,0):C.UVGC_647_42,(0,3,0):C.UVGC_647_42})

V_119 = CTVertex(name = 'V_119',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_621_18,(0,1,0):C.UVGC_604_3,(0,2,0):C.UVGC_604_3})

V_120 = CTVertex(name = 'V_120',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_621_18,(0,1,0):C.UVGC_604_3,(0,2,0):C.UVGC_604_3})

V_121 = CTVertex(name = 'V_121',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_621_18,(0,1,0):C.UVGC_604_3,(0,2,0):C.UVGC_604_3})

V_122 = CTVertex(name = 'V_122',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_675_107,(0,2,0):C.UVGC_675_107,(0,1,0):C.UVGC_672_104,(0,3,0):C.UVGC_672_104})

V_123 = CTVertex(name = 'V_123',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_621_18,(0,1,0):C.UVGC_604_3,(0,2,0):C.UVGC_604_3})

V_124 = CTVertex(name = 'V_124',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.b] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_664_76,(0,0,1):C.UVGC_664_77,(0,0,2):C.UVGC_664_78,(0,0,3):C.UVGC_664_79,(0,1,0):C.UVGC_663_74,(0,1,3):C.UVGC_663_75})

