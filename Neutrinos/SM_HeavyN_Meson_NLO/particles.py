# This file was automatically created by FeynRules 2.3.36
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Tue 25 Apr 2023 15:27:19


from __future__ import division
from object_library import all_particles, Particle
import parameters as Param

import propagators as Prop

a = Particle(pdg_code = 22,
             name = 'a',
             antiname = 'a',
             spin = 3,
             color = 1,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'a',
             antitexname = 'a',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

Z = Particle(pdg_code = 23,
             name = 'Z',
             antiname = 'Z',
             spin = 3,
             color = 1,
             mass = Param.MZ,
             width = Param.WZ,
             texname = 'Z',
             antitexname = 'Z',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

W__plus__ = Particle(pdg_code = 24,
                     name = 'W+',
                     antiname = 'W-',
                     spin = 3,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'W+',
                     antitexname = 'W-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

W__minus__ = W__plus__.anti()

g = Particle(pdg_code = 21,
             name = 'g',
             antiname = 'g',
             spin = 3,
             color = 8,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'g',
             antitexname = 'g',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

ghA = Particle(pdg_code = 9000001,
               name = 'ghA',
               antiname = 'ghA~',
               spin = -1,
               color = 1,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghA',
               antitexname = 'ghA~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghA__tilde__ = ghA.anti()

ghZ = Particle(pdg_code = 9000002,
               name = 'ghZ',
               antiname = 'ghZ~',
               spin = -1,
               color = 1,
               mass = Param.MZ,
               width = Param.WZ,
               texname = 'ghZ',
               antitexname = 'ghZ~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghZ__tilde__ = ghZ.anti()

ghWp = Particle(pdg_code = 9000003,
                name = 'ghWp',
                antiname = 'ghWp~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.WW,
                texname = 'ghWp',
                antitexname = 'ghWp~',
                charge = 1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWp__tilde__ = ghWp.anti()

ghWm = Particle(pdg_code = 9000004,
                name = 'ghWm',
                antiname = 'ghWm~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.WW,
                texname = 'ghWm',
                antitexname = 'ghWm~',
                charge = -1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWm__tilde__ = ghWm.anti()

ghG = Particle(pdg_code = 82,
               name = 'ghG',
               antiname = 'ghG~',
               spin = -1,
               color = 8,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghG',
               antitexname = 'ghG~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghG__tilde__ = ghG.anti()

ve = Particle(pdg_code = 12,
              name = 've',
              antiname = 've~',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 've',
              antitexname = 've~',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 1,
              Y = 0)

ve__tilde__ = ve.anti()

vm = Particle(pdg_code = 14,
              name = 'vm',
              antiname = 'vm~',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'vm',
              antitexname = 'vm~',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 1,
              Y = 0)

vm__tilde__ = vm.anti()

vt = Particle(pdg_code = 16,
              name = 'vt',
              antiname = 'vt~',
              spin = 2,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'vt',
              antitexname = 'vt~',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 1,
              Y = 0)

vt__tilde__ = vt.anti()

e__minus__ = Particle(pdg_code = 11,
                      name = 'e-',
                      antiname = 'e+',
                      spin = 2,
                      color = 1,
                      mass = Param.Me,
                      width = Param.ZERO,
                      texname = 'e-',
                      antitexname = 'e+',
                      charge = -1,
                      GhostNumber = 0,
                      LeptonNumber = 1,
                      Y = 0)

e__plus__ = e__minus__.anti()

mu__minus__ = Particle(pdg_code = 13,
                       name = 'mu-',
                       antiname = 'mu+',
                       spin = 2,
                       color = 1,
                       mass = Param.MMU,
                       width = Param.ZERO,
                       texname = 'mu-',
                       antitexname = 'mu+',
                       charge = -1,
                       GhostNumber = 0,
                       LeptonNumber = 1,
                       Y = 0)

mu__plus__ = mu__minus__.anti()

ta__minus__ = Particle(pdg_code = 15,
                       name = 'ta-',
                       antiname = 'ta+',
                       spin = 2,
                       color = 1,
                       mass = Param.MTA,
                       width = Param.WTA,
                       texname = 'ta-',
                       antitexname = 'ta+',
                       charge = -1,
                       GhostNumber = 0,
                       LeptonNumber = 1,
                       Y = 0)

ta__plus__ = ta__minus__.anti()

u = Particle(pdg_code = 2,
             name = 'u',
             antiname = 'u~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'u',
             antitexname = 'u~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

u__tilde__ = u.anti()

c = Particle(pdg_code = 4,
             name = 'c',
             antiname = 'c~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'c',
             antitexname = 'c~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

c__tilde__ = c.anti()

t = Particle(pdg_code = 6,
             name = 't',
             antiname = 't~',
             spin = 2,
             color = 3,
             mass = Param.MT,
             width = Param.WT,
             texname = 't',
             antitexname = 't~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

t__tilde__ = t.anti()

d = Particle(pdg_code = 1,
             name = 'd',
             antiname = 'd~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'd',
             antitexname = 'd~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

d__tilde__ = d.anti()

s = Particle(pdg_code = 3,
             name = 's',
             antiname = 's~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 's',
             antitexname = 's~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

s__tilde__ = s.anti()

b = Particle(pdg_code = 5,
             name = 'b',
             antiname = 'b~',
             spin = 2,
             color = 3,
             mass = Param.MB,
             width = Param.ZERO,
             texname = 'b',
             antitexname = 'b~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

b__tilde__ = b.anti()

H = Particle(pdg_code = 25,
             name = 'H',
             antiname = 'H',
             spin = 1,
             color = 1,
             mass = Param.MH,
             width = Param.WH,
             texname = 'H',
             antitexname = 'H',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

G0 = Particle(pdg_code = 250,
              name = 'G0',
              antiname = 'G0',
              spin = 1,
              color = 1,
              mass = Param.MZ,
              width = Param.WZ,
              texname = 'G0',
              antitexname = 'G0',
              goldstone = True,
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

G__plus__ = Particle(pdg_code = 251,
                     name = 'G+',
                     antiname = 'G-',
                     spin = 1,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'G+',
                     antitexname = 'G-',
                     goldstone = True,
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

G__minus__ = G__plus__.anti()

N1 = Particle(pdg_code = 72,
              name = 'N1',
              antiname = 'N1',
              spin = 2,
              color = 1,
              mass = Param.mN1,
              width = Param.WN1,
              texname = 'N1',
              antitexname = 'N1',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

N2 = Particle(pdg_code = 74,
              name = 'N2',
              antiname = 'N2',
              spin = 2,
              color = 1,
              mass = Param.mN2,
              width = Param.WN2,
              texname = 'N2',
              antitexname = 'N2',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

N3 = Particle(pdg_code = 76,
              name = 'N3',
              antiname = 'N3',
              spin = 2,
              color = 1,
              mass = Param.mN3,
              width = Param.WN3,
              texname = 'N3',
              antitexname = 'N3',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

K__plus__ = Particle(pdg_code = 321,
                     name = 'K+',
                     antiname = 'K-',
                     spin = 1,
                     color = 1,
                     mass = Param.MK,
                     width = Param.ZERO,
                     texname = 'K+',
                     antitexname = 'K-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

K__minus__ = K__plus__.anti()

Pi__plus__ = Particle(pdg_code = 211,
                      name = 'Pi+',
                      antiname = 'Pi-',
                      spin = 1,
                      color = 1,
                      mass = Param.MPip,
                      width = Param.ZERO,
                      texname = 'Pi+',
                      antitexname = 'Pi-',
                      charge = 1,
                      GhostNumber = 0,
                      LeptonNumber = 0,
                      Y = 0)

Pi__minus__ = Pi__plus__.anti()

D__plus__ = Particle(pdg_code = 411,
                     name = 'D+',
                     antiname = 'D-',
                     spin = 1,
                     color = 1,
                     mass = Param.MDd,
                     width = Param.ZERO,
                     texname = 'D+',
                     antitexname = 'D-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

D__minus__ = D__plus__.anti()

Pi0 = Particle(pdg_code = 111,
               name = 'Pi0',
               antiname = 'Pi0',
               spin = 1,
               color = 1,
               mass = Param.MPi0,
               width = Param.ZERO,
               texname = 'Pi0',
               antitexname = 'Pi0',
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

Eta = Particle(pdg_code = 221,
               name = 'Eta',
               antiname = 'Eta',
               spin = 1,
               color = 1,
               mass = Param.Meta,
               width = Param.ZERO,
               texname = 'Eta',
               antitexname = 'Eta',
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

Etap = Particle(pdg_code = 331,
                name = 'Etap',
                antiname = 'Etap',
                spin = 1,
                color = 1,
                mass = Param.Metap,
                width = Param.ZERO,
                texname = 'Etap',
                antitexname = 'Etap',
                charge = 0,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

Ds__plus__ = Particle(pdg_code = 431,
                      name = 'Ds+',
                      antiname = 'Ds-',
                      spin = 1,
                      color = 1,
                      mass = Param.MDs,
                      width = Param.ZERO,
                      texname = 'Ds+',
                      antitexname = 'Ds-',
                      charge = 1,
                      GhostNumber = 0,
                      LeptonNumber = 0,
                      Y = 0)

Ds__minus__ = Ds__plus__.anti()

B__plus__ = Particle(pdg_code = 521,
                     name = 'B+',
                     antiname = 'B-',
                     spin = 1,
                     color = 1,
                     mass = Param.MBu,
                     width = Param.ZERO,
                     texname = 'B+',
                     antitexname = 'B-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

B__minus__ = B__plus__.anti()

Bc__plus__ = Particle(pdg_code = 541,
                      name = 'Bc+',
                      antiname = 'Bc-',
                      spin = 1,
                      color = 1,
                      mass = Param.MBc,
                      width = Param.ZERO,
                      texname = 'Bc+',
                      antitexname = 'Bc-',
                      charge = 1,
                      GhostNumber = 0,
                      LeptonNumber = 0,
                      Y = 0)

Bc__minus__ = Bc__plus__.anti()

rho0 = Particle(pdg_code = 113,
                name = 'rho0',
                antiname = 'rho0',
                spin = 3,
                color = 1,
                mass = Param.Mrho0,
                width = Param.Wrho0,
                texname = 'rho0',
                antitexname = 'rho0',
                charge = 0,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

rho__plus__ = Particle(pdg_code = 213,
                       name = 'rho+',
                       antiname = 'rho-',
                       spin = 3,
                       color = 1,
                       mass = Param.Mrho,
                       width = Param.Wrho,
                       texname = 'rho+',
                       antitexname = 'rho-',
                       charge = 1,
                       GhostNumber = 0,
                       LeptonNumber = 0,
                       Y = 0)

rho__minus__ = rho__plus__.anti()

omega = Particle(pdg_code = 223,
                 name = 'omega',
                 antiname = 'omega',
                 spin = 3,
                 color = 1,
                 mass = Param.Mom,
                 width = Param.Wom,
                 texname = 'omega',
                 antitexname = 'omega',
                 charge = 0,
                 GhostNumber = 0,
                 LeptonNumber = 0,
                 Y = 0)

Kstar__plus__ = Particle(pdg_code = 323,
                         name = 'Kstar+',
                         antiname = 'Kstar-',
                         spin = 3,
                         color = 1,
                         mass = Param.MKstar,
                         width = Param.WKstar,
                         texname = 'Kstar+',
                         antitexname = 'Kstar-',
                         charge = 1,
                         GhostNumber = 0,
                         LeptonNumber = 0,
                         Y = 0)

Kstar__minus__ = Kstar__plus__.anti()

phimeson = Particle(pdg_code = 333,
                    name = 'phimeson',
                    antiname = 'phimeson',
                    spin = 3,
                    color = 1,
                    mass = Param.Mphi,
                    width = Param.Wphi,
                    texname = 'phimeson',
                    antitexname = 'phimeson',
                    charge = 0,
                    GhostNumber = 0,
                    LeptonNumber = 0,
                    Y = 0)

