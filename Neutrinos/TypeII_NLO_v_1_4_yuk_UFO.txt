Requestor: Jan Gavranovic
Content: Type II seesaw model at NLO with doubly charged Higgs bosons
Validation slides: https://indico.cern.ch/event/1388871/contributions/5839149/attachments/2811782/4908739/typeII_mc_request.pdf
Paper: https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.075022
Webpage: https://feynrules.irmp.ucl.ac.be/wiki/TypeIISeesaw#no1
Note: updated model has been received from the original authors. Yukawa couplings can be modified in this version.
Jira: https://its.cern.ch/jira/browse/AGENE-2260